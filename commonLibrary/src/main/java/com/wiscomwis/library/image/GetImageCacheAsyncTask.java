package com.wiscomwis.library.image;

import android.content.Context;
import android.os.AsyncTask;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.wiscomwis.library.util.SharedPreferenceUtil;

import java.io.File;

/**
 * Created by tianzhentao on 2018/5/24.
 */

public class GetImageCacheAsyncTask extends AsyncTask<String, Void, File> {
    private final Context context;
    private String imgUrl;

    public GetImageCacheAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected File doInBackground(String... params) {
         imgUrl =  params[0];
        try {
            return Glide.with(context)
                    .load(imgUrl)
                    .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                    .get();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(File result) {
        if (result == null) {
            return;
        }
        //此path就是对应文件的缓存路径
        String path = result.getPath();
        SharedPreferenceUtil.setStringValue(context,"cacheImg",imgUrl,path);

    }
}
