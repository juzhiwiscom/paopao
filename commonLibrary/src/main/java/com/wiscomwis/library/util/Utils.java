package com.wiscomwis.library.util;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;

import java.text.DecimalFormat;
import java.util.List;

/**
 * A collection of common util methods
 * Created by zhangdroid on 2017/5/11.
 */
public class Utils {

    private Utils() {
    }

    /**
     * Check a list is empty or not
     *
     * @param list the list to check
     * @return true if the list is empty, othewise not
     */
    public static boolean isListEmpty(List<?> list) {
        return list == null || list.size() == 0;
    }

    /**
     * Get meta data which declare in AndroidManifest
     */
    public static String getMetaData(Context context, String name) {
        try {
            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            return ai.metaData.getString(name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 获取指定位数的小数
     *
     * @param number   double型原始数据
     * @param decimals 小数点后位数, 不能小于1
     */
    public static String getDecimal(double number, int decimals) {
        if (decimals < 1) {
            return number + "";
        }
        StringBuilder builder = new StringBuilder("0.");
        for (int i = 0; i < decimals; i++) {
            builder.append("0");
        }
        DecimalFormat format = new DecimalFormat(builder.toString().trim());// 保留一位小数
        return format.format(number);
    }

    /**
     * 获得AnimationDrawable，图片动画
     *
     * @param list         动画需要播放的图片集合
     * @param isRepeatable 是否可以重复
     * @param duration     帧间隔（毫秒）
     * @return
     */
    public static AnimationDrawable getFrameAnim(List<Drawable> list, boolean isRepeatable, int duration) {
        AnimationDrawable animationDrawable = new AnimationDrawable();
        animationDrawable.setOneShot(!isRepeatable);
        if (!isListEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                animationDrawable.addFrame(list.get(i), duration);
            }
        }
        return animationDrawable;
    }
    public static String getClipboardText(Context context) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        if (!clipboard.hasPrimaryClip()) {
            return "";
        }
        ClipData clipData = clipboard.getPrimaryClip();
        //获取 ClipDescription
//        ClipDescription clipDescription = clipboard.getPrimaryClipDescription();
        //获取 lable
//        String lable = clipDescription.getLabel().toString();
        //获取 text
        String text = clipData.getItemAt(0).coerceToText(context).toString();
        return text;
    }
}