package com.wiscomwis.library.util;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

/**
 * A utility of file I/O
 * Created by zhangdroid on 2017/5/11.
 */
public class FileUtil {

    private FileUtil() {
    }

    /**
     * 判断是否有SD卡
     *
     * @return
     */
    public static boolean isSDCardExist() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * 获得SD卡目录路径，应用内所有数据均存放在该目录下
     *
     * @param fileName 要创建的文件夹名称
     * @return 根目录下应用文件夹
     */
    public static String getExternalStorageDirectory(String fileName) {
        if (isSDCardExist()) {
            return Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + fileName;
        }
        return null;
    }

    /**
     * 获得应用文件存放路径（不需要SD卡读写权限）
     *
     * @param context 上下文对象
     * @param type    {@link Context#getExternalFilesDir(String)}
     * @return
     * @see Context#getExternalFilesDir(String)
     */
    public static String getExternalFilesDir(Context context, String type) {
        if(context.getExternalFilesDir(type)!=null){
            return context.getExternalFilesDir(type).getAbsolutePath();
        }
        return null;
    }

    /**
     * 删除文件
     *
     * @param path 需要删除的文件路径
     */
    public static void deleteFile(String path) {
        if (!TextUtils.isEmpty(path)) {
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
        }
    }

    /**
     * 读取assets目录下的文件
     *
     * @param context  上下文对象
     * @param fileName assets目录下的json文件路径
     * @return 返回String
     */
    public static String getStringFromAssets(Context context, String fileName) {
        String jsonStr = null;
        if (!TextUtils.isEmpty(fileName)) {
            BufferedReader bufferedReader = null;
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(fileName)));
                String line;
                StringBuilder buffer = new StringBuilder();
                while (!TextUtils.isEmpty((line = bufferedReader.readLine()))) {
                    buffer.append(line);
                }
                jsonStr = buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return jsonStr;
    }

    /**
     * 根据当前时间点生成文件名（不重复）
     *
     * @return 生成文件名格式为：20160721-180810
     */
    public static String createFileNameByTime() {
        StringBuilder stringBuilder = new StringBuilder();
        Calendar calendar = Calendar.getInstance();
        stringBuilder.append(calendar.get(Calendar.YEAR))
                .append(DateTimeUtil.pad(calendar.get(Calendar.MONTH) + 1))
                .append(DateTimeUtil.pad(calendar.get(Calendar.DAY_OF_MONTH)))
                .append("-")
                .append(DateTimeUtil.pad(calendar.get(Calendar.HOUR_OF_DAY)))
                .append(DateTimeUtil.pad(calendar.get(Calendar.MINUTE)))
                .append(DateTimeUtil.pad(calendar.get(Calendar.SECOND)));
        return stringBuilder.toString();
    }

    /**
     * 根据日期生成文件名
     *
     * @return 生成文件名格式为：20160721-180810
     */
    public static String createFileNameByDate(String date) {
        if (TextUtils.isEmpty(date)) {
            return null;
        }
        Date d = null;
        try {
            d = DateTimeUtil.timeFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(d.getYear() + 1900)
                .append(DateTimeUtil.pad(d.getMonth() + 1))
                .append(DateTimeUtil.pad(d.getDate()))
                .append("-")
                .append(DateTimeUtil.pad(d.getHours()))
                .append(DateTimeUtil.pad(d.getMinutes()))
                .append(DateTimeUtil.pad(d.getSeconds()));
        return stringBuilder.toString();
    }

    /**
     * 计算系统缓存大小
     *
     * @param context
     */
    public static String getTotalCacheSize(Context context) {
        long cacheSize = getFolderSize(context.getCacheDir());// 内部缓存
        if (isSDCardExist()) {// 若有外部SD卡，则计算SD缓存
            cacheSize += getFolderSize(context.getExternalCacheDir());
        }
        return getFormatSize(cacheSize);
    }

    /**
     * 清空缓存
     *
     * @param context
     */
    public static void clearAllCache(Context context) {
        deleteDir(context.getCacheDir());
        if (isSDCardExist()) {
            deleteDir(context.getExternalCacheDir());
        }
    }

    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                return deleteDir(new File(dir, children[i]));
            }
        }
        return dir.delete();
    }

    /**
     * 计算文件夹内存大小
     *
     * @param file
     */
    public static long getFolderSize(File file) {
        long size = 0;
        try {
            File[] fileList = file.listFiles();
            for (int i = 0; i < fileList.length; i++) {
                if (fileList[i].isDirectory()) { // 如果下面还有文件
                    size = size + getFolderSize(fileList[i]);
                } else {
                    size = size + fileList[i].length();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return size;
    }

    /**
     * 格式化单位
     */
    public static String getFormatSize(double size) {
        double kiloByte = size / 1024;
        if (kiloByte < 1) {
            return String.valueOf(size) + "B";
        }
        double megaByte = kiloByte / 1024;
        if (megaByte < 1) {
            BigDecimal result1 = new BigDecimal(Double.toString(kiloByte));
            return result1.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "KB";
        }
        // MB
        double gigaByte = megaByte / 1024;
        if (gigaByte < 1) {
            BigDecimal result2 = new BigDecimal(Double.toString(megaByte));
            return result2.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "MB";
        }
        // GB
        double teraBytes = gigaByte / 1024;
        if (teraBytes < 1) {
            BigDecimal result3 = new BigDecimal(Double.toString(gigaByte));
            return result3.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "GB";
        }
        // TB
        BigDecimal result4 = new BigDecimal(teraBytes);
        return result4.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "TB";
    }

}