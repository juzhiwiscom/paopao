package com.wiscomwis.library.widget.tab;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.wiscomwis.library.widget.VerticalViewPager;

/**
 * Created by tianzhentao on 2018/5/30.
 */

public class NewVerticalViewPager extends VerticalViewPager {
    private int preX=0;

    public NewVerticalViewPager(Context context) {
        super(context);
    }

    public NewVerticalViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent even) {

        if(even.getAction()==MotionEvent.ACTION_DOWN)
        {
            preX=(int) even.getX();
        }else
        {
            if(Math.abs((int)even.getX()-preX)>20)
            {
                return true;
            }else
            {
                preX=(int) even.getX();
            }
        }
        return super.onInterceptTouchEvent(even);
    }
}
