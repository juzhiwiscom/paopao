package com.wiscomwis.playerlibrary;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.wiscomwis.playerlibrary.gsyvideoplayer.video.StandardGSYVideoPlayer;


/**
 * 无任何控制ui的播放
 * Created by guoshuyu on 2017/8/6.
 */

public class EmptyControlVideo extends StandardGSYVideoPlayer {

    private ImageView ivBg;

    public EmptyControlVideo(Context context, Boolean fullFlag) {
        super(context, fullFlag);
    }

    public EmptyControlVideo(Context context) {
        super(context);
    }

    public EmptyControlVideo(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public int getLayoutId() {
        return R.layout.empty_control_video;
    }

    @Override
    protected void touchSurfaceMoveFullLogic(float absDeltaX, float absDeltaY) {
        super.touchSurfaceMoveFullLogic(absDeltaX, absDeltaY);
        //不给触摸快进，如果需要，屏蔽下方代码即可
        mChangePosition = false;

        //不给触摸音量，如果需要，屏蔽下方代码即可
        mChangeVolume = false;

        //不给触摸亮度，如果需要，屏蔽下方代码即可
        mBrightness = false;
    }

    @Override
    protected void touchDoubleUp() {
        //super.touchDoubleUp();
        //不需要双击暂停
    }
    public void setPhoto(String url){
         ivBg = (ImageView) findViewById(R.id.iv_bg);
        Bitmap bmp= BitmapFactory.decodeFile(url);
        ivBg.setImageBitmap(bmp);
    }


    @Override
    public void onPrepared() {
        super.onPrepared();

        findViewById(R.id.progress_empty).setVisibility(View.GONE);
        findViewById(R.id.iv_bg_show).setVisibility(View.GONE);
    }


}
