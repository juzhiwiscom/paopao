package com.wiscomwis.facetoface.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 跳转支付页面需要传递的参数
 * Created by zhangdroid on 2017/6/10.
 */
public class PayParcelable implements Parcelable {
    // 支付拦截来源
    public String fromTag;
    public int type;//0为包月，1为我的钥匙
    public int paySource;//1礼物对方空间页礼物拦截、2礼物广场页面礼物拦截、3礼物聊天页面礼物拦截、4礼物视频聊天礼物拦截
    public PayParcelable(String fromTag,int type,int paySource) {
        this.fromTag = fromTag;
        this.type=type;
        this.paySource=paySource;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fromTag);
        dest.writeInt(this.type);
        dest.writeInt(this.paySource);
    }

    protected PayParcelable(Parcel in) {
        this.fromTag = in.readString();
        this.type = in.readInt();
        this.paySource = in.readInt();
    }

    public static final Parcelable.Creator<PayParcelable> CREATOR = new Parcelable.Creator<PayParcelable>() {
        @Override
        public PayParcelable createFromParcel(Parcel source) {
            return new PayParcelable(source);
        }

        @Override
        public PayParcelable[] newArray(int size) {
            return new PayParcelable[size];
        }
    };
}
