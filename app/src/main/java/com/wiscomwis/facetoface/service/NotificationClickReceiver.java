package com.wiscomwis.facetoface.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.CloseRingtoneEvent;
import com.wiscomwis.facetoface.parcelable.VideoInviteParcelable;
import com.wiscomwis.facetoface.ui.main.MainActivity;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by tianzhentao on 2018/7/24.
 */

public class NotificationClickReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("Main===", "userClick:我被点击啦！！！ ");
        UserPreference.setClickNotification(true);
        EventBus.getDefault().post(new CloseRingtoneEvent(true));
        VideoInviteParcelable parcelableExtra1 = intent.getParcelableExtra("intent_key_main_parcelable");
        boolean inviteActivity = intent.getBooleanExtra("inviteActivity", true);
        Intent newIntent = new Intent(context, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        newIntent.putExtra("intent_key_main_parcelable",parcelableExtra1);
        newIntent.putExtra("inviteActivity",inviteActivity);
        context.startActivity(newIntent);
    }
}
