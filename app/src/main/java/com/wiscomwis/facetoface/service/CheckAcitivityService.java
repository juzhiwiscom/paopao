package com.wiscomwis.facetoface.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by tianzhentao on 2018/6/11.
 */

public class CheckAcitivityService extends Service {


    @Override
    public void onCreate() {
        Log.i("MainActivity","serviceonCreate");
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Log.i("MainActivity","serviceonDestroy");
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
