package com.wiscomwis.facetoface.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.wiscomwis.facetoface.event.UnReadMsgEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by tianzhentao on 2018/5/29.
 */

public class MsgNotifyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        EventBus.getDefault().post(new UnReadMsgEvent());
    }
}
