package com.wiscomwis.facetoface.service;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.hyphenate.chat.EMMipushReceiver;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.xiaomi.mipush.sdk.ErrorCode;
import com.xiaomi.mipush.sdk.MiPushClient;
import com.xiaomi.mipush.sdk.MiPushCommandMessage;
import com.xiaomi.mipush.sdk.MiPushMessage;

import java.util.List;

/**
 * Created by tianzhentao on 2018/6/21.
 * * 3、DemoMessageReceiver 的 onReceivePassThroughMessage 方法用来接收服务器向客户端发送的透传消息。<br/>
 * 4、DemoMessageReceiver 的 onNotificationMessageClicked 方法用来接收服务器向客户端发送的通知消息，
 * 这个回调方法会在用户手动点击通知后触发。<br/>
 * 5、DemoMessageReceiver 的 onNotificationMessageArrived 方法用来接收服务器向客户端发送的通知消息，
 * 这个回调方法是在通知消息到达客户端时触发。另外应用在前台时不弹出通知的通知消息到达客户端也会触发这个回调函数。<br/>
 * 6、DemoMessageReceiver 的 onCommandResult 方法用来接收客户端向服务器发送命令后的响应结果。<br/>
 * 7、DemoMessageReceiver 的 onReceiveRegisterResult 方法用来接收客户端向服务器发送注册命令后的响应结果。<br/>
 * 8、以上这些方法运行在非 UI 线程中。
 */

public class MiPushReceive extends EMMipushReceiver {
    // 当前账户的 regId
    private String regId = null;

    /**
     * 接收客户端向服务器发送注册命令消息后返回的响应
     *
     * @param context 上下文对象
     * @param miPushCommandMessage 注册结果
     */
    @Override
    public void onReceiveRegisterResult(Context context, MiPushCommandMessage miPushCommandMessage) {
        super.onReceiveRegisterResult(context, miPushCommandMessage);
        String command = miPushCommandMessage.getCommand();
        List<String> arguments = miPushCommandMessage.getCommandArguments();
        String cmdArg1 = null;
        String cmdArg2 = null;
        if (arguments != null && arguments.size() > 0) {
            cmdArg1 = arguments.get(0);
        }
        if (arguments != null && arguments.size() > 1) {
            cmdArg2 = arguments.get(1);
        }
        Log.e("AAAAA","xiaomiPushToken="+cmdArg1);
        String phonename = Build.MANUFACTURER;
        if (phonename != null && phonename != null && phonename != ""&&phonename.equals("Xiaomi")) {
            if (MiPushClient.COMMAND_REGISTER.equals(command)) {
                if (miPushCommandMessage.getResultCode() == ErrorCode.SUCCESS) {
                    // 这里可以获取到当前账户的 regId，可以发送给自己的服务器，用来做一些业务处理
                    regId = cmdArg1;
//                if(!TextUtils.isEmpty(regid)||regid.equals("null")){
                    ApiManager.getMiPushToken(cmdArg1,new IGetDataListener<BaseModel>() {
                        @Override
                        public void onResult(BaseModel baseModel, boolean isEmpty) {
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                        }
                    });
                }
            }
            UserPreference.setRegId(cmdArg1);

        }


//        VMLog.d("onReceiveRegisterResult regId: %s", regId);
    }

    /**
     * 接收服务器推送的透传消息
     *
     * @param context 上下文对象
     * @param miPushMessage 推送消息对象
     */
    @Override
    public void onReceivePassThroughMessage(Context context, MiPushMessage miPushMessage) {
        super.onReceivePassThroughMessage(context, miPushMessage);
    }

    /**
     * 接收服务器推送的通知栏消息（消息到达客户端时触发，并且可以接收应用在前台时不弹出通知的通知消息）
     *
     * @param context 上下文
     * @param miPushMessage 推送消息对象
     */
    @Override public void onNotificationMessageArrived(Context context, MiPushMessage miPushMessage) {
        miPushMessage.setTitle("这里是客户端设置 title");
        super.onNotificationMessageArrived(context, miPushMessage);
        //        清除小米推送的消息
        int notifyId = miPushMessage.getNotifyId();
        MiPushClient.clearNotification(context,notifyId);
    }

    /**
     * 接收服务器发来的通知栏消息（用户点击通知栏时触发）
     *
     * @param context 上下文对象
     * @param miPushMessage 消息对象
     */
    @Override public void onNotificationMessageClicked(Context context, MiPushMessage miPushMessage) {
        super.onNotificationMessageClicked(context, miPushMessage);
    }

    /**
     * 接收客户端向服务器发送命令消息后返回的响应
     *
     * @param context 上下文对象
     * @param miPushCommandMessage 服务器响应的命令消息对象
     */
    @Override public void onCommandResult(Context context, MiPushCommandMessage miPushCommandMessage) {
        super.onCommandResult(context, miPushCommandMessage);
    }





//
//    private String mRegId;
//    private String mTopic;
//    private String mAlias;
//    private String mAccount;
//    private String mStartTime;
//    private String mEndTime;
//
//    @Override
//    public void onReceivePassThroughMessage(Context context, MiPushMessage message) {
//        Log.v(BaseApplication.TAG,
//                "onReceivePassThroughMessage is called. " + message.toString());
//        String log = context.getString(R.string.recv_passthrough_message, message.getContent());
////        MainActivity.logList.add(0, getSimpleDate() + " " + log);
//
//        if (!TextUtils.isEmpty(message.getTopic())) {
//            mTopic = message.getTopic();
//        } else if (!TextUtils.isEmpty(message.getAlias())) {
//            mAlias = message.getAlias();
//        }
//
//        Message msg = Message.obtain();
//        msg.obj = log;
//        String messageId = message.getMessageId();
//        showInfo("获得一条透传消息",messageId);
////        DemoApplication.getHandler().sendMessage(msg);
//    }
//
//    private void showInfo(String msg, String messageId) {
//        Intent intent = new Intent(BaseApplication.getGlobalContext(), MainActivity.class);
//        intent.setFlags(
//                Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        intent.putExtra(LaunchHelper.INTENT_KEY_COMMON, true);
//        NotificationHelper.getInstance(BaseApplication.getGlobalContext())
//                .setTicker(msg)
//                .setIcon(R.mipmap.vivologo)
//                .setTitle("泡泡")
//                .setMessage(msg)
//                .setTime(0)
//                .setDefaults()
//                .setPendingIntentActivity(1, intent, PendingIntent.FLAG_UPDATE_CURRENT,true)
//                .notify(Integer.parseInt(messageId));
//    }
//
//    @Override
//    public void onNotificationMessageClicked(Context context, MiPushMessage message) {
////        Log.v(BaseApplication.TAG,
////                "onNotificationMessageClicked is called. " + message.toString());
////        String log = context.getString(R.string.click_notification_message, message.getContent());
//////        MainActivity.logList.add(0, getSimpleDate() + " " + log);
////
////        if (!TextUtils.isEmpty(message.getTopic())) {
////            mTopic = message.getTopic();
////        } else if (!TextUtils.isEmpty(message.getAlias())) {
////            mAlias = message.getAlias();
////        }
////
////        Message msg = Message.obtain();
////        if (message.isNotified()) {
////            msg.obj = log;
////        }
////        String messageId = message.getMessageId();
////        showInfo("获得一条通知消息",messageId);
////        DemoApplication.getHandler().sendMessage(msg);
//        message.setTitle("接收服务端发来的消息 title");
//        super.onNotificationMessageClicked(context, message);
//    }
//
//    @Override
//    public void onNotificationMessageArrived(Context context, MiPushMessage message) {
////        Log.v(BaseApplication.TAG,
////                "onNotificationMessageArrived is called. " + message.toString());
////        String log = context.getString(R.string.arrive_notification_message, message.getContent());
//////        MainActivity.logList.add(0, getSimpleDate() + " " + log);
////
////        if (!TextUtils.isEmpty(message.getTopic())) {
////            mTopic = message.getTopic();
////        } else if (!TextUtils.isEmpty(message.getAlias())) {
////            mAlias = message.getAlias();
////        }
////
////        Message msg = Message.obtain();
////        msg.obj = log;
////        String messageId = message.getMessageId();
////        showInfo("获得一条通知消息",messageId);
////        DemoApplication.getHandler().sendMessage(msg);
//
//        message.setTitle("这里是客户端设置 title");
//        super.onNotificationMessageArrived(context, message);
//    }
//
//    @Override
//    public void onCommandResult(Context context, MiPushCommandMessage message) {
////        Log.v(BaseApplication.TAG,
////                "onCommandResult is called. " + message.toString());
////        String command = message.getCommand();
////        List<String> arguments = message.getCommandArguments();
////        String cmdArg1 = ((arguments != null && arguments.size() > 0) ? arguments.get(0) : null);
////        String cmdArg2 = ((arguments != null && arguments.size() > 1) ? arguments.get(1) : null);
////        String log;
////        if (MiPushClient.COMMAND_REGISTER.equals(command)) {
////            if (message.getResultCode() == ErrorCode.SUCCESS) {
////                mRegId = cmdArg1;
////                log = context.getString(R.string.register_success);
////            } else {
////                log = context.getString(R.string.register_error);
////            }
////        } else if (MiPushClient.COMMAND_SET_ALIAS.equals(command)) {
////            if (message.getResultCode() == ErrorCode.SUCCESS) {
////                mAlias = cmdArg1;
////                log = context.getString(R.string.set_alias_success, mAlias);
////            } else {
////                log = context.getString(R.string.set_alias_fail, message.getReason());
////            }
////        } else if (MiPushClient.COMMAND_UNSET_ALIAS.equals(command)) {
////            if (message.getResultCode() == ErrorCode.SUCCESS) {
////                mAlias = cmdArg1;
////                log = context.getString(R.string.unset_alias_success, mAlias);
////            } else {
////                log = context.getString(R.string.unset_alias_fail, message.getReason());
////            }
////        } else if (MiPushClient.COMMAND_SET_ACCOUNT.equals(command)) {
////            if (message.getResultCode() == ErrorCode.SUCCESS) {
////                mAccount = cmdArg1;
////                log = context.getString(R.string.set_account_success, mAccount);
////            } else {
////                log = context.getString(R.string.set_account_fail, message.getReason());
////            }
////        } else if (MiPushClient.COMMAND_UNSET_ACCOUNT.equals(command)) {
////            if (message.getResultCode() == ErrorCode.SUCCESS) {
////                mAccount = cmdArg1;
////                log = context.getString(R.string.unset_account_success, mAccount);
////            } else {
////                log = context.getString(R.string.unset_account_fail, message.getReason());
////            }
////        } else if (MiPushClient.COMMAND_SUBSCRIBE_TOPIC.equals(command)) {
////            if (message.getResultCode() == ErrorCode.SUCCESS) {
////                mTopic = cmdArg1;
////                log = context.getString(R.string.subscribe_topic_success, mTopic);
////            } else {
////                log = context.getString(R.string.subscribe_topic_fail, message.getReason());
////            }
////        } else if (MiPushClient.COMMAND_UNSUBSCRIBE_TOPIC.equals(command)) {
////            if (message.getResultCode() == ErrorCode.SUCCESS) {
////                mTopic = cmdArg1;
////                log = context.getString(R.string.unsubscribe_topic_success, mTopic);
////            } else {
////                log = context.getString(R.string.unsubscribe_topic_fail, message.getReason());
////            }
////        } else if (MiPushClient.COMMAND_SET_ACCEPT_TIME.equals(command)) {
////            if (message.getResultCode() == ErrorCode.SUCCESS) {
////                mStartTime = cmdArg1;
////                mEndTime = cmdArg2;
////                log = context.getString(R.string.set_accept_time_success, mStartTime, mEndTime);
////            } else {
////                log = context.getString(R.string.set_accept_time_fail, message.getReason());
////            }
////        } else {
////            log = message.getReason();
////        }
//////        MainActivity.logList.add(0, getSimpleDate() + "    " + log);
////
////        Message msg = Message.obtain();
////        msg.obj = log;
//////        DemoApplication.getHandler().sendMessage(msg);
//        super.onCommandResult(context, message);
//
//    }
//
//    @Override
//    public void onReceiveRegisterResult(Context context, MiPushCommandMessage message) {
//        Log.v(BaseApplication.TAG,
//                "onReceiveRegisterResult is called. " + message.toString());
//        String command = message.getCommand();
//        List<String> arguments = message.getCommandArguments();
//        String cmdArg1 = ((arguments != null && arguments.size() > 0) ? arguments.get(0) : null);
//        String log;
//        if (MiPushClient.COMMAND_REGISTER.equals(command)) {
//            if (message.getResultCode() == ErrorCode.SUCCESS) {
//                mRegId = cmdArg1;
//                log = context.getString(R.string.register_success);
//            } else {
//                log = context.getString(R.string.register_error);
//            }
//        } else {
//            log = message.getReason();
//        }
//
//        Message msg = Message.obtain();
//        msg.obj = log;
////        DemoApplication.getHandler().sendMessage(msg);
//    }
//
//    @SuppressLint("SimpleDateFormat")
//    private static String getSimpleDate() {
//        return new SimpleDateFormat("MM-dd hh:mm:ss").format(new Date());
//    }
}
