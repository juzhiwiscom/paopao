package com.wiscomwis.facetoface.hms.agent.push.handler;

import  com.wiscomwis.facetoface.hms.agent.common.handler.ICallbackCode;

/**
 * enableReceiveNormalMsg 回调
 */
public interface EnableReceiveNormalMsgHandler extends ICallbackCode {
}
