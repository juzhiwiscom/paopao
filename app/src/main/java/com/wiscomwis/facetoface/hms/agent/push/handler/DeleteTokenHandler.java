package com.wiscomwis.facetoface.hms.agent.push.handler;

import  com.wiscomwis.facetoface.hms.agent.common.handler.ICallbackCode;

/**
 * deleteToken 回调
 */
public interface DeleteTokenHandler extends ICallbackCode {
}
