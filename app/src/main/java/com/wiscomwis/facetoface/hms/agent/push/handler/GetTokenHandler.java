package com.wiscomwis.facetoface.hms.agent.push.handler;

import  com.wiscomwis.facetoface.hms.agent.common.handler.ICallbackCode;

/**
 * 获取 pushtoken 回调
 */
public interface GetTokenHandler extends ICallbackCode {
}
