package com.wiscomwis.facetoface.hms.agent.push.handler;

import  com.wiscomwis.facetoface.hms.agent.common.handler.ICallbackCode;

/**
 * queryAgreement 回调
 */
public interface QueryAgreementHandler extends ICallbackCode {
}
