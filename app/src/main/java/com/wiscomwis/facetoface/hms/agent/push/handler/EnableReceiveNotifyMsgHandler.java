package com.wiscomwis.facetoface.hms.agent.push.handler;

import  com.wiscomwis.facetoface.hms.agent.common.handler.ICallbackCode;

/**
 * enableReceiveNotifyMsg 回调
 */
public interface EnableReceiveNotifyMsgHandler extends ICallbackCode {
}
