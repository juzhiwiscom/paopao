package com.wiscomwis.facetoface.hms.agent.push.handler;

import  com.wiscomwis.facetoface.hms.agent.common.handler.ICallbackCode;

/**
 * getPushState 回调
 */
public interface GetPushStateHandler extends ICallbackCode {
}
