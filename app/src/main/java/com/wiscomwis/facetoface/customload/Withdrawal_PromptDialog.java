package com.wiscomwis.facetoface.customload;

import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.library.dialog.BaseDialogFragment;
import com.wiscomwis.library.dialog.OnDialogClickListener;

/**
 * A common alert dialog
 * Created by zhangdroid on 2017/5/26.
 */
public class Withdrawal_PromptDialog extends BaseDialogFragment {
    private static Withdrawal_PromptDialog mAlertDialog;
    private OnDialogClickListener mOnDialogClickListener;

    private static Withdrawal_PromptDialog newInstance(String title, String message, String positive, String negative,
                                                       boolean isCancelable, OnDialogClickListener listener) {
        Withdrawal_PromptDialog alertDialog = new Withdrawal_PromptDialog();
        alertDialog.setArguments(getDialogBundle(title, message, positive, negative, isCancelable));
        alertDialog.mOnDialogClickListener = listener;
        return alertDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_withdrawal_prompt;
    }

    @Override
    protected void setDialogContentView(View view) {
        TextView tv_name = (TextView) view.findViewById(R.id.withdrawal_prompt_dialog_name);
        TextView tv_account = (TextView) view.findViewById(R.id.withdrawal_prompt_dialog_account);
        TextView tv_money = (TextView) view.findViewById(R.id.withdrawal_prompt_dialog_money);
        Button btnNegative = (Button) view.findViewById(R.id.withdrawal_prompt_dialog_negative);
        Button btnPositive = (Button) view.findViewById(R.id.withdrawal_prompt_dialog_positive);
        String message = getDialogMessage();
        String negative = getDialogNegative();
        String positive = getDialogPositive();
        if (!TextUtils.isEmpty(message)) {
            tv_name.setText(message);
        }
        if (!TextUtils.isEmpty(negative)) {
            tv_account.setText(negative);
        }
        if (!TextUtils.isEmpty(positive)) {
            tv_money.setText(positive);
        }
        if (null != mOnDialogClickListener) {
            btnNegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    mOnDialogClickListener.onNegativeClick(v);
                }
            });
            btnPositive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    mOnDialogClickListener.onPositiveClick(v);
                }
            });
        }
    }

    public static void show(FragmentManager fragmentManager, String title, String message, String positive, String negative,
                            OnDialogClickListener listener) {
        show(fragmentManager, title, message, positive, negative, true, listener);
    }

    public static void showNoCanceled(FragmentManager fragmentManager, String title, String message, String positive, String negative,
                                     OnDialogClickListener listener) {
        show(fragmentManager, title, message, positive, negative, false, listener);
    }

    private static void show(FragmentManager fragmentManager, String title, String message, String positive, String negative,
                            boolean isCancelable, OnDialogClickListener listener) {
        mAlertDialog = newInstance(title, message, positive, negative, isCancelable, listener);
        mAlertDialog.show(fragmentManager, "alert");
    }

}
