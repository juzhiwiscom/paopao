package com.wiscomwis.facetoface.common;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.meizu.cloud.pushsdk.util.MzSystemUtils;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseApplication;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.Country;
import com.wiscomwis.facetoface.data.model.LocationInfo;
import com.wiscomwis.facetoface.data.model.State;
import com.wiscomwis.facetoface.data.model.UploadInfoParams;
import com.wiscomwis.facetoface.data.model.UserPhoto;
import com.wiscomwis.facetoface.data.preference.PlatformPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.library.util.SharedPreferenceUtil;
import com.wiscomwis.library.util.Utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.InvalidAlgorithmParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * 公用工具类
 * Created by zhangdroid on 2017/6/3.
 */
public class Util {
    private static long time = System.currentTimeMillis();
    private static long TIME_OUT=3*1000;
    private static MediaPlayer mMediaPlayer;

    private Util() {
    }

    /**
     * 将List<UserPhoto>转成List<String>
     *
     * @param list
     * @return
     */
    public static List<String> convertPhotoUrl(List<UserPhoto> list) {
        List<String> stringList = new ArrayList<>();
        if (!Utils.isListEmpty(list)) {
            for (UserPhoto item : list) {
                if (null != item) {
                    stringList.add(item.getFileUrl());
                }
            }
        }
        return stringList;
    }

    /**
     * 获得UploadInfoParamsJSON字符串
     */
    public static String getUploadInfoParamsJsonString(UploadInfoParams uploadInfoParams) {
        // 创建以uploadInfoParams为Key的JSON字符串
        Map<String, UploadInfoParams> map = new HashMap<>();
        map.put("uploadInfoParams", uploadInfoParams);
        return new Gson().toJson(map);
    }

    /**
     * @return 随机返回默认图片
     */
    public static int getDefaultImage() {
        int[] defaultImgs = {R.drawable.my_photo_icon, R.drawable.my_photo_icon, R.drawable.my_photo_icon,
                R.drawable.my_photo_icon, R.drawable.my_photo_icon};
        return defaultImgs[new Random().nextInt(defaultImgs.length)];
    }

    /**
     * @return 随机返回默认圆形图片
     */
    public static int getDefaultImageCircle() {
        int[] defaultImgs = {R.drawable.my_circle_icon, R.drawable.my_circle_icon, R.drawable.my_circle_icon,
                R.drawable.my_circle_icon, R.drawable.my_circle_icon};
        return defaultImgs[new Random().nextInt(defaultImgs.length)];
    }

    /**
     * 计时：将秒数转换成时间字符串
     *
     * @param seconds
     * @return
     */
    public static String convertSecondsToString(int seconds) {
        StringBuilder stringBuilder = new StringBuilder();
        if (seconds < 60) {// 一分钟内
            stringBuilder.append("00:")
                    .append(pad(seconds));
        } else if (seconds >= 60 && seconds < 60 * 60) {// 1小时内
            stringBuilder.append(pad(seconds / 60))
                    .append(":")
                    .append(pad(seconds % 60));
        } else if (seconds >= 60 * 60 && seconds < 24 * 60 * 60) {// 1天内
            stringBuilder.append(pad(seconds / 60 * 60))
                    .append(":")
                    .append(pad((seconds % 60 * 60) / 60))
                    .append(":")
                    .append(pad((seconds % 60 * 60) % 60));
        }
        return stringBuilder.toString();
    }
    /**
     * 计时：将毫秒数转换成分钟，小时
     *
     * @param seconds
     * @return
     */
    public static String convertTimeToString(long seconds) {
        String getTime;
        if(seconds<1000*60*60){
           return getTime=seconds/(1000*60)+"分钟";//分钟
        }else {
            return getTime=seconds/(1000*60*60)+"小时";//小时
        }
    }

    /**
     * 小于10的数前面补0
     *
     * @param number
     * @return
     */
    public static String pad(int number) {
        if (number < 10) {
            return "0" + number;
        } else {
            return String.valueOf(number);
        }
    }

    /**
     * 读取assets目录下的json文件
     *
     * @param context  上下文对象
     * @param fileName assets目录下的json文件路径
     */
    public static String getJsonStringFromAssets(Context context, String fileName) {
        String jsonStr = null;
        if (!TextUtils.isEmpty(fileName)) {
            BufferedReader bufferedReader = null;
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(fileName)));
                String line;
                StringBuilder buffer = new StringBuilder();
                while (!TextUtils.isEmpty((line = bufferedReader.readLine()))) {
                    buffer.append(line);
                }
                jsonStr = buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return jsonStr;
    }

    /**
     * 从assets目录下读取国家和语音 json数据结构是一样的，因此就写成Country一个类
     */
    public static List<Country> getCountryFromJson(String fileName) {
        // 解析国家json文件
        List<Country> countryStateList = stringToList(getJsonStringFromAssets(getContext(), fileName), Country.class);
        if (countryStateList != null) {
            return countryStateList;
        }
        return null;
    }

    /**
     * 从assets目录下读取州信息
     */
    public static List<State> getStateFromJson(String fileName) {
        // 解析国家json文件
        List<State> countryStateList = stringToList(getJsonStringFromAssets(getContext(), fileName), State.class);
        if (countryStateList != null) {
            return countryStateList;
        }
        return null;
    }
    /**
     * 获得定位信息
     */
    public static String getUploadLocation(LocationInfo uploadInfoParams) {
        // 创建以uploadInfoParams为Key的JSON字符串
        Map<String, LocationInfo> map = new HashMap<>();
        map.put("locationInfo", uploadInfoParams);
        return new Gson().toJson(map);
    }


    /**
     * 获取星座数据
     *
     * @return
     */
    public static List<Map<String, String>> getSignString() {
        String userCountry = PlatformPreference.getPlatformInfo().getCountry();
        List<Map<String, String>> list = new ArrayList<>();
        if (!TextUtils.isEmpty(userCountry) && "China".equals(userCountry)) {
            List<Country> countryFromJson = Util.getCountryFromJson("SignChinese.json");
            for (Country state : countryFromJson) {
                Map<String, String> map = new HashMap<>();
                map.put(state.getGuid(), state.getName());
                list.add(map);
            }
        }
        return list;
    }

    public static Context getContext() {
        return BaseApplication.getGlobalContext();
    }

    public static boolean isListEmpty(List<?> list) {
        return list == null || list.size() == 0;
    }

    /**
     * 通过gson把json字符串转成list集合
     *
     * @param json
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> List<T> stringToList(String json, Class<T> cls) {
        Gson gson = new Gson();
        List<T> list = new ArrayList<T>();
        JsonArray array = new JsonParser().parse(json).getAsJsonArray();
        for (final JsonElement elem : array) {
            list.add(gson.fromJson(elem, cls));
        }
        return list;
    }

    public static String getLacalCountry() {
        String country = null;
        String CT = "China";
//        String CT = Locale.getDefault().getCountry();
//        switch (CT){
//            case "TW":
//                country = "Taiwan";
//                break;
//            case "HK":
//                country = "Taiwan";
//                break;
//            case "CN":
//                country = "China";
//                break;
//            case "US":
//                country = "United State";
//                break;
//            case "IN":
//                country = "India";
//                break;
//            case "UK":
//                country = "UnitedKingdom";
//                break;
//
//        }
        return CT;
    }

    public static String getLacalLanguage() {
        String language = null;
        String LG = "Simplified";
//        String LG = Locale.getDefault().getLanguage();
//        switch (LG){
//            case "zh":
//                if(getLacalCountry().equals("China")){
//                    language="Simplified";
//                }else if(getLacalCountry().equals("Taiwan")){
//                    language="Traditional";
//                }
//                break;
//            case "en":
//                language = "English";
//                break;
//
//        }
        return LG;
    }

    /**
     * 获得国家列表
     *
     * @return
     */
    public static List<String> getCountryList() {
        List<String> list = new ArrayList<String>();
        list.add("United States");
        list.add("Australia");
        list.add("India");
        list.add("Indonesia");
        list.add("United Kingdom");
        list.add("Canada");
        list.add("New Zealand");
        list.add("Ireland");
        list.add("South Africa");
        list.add("Singapore");
        list.add("Pakistan");
        list.add("Philippines");
        list.add("Hong Kong");
        return list;
    }

    public static String getMD5(String val, String salt) {
        MessageDigest md5 = null;
        byte[] m = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
            md5.update((val + salt).getBytes());
            m = md5.digest();//加密
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return getString(m);
    }

    private static String getString(byte[] b) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < b.length; i++) {
            sb.append(Long.toString((int) b[i] & 0xff, 16));
        }
        return sb.toString();
    }

    /**
     * 隐藏键盘
     */
    public static void hideKeyboard(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * 对字符串进行md5/AES加密
     */
    public static String getAes(String str) {
        StringBuffer sb = new StringBuffer();
        String md5Value = AESUtils.getMd5Value(str);
        String times = DataUtil.getTime();
        sb.append(md5Value);
        sb.append("-");
        sb.append(times);
        String encode = null;
        try {
            encode = AESUtils.encode(sb.toString());
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return encode;
    }

    /**
     * 在使用 okhttp 的时候，head 的一些项是中文，导致网络请求失败.
     * 挑出不合要求的字符，把这些字符单独转码
     *
     * @param headInfo
     * @return
     */
    public static String encodeHeadInfo(String headInfo) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0, length = headInfo.length(); i < length; i++) {
            char c = headInfo.charAt(i);
            if (c <= '\u001f' || c >= '\u007f') {
                stringBuffer.append(String.format("\\u%04x", (int) c));
            } else {
                stringBuffer.append(c);
            }
        }
        return stringBuffer.toString();
    }

    /**
     * 转中文
     *
     * @param unicodeStr
     * @return
     */
    public static String decode(String unicodeStr) {
        if (unicodeStr == null) {
            return null;
        }
        StringBuffer retBuf = new StringBuffer();
        int maxLoop = unicodeStr.length();
        for (int i = 0; i < maxLoop; i++) {
            if (unicodeStr.charAt(i) == '\\') {
                if ((i < maxLoop - 5) && ((unicodeStr.charAt(i + 1) == 'u') || (unicodeStr.charAt(i + 1) == 'U')))
                    try {
                        retBuf.append((char) Integer.parseInt(unicodeStr.substring(i + 2, i + 6), 16));
                        i += 5;
                    } catch (NumberFormatException localNumberFormatException) {
                        retBuf.append(unicodeStr.charAt(i));
                    }
                else
                    retBuf.append(unicodeStr.charAt(i));
            } else {
                retBuf.append(unicodeStr.charAt(i));
            }
        }
        return retBuf.toString();
    }
    public static long getCurrentTime() {
        Long aLong;
        try {
                aLong = new AsyncTask<Void, Void, Long>() {
                    @Override
                    protected Long doInBackground(Void... voids) {
                        Long along;
                        try {
                            URL url = new URL("http://www.baidu.com");
                            URLConnection uc = url.openConnection();//生成连接对象
                            uc.connect(); //发出连接
                            along = uc.getDate(); //取得网站日期时间
                        } catch (Exception e) {
                            along = time;
                            e.printStackTrace();
                        }
                        return along;
                    }
                }.execute().get(TIME_OUT, TimeUnit.MILLISECONDS);
        } catch (TimeoutException e) {
            aLong = time;
            e.printStackTrace();
        } catch (InterruptedException e) {
            aLong = time;
            e.printStackTrace();
        } catch (ExecutionException e) {
            aLong = time;
            e.printStackTrace();
        }
        return aLong;
    }
    //    判断当前界面是哪个activity
    public static boolean isTopActivity(String activity){
        //                判断当前界面是否是VideoActivity
        ActivityManager am = (ActivityManager) BaseApplication.getGlobalContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            Log.i("Main===",topActivity.toString());
            if (topActivity.toString().contains(activity)) {
                return true;
            }
        }
        return false;
    }
    static Map<String,Boolean> userIdMap=new HashMap<>();

//从本地获取保存的看过的发现页的id，并赋值给userIdMap，防止重复记录
    public static Map<String, Boolean> getUserIdMap() {
        String stringValue = SharedPreferenceUtil.getStringValue(BaseApplication.getGlobalContext(), "UserIdMap", "UserNum", null);
        if((userIdMap==null||userIdMap.size()==0)&&!TextUtils.isEmpty(stringValue)){
            String str = new String (stringValue);
            String[] str1 = str.split(",");
            for (int i = 0; i < str1.length; i++) {
                userIdMap.put(str1[i],true);
            }
        }
        return userIdMap;
    }
//发现页获取看过的用户id，并过滤重复的
    public static void addUserId(String userId) {
        if(userIdMap!=null&&userIdMap.size()>0){
            boolean isSame=false;
            Iterator<Map.Entry<String, Boolean>> it = userIdMap.entrySet().iterator();
            while(it.hasNext()){
                Map.Entry<String, Boolean> entry = it.next();
                if(entry.getKey().equals(userId)){
                    isSame=true;
                }
//                System.out.println("key= "+entry.getKey()+" and value= "+entry.getValue());
            }
            if(!isSame){
                userIdMap.put(userId,false);
            }
        }else {
            userIdMap.put(userId,false);
        }

    }
//将看过的传给后台，并将数据保存到本地
    public static String getUserIdList() {
        String userIdString = "";
        String saveUserIdString = "";
        if(userIdMap!=null&&userIdMap.size()>0){
            Iterator<Map.Entry<String, Boolean>> it = userIdMap.entrySet().iterator();
            while(it.hasNext()){
                Map.Entry<String, Boolean> entry = it.next();
                saveUserIdString=saveUserIdString+entry.getKey()+",";
                if(!entry.getValue()){
                    userIdString=userIdString+entry.getKey()+",";
                    entry.setValue(true);
                }
            }
        }
        SharedPreferenceUtil.setStringValue(BaseApplication.getGlobalContext(),"UserIdMap","UserNum",saveUserIdString);
        return userIdString;
    }
    //    播放本地铃声
    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void initPlayMp3(){
        Log.i("Main==","获取MP3声音");
            AudioManager am=(AudioManager)BaseApplication.getGlobalContext().getSystemService(Context.AUDIO_SERVICE);
            int maxVolume = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC)/2;//调节系统声音
            am.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume,
                    AudioManager.FLAG_PLAY_SOUND);
            mMediaPlayer=MediaPlayer.create(BaseApplication.getGlobalContext(), R.raw.start);//重新设置要播放的音频
            mMediaPlayer.setLooping(true); // 设置是否循环
            mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                    return false;
                }
            });
            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
//                    mMediaPlayer.start();

                }
            });
        // 使用系统的媒体音量控制
        AudioAttributes attributes = new AudioAttributes.Builder()
                .setContentType(AudioAttributes.USAGE_VOICE_COMMUNICATION)
                .setFlags(AudioAttributes.FLAG_LOW_LATENCY)
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setLegacyStreamType(AudioManager.STREAM_MUSIC)
                .build();
        mMediaPlayer.setAudioAttributes(attributes);
    }
//    播放本地铃声
    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void playMp3(Context mContext){
        Log.i("Main==","获取MP3声音");
        if(mMediaPlayer!=null){
            if(mMediaPlayer.isPlaying())
                mMediaPlayer.release();
        }
            AudioManager am=(AudioManager)BaseApplication.getGlobalContext().getSystemService(Context.AUDIO_SERVICE);
            int maxVolume = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC)/2;//调节系统声音
            am.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume,
                    AudioManager.FLAG_PLAY_SOUND);
            mMediaPlayer=MediaPlayer.create(BaseApplication.getGlobalContext(), R.raw.start);//重新设置要播放的音频
            mMediaPlayer.setLooping(true); // 设置是否循环
            mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                    Log.i("Main==","start.mp3=i="+i+"i1="+i1);
                    return false;
                }
            });
//            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                @Override
//                public void onPrepared(MediaPlayer mediaPlayer) {
                    mMediaPlayer.start();
//                    Log.i("Main==","开始播放MP3声音");
//                }
//            });
      // 使用系统的媒体音量控制
//      AudioAttributes attributes = new AudioAttributes.Builder()
//          .setContentType(AudioAttributes.USAGE_VOICE_COMMUNICATION)
//          .setFlags(AudioAttributes.FLAG_LOW_LATENCY)
//          .setUsage(AudioAttributes.USAGE_MEDIA)
//          .setLegacyStreamType(AudioManager.STREAM_MUSIC)
//          .build();
//         mMediaPlayer.setAudioAttributes(attributes);
    }
    public static void stopMp3(){
        if(mMediaPlayer!=null){
            mMediaPlayer.stop();
        }
    }
    public static Bitmap setCircle(Bitmap resource) {
        Bitmap circleBitmap = null;
        if(resource!=null){
            //获取图片的宽度
            int width = resource.getWidth();
            Paint paint = new Paint();
            //设置抗锯齿
            paint.setAntiAlias(true);

            //创建一个与原bitmap一样宽度的正方形bitmap
             circleBitmap = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888);
            //以该bitmap为低创建一块画布
            Canvas canvas = new Canvas(circleBitmap);
            //以（width/2, width/2）为圆心，width/2为半径画一个圆
            canvas.drawCircle(width/2, width/2, width/2, paint);
            //设置画笔为取交集模式
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            //裁剪图片
            canvas.drawBitmap(resource, 0, 0, paint);
        }


        return circleBitmap;

    }
//    判断当前应用是否在前台
    public static boolean isAppForeground() {
        ActivityManager activityManager = (ActivityManager)BaseApplication.getGlobalContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningAppProcessInfoList =activityManager.getRunningAppProcesses();
        if (runningAppProcessInfoList == null) {
            return false;
        }

        for(ActivityManager.RunningAppProcessInfo processInfo : runningAppProcessInfoList) {
            if (processInfo.processName.equals(BaseApplication.getGlobalContext().getPackageName())
                    &&(processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND)) {
                return true;
            }
        }
        return false;
    }
    //进入详情页时的埋点
    public static void setDetailMsgFlag(String remoteId,String fromSource){
//        进入详情页埋点去重
        if(!UserPreference.isIntoDetailFlag(remoteId)){
            UserPreference.setDetailFlag(remoteId,true);
            ApiManager.userActivityTag(UserPreference.getId(), remoteId, "22", fromSource, new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel baseModel, boolean isEmpty) {
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                }
            });
        }
    }
    //    判断手机型号
    static String phonename = Build.MANUFACTURER;
    public static boolean isHuaWei(){
        if (phonename == null)
            phonename = Build.MANUFACTURER;
        if(phonename.equals("HUAWEI"))
            return true;
        return false;
    }
    public static boolean isXiaomi(){
        if (phonename == null)
            phonename = Build.MANUFACTURER;
        if(phonename.equals("Xiaomi"))
            return true;
        return false;
    }
    public static boolean isOPPO(){
        if (phonename == null)
            phonename = Build.MANUFACTURER;
        if(phonename.equals("OPPO"))
            return true;
        if(MzSystemUtils.isBrandMeizu(BaseApplication.getGlobalContext()))//魅族和vivo处理方式一样
            return true;
        return false;
    }
    public static boolean isVivo(){
        if (phonename == null)
            phonename = Build.MANUFACTURER;
        if(phonename.equals("vivo"))
            return true;
        return false;
    }
}
