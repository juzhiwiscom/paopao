package com.wiscomwis.facetoface.common;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.PowerManager;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.hyphenate.EMCallBack;
import com.hyphenate.EMConnectionListener;
import com.hyphenate.EMError;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMCmdMessageBody;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.chat.EMImageMessageBody;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMOptions;
import com.hyphenate.chat.EMTextMessageBody;
import com.hyphenate.exceptions.HyphenateException;
import com.meizu.cloud.pushsdk.PushManager;
import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseApplication;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.CustomCmd;
import com.wiscomwis.facetoface.data.model.HuanXinUser;
import com.wiscomwis.facetoface.data.model.NettyMessage;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.event.CloseRingtoneEvent;
import com.wiscomwis.facetoface.event.FinishChatActivityEvent;
import com.wiscomwis.facetoface.event.MessageArrive;
import com.wiscomwis.facetoface.event.RingtoneEvent;
import com.wiscomwis.facetoface.event.UnreadMsgChangedEvent;
import com.wiscomwis.facetoface.parcelable.VideoInviteParcelable;
import com.wiscomwis.facetoface.ui.main.MainActivity;
import com.wiscomwis.facetoface.ui.video.VideoInviteActivity2;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.NotificationHelper;
import com.wiscomwis.library.util.SharedPreferenceUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * 环信帮助类（环信注册由后台完成，在调用注册接口时后台直接注册环信，返回用户account和密码）
 * Created by zhangdroid on 2017/6/29.
 */
public class HyphenateHelper {
    private static final String TAG = HyphenateHelper.class.getSimpleName();
    private static HyphenateHelper sInstance;
    // 环信消息监听器
    private EMMessageListener mMessageListener;
    private boolean isChatActivity = true;
    private String huanxinId = "";
    private WrittingCmdCallback mWrittingCmdCallback;
    private int loginNum=0;
    private boolean isVideoInviteActivity2=true;
    private String userId="123456";

    private HyphenateHelper() {
    }



    /**
     * 环信登录回调
     */
    public interface OnLoginCallback {
        /**
         * 环信登录成功
         */
        void onSuccess();

        /**
         * 环信登录失败
         */
        void onFailed();
    }
    /**
     * 正在输入的透传消息回调
     * @return
     */
    public interface WrittingCmdCallback{
        /**
         * 消息到了
         */
        void msgIsWritting();
    }

    public static HyphenateHelper getInstance() {
        if (null == sInstance) {
            synchronized (HyphenateHelper.class) {
                if (null == sInstance) {
                    sInstance = new HyphenateHelper();
                }
            }
        }
        return sInstance;
    }

    /**
     * 消息发送监听器
     */
    public interface OnMessageSendListener {
        /**
         * 发送成功
         *
         * @param emMessage 发送的消息对象
         */
        void onSuccess(EMMessage emMessage);

        /**
         * 发送失败
         */
        void onError();
    }

    /**
     * 初始化环信
     */
    public void init(Context context) {
        Log.i(TAG, "init");
        EMOptions emOptions = new EMOptions();
        emOptions.setSortMessageByServerTime(true);
        emOptions.setRequireAck(false);
        // GCM
//        emOptions.setGCMNumber("");
        // 设置小米推送 appID 和 appKey
        emOptions.setMipushConfig(C.XIAOMI_APPID, C.XIAOMI_APPKEY);
//        emOptions.setMipushConfig();
        EMClient.getInstance().init(context.getApplicationContext(), emOptions);
        // 开启debug模式，上线的时候需要关闭
        EMClient.getInstance().setDebugMode(true);

    }

    /**
     * 给环信设置监听
     */
    public void setOnClick() {
        // 设置连接监听
        setConnectionListener();
        // 设置消息监听
        setGlobalMessageListener();
    }

    /**
     * 登录
     *
     * @param account  用户account
     * @param password 密码
     */
    public void login(String account, String password, final OnLoginCallback onLoginCallback) {
        if (isLoggedIn()) {
            return;
        }
        if (TextUtils.isEmpty(account) || TextUtils.isEmpty(password)) {
            if (null != onLoginCallback) {
                onLoginCallback.onFailed();
            }
            return;
        }
        final long firstLogin = System.currentTimeMillis();
        Log.e("AAAAAAA", "Login: 调用登陆环信接口---------=====时间戳=" + firstLogin);
        EMClient.getInstance().login(account, password, new EMCallBack() {
            @Override
            public void onSuccess() {
                long onSuccess = System.currentTimeMillis();
                long time = (onSuccess - firstLogin) / 1000;
                Log.e("AAAAAAA", "onSuccess:登陆成功---===时间戳="+onSuccess+"---用时="+time);
//                huanxinFail(1,"测试，环信登陆成");
                // 登录成功后加载聊天会话
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        HyphenateHelper.getAllMsgFromHuanXin();
                        EMClient.getInstance().chatManager().loadAllConversations();
                    }
                }).start();
                if (null != onLoginCallback) {
                    onLoginCallback.onSuccess();
                }
            }
            @Override
            public void onError(int code, String error) {
                long onSuccess = System.currentTimeMillis();
                long time = (onSuccess - firstLogin) / 1000;
                Log.e("AAAAAAA", "onError:登陆失败---===时间戳="+onSuccess+"---用时="+time);
                Log.e("AAAAAAA", "onError: 环信登录出现的问题::" + code + "----" + error);
                ++loginNum;
                if(loginNum>=4)
                    huanxinFail(code,error);
                if (null != onLoginCallback) {
                    onLoginCallback.onFailed();
                }
            }

            @Override
            public void onProgress(int progress, String status) {
            }
        });
    }
    /**
     * 环信登陆失败埋点
     * @param code
     * @param error
     */
    private void huanxinFail(int code, String error) {
        //                埋点：
        String content = "onError: 环信登录出现的问题::" + code + "----" + error;
        ApiManager.userActivityTag(UserPreference.getId(), content,new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }


    /**
     * 登出
     */
    public void logout() {
        EMClient.getInstance().logout(true, new EMCallBack() {

            @Override
            public void onSuccess() {
                // TODO Auto-generated method stub
                String content = "onError: 环信退出成功:";



            }

            @Override
            public void onProgress(int progress, String status) {
                // TODO Auto-generated method stub
                Log.e("AAAAAAA", "onProgress:=="+progress+"==status="+status);
                String content = "onError: 环信退出err:";


            }

            @Override
            public void onError(int code, String message) {
                // TODO Auto-generated method stub
                Log.e("AAAAAAA", "onError:=="+code+"==message="+message);
                String content = "onError: 环信退出err:";


            }
        });
        // 绑定了GCM，参数必须为true，登出后会解绑GCM
//        EMClient.getInstance().logout(true);
        // 移除消息接收监听
        removeMessageListener(mMessageListener);

    }

    /**
     * @return 是否登录过
     */
    public boolean isLoggedIn() {
        return EMClient.getInstance().isLoggedInBefore();
    }

    /**
     * 设置连接状态监听
     */
    private void setConnectionListener() {
        EMClient.getInstance().addConnectionListener(new EMConnectionListener() {
            @Override
            public void onConnected() {
                Log.i(TAG, "onConnected");
            }

            @Override
            public void onDisconnected(int errorCode) {
                if (errorCode == EMError.USER_REMOVED) {
                    // 帐号已经被移除
                } else if (errorCode == EMError.USER_LOGIN_ANOTHER_DEVICE) {
                    // 帐号在其他设备登录
                    if (EMClient.getInstance().isLoggedInBefore()) {
                        logout();
                    }
                }
            }
        });
    }

    /**
     * 设置全局消息监听
     */
    private void setGlobalMessageListener() {

        mMessageListener = new EMMessageListener() {
            @Override
            public void onMessageReceived(List<EMMessage> messages) {
                Log.e("BBBBBBB","接收到新消息");

                // 接收到新消息
                HuanXinUser user = null;
                for (EMMessage emMessage : messages) {
                    String userName = "";
                    String userPic = "";
                    String userAccount = "";
                    String userGuid = "";
                    String msgContent = "";
                    int extendType;
                    int notice = 0;
                    msgContent = emMessage.getStringAttribute("msg", "");
                    Log.e("msgcontent", msgContent);
                    if (!TextUtils.isEmpty(msgContent)) {
                        NettyMessage nettyMessage = setMsgContent(msgContent);
                        if (nettyMessage != null) {
                            Context context = BaseApplication.getGlobalContext();
                            userName = nettyMessage.getSendUserName();
                            userPic = nettyMessage.getSendUserIcon();
                            userAccount = nettyMessage.getSendUserAccount();
                            userGuid = String.valueOf(nettyMessage.getSendUserId());
                            String msgId = nettyMessage.getMsgId();
                            if(!TextUtils.isEmpty(userGuid)&&!TextUtils.isEmpty(msgId))
                                addTag(userGuid,msgId);
                            extendType = nettyMessage.getExtendType();
                            notice = nettyMessage.getNotice();
                            SharedPreferenceUtil.setBooleanValue(context,"isSendMsg",userAccount,true);
                            String lastMsg = context.getString(R.string.how_are_you);
                            if (emMessage.getType() == EMMessage.Type.TXT) {
                                EMTextMessageBody emTextMessageBody = (EMTextMessageBody) emMessage.getBody();
                                if (nettyMessage.getExtendType() == 2 || emTextMessageBody.getMessage().equals("msgCall")) {
                                    lastMsg = context.getString(R.string.video_invitations);
                                } else if (nettyMessage.getExtendType() == 3) {
                                    lastMsg = context.getString(R.string.one_private_message);
                                } else if (nettyMessage.getExtendType() == 5) {
                                    lastMsg = context.getString(R.string.gift);
                                }else if(nettyMessage.getExtendType() == 19){
                                    lastMsg = context.getString(R.string.voice_message);
                                } else {
                                    if(emTextMessageBody.getMessage().contains(".mp3")){
                                        lastMsg = context.getString(R.string.voice_message);
                                    }else{
                                        lastMsg = emTextMessageBody.getMessage();
                                        if(lastMsg.contains(C.withdraw_msg)){
                                           String withdrawSendTime=lastMsg.split("对")[0];
                                            UserPreference.setWithdrawMsg(userAccount,withdrawSendTime);
                                            lastMsg=C.withdraw_msg;
                                        }
                                    }
                                }
                            } else if (emMessage.getType() == EMMessage.Type.VOICE) {
                                lastMsg = context.getString(R.string.voice_message);
                            } else if (emMessage.getType() == EMMessage.Type.IMAGE) {
                                lastMsg = context.getString(R.string.photo_message);
                            }
//                            if (isChatActivity&&!UserPreference.isReception()) {
                            //        删除魅族的通知栏消息
                            PushManager.clearNotification(BaseApplication.getGlobalContext());

                            if (notice == 1) {
                                    showNotification(emMessage);
                                    wakeUpAndUnlock(BaseApplication.getGlobalContext());
                                }
                            shake(BaseApplication.getGlobalContext());

//                            }
                            String lockVideoId = UserPreference.getLockVideoId();
//                            判断是锁屏时拨打的电话，且对方已取消
                            if(lockVideoId!=null&&lockVideoId.equals(userGuid)&&lastMsg.contains("已取消")){
                                EventBus.getDefault().post(new CloseRingtoneEvent(false));
                            }

                            user = new HuanXinUser(userGuid, userName, userPic, userAccount, "1", lastMsg, 0, String.valueOf(emMessage.getMsgTime()), extendType);
                            huanxinId = userGuid;
                            if (!TextUtils.isEmpty(huanxinId)) {
                                EMConversation conversation = EMClient.getInstance().chatManager().getConversation(userAccount);
                                if (conversation != null) {
                                    user.setMsgNum(conversation.getUnreadMsgCount());
                                }
                            }
                            DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
                            if (!TextUtils.isEmpty(nettyMessage.getMsgId())) {
                                msgReceived(nettyMessage.getMsgId());
                            }
                        }
                        //消息过来时刷新聊天列表
                        EventBus.getDefault().post(new MessageArrive(huanxinId));
                        EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
                        if (emMessage.getType() == EMMessage.Type.IMAGE) {
//                            如果是图片，将图片缓存到本地
                            cacheImg(emMessage,userPic);
                        }
                    }
                }


            }

            @Override
            public void onCmdMessageReceived(List<EMMessage> messages) {
                for (EMMessage message : messages) {
                    try {
                        if(message.getBody().toString().contains("cmd")){
                            String json = message.getBody().toString();
                            String passthrough = json.substring(4);
                            //自定义透传消息
                            if(!TextUtils.isEmpty(passthrough)){
                                String substring = passthrough.substring(1, passthrough.length() - 1);
                                if(!TextUtils.isEmpty(substring)){
                                    CustomCmd customCmd = new Gson().fromJson(substring, CustomCmd.class);
                                    if(customCmd!=null){
                                        if(customCmd.getType()==1){
                                             EventBus.getDefault().post(customCmd);
                                        }
                                    }
                                }
                            }
                        }
                        String msg = message.getStringAttribute("msg").toString();
                        if (!TextUtils.isEmpty(msg)) {
                            NettyMessage pushMessage = new Gson().fromJson(msg, NettyMessage.class);
                            if (pushMessage != null) {
                                if (pushMessage.getExtendType() == 7) {//页脚的策略信息
                                    DataPreference.saveYeJiaoJsonData(msg);//保存透传的json数据
                                    DataPreference.saveYeJiaoTime(System.currentTimeMillis());//保存透传信息过来时的时间
                                    EventBus.getDefault().post(pushMessage);
                                }
                                //        删除魅族的通知栏消息
                                PushManager.clearNotification(BaseApplication.getGlobalContext());
                                if(pushMessage.getExtendType()==12){//透传召回
                                    showReceiveNotification(pushMessage);
                                }
                                Log.e("AAAAAAA", "HyphenateHelper==onCmdMessageReceived: "+pushMessage.getBaseType()+"===="+pushMessage.getExtendType()+"==="+pushMessage.getType());
                                if (pushMessage.getBaseType() == 1) {
                                        if (pushMessage.getExtendType() == 10&&(!Util.isTopActivity("VideoInviteActivity"))) {//群发视频的呼叫信
                                            if (Long.valueOf(DataUtil.getTime()) - Long.valueOf(DataUtil.formatDate(pushMessage.getSendTime())) <= 110000) {
                                                VideoInviteParcelable videoInviteParcelable = new VideoInviteParcelable(true, pushMessage.getSendUserId(), pushMessage.getSendUserAccount(), pushMessage.getSendUserName(), pushMessage.getSendUserIcon(), 0, 0);
                                                videoInviteParcelable.setChannelId(String.valueOf(pushMessage.getSendUserId()) + String.valueOf(pushMessage.getRecvUserId()));
                                                EventBus.getDefault().post(new FinishChatActivityEvent());
//                                                判断当前界面是否是正在邀请，不是的话，跳转邀请界面
                                                String phonename = Build.MANUFACTURER;
                                                Log.e("BBBBBBB","声网亮屏幕并准备进入VideoInviteActivity+手机型号：："+phonename);
                                                    if (Util.isVivo()) {
                                                        if(UserPreference.isReception()){//应用处于打开状态
                                                            LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity2.class, videoInviteParcelable);
                                                        }else {
                                                            if(UserPreference.isLockScreen()){//应用处于锁屏状态
                                                                EventBus.getDefault().post(new RingtoneEvent(videoInviteParcelable, false));
                                                            }else {
                                                                LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity2.class, videoInviteParcelable);
                                                            }
                                                        }
                                                    }else if(Util.isOPPO()) {
                                                        boolean launcher = Util.isTopActivity("Launcher");
                                                        if(launcher){
                                                            EventBus.getDefault().post(new RingtoneEvent(videoInviteParcelable, false));
                                                        }else {
                                                            LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity2.class, videoInviteParcelable);
                                                        }
                                                    } else {
                                                        LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity2.class, videoInviteParcelable);
                                                    }
                                                wakeUpAndUnlock(BaseApplication.getGlobalContext());
                                               }
                                        }
                                        if (pushMessage.getExtendType() == 11&&(!Util.isTopActivity("VideoInviteActivity"))) {//群发语音的呼叫信
                                            if (Long.valueOf(DataUtil.getTime()) - Long.valueOf(DataUtil.formatDate(pushMessage.getSendTime())) <= 110000) {
                                                VideoInviteParcelable videoInviteParcelable = new VideoInviteParcelable(true, pushMessage.getSendUserId(), pushMessage.getSendUserAccount(), pushMessage.getSendUserName(), pushMessage.getSendUserIcon(), 1, 0);
                                                videoInviteParcelable.setChannelId(String.valueOf(pushMessage.getSendUserId()) + String.valueOf(pushMessage.getRecvUserId()));
                                                String phonename = Build.MANUFACTURER;
                                                EventBus.getDefault().post(new FinishChatActivityEvent());
                                                Log.e("BBBBBBB","声网亮屏幕并准备进入VideoInviteActivity+手机型号：："+phonename);
                                                if (phonename != null && phonename != null && phonename != "") {
                                                    if ((phonename.equals("vivo")||Util.isOPPO())&&!UserPreference.isReception()) {
                                                        EventBus.getDefault().post(new RingtoneEvent(videoInviteParcelable,false));
                                                    }else {
                                                        LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity2.class, videoInviteParcelable);
                                                    }
                                                }else {
                                                    LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity2.class, videoInviteParcelable);
                                                }
//                                                    LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity2.class, videoInviteParcelable);
                                                    wakeUpAndUnlock(BaseApplication.getGlobalContext());
                                            }
                                        }

                                }
                            }
                            CustomCmd customCmd = new Gson().fromJson(msg, CustomCmd.class);
                            if(customCmd!=null){
                                if(customCmd.getType()==1){
                                    EventBus.getDefault().post(customCmd);
                                }
                            }
                        }
                    } catch (HyphenateException e) {
                        e.printStackTrace();
                    }

                }
            }

            /**
             * 锁屏状态下点亮屏幕
             */
            private void wakeUpAndUnlock(Context context) {
                //屏锁管理器
                KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
                KeyguardManager.KeyguardLock kl = km.newKeyguardLock("unLock");
                //解锁
                kl.disableKeyguard();
                //获取电源管理器对象
                PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                //获取PowerManager.WakeLock对象,后面的参数|表示同时传入两个值,最后的是LogCat里用的Tag
                PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_DIM_WAKE_LOCK, "bright");
                //点亮屏幕
                wl.acquire();
                //释放
                wl.release();
            }

            @Override
            public void onMessageRead(List<EMMessage> messages) {
                Log.e("AAAAAAA", "onMessageRead: " + messages.size());
            }

            @Override
            public void onMessageDelivered(List<EMMessage> messages) {
                Log.e("AAAAAAA", "onMessageDelivered: " + messages.size());
            }

            @Override
            public void onMessageRecalled(List<EMMessage> list) {
                Log.e("AAAAAAA", "onMessageRecalled: " + list.size());

            }

            @Override
            public void onMessageChanged(EMMessage message, Object change) {
                Log.e("AAAAAAA", "onMessageChanged: 消息发生变动");
            }
        };
        setMessageListener(mMessageListener);
    }

    private void addTag(String sendGuid, String msgId) {
        ApiManager.userActivityContentTag(UserPreference.getId(), sendGuid, "28","1",msgId, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });    }

    /**
     * 客户端收到消息后，向服务器回执，确认已送达
     * @param msgId
     */
    private static void msgReceived(String msgId) {
        ApiManager.msgReceived(msgId, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }


    private static void cacheImg(EMMessage emMessage, String userPic) {
        final EMImageMessageBody emImageMessageBody = (EMImageMessageBody) emMessage.getBody();
        if(emImageMessageBody!=null){
            String remoteUrl = emImageMessageBody.getRemoteUrl();
            if(!TextUtils.isEmpty(remoteUrl)){
                new getImageCacheAsyncTask(BaseApplication.getGlobalContext()).execute(remoteUrl);
                new getImageCacheAsyncTask(BaseApplication.getGlobalContext()).execute(userPic);

            }
        }
    }

    //解析发过来的数据
    private static NettyMessage setMsgContent(String msgContent) {
        if (!TextUtils.isEmpty(msgContent)) {
            NettyMessage nettyMessage = new Gson().fromJson(msgContent, NettyMessage.class);
            return nettyMessage;
        } else {
            return null;
        }
    }

    private void showNotification(EMMessage emMessage) {
        boolean applicationBroughtToBackground = isApplicationBroughtToBackground(BaseApplication.getGlobalContext());
        if (null != emMessage&&applicationBroughtToBackground) {
            String userName = "";
            String userPic = "";
            String userGuid = "";
            String userPrice = "";
            String msgContent = "";
            msgContent = emMessage.getStringAttribute("msg", "");
            if (!TextUtils.isEmpty(msgContent)) {
                NettyMessage nettyMessage = setMsgContent(msgContent);
                if (nettyMessage != null) {
                    userName = nettyMessage.getSendUserName();
                    userPic = nettyMessage.getSendUserIcon();
                    userGuid = String.valueOf(nettyMessage.getSendUserId());
                }
            }
            Context context = BaseApplication.getGlobalContext();
            // 根据消息类型显示不同的内容
//            String message = "";
//            if (emMessage.getType() == EMMessage.Type.TXT) {// 文字
//                EMTextMessageBody emTextMessageBody = (EMTextMessageBody) emMessage.getBody();
//                if (null != emTextMessageBody) {
//                    if (emTextMessageBody.getMessage().equals("msgCall")) {
//                        message = context.getString(R.string.video_invitations);
//                    } else if (emTextMessageBody.getMessage().contains("{") && emTextMessageBody.getMessage().contains("}")) {
//                        message = "礼物";
//                    } else  if(emTextMessageBody.getMessage().contains(".mp3")){
//                        message = context.getString(R.string.voice_message);
//                    } else {
//                        message = TextUtils.isEmpty(emTextMessageBody.getMessage()) ? context.getString(R.string.private_news) : emTextMessageBody.getMessage();
//                    }
//
//                }
//
//            } else if (emMessage.getType() == EMMessage.Type.VOICE) {// 语音
//                message = context.getString(R.string.chat_type_voice);
//            } else if (emMessage.getType() == EMMessage.Type.IMAGE) {// 图片
//                message = context.getString(R.string.chat_type_image);
//            }
//            Intent intent = new Intent(context, ChatActivity.class);
//            if (userGuid != null && userGuid.length() > 0) {
//                intent.putExtra(LaunchHelper.INTENT_KEY_COMMON, new ChatParcelable(Long.parseLong(userGuid), emMessage.getFrom(), userName, userPic, 1));
//            }
//            NotificationHelper.getInstance(context)
//                    .setTicker(context.getString(R.string.chat_new_msg_received, userGuid.equals("10000") ? context.getString(R.string.kefu_desc) : userName))
//                    .setIcon(R.mipmap.vivologo)
//                    .setTitle(userName)
//                    .setMessage(message)
//                    .setTime(emMessage.getMsgTime())
//                    .setDefaults()
//                    .setPendingIntentActivity(Integer.parseInt(emMessage.getFrom()), intent, PendingIntent.FLAG_UPDATE_CURRENT)
//                    .notify(Integer.parseInt(userGuid));

            Intent intent = new Intent(context, MainActivity.class);
            intent.setFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(LaunchHelper.INTENT_KEY_COMMON, true);

            int userNum = 0;
            int msgNum=0;
            String msg="";
            List<HuanXinUser> allAcount = DbModle.getInstance().getUserAccountDao().getAllAcount();
            for (HuanXinUser huanXinUser : allAcount) {
                if(huanXinUser!=null){
                    if(huanXinUser.getMsgNum()>0){
                        userNum+=1;
                        msgNum+=huanXinUser.getMsgNum();
                    }

                }
            }
            if(msgNum>0){
                if(userNum<=1){
                    msg="有人给你发了"+msgNum+"条新消息";
                }else {
                    msg="有"+userNum+"个人给你发了"+msgNum+"条新消息";
                }
                NotificationHelper.getInstance(context)
                        .setTicker(msg)
                        .setIcon(R.mipmap.logo)
                        .setTitle("泡泡")
                        .setMessage(msg)
                        .setTime(emMessage.getMsgTime())
                        .setDefaults()
                        .setPendingIntentActivity(Integer.parseInt(emMessage.getFrom()), intent, PendingIntent.FLAG_UPDATE_CURRENT,true)
                        .notify(Integer.parseInt(userGuid));


            }

        }
    }

    private void showReceiveNotification(NettyMessage pushMsg) {

        if (null != pushMsg) {
            Context context = BaseApplication.getGlobalContext();
            Intent intent = new Intent(context, MainActivity.class);
            intent.setFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            NotificationHelper.getInstance(context)
                    .setTicker(context.getString(R.string.chat_new_msg_received, pushMsg.getSendUserName()))
                    .setIcon(R.mipmap.logo)
                    .setTitle(pushMsg.getSendUserName())
                    .setMessage(pushMsg.getContent())
                    .setTime(System.currentTimeMillis())
                    .setDefaults()
                    .setPendingIntentActivity(0, intent, PendingIntent.FLAG_UPDATE_CURRENT,true)
                    .notify(1);

        }
    }

    /**
     * 设置消息监听
     */
    public void setMessageListener(EMMessageListener listener) {
        if (null != listener) {
            EMClient.getInstance().chatManager().addMessageListener(listener);
        }
    }

    /**
     * 移除接收消息监听器
     */
    public void removeMessageListener(EMMessageListener listener) {
        if (null != listener) {
            EMClient.getInstance().chatManager().removeMessageListener(listener);
        }
    }

    /**
     * 发送消息（仅单聊）
     *
     * @param emMessage
     * @param listener
     */
    private void sendMessage(final EMMessage emMessage, final OnMessageSendListener listener) {
        if (null != emMessage) {
            emMessage.setChatType(EMMessage.ChatType.Chat);
            emMessage.setMessageStatusCallback(new EMCallBack() {
                @Override
                public void onSuccess() {
                    if (null != listener) {
                        listener.onSuccess(emMessage);
                    }
                }

                @Override
                public void onError(int code, String error) {
                    Log.e("AAAAAAA", "onError: 这是在HyphenateHelper消息发送失败的code=" + code + "----错误信息为" + error);
                    setTag("onError: 这是在HyphenateHelper消息发送失败的code=" + code + "----错误信息为:" + error);
                    if (null != listener) {
                        listener.onError();
                    }
                }

                @Override
                public void onProgress(int progress, String status) {
                }
            });

            //设置扩展的消息
            NettyMessage nettyMessage = new NettyMessage();
            nettyMessage.setSendUserAccount(UserPreference.getAccount());
            nettyMessage.setSendUserName(UserPreference.getNickname());
            nettyMessage.setSendUserIcon(UserPreference.getMiddleImage());
            nettyMessage.setSendUserId(Long.parseLong(UserPreference.getId()));
            nettyMessage.setNotice(1);
            nettyMessage.setExtendType(1);
            nettyMessage.setExt(null);
            nettyMessage.setSendTime(Util.getCurrentTime()+"");
            emMessage.setAttribute("msg", new Gson().toJson(nettyMessage));
            EMClient.getInstance().chatManager().sendMessage(emMessage);
        }
    }
    /**
     * 发送消息（消息撤回）
     *
     * @param emMessage
     * @param listener
     */
    private void sendMessage(final EMMessage emMessage,boolean isWithdraw, final OnMessageSendListener listener) {
        if (null != emMessage) {
            emMessage.setChatType(EMMessage.ChatType.Chat);
            emMessage.setMessageStatusCallback(new EMCallBack() {
                @Override
                public void onSuccess() {
                    if (null != listener) {
                        listener.onSuccess(emMessage);
                    }
                }

                @Override
                public void onError(int code, String error) {
                    Log.e("AAAAAAA", "onError: 这是在HyphenateHelper消息发送失败的code=" + code + "----错误信息为" + error);
                    if (null != listener) {
                        listener.onError();
                    }
                }

                @Override
                public void onProgress(int progress, String status) {
                }
            });

            //设置扩展的消息
            NettyMessage nettyMessage = new NettyMessage();
            nettyMessage.setSendUserAccount(UserPreference.getAccount());
            nettyMessage.setSendUserName(UserPreference.getNickname());
            nettyMessage.setSendUserIcon(UserPreference.getMiddleImage());
            nettyMessage.setSendUserId(Long.parseLong(UserPreference.getId()));
            nettyMessage.setNotice(1);
            nettyMessage.setExtendType(1);
            nettyMessage.setExt(null);
            nettyMessage.setWithdraw(1);
            nettyMessage.setSendTime(Util.getCurrentTime()+"");
            emMessage.setAttribute("msg", new Gson().toJson(nettyMessage));
            EMClient.getInstance().chatManager().sendMessage(emMessage);
        }
    }
    /**
     * 发送文字消息（消息撤回）
     *
     * @param account 接收方account
     * @param text    消息内容
     */
    public void sendTextMessage(String account, String text,boolean isWithdraw, OnMessageSendListener listener) {
        Log.i(TAG, "sendTextMessage#account = " + account + " text = " + text);
        // 登录过环信，加载
        if (!isLoggedIn())
            HyLogin(0);
        sendMessage(EMMessage.createTxtSendMessage(text, account), isWithdraw,listener);
    }

    /**
     *  正在输入中的透传消息
     * @param account    发送方account
     * @param action     发送的内容
     * @param type       发送的类型
     */
    public void sendCmdMessage(String account,String action,int type){
        EMMessage cmdMsg = EMMessage.createSendMessage(EMMessage.Type.CMD);
        CustomCmd customCmd=new CustomCmd();
        customCmd.setSendUserAccount(UserPreference.getAccount());
        customCmd.setContent(action);
        customCmd.setReservedField("预留");
        customCmd.setSendUserImageUrl(UserPreference.getMiddleImage());
        customCmd.setSendUserId(UserPreference.getId());
        customCmd.setType(type);
        EMCmdMessageBody cmdBody = new EMCmdMessageBody(new Gson().toJson(customCmd));
        cmdMsg.setTo(account);
        cmdMsg.addBody(cmdBody);
//        cmdMsg.setAttribute("passthrough",new Gson().toJson(customCmd));
        EMClient.getInstance().chatManager().sendMessage(cmdMsg);
    }
    /**
     * 发送文字消息（仅单聊）
     *
     * @param account 接收方account
     * @param text    消息内容
     */
    public void sendTextMessage(String account, String text, OnMessageSendListener listener) {
        Log.i(TAG, "sendTextMessage#account = " + account + " text = " + text);
        userId=account;
        // 登录过环信，加载
        if (!isLoggedIn())
            HyLogin(0);
        sendMessage(EMMessage.createTxtSendMessage(text, account), listener);
    }
    private void HyLogin(final long mGuid) {
        String account = UserPreference.getAccount();
        String password = UserPreference.getPassword();
        HyphenateHelper.getInstance().login(UserPreference.getAccount(), UserPreference.getPassword(), new HyphenateHelper.OnLoginCallback() {
            public void onSuccess() {
                if(mGuid!=0){
                    EventBus.getDefault().post(new MessageArrive(mGuid+""));//发送完成之后更新聊天列表
                }
                Log.e("AAAAAAA", "onSussess: 登录成功---------======登录次数" + loginNum);

            }

            @Override
            public void onFailed() {
                Log.e("AAAAAAA", "onFailed: 登录失败---------======" + loginNum);
                ++loginNum;
                if (loginNum >= 4) {
                    return;
                }
                HyLogin(mGuid);//登录失败5次之后不再登录
            }
        });
    }

    /**
     * 发送语音消息（仅单聊）
     *
     * @param account  接收方account
     * @param filePath 语音文件路径
     * @param duration 语音时间长度(单位秒)
     */
    public void sendVoiceMessage(String account, String filePath, int duration, OnMessageSendListener listener) {
        Log.i(TAG, "sendVoiceMessage#account = " + account + " filePath = " + filePath + " duration = " + duration);
        sendMessage(EMMessage.createVoiceSendMessage(filePath, duration, account), listener);
    }

    /**
     * 发送图片消息（仅单聊）
     *
     * @param account  接收方account
     * @param filePath 图片文件路径
     */
    public void sendImageMessage(String account, String filePath, OnMessageSendListener listener) {
        Log.i(TAG, "sendImageMessage#account = " + account + " filePath = " + filePath);
        // 第二个参数表示是否发送原图，图片超过100k会默认压缩后发送
        sendMessage(EMMessage.createImageSendMessage(filePath, false, account), listener);
    }

    public EMConversation getConversation(String account) {
        // 查询单聊会话
        return EMClient.getInstance().chatManager().getConversation(account, EMConversation.EMConversationType.Chat);
    }

    /**
     * 初始化某个用户的聊天记录
     *
     *
     * @param mGuid
     * @param account  用户account
     * @param pageSize 每页显示的聊天记录条数
     * @return
     */
    public List<EMMessage> initConversation(final long mGuid, String account, int pageSize) {
        // 查询单聊会话
        EMConversation emConversation = getConversation(account);
        if (null != emConversation) {
            // 标记所有未读消息为已读
            emConversation.markAllMessagesAsRead();
            return emConversation.loadMoreMsgFromDB(null, pageSize);
        }else {
            HyLogin(mGuid);
        }
        return null;
    }

    /**
     * 获取某个用户的最后一条信息
     */
    public List<EMMessage> getUnReadMsg(String account) {
        EMConversation emConversation = getConversation(account);
        if (emConversation != null) {
            return emConversation.loadMoreMsgFromDB(null, 5);
        }
        return null;
    }

    /**
     * 获取以前的聊天记录
     */
    public List<EMMessage> addConversation(String account, String msgId, int pageSize) {
        EMConversation conversation = getConversation(account);
        if (conversation != null) {
            return conversation.loadMoreMsgFromDB(msgId, pageSize);
        }
        return null;
    }

    /**
     * 查询更多聊天记录，以最后一条消息查询
     *
     * @param account
     * @param pageSize
     * @return
     */
    public List<EMMessage> loadMoreMessages(String account, int pageSize) {
        // 查询单聊会话
        EMConversation emConversation = getConversation(account);
        if (null != emConversation) {
            // 查询最后一条消息id前pageSize条消息记录
            return emConversation.loadMoreMsgFromDB(emConversation.getLastMessage().getMsgId(), pageSize);
        }
        return null;
    }

    /**
     * @return 与某个用户全部聊天记录条数
     */
    public int getAllMsgCount(String account) {
        EMConversation emConversation = getConversation(account);
        if (null != emConversation) {
            return emConversation.getAllMsgCount();
        }
        return 0;
    }

    /**
     * 下载消息附件
     *
     * @param emMessage
     */
    public void downloadAttachment(EMMessage emMessage) {
        if (null != emMessage) {
            EMClient.getInstance().chatManager().downloadAttachment(emMessage);
        }
    }

    /**
     * @return 全部未读消息数
     */
    public int getUnreadMsgCount() {
        int unreadMessageCount = EMClient.getInstance().chatManager().getUnreadMessageCount();
        return unreadMessageCount;
    }

    /**
     * 获取某个人未读消息个数
     *
     * @param username
     * @return
     */
    public int getConverUnreadMsgCount(String username) {
        EMConversation conversation = EMClient.getInstance().chatManager().getConversation(username);
        int unreadMsgCount = 0;
        try {
            unreadMsgCount = conversation.getUnreadMsgCount();
        } catch (Exception e) {

        }
        return unreadMsgCount;
    }

    /**
     * @return 所有会话（聊天记录）
     */
    public static Map<String, EMConversation> getAllConversations() {
        return EMClient.getInstance().chatManager().getAllConversations();
    }
    /**
     * @return 从环信获取所有会话，并将离线会话添加到数据库中
     */
    public static void getAllMsgFromHuanXin() {
        Map<String, EMConversation> allConversations = getAllConversations();
        List<HuanXinUser> allAcount = DbModle.getInstance().getUserAccountDao().getAllAcount();
        if (allAcount != null && allConversations!=null&&allConversations.size()>allAcount.size()) {
            for (Map.Entry<String, EMConversation> entry : allConversations.entrySet()) {
                boolean isLiXianMsg=true;
                for (int i = 0; i < allAcount.size(); i++) {
                    String key = entry.getKey();
                    String hxId = allAcount.get(i).getAccount();
                    if(key.equals(hxId)){
                        isLiXianMsg=false;
                    }
                }
                if(isLiXianMsg){
//                    得到离线消息
                    List<EMMessage> messages = entry.getValue().getAllMessages();
                    // 接收到新消息
                    HuanXinUser user = null;
                    for (EMMessage emMessage : messages) {
                        String userName = "";
                        String userPic = "";
                        String userAccount = "";
                        String userGuid = "";
                        String msgContent = "";
                        int extendType;
                        int notice = 0;
                        msgContent = emMessage.getStringAttribute("msg", "");
                        Log.e("msgcontent", msgContent);
                        if (!TextUtils.isEmpty(msgContent)) {
                            NettyMessage nettyMessage = setMsgContent(msgContent);
                            String huanxinId="";
                            if (nettyMessage != null) {
                                Context context = BaseApplication.getGlobalContext();
                                userName = nettyMessage.getSendUserName();
                                userPic = nettyMessage.getSendUserIcon();
                                userAccount = nettyMessage.getSendUserAccount();
                                userGuid = String.valueOf(nettyMessage.getSendUserId());
                                extendType = nettyMessage.getExtendType();
                                notice = nettyMessage.getNotice();
                                SharedPreferenceUtil.setBooleanValue(context,"isSendMsg",userAccount,true);
                                String lastMsg = context.getString(R.string.how_are_you);
                                if (emMessage.getType() == EMMessage.Type.TXT) {
                                    EMTextMessageBody emTextMessageBody = (EMTextMessageBody) emMessage.getBody();
                                    if (nettyMessage.getExtendType() == 2 || emTextMessageBody.getMessage().equals("msgCall")) {
                                        lastMsg = context.getString(R.string.video_invitations);
                                    } else if (nettyMessage.getExtendType() == 3) {
                                        lastMsg = context.getString(R.string.one_private_message);
                                    } else if (nettyMessage.getExtendType() == 5) {
                                        lastMsg = context.getString(R.string.gift);
                                    }else if(nettyMessage.getExtendType() == 19){
                                        lastMsg = context.getString(R.string.voice_message);
                                    } else {
                                        if(emTextMessageBody.getMessage().contains(".mp3")){
                                            lastMsg = context.getString(R.string.voice_message);
                                        }else{
                                            lastMsg = emTextMessageBody.getMessage();
                                        }
                                    }
                                } else if (emMessage.getType() == EMMessage.Type.VOICE) {
                                    lastMsg = context.getString(R.string.voice_message);
                                } else if (emMessage.getType() == EMMessage.Type.IMAGE) {
                                    lastMsg = context.getString(R.string.photo_message);
                                }
                                user = new HuanXinUser(userGuid, userName, userPic, userAccount, "1", lastMsg, 0, String.valueOf(emMessage.getMsgTime()), extendType);
                                huanxinId = userGuid;
                                if (!TextUtils.isEmpty(huanxinId)) {
                                    EMConversation conversation = EMClient.getInstance().chatManager().getConversation(userAccount);
                                    if (conversation != null) {
                                        user.setMsgNum(conversation.getUnreadMsgCount());
                                    }
                                }
                                DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
                                if (!TextUtils.isEmpty(nettyMessage.getMsgId())) {
                                    msgReceived(nettyMessage.getMsgId());
                                }
                            }
                            //消息过来时刷新聊天列表
                            EventBus.getDefault().post(new MessageArrive(huanxinId));
                            EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));


                            if (emMessage.getType() == EMMessage.Type.IMAGE) {
//                            如果是图片，将图片缓存到本地
                                cacheImg(emMessage,userPic);

                            }
                        }
                    }


                }

            }
        }
    }


    /**
     * 判断是否在ChatActivity这个界面
     */
    public void isChatActivity() {
        isChatActivity = false;
    }

    /**
     * 离开ChatActivity这个界面时把标记改为true
     */
    public void isNotChatActivity() {
        isChatActivity = true;
    }
    /**
     * 判断是否在VideoInviteActivity2这个界面
     */
    public void isVideoInviteActivity2() {
        isVideoInviteActivity2=false;

    }
    /**
     * 离开VideoInviteActivity2这个界面时把标记改为true
     */
    public void isNotVideoInviteActivity2() {
        isVideoInviteActivity2 = true;
    }

    /**
     * 清空所有未读消息
     */
    public void clearAllUnReadMsg() {
        //所有未读消息数清零
        EMClient.getInstance().chatManager().markAllConversationsAsRead();
    }

    /**
     * 删除某个人的聊天记录
     */
    public void clearCoversation(String account) {
        //如果需要保留聊天记录，传false
        EMClient.getInstance().chatManager().deleteConversation(account, true);
    }

    /**
     * 指定会话消息未读数清零
     */
    public void markMsgAsRead(String account) {
        EMConversation conversation = EMClient.getInstance().chatManager().getConversation(account);
        if(conversation!=null)
           conversation.markAllMessagesAsRead();
    }
    /**
     *判断当前应用程序处于前台还是后台
     */
    public static boolean isApplicationBroughtToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }


    public static class getImageCacheAsyncTask extends AsyncTask<String, Void, File> {
        private final Context context;
        private String imgUrl;

        public getImageCacheAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected File doInBackground(String... params) {
             imgUrl =  params[0];
            try {
                return Glide.with(context)
                        .load(imgUrl)
                        .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                        .get();
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(File result) {
            if (result == null) {
                return;
            }
            //此path就是对应文件的缓存路径
            String path = result.getPath();
            Log.e("path", path);
            SharedPreferenceUtil.setStringValue(context,"cacheImg",imgUrl,path);

//            Bitmap bmp= BitmapFactory.decodeFile(path);
//            img.setImageBitmap(bmp);

        }
    }
    /**
     * 接通或者挂断震动一下
     *
     * @param context
     */
    public static void shake(Context context) {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(1000);
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(context, notification);
        r.play();
    }
    private void setTag(String errorInfo){
        ApiManager.userActivityTag(UserPreference.getId(), userId, "30",errorInfo, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }


}
