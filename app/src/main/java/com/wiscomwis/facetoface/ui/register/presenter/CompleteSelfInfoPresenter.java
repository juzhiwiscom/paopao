package com.wiscomwis.facetoface.ui.register.presenter;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.Toast;

import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.ui.main.MainActivity;
import com.wiscomwis.facetoface.ui.register.contract.CompleteSelfhoodInfoContract;

/**
 * Created by zhangdroid on 2017/5/31.
 */
public class CompleteSelfInfoPresenter implements CompleteSelfhoodInfoContract.IPresenter {
    private CompleteSelfhoodInfoContract.IView mCompleteInfo;
    private Activity mContext;

    public CompleteSelfInfoPresenter(CompleteSelfhoodInfoContract.IView mCompleteInfo) {
        this.mCompleteInfo = mCompleteInfo;
        mContext = (Activity) mCompleteInfo.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void commitFemaleData() {
        if(!TextUtils.isEmpty(mCompleteInfo.getFemale1())&&!TextUtils.isEmpty(mCompleteInfo.getFemale2())&&!TextUtils.isEmpty(mCompleteInfo.getFemale3())){
            String tag_1 = DataPreference.getPersonMapKey(mCompleteInfo.getFemale1(), 5);
            String tag_2 = DataPreference.getPersonMapKey(mCompleteInfo.getFemale2(), 5);
            String tag_3 = DataPreference.getPersonMapKey(mCompleteInfo.getFemale3(), 5);
            if(!TextUtils.isEmpty(tag_1)){
                setMaleTag(tag_1+","+tag_2+","+tag_3);//埋点
                uploadTag(tag_1+","+tag_2+","+tag_3);//上传
            }
            Intent intent = new Intent(mContext, MainActivity.class);
            mContext.startActivity(intent);
            mContext.finish();
//            LaunchHelper.getInstance().launchFinish(mContext, MainActivity.class);
        }else {
            Toast.makeText(mContext, "请选择3个标签", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void commitMaleData(String tag1,String tag2,String tag3) {
        if(!TextUtils.isEmpty(tag1)&&!TextUtils.isEmpty(tag2)&&!TextUtils.isEmpty(tag3)){
            String tag_1 = DataPreference.getPersonMapKey(tag1, 4);
            String tag_2 = DataPreference.getPersonMapKey(tag2, 4);
            String tag_3 = DataPreference.getPersonMapKey(tag3, 4);
            if(!TextUtils.isEmpty(tag_1)){
                setMaleTag(tag_1+","+tag_2+","+tag_3);//埋点
                uploadTag(tag_1+","+tag_2+","+tag_3);//上传
            }
        }else if(!TextUtils.isEmpty(tag1)&&TextUtils.isEmpty(tag2)&&TextUtils.isEmpty(tag3)){
            String tag_1 = DataPreference.getPersonMapKey(tag1, 4);
            if(!TextUtils.isEmpty(tag_1)){
                setMaleTag(tag_1);//埋点
                uploadTag(tag_1);//上传
            }
        } else if (!TextUtils.isEmpty(tag1)&&!TextUtils.isEmpty(tag2)&&TextUtils.isEmpty(tag3)) {
            String tag_1 = DataPreference.getPersonMapKey(tag1, 4);
            String tag_2 = DataPreference.getPersonMapKey(tag2, 4);
            if(!TextUtils.isEmpty(tag_1)){
                setMaleTag(tag_1+","+tag_2);//埋点
                uploadTag(tag_1+","+tag_2);//上传
            }
        }else{
            Toast.makeText(mContext, "请选择1~3个标签", Toast.LENGTH_SHORT).show();
        }

    }

    private void setMaleTag(String tagPos){
        ApiManager.userActivityTag(UserPreference.getId(), "remoteId", "17",tagPos, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }
    private void uploadTag(String tag){
        ApiManager.uploadUserTag(tag, new IGetDataListener<String>() {
            @Override
            public void onResult(String baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }
}
