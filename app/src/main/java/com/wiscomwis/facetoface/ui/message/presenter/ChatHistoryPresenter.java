package com.wiscomwis.facetoface.ui.message.presenter;

import android.content.Context;
import android.util.Log;

import com.wiscomwis.facetoface.data.model.HuanXinUser;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.ui.message.adapter.NewRecyclerAdapter;
import com.wiscomwis.facetoface.ui.message.contract.ChatHistoryContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangdroid on 2017/7/6.
 */
public class ChatHistoryPresenter implements ChatHistoryContract.IPresenter {
    private ChatHistoryContract.IView mChatHistoryView;
    private Context mContext;
    private NewRecyclerAdapter mAdapter;
    List<HuanXinUser> allAcount = null;


    public ChatHistoryPresenter(ChatHistoryContract.IView view) {
        this.mChatHistoryView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
        mAdapter = new NewRecyclerAdapter(mContext, UserPreference.isAnchor());
        mChatHistoryView.setAdapter(mAdapter);
    }

    @Override
    public void loadConversationList() {
        mAdapter.reSet();
        allAcount = DbModle.getInstance().getUserAccountDao().getAllAcount();
        if (allAcount != null && allAcount.size() > 0) {
            if (!UserPreference.isAnchor()) {
                List<HuanXinUser> priList = new ArrayList<>();
                List<HuanXinUser> normalList = new ArrayList<>();
                for (HuanXinUser user : allAcount) {
                    if (!user.getHxId().equals("10000")) {
                        if (user.getExtendType() == 3 || user.getExtendType() == 2 || user.getExtendType() == 4) {
                            priList.add(user);
                        } else {
                            normalList.add(user);
                        }
                    } else {
                        normalList.add(user);
                    }
                }
                Log.e("size", "prisize" + priList.size() + "normalsize" + normalList.size());
                mAdapter.setNormalList(normalList);
                mAdapter.setPrivateList(priList);
            } else {
                List<HuanXinUser> normalList = new ArrayList<>();
                List<HuanXinUser> helloList = new ArrayList<>();
                for (HuanXinUser user : allAcount) {
                    if (!user.getHxId().equals("10000")) {
                        if (user.getExtendType() == 8) {
                            helloList.add(user);
                        } else {
                            normalList.add(user);
                        }
                    }
                }
                Log.e("size", "helloListsize" + helloList.size() + "normalsize" + normalList.size());
                mAdapter.setHellolList(helloList);
                mAdapter.setNormalList(normalList);
            }
        }
    }


    @Override
    public void refresh() {
        mChatHistoryView.toggleShowEmpty(false, null);
        initloadData();
    }

    @Override
    public void loadMore() {
        mChatHistoryView.hideRefresh(1);
    }

    @Override
    public void initloadData() {
        if (allAcount != null && allAcount.size() > 0) {
            allAcount.clear();
            allAcount = null;
        }
        loadConversationList();
    }

    @Override
    public List<HuanXinUser> getAllAcount() {
        return allAcount;
    }
}
