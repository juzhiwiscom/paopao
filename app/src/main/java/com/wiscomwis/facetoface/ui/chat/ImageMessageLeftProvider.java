package com.wiscomwis.facetoface.ui.chat;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hyphenate.chat.EMImageMessageBody;
import com.hyphenate.chat.EMMessage;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.SeeDetailEvent;
import com.wiscomwis.facetoface.parcelable.BigPhotoParcelable;
import com.wiscomwis.facetoface.ui.photo.BigPhotoActivity;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.adapter.provider.ItemViewProvider;
import com.wiscomwis.library.image.CropCircleTransformation;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.DateTimeUtil;
import com.wiscomwis.library.util.DeviceUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.SharedPreferenceUtil;
import com.wiscomwis.library.widget.ChatImageView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * 聊天图片消息（接收方）
 * Created by zhangdroid on 2017/6/29.
 */
public class ImageMessageLeftProvider implements ItemViewProvider<EMMessage> {
    private Context mContext;
    private String mAvatarUrl;
    private List<EMMessage> messageList;

    public ImageMessageLeftProvider(Context context, String url, List<EMMessage> messageList) {
        this.mContext = context;
        this.mAvatarUrl = url;
        this.messageList = messageList;
    }

    @Override
    public int getItemViewLayoutResId() {
        return R.layout.item_chat_image_left;
    }

    @Override
    public boolean isViewType(EMMessage item, int position) {
        return (item.getType() == EMMessage.Type.IMAGE && item.direct() == EMMessage.Direct.RECEIVE);
    }

    @Override
    public void convert(EMMessage emMessage, int position, RecyclerViewHolder holder) {
        // 头像
        ImageView ivAvatar = (ImageView) holder.getView(R.id.item_chat_image_avatar_left);
        String cacheIg = UserPreference.getCacheIg(mAvatarUrl);
        if(!TextUtils.isEmpty(cacheIg)){
            Bitmap bitmap = BitmapFactory.decodeFile(cacheIg);
            Bitmap bitmap1 = Util.setCircle(bitmap);
            if(bitmap1!=null)
              ivAvatar.setImageBitmap(bitmap1);
        }else {
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().transform(new CropCircleTransformation(mContext)).placeHolder(Util.getDefaultImageCircle())
                    .error(Util.getDefaultImageCircle()).url(mAvatarUrl).imageView(ivAvatar).build());
        }
        ivAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 发送事件，查看用户详情
                EventBus.getDefault().post(new SeeDetailEvent());
            }
        });
        if (null != emMessage) {

            if (position > 0) {
                long msgTime = messageList.get(position - 1).getMsgTime();
                if ((emMessage.getMsgTime()-msgTime) / (1000 * 60) > 5) {
                    holder.getView(R.id.item_chat_image_time_left).setVisibility(View.VISIBLE);
                    holder.setText(R.id.item_chat_image_time_left, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
                } else {
                    holder.getView(R.id.item_chat_image_time_left).setVisibility(View.GONE);
                }
            } else {
                holder.getView(R.id.item_chat_image_time_left).setVisibility(View.VISIBLE);
                holder.setText(R.id.item_chat_image_time_left, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
            }


            final EMImageMessageBody emImageMessageBody = (EMImageMessageBody) emMessage.getBody();
            if (null != emImageMessageBody) {
                ChatImageView chatImageView = (ChatImageView) holder.getView(R.id.item_chat_image_left);
                // 设置图片规格：1/3屏幕宽度，宽高比3：4
                int width = DeviceUtil.getScreenWidth(mContext) / 3;
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, (int) ((4 / 3.0f) * width));
                chatImageView.setLayoutParams(layoutParams);

                String stringValue1 = SharedPreferenceUtil.getStringValue(mContext, "cacheImg", emImageMessageBody.getRemoteUrl(), "url");
                if(!stringValue1.equals("url")){
                                Bitmap bmp= BitmapFactory.decodeFile(stringValue1);
                    chatImageView.setImageBitmap(bmp);
//                    Glide.with(mContext).load(new File(stringValue)).error(Util.getDefaultImage())
//                            .diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(Util.getDefaultImage()).into(chatImageView);
                }else {
                    Log.e("AAAAAAA", "convert: -----------------------------" + emImageMessageBody.getThumbnailUrl());
                    ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage())
                            .url(emImageMessageBody.getThumbnailUrl()).imageView(chatImageView).build());
                }
               chatImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 点击查看大图
                        List<String> list = new ArrayList<String>();
                        list.add(emImageMessageBody.getRemoteUrl());
                        LaunchHelper.getInstance().launch(mContext, BigPhotoActivity.class, new BigPhotoParcelable(0, list));
                    }
                });
            }
        }
    }

}
