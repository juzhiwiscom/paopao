package com.wiscomwis.facetoface.ui.photo;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragmentActivity;
import com.wiscomwis.facetoface.beauty.beauty.filter.GPUImageFilter;
import com.wiscomwis.facetoface.beauty.beauty.filter.MagicCameraDisplay;
import com.wiscomwis.facetoface.beauty.beauty.utils.MagicFilterFactory;
import com.wiscomwis.facetoface.beauty.beauty.utils.MagicFilterType;
import com.wiscomwis.facetoface.beauty.camera.CameraInterface;
import com.wiscomwis.facetoface.beauty.scanner.MediaScanner;
import com.wiscomwis.facetoface.beauty.scanner.ScannerListener;
import com.wiscomwis.facetoface.beauty.utils.DisplayUtil;
import com.wiscomwis.facetoface.common.BitmapUtils;
import com.wiscomwis.library.net.NetUtil;

import java.io.File;

import butterknife.BindView;

/**
 * Created by xuzhaole on 2018/3/1.
 */

public class ShortRecordActivity extends BaseFragmentActivity implements View.OnClickListener {

    @BindView(R.id.surfaceview)
    GLSurfaceView surfaceview;
    @BindView(R.id.iv_finish)
    ImageView iv_finish;
    @BindView(R.id.iv_swicth_camera)
    ImageView iv_swicth_camera;
    @BindView(R.id.tv_record_time)
    TextView tv_record_time;
    @BindView(R.id.iv_record_complete)
    ImageView iv_record_complete;
    @BindView(R.id.iv_record_start)
    ImageView iv_record_start;

    private MagicCameraDisplay mMagicCameraDisplay;

    OrientationEventListener orientationEventListener;

    int rotationRecord = 90;

    int rotationFlag = 90;

    boolean mIsRecording = false;//是否正在录像

    private int time = 0;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    handler.removeCallbacks(runnable);
                    endRecord();
                    break;
                default:
                    time++;
                    tv_record_time.setText(getTime(time));
                    break;
            }
            super.handleMessage(msg);
        }
    };

    private String getTime(int time) {
        String getTime = "00:00";
        if (time < 10) {
            getTime = "00:0" + time;
        } else if (time < 60) {
            getTime = "00:" + time;
        }
        return getTime;
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (time >= 10) {
                Message message = new Message();
                message.what = 1;
                handler.sendMessage(message);
            } else {
                Message message = new Message();
                message.what = 0;
                handler.sendMessage(message);
            }
            handler.postDelayed(runnable, 1000);
        }
    };


    private void endRecord() {
        mIsRecording = false;
        mMagicCameraDisplay.stopRecording();
        final String mediaOutPath = mMagicCameraDisplay.getMediaOutPath();
        MediaScannerConnection.scanFile(this, new String[]{mediaOutPath}, null, new MediaScannerConnection.OnScanCompletedListener() {
            @Override
            public void onScanCompleted(String s, Uri uri) {
                Log.e("ssss", s);
                getBitmapFinish(s);
            }
        });
//        new MediaScannerConnection(this, new MediaScannerConnection.MediaScannerConnectionClient() {
//            @Override
//            public void onMediaScannerConnected() {
//
//            }
//
//            @Override
//            public void onScanCompleted(String s, Uri uri) {
//
//            }
//        }).scanFile(mediaOutPath, MediaScannerConnection.);
//        MediaScanner mediaScanner = new MediaScanner(this, new ScannerListener() {
//            @Override
//            public void oneComplete(final String path, Uri uri) {
//            }
//
//            @Override
//            public void allComplete(String[] filePaths) {
//                Log.e("allComplete", "allComplete");
//                Log.e("filepaths", filePaths[0]);
//                getBitmapFinish(filePaths[0]);
//
//            }
//        });
////        String mediaOutPath = mMagicCameraDisplay.getMediaOutPath();
//        mediaScanner.scan(mediaOutPath);


    }

    private void getBitmapFinish(final String mediaOutPath) {
//        Bitmap bitmap = null;
//        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        File file = new File(mediaOutPath);

        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(mediaOutPath, MediaStore.Video.Thumbnails.MINI_KIND);
//        mmr.setDataSource(file.getAbsolutePath());
//            bitmap = mmr.getFrameAtTime();
//
//        mmr.release();

        Log.e("bitmap", bitmap == null ? "null" : bitmap.getHeight() + "");

        final Bitmap finalBitmap = bitmap;
        new AsyncTask<Void, Void, File>() {
            @Override
            protected File doInBackground(Void... voids) {
                return BitmapUtils.compressImage(ShortRecordActivity.this, finalBitmap);
            }

            @Override
            protected void onPostExecute(File file) {
                Intent intent = new Intent(ShortRecordActivity.this, GetVideoActivity.class);

                if (file == null) {
                    //将结果返回上个界面
                    intent.putExtra("bitmapFilePath", "");
                } else {
                    intent.putExtra("bitmapFilePath", file.getAbsolutePath());
                }
                //将结果返回上个界面
                intent.putExtra("duration", time + "");
                intent.putExtra("videoFilePath", mediaOutPath);
                setResult(RESULT_OK, intent);
                finish();
            }
        }.execute();
    }

    /**
     * 开始录制时候的状态
     */
    private void startRecordUI() {
        iv_swicth_camera.setVisibility(View.GONE); // 旋转摄像头关闭
        iv_finish.setVisibility(View.GONE);
        iv_record_complete.setVisibility(View.VISIBLE);
        iv_record_start.setVisibility(View.GONE);
    }

    /**
     * 旋转界面UI
     */
    private void rotationUIListener() {
        orientationEventListener = new OrientationEventListener(this) {
            @Override
            public void onOrientationChanged(int rotation) {
                if (!mIsRecording) {
                    if (((rotation >= 0) && (rotation <= 30)) || (rotation >= 330)) {
                        // 竖屏拍摄
                        if (rotationFlag != 0) {
                            //旋转logo
                            rotationAnimation(rotationFlag, 0);
                            //这是竖屏视频需要的角度
                            rotationRecord = 90;
                            //这是记录当前角度的flag
                            rotationFlag = 0;
                        }
                    } else if (((rotation >= 230) && (rotation <= 310))) {
                        // 横屏拍摄
                        if (rotationFlag != 90) {
                            //旋转logo
                            rotationAnimation(rotationFlag, 90);
                            //这是正横屏视频需要的角度
                            rotationRecord = 0;
                            //这是记录当前角度的flag
                            rotationFlag = 90;
                        }
                    } else if (rotation > 30 && rotation < 95) {
                        // 反横屏拍摄
                        if (rotationFlag != 270) {
                            //旋转logo
                            rotationAnimation(rotationFlag, 270);
                            //这是反横屏视频需要的角度
                            rotationRecord = 180;
                            //这是记录当前角度的flag
                            rotationFlag = 270;
                        }
                    }
                }
            }
        };
        orientationEventListener.enable();
    }

    private void rotationAnimation(int from, int to) {
        ValueAnimator progressAnimator = ValueAnimator.ofInt(from, to);
        progressAnimator.setDuration(300);
        progressAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int currentAngle = (int) animation.getAnimatedValue();
                // videoTime.setRotation(currentAngle);
                iv_swicth_camera.setRotation(currentAngle);
            }
        });
        progressAnimator.start();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_swicth_camera:
                mMagicCameraDisplay.switchCamera();
                break;
            case R.id.iv_record_start://开始录制视频
                startRecordUI();
                mIsRecording = true;
                mMagicCameraDisplay.startRecording();
                handler.postDelayed(runnable, 1000);
                break;
            case R.id.iv_record_complete://录制完成，返回
                handler.sendEmptyMessage(1);
                break;
            case R.id.iv_finish:
                finish();
                break;
        }
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_short_record;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        mMagicCameraDisplay = new MagicCameraDisplay(this, surfaceview);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                openBeauty();
            }
        }, 1000);

        float previewRate = DisplayUtil.getScreenRate(this); // 默认全屏的比例预览
        CameraInterface.getInstance().setPreviewRate(previewRate);
        initView();
    }


    protected void openBeauty() {
        GPUImageFilter mFilter = MagicFilterFactory.getFilters(
                MagicFilterType.BEAUTY, this);
        mMagicCameraDisplay.setFilter(mFilter);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mIsRecording) {
            mMagicCameraDisplay.startRecording();
        }
        if (mMagicCameraDisplay != null) {
            mMagicCameraDisplay.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mIsRecording) {
            mMagicCameraDisplay.stopRecording();
            mIsRecording = false;
        }
    }

    @Override
    protected void onDestroy() {
        onCloseCamera();
        mIsRecording = false;
        super.onDestroy();
    }

    /**
     * 关闭摄像头，同时释放美颜
     */
    private void onCloseCamera() {
        CameraInterface.getInstance().onPause();
        if (mMagicCameraDisplay != null)
            mMagicCameraDisplay.onDestroy();
    }

    private void initView() {
        rotationUIListener();
    }


    @Override
    protected void setListeners() {
        iv_swicth_camera.setOnClickListener(this);
        iv_finish.setOnClickListener(this);
        iv_record_start.setOnClickListener(this);
        iv_record_complete.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onBackPressed() {
        if (!mIsRecording) {
            super.onBackPressed();
        }
    }
}
