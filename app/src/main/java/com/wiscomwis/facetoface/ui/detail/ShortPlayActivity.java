package com.wiscomwis.facetoface.ui.detail;

import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragmentActivity;
import com.wiscomwis.facetoface.parcelable.ShortPlayParcelable;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.playerlibrary.EmptyControlVideo;
import com.wiscomwis.playerlibrary.gsyvideoplayer.GSYVideoManager;

import butterknife.BindView;

/**
 * 短视频播放
 */

public class ShortPlayActivity extends BaseFragmentActivity {
    @BindView(R.id.player_empty)
    EmptyControlVideo player_empty;
    private ShortPlayParcelable mShortPlayParcelable;
    private String stringExtra;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_short_play;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mShortPlayParcelable = (ShortPlayParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        stringExtra = mShortPlayParcelable.urlData;
        if (!TextUtils.isEmpty(stringExtra)){
            player_empty.setUp(stringExtra, true, "123");
            player_empty.setLooping(true);
            player_empty.startPlayLogic();
        }
    }


    @Override
    protected void setListeners() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        GSYVideoManager.instance().onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player_empty.release();
    }

    @Override
    protected void onPause() {
        super.onPause();
        GSYVideoManager.instance().onPause();

    }
}
