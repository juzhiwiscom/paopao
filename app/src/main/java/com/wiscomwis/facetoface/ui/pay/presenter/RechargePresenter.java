package com.wiscomwis.facetoface.ui.pay.presenter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.HostInfo;
import com.wiscomwis.facetoface.data.model.MyInfo;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.data.model.UserDetail;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.ui.homepage.HomepageAdapter;
import com.wiscomwis.facetoface.ui.pay.contract.RechargeContract;
import com.wiscomwis.facetoface.ui.pay.fragment.DredgeVipFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public class RechargePresenter implements RechargeContract.IPresenter {
    private static final String TAG = RechargePresenter.class.getSimpleName();
    private RechargeContract.IView mPayView;
    private Context mContext;

    public RechargePresenter(RechargeContract.IView view) {
        this.mPayView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }


    @Override
    public void getPayWay(String fromTag) {

    }

    @Override
    public void addTabs() {
        HomepageAdapter homepageAdapter = new HomepageAdapter(mPayView.getManager());
        List<Fragment> fragmentList = new ArrayList<>();
        List<String>  tabList = Arrays.asList(mContext.getString(R.string.open_vip),mContext.getString(R.string.buy_keys));
//        fragmentList.add(new DredgeVipFragment(mPayView.getPaySource()));
        DredgeVipFragment dredgeVipFragment = new DredgeVipFragment();
        fragmentList.add(dredgeVipFragment);
        homepageAdapter.setData(fragmentList, tabList);
        mPayView.setAdapter(homepageAdapter);
        dredgeVipFragment.setPaySource(mPayView.getPaySource());
    }

    @Override
    public void getMyInfo() {
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if(myInfo!=null){
                    UserDetail userDetail = myInfo.getUserDetail();
                  if(userDetail!=null){
                      HostInfo hostInfo =
                              userDetail.getHostInfo();
                      if(hostInfo!=null){
                          mPayView.getNowMoney(String.valueOf(hostInfo.getBalance()));
                      }
                      UserBase userBase = userDetail.getUserBase();
                      if (userBase!=null) {
                          UserPreference.saveUserInfo(userBase);
                      }
                      String vipDays = userDetail.getVipDays();
                      if(!TextUtils.isEmpty(vipDays)&&vipDays.length()>0){
                          int i = Integer.parseInt(vipDays);
                          if (i>0||i==-1) {
                              mPayView.isVipUser();
                          }
                      }
                  }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

}
