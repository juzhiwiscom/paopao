package com.wiscomwis.facetoface.ui.personalcenter.contract;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;
import com.wiscomwis.facetoface.ui.detail.adapter.AlbumPhotoAdapter;

import java.io.File;

/**
 * Created by zhangdroid on 2017/5/27.
 */
public interface PersonContract {

    interface IView extends BaseView {

        /**
         * 设置用户头像
         *
         * @param url
         */
        void setUserAvatar(String url,String status);

        /**
         * 设置用户昵称
         *
         * @param name
         */
        void setUserName(String name);

        /**
         * 设置用户ID
         *
         * @param id
         */
        void setUserId(String id);

        /**
         * 设置用户接听率/投诉率
         *
         * @param receivePrecent  接听率
         * @param complainPrecent 投诉率
         */
        void setUserPrecent(String receivePrecent, String complainPrecent);

        /**
         * 设置普通用户账户余额
         *
         * @param balance 余额
         */
        void setBalance(float balance);

        /**
         * 设置是否为主播
         */
        void setIsAnchor(boolean isAnchor);

        /**
         * 设置累计收入
         *
         * @param income 收入
         */
        void setAccumulatedIncome(String income);

        /**
         * 设置主播当前账户金币数
         *
         * @param
         */
        void setAnchorCurrentIncome(String currentIncome);

        /**
         * 设置主播价格
         *
         * @param price 价格
         */
        void setPrice(String price,int audioPrice);
        /**
         * vip天数
         */
        void vipDays(int days);
        /**
         * 设置adapter
         */
        void setAdapter(AlbumPhotoAdapter adapter);
        /**
         * 让占位符消失
         */
        void picGone();
        /**
         * 支付总开关
         */
        void switchAllPay();
        /**
         * 我的钱包开关
         */
        void switchWallet();
        /**
         * 我的钻石
         * @param b
         */
        void switchDionmads(boolean b);
        /**
         * vip开关
         * @param isVipGone
         */
        void switchVip(boolean isVipGone);
        /**
         * 获取审核的状态
         */
        void getCheckStatus(int pos,int showStatus);

        void toggleNoDistrub(boolean toggle);

        /**
         * 显示加载
         */
        void showLoading();
        /**
         * 隐藏加载
         */
        void hideLoading();
    }

    interface IPresenter extends BasePresenter {

        /**
         * 获取当前用户信息并设置
         */
        void getUserInfo();

        /**
         * 更新普通用户余额
         */
        void updateBalance();

        /**
         * 更新播主价格
         */
        void updatePrice(String newPrice);
        /**
         * 访问网络更新信息
         *
         */
        void getUploadInfo();
        /**
         *获取审核状态
         */
        void getCheckStatus();
        /**
         * 上传照片
         */
        void upLoadAvator(File file, boolean type);

        /**
         * 勿扰模式
         * @param toggle
         */
        void toggleNoDistrub(boolean toggle);

        void getNoDistrubState();
    }

}
