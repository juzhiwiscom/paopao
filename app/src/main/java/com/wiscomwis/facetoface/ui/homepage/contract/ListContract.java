package com.wiscomwis.facetoface.ui.homepage.contract;

import android.support.v7.widget.RecyclerView;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/6/2.
 */
public interface ListContract {

    interface IView extends BaseView {

        /**
         * 隐藏下拉刷新
         */
        void hideRefresh(int delaySeconds);

        void toggleShowError(boolean toggle, String msg);

        void toggleShowEmpty(boolean toggle, String msg);


        /**
         * 设置adapter
         *
         * @param adapter adapter
         */
        void setAdapter(RecyclerView.Adapter adapter);
    }

    interface IPresenter extends BasePresenter {

        /**
         * 加载推荐用户列表
         *
         * @param type {@link com.wiscomwis.facetoface.C.homepage}
         */
        void loadRecommendUserList(int type);

        /**
         * 下拉刷新
         */
        void refresh();

        /**
         * 修改打招呼成功后的状态
         */
        void sayHelloStatus();

        void start(RecyclerView recyclerView);
    }

}
