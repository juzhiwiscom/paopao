package com.wiscomwis.facetoface.ui.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.xujiaji.happybubble.BubbleDialog;

/**
 * Created by tianzhentao on 2018/7/13.
 */

public class CustomOperateDialog extends BubbleDialog implements View.OnClickListener {
    private ViewHolder mViewHolder;
    private OnClickCustomButtonListener mListener;

    public CustomOperateDialog(Context context) {
        super(context);
        calBar(true);
        setTransParentBackground();
        setPosition(Position.TOP);
        View rootView = LayoutInflater.from(context).inflate(R.layout.dialog_msg_withdraw, null);
        mViewHolder = new ViewHolder(rootView);
        addContentView(rootView);
        mViewHolder.withdraw.setOnClickListener(this);
        mViewHolder.delete.setOnClickListener(this);
        mViewHolder.cancel.setOnClickListener(this);
        mViewHolder.copy.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (mListener != null)
            mListener.onClick(((TextView)v).getText().toString());
    }

    private static class ViewHolder {
        TextView withdraw, delete, cancel,copy;
        public ViewHolder(View rootView) {
            withdraw = (TextView) rootView.findViewById(R.id.btn_withdraw);
            delete = (TextView) rootView.findViewById(R.id.btn_delete);
            cancel = (TextView) rootView.findViewById(R.id.btn_cancel);
            copy = (TextView) rootView.findViewById(R.id.btn_copy);
        }
    }

    public void setClickListener(OnClickCustomButtonListener l) {
        this.mListener = l;
    }

    public interface OnClickCustomButtonListener {
        void onClick(String str);
    }
}
