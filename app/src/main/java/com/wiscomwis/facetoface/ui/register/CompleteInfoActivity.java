package com.wiscomwis.facetoface.ui.register;

import android.content.Context;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseTopBarActivity;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.ui.dialog.AgeSelectDialog;
import com.wiscomwis.facetoface.ui.photo.GetPhotoActivity;
import com.wiscomwis.facetoface.ui.register.contract.CompleteInfoContract;
import com.wiscomwis.facetoface.ui.register.presenter.CompleteInfoPresenter;
import com.wiscomwis.library.dialog.LoadingDialog;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.ToastUtil;

import java.io.File;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/10/24.
 */

public class CompleteInfoActivity extends BaseTopBarActivity implements View.OnClickListener, CompleteInfoContract.IView {
    @BindView(R.id.complete_info_iv_avatar)
    ImageView iv_avatar;
    @BindView(R.id.ll_root)
    LinearLayout ll_root;
    @BindView(R.id.complete_info_tv_age)
    TextView tv_age;
    @BindView(R.id.complete_info_btn_finish)
    Button btn_finish;
    @BindView(R.id.complete_info_et_nickname)
    EditText et_nickname;
    @BindView(R.id.complete_info_ll_age)
    LinearLayout ll_age;
    private CompleteInfoPresenter completeInfoPresenter;
    private String imageUrl;
    private boolean isFirstClick=true;
    private File mFile;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_complete_info;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return null;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        completeInfoPresenter = new CompleteInfoPresenter(this);
    }

    @Override
    protected void setListeners() {
        btn_finish.setOnClickListener(this);
        iv_avatar.setOnClickListener(this);
        ll_age.setOnClickListener(this);
        ll_root.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
        completeInfoPresenter.loadInfoData();
        if (UserPreference.isMale()) {
            et_nickname.setHint(UserPreference.getNickname());
        } else {
            et_nickname.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    checkStatus();
                }
            });
        }
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.complete_info_ll_age:
                if (!TextUtils.isEmpty(tv_age.getText().toString())) {
                    AgeSelectDialog.show(getSupportFragmentManager(), tv_age.getText().toString(), new AgeSelectDialog.OnAgeSelectListener() {
                        @Override
                        public void onSelected(String age) {
                            tv_age.setText(age);
//                            btn_finish.setBackgroundColor(Color.parseColor("#fd698b"));
                            if (UserPreference.isMale()) {
                                btn_finish.setBackgroundResource(R.drawable.shape_vip_pay_bg);
                                canOnClick();
                            } else {
                                checkStatus();
                            }

                        }
                    });
                } else {
                    AgeSelectDialog.show(getSupportFragmentManager(), "22", new AgeSelectDialog.OnAgeSelectListener() {
                        @Override
                        public void onSelected(String age) {
                            tv_age.setText(age);
                            if (UserPreference.isMale()) {
                                btn_finish.setBackgroundResource(R.drawable.shape_vip_pay_bg);
                                canOnClick();
                            } else {
                                checkStatus();
                            }
                        }
                    });
                }
                break;
            case R.id.complete_info_iv_avatar:
                GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(File file) {
                        mFile=file;
                        completeInfoPresenter.upLoadAvator(file, true);
                    }
                });
                break;
            case R.id.complete_info_btn_finish:
                if (TextUtils.isEmpty(tv_age.getText().toString())) {
                    ToastUtil.showShortToast(mContext, "请选择你的年龄");
                }else {
                    if(!UserPreference.isMale()){
                        if(TextUtils.isEmpty(getNickname())){
                            ToastUtil.showShortToast(mContext, "请填写你的昵称");
                        }else if(mFile==null){
                                ToastUtil.showShortToast(mContext, "请上传你的头像");
                            }else{
                                completeInfoPresenter.completeInfo();
                            }
                    }else {
                        completeInfoPresenter.completeInfo();
                    }
                }
                break;
            case R.id.ll_root:
                InputMethodManager imm = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                break;
        }
    }
    //埋点
    private static void setMsgFlag(){
        ApiManager.userActivityTag(UserPreference.getId(), "", "21", "1", new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }
    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public String getAge() {
        return tv_age.getText().toString();
    }

    @Override
    public String getNickname() {
        return et_nickname.getText().toString().trim();
    }

    @Override
    public void setUserAvator(String url) {
        iv_avatar.setImageResource(R.drawable.check_circle_icon);
        this.imageUrl = url;
        if (!UserPreference.isMale()) {
            checkStatus();
        }
    }

    @Override
    public void canOnClick() {
        btn_finish.setEnabled(true);
    }

    private void checkStatus() {

//            if(TextUtils.isEmpty(getNickname())){
//                ToastUtil.showShortToast(mContext, "请填写你的昵称");
//                btn_finish.setEnabled(false);
//            }else if(mFile==null){
//                ToastUtil.showShortToast(mContext, "请上传你的头像");
//                btn_finish.setEnabled(false);
//            }else{
//                canOnClick();
//                btn_finish.setBackgroundResource(R.drawable.shape_vip_pay_bg);
//                completeInfoPresenter.completeInfo();
//            }
        if (!TextUtils.isEmpty(et_nickname.getText().toString())
                && !TextUtils.isEmpty(tv_age.getText().toString())
                && !TextUtils.isEmpty(imageUrl)) {
            canOnClick();
            btn_finish.setBackgroundResource(R.drawable.shape_vip_pay_bg);
        } else {
            btn_finish.setEnabled(true);
        }
    }
}
