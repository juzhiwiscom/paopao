package com.wiscomwis.facetoface.ui.homepage.presenter;

import android.content.Context;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;

import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.MyInfo;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.data.model.UserDetail;
import com.wiscomwis.facetoface.data.preference.SwitchPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.parcelable.ListParcelable;
import com.wiscomwis.facetoface.parcelable.PayParcelable;
import com.wiscomwis.facetoface.ui.follow.FindUserActivity;
import com.wiscomwis.facetoface.ui.homepage.HomepageAdapter;
import com.wiscomwis.facetoface.ui.homepage.ListFragment;
import com.wiscomwis.facetoface.ui.homepage.ListFragment2;
import com.wiscomwis.facetoface.ui.homepage.contract.HomepageContract;
import com.wiscomwis.facetoface.ui.pay.activity.RechargeActivity;
import com.wiscomwis.library.dialog.AlertDialog;
import com.wiscomwis.library.dialog.OnDialogClickListener;
import com.wiscomwis.library.util.LaunchHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by zhangdroid on 2017/5/31.
 */
public class HomepagePresenter implements HomepageContract.IPresenter {
    private HomepageContract.IView mHomepageView;
    private Context mContext;
    private boolean isFirstClick=true;

    public HomepagePresenter(HomepageContract.IView view) {
        this.mHomepageView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void addTabs() {
        HomepageAdapter homepageAdapter = new HomepageAdapter(mHomepageView.getManager());
        List<Fragment> fragmentList = new ArrayList<>();
        List<String> tabList;
        if (UserPreference.isAnchor()) {// 播主
            tabList = Arrays.asList(mContext.getString(R.string.homepage_goddess),
                    mContext.getString(R.string.homepage_active_friend));
            fragmentList.add(ListFragment.newInstance(new ListParcelable(C.homepage.TYPE_GODDESS)));
            fragmentList.add(ListFragment2.newInstance(new ListParcelable(C.homepage.TYPE_ACTIVE_FRIEND)));
//            fragmentList.add(ListFragment3.newInstance(new ListParcelable(C.homepage.TYPE_NEW_FRIEND)));
        } else {// 普通用户
            tabList = Arrays.asList(mContext.getString(R.string.homepage_goddess),
                    mContext.getString(R.string.homepage_active_anchor));
            fragmentList.add(ListFragment.newInstance(new ListParcelable(C.homepage.TYPE_GODDESS)));
            fragmentList.add(ListFragment2.newInstance(new ListParcelable(C.homepage.TYPE_ACTIVE_ANCHOR)));
//            fragmentList.add(ListFragment3.newInstance(new ListParcelable(C.homepage.TYPE_NEW)));
        }
        homepageAdapter.setData(fragmentList, tabList);
        mHomepageView.setAdapter(homepageAdapter);
    }

    @Override
    public void isVipUser() {
        if(isFirstClick){
            isFirstClick=false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isFirstClick=true;
                }
            },2000);
            if (SwitchPreference.getVipPay()==1) {
                if(UserPreference.isAnchor()){
                    LaunchHelper.getInstance().launch(mContext, FindUserActivity.class);
                }else{
                    ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
                        @Override
                        public void onResult(MyInfo myInfo, boolean isEmpty) {
                            if(myInfo!=null) {
                                UserDetail userDetail = myInfo.getUserDetail();
                                if(userDetail!=null){
                                    String vipDays = userDetail.getVipDays();
                                    if(!TextUtils.isEmpty(vipDays)&&vipDays.length()>0){
                                        int i = Integer.parseInt(vipDays);
                                        if(i==0){
                                            AlertDialog.showNoCanceled(mHomepageView.getManager(), "", mContext.getString(R.string.not_vip_not_find),
                                                    mContext.getString(R.string.to_open), mContext.getString(R.string.cancel), new OnDialogClickListener() {
                                                        @Override
                                                        public void onNegativeClick(View view) {
                                                        }
                                                        @Override
                                                        public void onPositiveClick(View view) {
                                                            // 充值
                                                            LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON,0,5));
                                                        }
                                                    }
                                            );
                                        }else{
                                            LaunchHelper.getInstance().launch(mContext, FindUserActivity.class);
                                        }

                                    }else{
                                        LaunchHelper.getInstance().launch(mContext, FindUserActivity.class);
                                    }
                                    UserBase userBase = userDetail.getUserBase();
                                    if (userBase!=null) {
                                        UserPreference.saveUserInfo(userBase);
                                    }
                                }
                            }
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {

                        }
                    });

                }
            }else{
                LaunchHelper.getInstance().launch(mContext, FindUserActivity.class);
            }

        }


    }

}
