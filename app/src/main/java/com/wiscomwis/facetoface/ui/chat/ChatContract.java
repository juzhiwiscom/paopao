package com.wiscomwis.facetoface.ui.chat;

import android.os.Message;
import android.view.MotionEvent;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/6/27.
 */
public interface ChatContract {

    interface IView extends BaseView {

        /**
         * 根据状态显示不同的录音提示框
         *
         * @param state {@link com.wiscomwis.facetoface.C.message}
         */
        void showRecordPopupWindow(int state);

        void dismissPopupWindow();

        /**
         * @return 待发送的文本
         */
        String getInputText();

        /**
         * 清除输入的文本
         */
        void clearInput();

        /**
         * 隐藏下拉刷新
         */
        void hideRefresh();

        void toggleShowEmpty(boolean toggle, String msg);

        /**
         * RecyclerView滑动到底部
         */
        void scroollToBottom();

        /**
         * @return 聊天记录适配器
         */
        ChatAdapter getChatAdapter();

        /**
         * Handler发送消息
         *
         * @param delayedMills 延时，单位：毫秒
         * @param msgType      消息类型{@link com.wiscomwis.facetoface.C.message}
         */
        void sendMessage(long delayedMills, int msgType);

        void sendMessage(Message message);

        /**
         * 显示支付拦截对话框
         *
         * @param tip
         */
        void showInterruptDialog(String tip);

        /**
         * 隐藏聊天框显示钥匙
         */
        void isGoneButtong(boolean flag);

        void setAQShow(int size);
    }

    interface IPresenter extends BasePresenter {

        /**
         * 处理异步任务
         *
         * @param message
         */
        void handleAsyncTask(Message message);

        /**
         * 初始化聊天记录列表
         */
        void initChatConversation();

        /**
         * 刷新，以加载更多
         */
        void refresh();

        /**
         * 发送文字消息
         */
        void sendTextMessage();

        /**
         * 发送图片消息
         *
         * @param isTakePhoto true表示拍照，false表示从相册选取
         */
        void sendImageMessage(boolean isTakePhoto);

        void handleTouchEventDown();

        void handleTouchEventMove(MotionEvent event);

        void handleTouchEventUp();
        void setRefresh2();
        void finish();
        /**
         * 消息已经读取
         */
        void msgRead(String guid);
        /**
         * 判断是否拉黑
         */
        void isBlock();

        void isHaveIntercept(long id);
        void updateDate(String message);

    }

}
