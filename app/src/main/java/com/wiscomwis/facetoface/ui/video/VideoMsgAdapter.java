package com.wiscomwis.facetoface.ui.video;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.data.model.PicInfo;
import com.wiscomwis.facetoface.data.model.PicInfoData;
import com.wiscomwis.facetoface.data.model.VideoMsg;
import com.wiscomwis.library.adapter.CommonRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;

/**
 * 视频时消息列表适配器
 * Created by zhangdroid on 2017/6/22.
 */
public class VideoMsgAdapter extends CommonRecyclerViewAdapter<VideoMsg> {

    public VideoMsgAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    public void convert(VideoMsg videoMsg, int position, RecyclerViewHolder holder) {
        if (null != videoMsg) {
            TextView tv_name = (TextView) holder.getView(R.id.item_video_nickname);
            TextView tv_msg = (TextView) holder.getView(R.id.item_video_msg);
            RelativeLayout rlmsg = (RelativeLayout) holder.getView(R.id.item_video_rl_msg);
            LinearLayout llgift = (LinearLayout) holder.getView(R.id.item_video_ll_gift);
            TextView tv_gift_num = (TextView) holder.getView(R.id.item_video_tv_gift_num);
            ImageView iv_gift = (ImageView) holder.getView(R.id.item_video_iv_gift);
            TextView tv_who = (TextView) holder.getView(R.id.item_video_tv_gift_name);
            TextView tv_sendwho = (TextView) holder.getView(R.id.item_video_tv_gift_send_who);
            if (videoMsg.getMessage().contains("{") && videoMsg.getMessage().contains("}")) {
                rlmsg.setVisibility(View.GONE);
                llgift.setVisibility(View.VISIBLE);
                PicInfoData picData = new Gson().fromJson(videoMsg.getMessage(), PicInfoData.class);
                if(picData!=null){
                    PicInfo picInfo = picData.getData();
                    if(picInfo!=null){
                        if (videoMsg.getType() == 1) {//接收的信息
                            tv_who.setText(videoMsg.getNickname());
                            tv_sendwho.setText("送我");
                        } else if (videoMsg.getType() == 0) {
                            tv_who.setText("我送");
                            tv_sendwho.setText("美女");
                        }
                        tv_gift_num.setText(picInfo.getPicNum());
                        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(picInfo.getPicIcon())
                                .placeHolder(0).error(0).imageView(iv_gift).build());
                    }
                }
            } else {
                rlmsg.setVisibility(View.VISIBLE);
                llgift.setVisibility(View.GONE);
                if (videoMsg.getType() == 1) {//接收的信息
                    tv_name.setTextColor(mContext.getResources().getColor(R.color.main_color));
                    tv_msg.setTextColor(mContext.getResources().getColor(R.color.main_color));
                } else if (videoMsg.getType() == 0) {//本地发送
                    tv_name.setTextColor(Color.WHITE);
                    tv_msg.setTextColor(Color.WHITE);
                }
                tv_name.setText(videoMsg.getNickname() + "：");
                tv_msg.setText(videoMsg.getMessage());
            }

        }
    }

}
