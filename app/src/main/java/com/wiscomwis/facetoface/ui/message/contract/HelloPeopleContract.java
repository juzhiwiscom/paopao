package com.wiscomwis.facetoface.ui.message.contract;

import android.support.v7.widget.RecyclerView;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;
import com.wiscomwis.facetoface.ui.message.adapter.ChatHistoryAdapter;
import com.wiscomwis.facetoface.ui.message.adapter.HelloPeopleAdapter;

/**
 * Created by xuzhaole on 2017/12/2.
 */

public interface HelloPeopleContract {
    interface IView extends BaseView {

        void setAdapter(RecyclerView.Adapter pagerAdapter);

        void toggleShowEmpty(boolean toggle, String msg);

        void toggleShowError(boolean toggle, String msg);

        /**
         * 隐藏下拉刷新
         */
        void hideRefresh(int delaySeconds);
    }

    interface IPresenter extends BasePresenter {
        void loadData();

        void refresh();

        void start(RecyclerView recyclerView);
    }
}
