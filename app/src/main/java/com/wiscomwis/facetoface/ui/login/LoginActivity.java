package com.wiscomwis.facetoface.ui.login;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragmentActivity;
import com.wiscomwis.facetoface.data.model.AccountPwdInfo;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.event.FinishEvent;
import com.wiscomwis.facetoface.parcelable.ListParcelable;
import com.wiscomwis.facetoface.ui.dialog.FindPasswordDialog;
import com.wiscomwis.facetoface.ui.register.FirstPageActivity;
import com.wiscomwis.library.dialog.LoadingDialog;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.SnackBarUtil;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;

/**
 * 登录页面
 * Created by zhangdroid on 2017/5/12.
 */
public class LoginActivity extends BaseFragmentActivity implements View.OnClickListener, LoginContract.IView {
    @BindView(R.id.login_root)
    RelativeLayout mLlRoot;
    @BindView(R.id.login)
    Button mBtnLogin;
    @BindView(R.id.login_register)
    TextView mTvRegister;
    @BindView(R.id.find_password)
    TextView mTvFindPassword;
    @BindView(R.id.login_activity_et_account)
    EditText et_account;
    @BindView(R.id.login_activity_et_pwd)
    EditText et_pwd;
    @BindView(R.id.login_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.iv_more)
    ImageView iv_more;
    @BindView(R.id.rl_root)
    RelativeLayout rl_root;


    InputMethodManager imm;
    private LoginPresenter mLoginPresenter;
    private ListParcelable mListParcelable;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mListParcelable= (ListParcelable) parcelable;
        if(mListParcelable!=null){
            //ListParcelable为MainActivity中选择加载对应fragment的Parcelable,只有一个参数，适合使用，不再创建
            Toast.makeText(LoginActivity.this, "您的账号在另一设备上登录", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        UserPreference.exitFromLoginView(true);
        mLoginPresenter = new LoginPresenter(this);
        mLoginPresenter.start();
        checkValid();
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    @Override
    protected void setListeners() {
        mBtnLogin.setOnClickListener(this);
        mTvRegister.setOnClickListener(this);
        mTvFindPassword.setOnClickListener(this);
        et_pwd.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        iv_more.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:// 登录
                UserPreference.exitFromLoginView(false);
                mLoginPresenter.login();
                break;

            case R.id.login_register:// 跳转注册页面
                UserPreference.exitFromLoginView(false);
                LaunchHelper.getInstance().launch(mContext, FirstPageActivity.class);
                break;

            case R.id.find_password:// 忘记密码
                mLoginPresenter.findPassword();
                break;
            case R.id.login_activity_et_pwd:
                checkValid();
                break;
            case R.id.login_activity_rl_back:
                exitAppDialog();
                break;
            case R.id.iv_more:
                final List<AccountPwdInfo> allAccountPwd = DbModle.getInstance().getUserAccountDao().getAllAccountPwd();
                if (allAccountPwd == null || allAccountPwd.size() == 0) {
                    return;
                }
                showPopupWindow(allAccountPwd);
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showPopupWindow(final List<AccountPwdInfo> allAccountPwd) {
        //设置contentView
        View contentView = LayoutInflater.from(LoginActivity.this).inflate(R.layout.popuplayout, null);
        final PopupWindow mPopWindow = new PopupWindow(contentView,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        mPopWindow.setContentView(contentView);
        //设置各个控件的点击响应
        ListView listview = (ListView) contentView.findViewById(R.id.listview);
        String[] mItems = new String[allAccountPwd.size()];
        for (int i = 0; i < allAccountPwd.size(); i++) {
            mItems[i] = allAccountPwd.get(i).getAccount();
        }
        MyAdapter adapter = new MyAdapter(mContext, allAccountPwd);
        listview.setAdapter(adapter);

        mPopWindow.setFocusable(true);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mPopWindow.dismiss();
                setAccount(allAccountPwd.get(i).getAccount());
                setPassword(allAccountPwd.get(i).getPwd());
            }
        });
        //显示PopupWindow
        mPopWindow.showAsDropDown(rl_root,50,10,Gravity.CENTER);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void setAccount(String account) {
        et_account.setText(account);
    }

    @Override
    public void setPassword(String password) {
        et_pwd.setText(password);
    }

    @Override
    public String getAccount() {
        return et_account.getText().toString();
    }

    @Override
    public String getPassword() {
        return et_pwd.getText().toString();
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void showFindPwdDialog(String account, String password) {
        FindPasswordDialog.show((String) TextUtils.concat(getString(R.string.find_pwd_account, account), "\n",
                getString(R.string.find_pwd_password, password)), getSupportFragmentManager());
    }

    /**
     * 检测是否可以登录
     */
    private void checkValid() {
        mBtnLogin.setEnabled(true);
    }

    @Subscribe
    public void onEvent(FinishEvent event) {
        finish();
    }

    //触摸屏幕，可以让软键盘消失
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (this.getCurrentFocus() != null) {
                if (this.getCurrentFocus().getWindowToken() != null) {
                    imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        }
        return super.onTouchEvent(event);
    }
    //重写onKeyDown方法,对按键(不一定是返回按键)监听
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {//当返回按键被按下
            exitAppDialog();
        }
        return false;
    }

    private void exitAppDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);//新建一个对话框
        dialog.setMessage("确定要退出应用吗?");//设置提示信息
        //设置确定按钮并监听
        dialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                    从登陆界面退出的，
                finish();//结束当前Activity
            }
        });
        //设置取消按钮并监听
        dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //这里什么也不用做
            }
        });
        dialog.show();//最后不要忘记把对话框显示出来
    }

}
