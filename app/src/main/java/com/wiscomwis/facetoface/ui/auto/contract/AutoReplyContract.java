package com.wiscomwis.facetoface.ui.auto.contract;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public interface AutoReplyContract {

    interface IView extends BaseView {

        /**
         * 显示加载
         */
        void showLoading();

        /**
         * 隐藏加载
         */
        void dismissLoading();

        void showNetworkError();

        void setAdapter(PagerAdapter pagerAdapter);

        FragmentManager getManager();

        void showAddCount(int replyCount,int helloCount);
    }

    interface IPresenter extends BasePresenter {

        void addTabs();

        void getAddCount();
    }

}
