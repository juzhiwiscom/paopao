package com.wiscomwis.facetoface.ui.video;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.PowerManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragmentActivity;
import com.wiscomwis.facetoface.common.AgoraHelper;
import com.wiscomwis.facetoface.common.CustomDialogAboutOther;
import com.wiscomwis.facetoface.common.RingManage;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.model.VideoMsg;
import com.wiscomwis.facetoface.data.preference.AnchorPreference;
import com.wiscomwis.facetoface.data.preference.BeanPreference;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.AgoraEvent;
import com.wiscomwis.facetoface.event.AgoraMediaEvent;
import com.wiscomwis.facetoface.event.FinishVideoActivity2;
import com.wiscomwis.facetoface.event.FinishVideoToChatActivityEvent;
import com.wiscomwis.facetoface.parcelable.VideoParcelable;
import com.wiscomwis.facetoface.ui.video.contract.VideoContract;
import com.wiscomwis.facetoface.ui.video.presenter.VideoPresenter;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.SnackBarUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 视频页面
 * Created by zhangdroid on 2017/5/27.
 */
public class VideoActivity extends BaseFragmentActivity implements View.OnClickListener, VideoContract.IView {
    @BindView(R.id.video_root)
    RelativeLayout mRlRoot;
    @BindView(R.id.video_local)
    FrameLayout mFlLocal;// 本地视频
    @BindView(R.id.video_remote)
    FrameLayout mFlRemote;// 远端视频
    // 控制区
    @BindView(R.id.video_report)
    ImageView mIvReport;// 举报
    @BindView(R.id.video_switch)
    ImageView mIvSwitch;// 切换摄像头
    @BindView(R.id.video_exit)
    ImageView mIvExit;// 关闭视频
    @BindView(R.id.video_time)
    TextView mTvTime;// 视频计时
    @BindView(R.id.video_balance)
    TextView mTvBalance;// 余额
    // 消息列表
    @BindView(R.id.video_chat_list)
    RecyclerView mRecyclerView;
    // 消息输入框和发送
    @BindView(R.id.video_input)
    EditText mEtInput;
    @BindView(R.id.video_send)
    Button mBtnSend;
    @BindView(R.id.video_chat_iv_msg)
    ImageView iv_msg;
    @BindView(R.id.video_rl_bottom)
    RelativeLayout rl_bottom;
    @BindView(R.id.video_chat_iv_vioce)
    ImageView iv_vioce;
    @BindView(R.id.video_activity_avatar_big)
    ImageView iv_avatar_big;
    @BindView(R.id.video_activity_invite_mask)
    View viewmask;
    @BindView(R.id.video_chat_iv_gift)
    ImageView iv_gift;
    InputMethodManager imm;
    private VideoParcelable mVideoParcelable;
    private VideoPresenter mVideoPresenter;
    private VideoMsgAdapter mVideoMsgAdapter;
    private boolean vioce_status = true;
    private static final int MSG_TYPE_TIMER = 0;
    private int mVideoTime;
    PowerManager powerManager = null;
    PowerManager.WakeLock wakeLock = null;
    private boolean isCanChat=true;
    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_TYPE_TIMER:// 计时
                    mVideoTime++;
                    mTvTime.setText(getString(R.string.duration) + mVideoPresenter.convertSecondsToString(mVideoTime));
                    // 每过一分钟计费
                    if (mVideoTime % 60 == 0) {
                        if (UserPreference.isAnchor()) {
                            // 播主累计收入
                            if (!TextUtils.isEmpty(mVideoParcelable.hostPrice)) {
                                AnchorPreference.setIncome((int) (Float.parseFloat(AnchorPreference.getIncome()) + Float.parseFloat(mVideoParcelable.hostPrice)));
                                mTvBalance.setText(AnchorPreference.getIncome());
                            }
                        } else {
                            // 普通用户扣除余额
                            if (!TextUtils.isEmpty(mVideoParcelable.hostPrice)) {
                                BeanPreference.setBeanCount(BeanPreference.getBeanCount() - Integer.parseInt(mVideoParcelable.hostPrice));
                                mTvBalance.setText(String.valueOf(BeanPreference.getBeanCount()));
                            }
                        }
                    }
                    mHandler.sendEmptyMessageDelayed(MSG_TYPE_TIMER, 1000);
                    break;
            }
        }
    };

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_video;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mVideoParcelable = (VideoParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        // 初始化消息列表
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mVideoMsgAdapter = new VideoMsgAdapter(this, R.layout.item_video_msg_list);
        // 添加HeaderView
//        HeaderAndFooterWrapper headerAndFooterWrapper = new HeaderAndFooterWrapper(mVideoMsgAdapter);
//        TextView headerView = new TextView(this);
//        headerView.setTextSize(14);
//        headerView.setTextColor(getResources().getColor(R.color.purple));
//        String warn = getString(R.string.video_warn);
//        SpannableString spannableString = new SpannableString(warn);
//        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.main_color)), 0, warn.indexOf("：") + 1,
//                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        headerView.setText(spannableString);
//        headerAndFooterWrapper.addHeaderView(headerView);
        mRecyclerView.setAdapter(mVideoMsgAdapter);
        // 根据用户类型，设置收入或余额
        if (UserPreference.isAnchor()) {
            mTvBalance.setText(AnchorPreference.getIncome());
        } else {
            mTvBalance.setText(String.valueOf(BeanPreference.getBeanCount()));
        }
        mVideoPresenter = new VideoPresenter(this, mVideoParcelable.channelId, mVideoParcelable.nickname, mVideoParcelable.imageUrl, String.valueOf(mVideoParcelable.guid), mVideoParcelable.account, mVideoParcelable.type, mVideoParcelable.isInvited);
        mVideoPresenter.start();
//        EventBus.getDefault().post(new FinishMassVideoOrVoiceActivity());
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        powerManager = (PowerManager) this.getSystemService(this.POWER_SERVICE);
        wakeLock = this.powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My Lock");
        if (mVideoParcelable.type == 1) {
            mFlRemote.setVisibility(View.GONE);
            mFlLocal.setVisibility(View.GONE);
            viewmask.setVisibility(View.VISIBLE);
            iv_avatar_big.setVisibility(View.VISIBLE);
            ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(mVideoParcelable.imageUrl)
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_avatar_big).build());
        }
        EventBus.getDefault().post(new FinishVideoActivity2());//销毁前一个activity
        RingManage.getInstance().closeRing();
        UserPreference.saveGiftsContent("0");

    }

    @Override
    protected void setListeners() {
        mIvReport.setOnClickListener(this);
        mIvSwitch.setOnClickListener(this);
        mIvExit.setOnClickListener(this);
        mBtnSend.setOnClickListener(this);
        iv_msg.setOnClickListener(this);
        mRlRoot.setOnClickListener(this);
        iv_vioce.setOnClickListener(this);
        iv_gift.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.video_follow:// 关注
//                mVideoPresenter.follow(String.valueOf(mVideoParcelable.guid));
//                break;

            case R.id.video_report:// 举报
                break;

            case R.id.video_switch:// 切换摄像头
                mVideoPresenter.switchCamera();
                break;

            case R.id.video_exit:// 关闭
                mVideoPresenter.closeVideo();
                break;

            case R.id.video_send:// 发送文字消息
                mVideoPresenter.sendMsg(mVideoParcelable.account);
                break;
            case R.id.video_chat_iv_msg:
                rl_bottom.setVisibility(View.VISIBLE);
                iv_msg.setVisibility(View.GONE);
                iv_vioce.setVisibility(View.GONE);
                break;
            case R.id.video_root:
                iv_msg.setVisibility(View.VISIBLE);
                iv_vioce.setVisibility(View.VISIBLE);
                imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                rl_bottom.setVisibility(View.GONE);
                break;
            case R.id.video_chat_iv_vioce:
                if (vioce_status) {
                    AgoraHelper.getInstance().closeVioce();
                    vioce_status = false;
                    iv_vioce.setImageResource(R.drawable.vioce_close);
                } else if (!vioce_status) {
                    AgoraHelper.getInstance().openVioce();
                    vioce_status = true;
                    iv_vioce.setImageResource(R.drawable.vioce_open);
                }
                break;
            case R.id.video_chat_iv_gift:
                CustomDialogAboutOther.giveGiftShow(VideoActivity.this, String.valueOf(mVideoParcelable.guid), mVideoParcelable.account, mVideoParcelable.imageUrl, mVideoParcelable.nickname, 0,true,4);
                break;
        }
    }

    @Override
    protected void loadData() {
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
        // 网络断开连接
        mVideoPresenter.closeVideo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mHandler!=null)
          mHandler.removeMessages(MSG_TYPE_TIMER);
    }

    @Override
    protected void onStop() {
        if (!mVideoParcelable.isInvited) {
            mVideoPresenter.sendDurationMsg();
        }
        EventBus.getDefault().post(new FinishVideoToChatActivityEvent(mVideoParcelable.guid,mVideoParcelable.account,mVideoParcelable.nickname,mVideoParcelable.imageUrl,0));
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        // do nothing 屏蔽返回键
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mRlRoot, msg);
    }

    @Override
    public void finishActivity() {
        this.finish();
    }

    @Override
    public void postDelayed(Runnable runnable, int delayedSecs) {
        mHandler.postDelayed(runnable, delayedSecs * 1000);
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public FrameLayout getLocalVideoView() {
        return mFlLocal;
    }

    @Override
    public FrameLayout getRemoteVideoView() {
        return mFlRemote;
    }

    @Override
    public void videoTimeing() {
        mHandler.sendEmptyMessageDelayed(MSG_TYPE_TIMER, 1000);
    }

    @Override
    public void setFollowVisibility(boolean isVisible) {
//        mIvFollow.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public String getInputMessage() {
        return mEtInput.getText().toString();
    }

    @Override
    public String getVideoTime() {
        return mVideoPresenter.convertSecondsToString(mVideoTime);
    }

    @Override
    public void clearInput() {
        mEtInput.setText("");
    }

    @Override
    public void updateMsgList(VideoMsg videoMsg) {
        // 更新
        List<VideoMsg> list = new ArrayList<>();
        list.add(videoMsg);
        mVideoMsgAdapter.appendToList(list);
        // 更新后自动滚动到列表底部
        mRecyclerView.scrollToPosition(mVideoMsgAdapter.getAdapterDataList().size() - 1);
        UserPreference.saveGiftsContent("0");
    }

    @Override
    public void startAnimation() {
//        viewKonfetti.build()
//                .addColors(Color.parseColor("#fd698b"),Color.YELLOW, Color.GREEN, Color.MAGENTA)
//                .setDirection(0.0, 359.0)
//                .setSpeed(1f, 5f)
//                .setFadeOutEnabled(true)
//                .setTimeToLive(2000L)
//                .addShapes(Shape.RECT, Shape.CIRCLE)
//                .setPosition(-50f,viewKonfetti.getWidth() + 50f, -50f, -50f)
//                .stream(300, 5000L);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AgoraMediaEvent event) {
        mVideoPresenter.handleVideoEvent(event);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AgoraEvent event) {
        mVideoPresenter.handleAgoraEvent(event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        wakeLock.acquire();
        mVideoPresenter.activityFront(true);
//        HyphenateHelper.getInstance().isChatActivity();进入视频聊天页面不显示消息提醒
    }

    @Override
    protected void onPause() {
        super.onPause();
        wakeLock.release();
        mVideoPresenter.activityFront(false);
//        HyphenateHelper.getInstance().isNotChatActivity();退出视频聊天页面显示消息提醒
        DataPreference.saveNoSeeVideo(true);//显示悬浮框
    }

}
