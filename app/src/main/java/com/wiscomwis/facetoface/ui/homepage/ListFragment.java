package com.wiscomwis.facetoface.ui.homepage;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragment;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.PaySuccessEvent;
import com.wiscomwis.facetoface.event.SayHelloEvent;
import com.wiscomwis.facetoface.event.SearchEvent;
import com.wiscomwis.facetoface.parcelable.ListParcelable;
import com.wiscomwis.facetoface.ui.homepage.contract.ListContract;
import com.wiscomwis.facetoface.ui.homepage.presenter.ListPresenter;
import com.wiscomwis.library.util.ArgumentUtil;
import com.wiscomwis.library.widget.AutoSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;

/**
 * 首页列表
 * Created by zhangdroid on 2017/5/31.
 */
public class ListFragment extends BaseFragment implements ListContract.IView {
    @BindView(R.id.list_swiperefresh)
    AutoSwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.list_recyclerview)
    RecyclerView mRecyclerView;
    private ListPresenter mListPresenter;
    private ListParcelable mListParcelable;
    // 初次进入时自动显示刷新，此时不调用刷新
    private boolean mIsFirstLoad;
    private boolean paySuccess=false;

    public static ListFragment newInstance(ListParcelable parcelable) {
        ListFragment listFragment = new ListFragment();
        listFragment.setArguments(ArgumentUtil.setArgumentBundle(parcelable));
        return listFragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_list;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected View getNoticeView() {
        return mSwipeRefreshLayout;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {
        mListParcelable = (ListParcelable) parcelable;
    }

    @Override
    protected void initViews() {
        mSwipeRefreshLayout.setColorSchemeResources(R.color.main_color);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.WHITE);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 2);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mListPresenter = new ListPresenter(this);
        mIsFirstLoad = true;
        Util.getUserIdMap();
    }

    @Override
    protected void setListeners() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mIsFirstLoad) {
                    mIsFirstLoad = false;
                } else {
                    mListPresenter.refresh();
                }
            }
        });
    }

    @Override
    protected void loadData() {
        mListPresenter.start(mRecyclerView);
        mListPresenter.loadRecommendUserList(mListParcelable.type);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
    }

    @Override
    public void hideRefresh(int delaySeconds) {
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                mListPresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowEmpty(false, null, null);
                mListPresenter.refresh();
            }
        });
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter) {
        mRecyclerView.setAdapter(adapter);
    }

    @Subscribe
    public void onEvent(SearchEvent searchEvent) {
        if (!mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(true);
        }
        mListPresenter.refresh();
    }
    @Subscribe
    public void onEvent(PaySuccessEvent event){
       paySuccess=true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(SayHelloEvent event) {
//        handler.sendEmptyMessage(1);//发送消息更新打招呼按钮
        mListPresenter.sayHelloStatus();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            mListPresenter.sayHelloStatus();
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        if(paySuccess){
            paySuccess=false;
            if (!mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.setRefreshing(true);
            }
            mListPresenter.refresh();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //埋点
        setMsgFlag();
    }
    private static void setMsgFlag(){
        String userIdList = Util.getUserIdList();
        if(!TextUtils.isEmpty(userIdList)){
            ApiManager.userActivityTag(UserPreference.getId(), "", "21", userIdList, new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel baseModel, boolean isEmpty) {
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                }
            });
        }


    }
}
