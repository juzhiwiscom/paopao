package com.wiscomwis.facetoface.ui.video.presenter;

import android.content.Context;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;

import com.hyphenate.chat.EMMessage;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.AgoraHelper;
import com.wiscomwis.facetoface.common.CustomDialogAboutPay;
import com.wiscomwis.facetoface.common.HyphenateHelper;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.HuanXinUser;
import com.wiscomwis.facetoface.data.model.VideoHeartBeat;
import com.wiscomwis.facetoface.data.model.VideoMsg;
import com.wiscomwis.facetoface.data.model.VideoStop;
import com.wiscomwis.facetoface.data.preference.AnchorPreference;
import com.wiscomwis.facetoface.data.preference.BeanPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.event.AgoraEvent;
import com.wiscomwis.facetoface.event.AgoraMediaEvent;
import com.wiscomwis.facetoface.event.FinishMassVideoOrVoiceActivity;
import com.wiscomwis.facetoface.event.UpdateChatDataEvent;
import com.wiscomwis.facetoface.ui.video.contract.VideoContract;
import com.wiscomwis.library.dialog.AlertDialog;
import com.wiscomwis.library.dialog.OnDialogClickListener;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by zhangdroid on 2017/6/13.
 */
public class VideoPresenter implements VideoContract.IPresenter {
    private VideoContract.IView mVideoView;
    private Context mContext;
    private String mChannelId;// 视频频道id
    private String mNickname;// 对方用户昵称
    private String mGuid;// 对方用户id
    private String mAccount;// 对方用户account
    private int mType;// 对方用户account
    private String mUserPic;// 对方用户account
    private boolean mIsInvited;// 对方用户account
    private boolean isFront = true;// 对方用户account
    private AlertDialog alertDialog;
    private int num=0;

    public VideoPresenter(VideoContract.IView view, String channelId, String nickname,
                          String userPic, String id, String account, int type, boolean isInvited) {
        this.mVideoView = view;
        this.mContext = view.obtainContext();
        this.mChannelId = channelId;
        this.mNickname = nickname;
        this.mGuid = id;
        this.mAccount = account;
        this.mType = type;
        this.mUserPic = userPic;
        this.mIsInvited = isInvited;
    }

    @Override
    public void start() {
        // 设置本地视频
        AgoraHelper.getInstance().setupLocalVideo(mContext, mVideoView.getLocalVideoView());
        AgoraHelper.getInstance().joinChannel(mChannelId);
    }

    @Override
    public void handleVideoEvent(final AgoraMediaEvent event) {
        if (null != event) {
            switch (event.eventCode) {
                case AgoraHelper.EVENT_CODE_SETUP_REMOTE_VIDEO:// 创建远端视频
                    AgoraHelper.getInstance().setupRemoteVideo(mContext, mVideoView.getRemoteVideoView(), event.uId);

                    //                    安卓8.0重新添加本地视频
                    String release = Build.VERSION.RELEASE;
                    String substring = release.substring(0,1);
                    boolean equals = substring.equals("8");
                    if(equals){
                        mVideoView.getLocalVideoView().removeAllViews();
                        AgoraHelper.getInstance().setupLocalVideo(mContext, mVideoView.getLocalVideoView());
                    }

                    break;

                case AgoraHelper.EVENT_CODE_MEDIA_JOIN_SUCCESS:// 成功加入视频频道
                    // 开始视频计时
                    if(UserPreference.isQunFa()==1){
//                        接通成功，如果是群发视频／语音，此时关掉群发，
                        UserPreference.setQuanFa(2);
//                        关掉群发页面
                        EventBus.getDefault().post(new FinishMassVideoOrVoiceActivity());
                    }
                    mVideoView.videoTimeing();
                    // 开始轮询
                    if (null != mVideoView) {
                        mVideoView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                doVideoHeartBeat();
                            }
                        }, 20);
                    }
                    break;

                case AgoraHelper.EVENT_CODE_USER_OFFLINE:// 用户离线或掉线
                    mVideoView.showTip(mContext.getString(R.string.video_leave));
                    AgoraHelper.getInstance().leaveChannel(mChannelId);
                    break;

                case AgoraHelper.EVENT_CODE_VIDEO_ERROR:// 视频发生错误
                    mVideoView.showTip(mContext.getString(R.string.video_error));
                    AgoraHelper.getInstance().leaveChannel(mChannelId);
//                    重新登陆声网
                    AgoraHelper.getInstance().login();
                    break;
            }
        }
    }


    @Override
    public void switchCamera() {
        AgoraHelper.getInstance().switchCamera();
    }

    @Override
    public void closeVideo() {
        // 挂断视频前先通知后台
        ApiManager.videoStop(mGuid, UserPreference.getId(), new IGetDataListener<VideoStop>() {
            @Override
            public void onResult(VideoStop videoStop, boolean isEmpty) {
                if (UserPreference.isAnchor()) {
                    // 更新播主收入
                    AnchorPreference.setIncome((int) videoStop.getTotalIncome());
                } else {
                    // 更新用户余额
                    BeanPreference.setBeanCount(videoStop.getBeanCount());
                }
                AgoraHelper.getInstance().leaveChannel(mChannelId);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                AgoraHelper.getInstance().leaveChannel(mChannelId);
            }
        });
    }

    @Override
    public void sendDurationMsg() {
        if (!TextUtils.isEmpty(mAccount)) {
            ApiManager.interruptText(Long.valueOf(mGuid), (mType == 1 ? mContext.getString(R.string.voice_call)
                    : mContext.getString(R.string.video_call))
                    + mVideoView.getVideoTime(), new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel baseModel, boolean isEmpty) {
                    HyphenateHelper.getInstance().sendTextMessage(mAccount,
                            (mType == 1 ? mContext.getString(R.string.voice_call)
                                    : mContext.getString(R.string.video_call))
                                    + mVideoView.getVideoTime(),
                            new HyphenateHelper.OnMessageSendListener() {
                                @Override
                                public void onSuccess(EMMessage emMessage) {
                                    String s = (mType == 1 ? mContext.getString(R.string.voice_call)
                                            : mContext.getString(R.string.video_call))
                                            + mVideoView.getVideoTime();
                                    EventBus.getDefault().post(new UpdateChatDataEvent(s));

                                }

                                @Override
                                public void onError() {
                                    String s = (mType == 1 ? mContext.getString(R.string.voice_call)
                                            : mContext.getString(R.string.video_call))
                                            + mVideoView.getVideoTime();
                                    EventBus.getDefault().post(new UpdateChatDataEvent(s));
                                }
                            });
                    HyphenateHelper.getInstance().markMsgAsRead(mAccount);
                    saveDataToSqlit(mNickname, String.valueOf(mGuid), mAccount, mUserPic,
                            (mType == 1 ? mContext.getString(R.string.voice_call)
                                    : mContext.getString(R.string.video_call))
                                    + mVideoView.getVideoTime(), String.valueOf(System.currentTimeMillis()), 1);
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                }
            });

//            HyphenateHelper.getInstance().sendTextMessage(mAccount,
//                    (mType == 1 ? mContext.getString(R.string.voice_call)
//                            : mContext.getString(R.string.video_call))
//                            + mVideoView.getVideoTime(),
//                    new HyphenateHelper.OnMessageSendListener() {
//                        @Override
//                        public void onSuccess(EMMessage emMessage) {
//                            HyphenateHelper.getInstance().markMsgAsRead(mAccount);
//                            saveDataToSqlit(mNickname, String.valueOf(mGuid), mAccount, mUserPic,
//                                    (mType == 1 ? mContext.getString(R.string.voice_call)
//                                            : mContext.getString(R.string.video_call))
//                                            + mVideoView.getVideoTime(), String.valueOf(System.currentTimeMillis()), 1);
//                            EventBus.getDefault().post(new MessageArrive(String.valueOf(mGuid)));
//                        }
//
//                        @Override
//                        public void onError() {
//                        }
//                    });
        }
    }

    private void saveDataToSqlit(String name, String id, String account, String pic, String msg, String time, int extendType) {
        if (!TextUtils.isEmpty(id)) {
            HuanXinUser user = new HuanXinUser(id, name, pic, account, "1", msg, 0, time, extendType);
            DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
        }
    }


    @Override
    public void sendMsg(String account) {
        String message = mVideoView.getInputMessage();
        if (!TextUtils.isEmpty(message)) {
            AgoraHelper.getInstance().sendTextMessage(account, message);
        } else {
            mVideoView.showTip(mContext.getString(R.string.video_send_tip));
        }
    }

    @Override
    public void handleAgoraEvent(AgoraEvent event) {
        switch (event.eventCode) {
            case AgoraHelper.EVENT_CODE_JOIN_CHANNEL_FAILED:// 加入频道失败
                mVideoView.showTip(mContext.getString(R.string.video_join_failed));
                AgoraHelper.getInstance().leaveChannel(mChannelId);
                break;

            case AgoraHelper.EVENT_CODE_LEAVE_CHANNEL_SELF:// 离开频道
                mVideoView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (mVideoView != null) {
                            mVideoView.finishActivity();
                        }
                    }
                }, 1);
                break;

            case AgoraHelper.EVENT_CODE_SEND_MSG_SUCCESS:// 发送消息成功
                // 更新消息列表并清空已发送消息
                if (UserPreference.getGiftsContent() != null) {
                    if (UserPreference.getGiftsContent().equals("0")) {
                        VideoMsg videoMsgSend = new VideoMsg();
                        videoMsgSend.setNickname(UserPreference.getNickname());
                        videoMsgSend.setMessage(mVideoView.getInputMessage());
                        videoMsgSend.setType(0);
                        mVideoView.updateMsgList(videoMsgSend);
                        mVideoView.clearInput();
                    } else {
                        VideoMsg videoMsgSend = new VideoMsg();
                        videoMsgSend.setNickname(UserPreference.getNickname());
                        videoMsgSend.setMessage(UserPreference.getGiftsContent());
                        videoMsgSend.setType(0);
                        mVideoView.updateMsgList(videoMsgSend);
                        mVideoView.clearInput();
                        mVideoView.startAnimation();
                    }
                } else {
                    VideoMsg videoMsgSend = new VideoMsg();
                    videoMsgSend.setNickname(UserPreference.getNickname());
                    videoMsgSend.setMessage(mVideoView.getInputMessage());
                    videoMsgSend.setType(0);
                    mVideoView.updateMsgList(videoMsgSend);
                    mVideoView.clearInput();
                }

                break;

            case AgoraHelper.EVENT_CODE_SEND_MSG_FAILED:// 发送消息失败
                mVideoView.showTip(mContext.getString(R.string.video_send_failed));
                break;

            case AgoraHelper.EVENT_CODE_RECEIVE_MSG:// 收到对方消息
                VideoMsg videoMsgReceived = new VideoMsg();
                videoMsgReceived.setNickname(mNickname);
                videoMsgReceived.setMessage(event.message);
                videoMsgReceived.setType(1);
                mVideoView.updateMsgList(videoMsgReceived);
                if (event.getMessage().contains("{") && event.getMessage().contains("}")) {
                    mVideoView.startAnimation();
                }
                break;
            case AgoraHelper.EVENT_CODE_SEND_GIFTS:

                break;
        }
    }

    @Override
    public String convertSecondsToString(int seconds) {
        return Util.convertSecondsToString(seconds);
    }

    /**
     * 视频通话心跳，向后台服务器上报心跳（轮询周期20秒）
     */
    private void doVideoHeartBeat() {
        ApiManager.videoHeartBeat(mGuid, new IGetDataListener<VideoHeartBeat>() {
            @Override

            public void onResult(VideoHeartBeat videoHeartBeat, boolean isEmpty) {
                if ("1".equals(videoHeartBeat.getConnectStatus())) {
                    num=0;
                    // 视频过程中余额不足
                    if ("-1".equals(videoHeartBeat.getBeanStatus())) {
                        mVideoView.showTip(mContext.getString(R.string.video_balance_not_empty));
                        AgoraHelper.getInstance().leaveChannel(mChannelId);
                        closeVideo();
                    } else {
                        if (null != mVideoView) {
                            mVideoView.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    doVideoHeartBeat();
                                }
                            }, 20);
                        }

                        if (isFront && !UserPreference.isAnchor() && videoHeartBeat.getAvalibleSeconds() <= 120) {
                            if (alertDialog == null) {
                                alertDialog = AlertDialog.newInstance("", mContext.getString(R.string.less_two_minutes),
                                        mContext.getString(R.string.buy), mContext.getString(R.string.cancel), false, new OnDialogClickListener() {
                                            @Override
                                            public void onNegativeClick(View view) {

                                            }

                                            @Override
                                            public void onPositiveClick(View view) {
                                                CustomDialogAboutPay.purchaseDiamondShow(mContext,11);
                                            }
                                        });
                                alertDialog.show(((FragmentActivity) mContext).getSupportFragmentManager(), "alert");

                            } else {
                                if (alertDialog.getDialog() == null) {
                                    alertDialog = null;
                                    alertDialog = AlertDialog.newInstance("", mContext.getString(R.string.less_two_minutes),
                                            mContext.getString(R.string.buy), mContext.getString(R.string.cancel), false, new OnDialogClickListener() {
                                                @Override
                                                public void onNegativeClick(View view) {

                                                }

                                                @Override
                                                public void onPositiveClick(View view) {
                                                    CustomDialogAboutPay.purchaseDiamondShow(mContext,11);
                                                }
                                            });
                                    alertDialog.show(((FragmentActivity) mContext).getSupportFragmentManager(), "alert");

                                }
                            }
                        }
                    }
                } else {// 连接异常
                    num++;
                    if (num==3) {
                        AgoraHelper.getInstance().leaveChannel(mChannelId);
                        closeVideo();
                    }else{
                        mVideoView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                doVideoHeartBeat();
                            }
                        }, 20);
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                    num++;
                    if (num==3) {
                        AgoraHelper.getInstance().leaveChannel(mChannelId);
                        closeVideo();
                    }else{
                        mVideoView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                doVideoHeartBeat();
                            }
                        }, 20);
                    }
            }
        });
    }

    @Override
    public void activityFront(boolean front) {
        isFront = front;
    }
}
