package com.wiscomwis.facetoface.ui.personalcenter.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.customload.Withdrawal_PromptDialog;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.WithdrawBean;
import com.wiscomwis.facetoface.event.WithdrawFinish;
import com.wiscomwis.facetoface.ui.personalcenter.contract.WithdrawContract;
import com.wiscomwis.library.dialog.OnDialogClickListener;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Administrator on 2017/7/14.
 */

public class WithdrawPresenter implements WithdrawContract.IPresenter {
    private WithdrawContract.IView mWithdrawView;
    private Context mContext;

    public WithdrawPresenter(WithdrawContract.IView view) {
        this.mWithdrawView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
        mWithdrawView = null;
    }

    @Override
    public void setInfo(final String name, final String money, final String type, final String account) {
        Withdrawal_PromptDialog.show(mWithdrawView.getFragment(), "", name, money, account, new OnDialogClickListener() {
            @Override
            public void onNegativeClick(View view) {

            }

            @Override
            public void onPositiveClick(View view) {
                //确定按钮
                startWithdrawal(name, account, type, money);
            }
        });
    }

    private void startWithdrawal(String name, String account, String type, String money) {
        ApiManager.withdraw(money, account, name, type, new IGetDataListener<WithdrawBean>() {
            @Override
            public void onResult(WithdrawBean baseModel, boolean isEmpty) {
                Toast.makeText(mContext, mContext.getString(R.string.withdraw_success), Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(new WithdrawFinish());
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                Toast.makeText(mContext, TextUtils.isEmpty(msg) ? mContext.getString(R.string.withdraw_success) : msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
