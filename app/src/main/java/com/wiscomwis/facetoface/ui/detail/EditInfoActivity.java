package com.wiscomwis.facetoface.ui.detail;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseAppCompatActivity;
import com.wiscomwis.facetoface.common.RingManage;
import com.wiscomwis.facetoface.common.TimeUtils;
import com.wiscomwis.facetoface.data.constant.DataDictManager;
import com.wiscomwis.facetoface.data.model.NettyMessage;
import com.wiscomwis.facetoface.data.model.VideoOrImage;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.event.RecordImageEvent;
import com.wiscomwis.facetoface.event.SingleLoginFinishEvent;
import com.wiscomwis.facetoface.event.UnReadMsgEvent;
import com.wiscomwis.facetoface.parcelable.ShortPlayParcelable;
import com.wiscomwis.facetoface.ui.detail.banner.Banner;
import com.wiscomwis.facetoface.ui.detail.banner.listener.OnBannerListener;
import com.wiscomwis.facetoface.ui.detail.banner.loader.GlideImageLoader;
import com.wiscomwis.facetoface.ui.detail.contract.EditInfoContract;
import com.wiscomwis.facetoface.ui.detail.presenter.EditInfoPresenter;
import com.wiscomwis.facetoface.ui.dialog.AgeSelectDialog;
import com.wiscomwis.facetoface.ui.dialog.DialogUtil;
import com.wiscomwis.facetoface.ui.dialog.OnDoubleDialogClickListener;
import com.wiscomwis.facetoface.ui.dialog.OnEditDoubleDialogClickListener;
import com.wiscomwis.facetoface.ui.dialog.OneWheelDialog;
import com.wiscomwis.library.dialog.LoadingDialog;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.SnackBarUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;

/**
 * 编辑个人信息页面
 * Created by zhangdroid on 2017/5/27.
 */
public class EditInfoActivity extends BaseAppCompatActivity implements View.OnClickListener, EditInfoContract.IView {
    @BindView(R.id.edit_info_root_layout)
    RelativeLayout mLlRoot;//根布局
    @BindView(R.id.edit_info_rl_age_sex)
    RelativeLayout rl_sex;//根布局
    @BindView(R.id.edit_info_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.banner)
    Banner banner;

    @BindView(R.id.edit_info_rl_album)
    RelativeLayout rl_album;//相册
    @BindView(R.id.edit_info_base_rl_nickname)
    RelativeLayout rl_nickname;
    @BindView(R.id.edit_info_base_tv_nickname)
    TextView et_nickname;//昵称
    @BindView(R.id.edit_info_base_rl_height)
    RelativeLayout rl_height;
    @BindView(R.id.edit_info_base_tv_height)
    TextView tv_height;//身高

    @BindView(R.id.edit_info_base_rl_marital_status)
    RelativeLayout rl_marital_status;
    @BindView(R.id.edit_info_base_tv_marital_status)
    TextView tv_marital_status;//婚姻状况

    @BindView(R.id.edit_info_rl_weight)
    RelativeLayout rl_weight;
    @BindView(R.id.edit_info_detail_tv_weight)
    TextView tv_weight;
    @BindView(R.id.edit_info_rl_sign)
    RelativeLayout rl_sign;
    @BindView(R.id.edit_info_detail_tv_sign)
    TextView tv_sign;
    @BindView(R.id.edit_info_rl_toobar)
    RelativeLayout rl_toobar;
    @BindView(R.id.edit_info_tv_nickname)
    TextView tv_nickname;
    @BindView(R.id.tv_id)
    TextView tv_id;
    @BindView(R.id.edit_info_tv_age)
    TextView tv_age;
    @BindView(R.id.edit_info_base_tv_sex)
    TextView tv_sex;
    @BindView(R.id.edit_info_base_tv_age)
    TextView tv_age2;
    @BindView(R.id.edit_info_tv_height)
    TextView tv_height2;
    @BindView(R.id.edit_info_tv_status)
    TextView tv_status;
    @BindView(R.id.et_ownwords)
    EditText et_ownwords;
    @BindView(R.id.edit_info_base_rl_age)
    RelativeLayout rl_age;
    @BindView(R.id.edit_info_tv_tag1)
    TextView tv_tag1;
    @BindView(R.id.edit_info_tv_tag2)
    TextView tv_tag2;
    @BindView(R.id.edit_info_tv_tag3)
    TextView tv_tag3;
    @BindView(R.id.edit_info_ll_tag)
    LinearLayout ll_tag;
    @BindView(R.id.tv_unread_info)
    TextView tvUnreadInfo;
    @BindView(R.id.iv_unread_exit)
    ImageView ivUnreadExit;
    @BindView(R.id.ll_unread_info)
    LinearLayout llUnreadInfo;
    private EditInfoPresenter mEditInfoPresenter;
    InputMethodManager imm;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_edit_info;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
    }

    @Override
    protected View getNoticeView() {
        return mLlRoot;
    }

    @Override
    protected void initViews() {
        mEditInfoPresenter = new EditInfoPresenter(this);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mEditInfoPresenter.getUserInfo();
        if (TimeUtils.timeIsPast()) {//判断页脚是否过期
            msgHandler.sendEmptyMessage(1);
        } else if (!TimeUtils.timeIsPast()) {
            msgHandler.sendEmptyMessage(2);
        }
        if (UserPreference.isMale()) {
            ll_tag.setVisibility(View.GONE);
        }
        setUnreadMsg();
    }

    @Override
    protected void setListeners() {
        rl_album.setOnClickListener(this);
        rl_nickname.setOnClickListener(this);
        rl_height.setOnClickListener(this);
        rl_marital_status.setOnClickListener(this);
        rl_weight.setOnClickListener(this);
        rl_sign.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        rl_age.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
        mEditInfoPresenter.setLocalInfo();
        mEditInfoPresenter.start();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    public void upBanner(List<VideoOrImage> list) {
        banner.update(list);
    }

    @Override
    public void setTag(String tag1, String tag2, String tag3) {
        tv_tag1.setText(tag1);
        tv_tag2.setText(tag2);
        tv_tag3.setText(tag3);
    }

    @Override
    public void isHaveTag() {
        ll_tag.setVisibility(View.GONE);
    }

    @Override
    public void startBanner(final List<VideoOrImage> list) {
        if (list != null && list.size() != 0) {

            banner.setOnBannerListener(new OnBannerListener() {
                @Override
                public void OnBannerClick(int position) {
                    if (list.get(position).isVideo() && !TextUtils.isEmpty(list.get(position).getVideoUrl())) {
                        LaunchHelper.getInstance().launch(mContext, ShortPlayActivity.class,
                                new ShortPlayParcelable(list.get(position).getVideoUrl(),list.get(position).getBitmapUrl()));
                    }
                }
            });
            banner.isAutoPlay(false).setImages(list).setImageLoader(new GlideImageLoader())
                    .start();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_info_rl_back:
                mEditInfoPresenter.upLoadMyInfo();
                break;
            case R.id.edit_info_rl_album:// 跳转到相册页面
                LaunchHelper.getInstance().launch(mContext, AlbumActivity.class);
                break;
            case R.id.edit_info_base_rl_nickname:// 昵称
                DialogUtil.showEditDoubleBtnDialog(getSupportFragmentManager(), mContext.getString(R.string.register_name), mContext.getString(R.string.video_input_hint), mContext.getString(R.string.setting_true), mContext.getString(R.string.cancel), true, new OnEditDoubleDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String content) {
                        if (!TextUtils.isEmpty(content.trim())) {
                            et_nickname.setText(content);
                            tv_nickname.setText(content);
                        }
                    }

                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.edit_info_base_rl_height:// 身高
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), true, DataPreference.getInchCmList(), mContext.getString(R.string.complete_height), mContext.getString(R.string.setting_true), mContext.getString(R.string.cancel), true, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_height.setText(selectedText);
                        tv_height2.setText(selectedText);
                    }

                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;

            case R.id.edit_info_base_rl_marital_status:// 婚姻状况
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), false, DataPreference.getRelationshipMapValue(), mContext.getString(R.string.marraige), mContext.getString(R.string.setting_true), mContext.getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_marital_status.setText(selectedText);
                    }

                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.edit_info_rl_weight:// 体重
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), false, DataPreference.getWeight(), mContext.getString(R.string.edit_info_weight), mContext.getString(R.string.setting_true), mContext.getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_weight.setText(selectedText + "kg");
                    }

                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.edit_info_rl_sign:// 星座
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), false, DataDictManager.getSignList(), mContext.getString(R.string.complete_signs), mContext.getString(R.string.setting_true), mContext.getString(R.string.cancel), true, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_sign.setText(selectedText);
                    }

                    @Override
                    public void onNegativeClick(View view) {

                    }
                });
                break;
            case R.id.edit_info_base_rl_age:
                AgeSelectDialog.show(getSupportFragmentManager(), "22", new AgeSelectDialog.OnAgeSelectListener() {
                    @Override
                    public void onSelected(String age) {
                        tv_age.setText(age + mContext.getString(R.string.edit_info_years_old));
                        tv_age2.setText(age + mContext.getString(R.string.edit_info_years_old));
                    }
                });
                break;
        }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override

    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void setIntroducation(String introducation) {

    }


    @Override
    public FragmentManager obtainFragmentManager() {
        return getSupportFragmentManager();
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public String stringNickName() {
        return tv_nickname.getText().toString();
    }

    @Override
    public String stringHeight() {
        return tv_height.getText().toString();
    }

    @Override
    public String stringRelation() {
        return tv_marital_status.getText().toString();
    }

    @Override
    public String stringWeight() {
        return tv_weight.getText().toString();
    }

    @Override
    public String stringSign() {
        return tv_sign.getText().toString();
    }

    @Override
    public String stringAge() {
        return tv_age.getText().toString();
    }

    @Override
    public void isMale(boolean male) {
        if (male) {
            rl_sex.setBackgroundResource(R.drawable.icon_male);
            tv_sex.setText(mContext.getString(R.string.register_male));
        } else {
            rl_sex.setBackgroundResource(R.drawable.icon_famle);
            tv_sex.setText(mContext.getString(R.string.register_female));
        }
    }

    @Override
    public void getNickNmae(String name) {
        tv_nickname.setText(name);
        et_nickname.setText(name);
    }

    @Override
    public void getUserId(String id) {
        tv_id.setText("ID:" + id);
    }

    @Override
    public void getAge(String age) {
        tv_age.setText(age + mContext.getString(R.string.edit_info_years_old));
        tv_age2.setText(age + mContext.getString(R.string.edit_info_years_old));
    }

    @Override
    public void getHeight(String height) {
        tv_height.setText(height + "cm");
        tv_height2.setText(height + "cm");

    }

    @Override
    public void getMarriage(String marriage) {
        tv_marital_status.setText(marriage);
    }

    @Override
    public void getWeight(String weight) {
        tv_weight.setText(weight + "kg");
    }

    @Override
    public void getSign(String sign) {
        tv_sign.setText(sign);
    }

    @Override
    public void setStatus(String stauts) {
        tv_status.setText(stauts);
    }

    @Override
    public String stringOwnWords() {
        return et_ownwords.getText().toString().trim();
    }

    @Override
    public void getOwnWords(String ownWords) {
        if (!TextUtils.isEmpty(ownWords)) {
            et_ownwords.setText(ownWords);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        banner.releaseBanner();
    }

    //触摸屏幕，可以让软键盘消失
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (this.getCurrentFocus() != null) {
                if (this.getCurrentFocus().getWindowToken() != null) {
                    imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        }
        return super.onTouchEvent(event);
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }

    @Override
    public void onBackPressed() {
        DialogUtil.showDoubleBtnDialog(getSupportFragmentManager(), "修改资料", "确认修改？", getString(R.string.confirm), getString(R.string.cancel), false, new OnDoubleDialogClickListener() {
            @Override
            public void onPositiveClick(View view) {
                mEditInfoPresenter.upLoadMyInfo();
            }

            @Override
            public void onNegativeClick(View view) {
                finish();
            }
        });
    }

    @Subscribe
    public void onEvent(RecordImageEvent event){
        mEditInfoPresenter.getUserInfo();
    }

    @Subscribe
    public void onEvent(NettyMessage event) {
        msgHandler.sendEmptyMessage(1);
    }

    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    isCanShowPop(true);
                    break;
                case 2:
                    RingManage.getInstance().closeRing();
                    break;
            }
        }
    };

    @Override
    protected void isCanShowPop(boolean flag) {
        super.isCanShowPop(flag);
    }
    private void setUnreadMsg() {
        int unReadInfo = DbModle.getInstance().getUserAccountDao().queryMsgNum();
        if(unReadInfo>0){
            llUnreadInfo.setVisibility(View.VISIBLE);
            tvUnreadInfo.setText("你有"+unReadInfo+"条未读消息，请查看");
            tvUnreadInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new UnReadMsgEvent());
                    finish();
                }
            });
            ivUnreadExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    llUnreadInfo.setVisibility(View.GONE);
                }
            });
        }else{
            llUnreadInfo.setVisibility(View.GONE);
        }
    }
}
