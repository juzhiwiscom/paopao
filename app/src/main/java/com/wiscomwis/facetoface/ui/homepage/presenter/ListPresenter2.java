package com.wiscomwis.facetoface.ui.homepage.presenter;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.data.api.ApiConstant;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.SearchCriteria;
import com.wiscomwis.facetoface.data.model.SearchUser;
import com.wiscomwis.facetoface.data.model.SearchUserList;
import com.wiscomwis.facetoface.data.preference.SearchPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.ui.homepage.ListFragmentBaseAdapter;
import com.wiscomwis.facetoface.ui.homepage.contract.ListContract;
import com.wiscomwis.facetoface.ui.homepage.contract.ListContract2;
import com.wiscomwis.facetoface.ui.main.MainActivity;
import com.wiscomwis.library.util.Utils;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangdroid on 2017/6/2.
 */
public class ListPresenter2 implements ListContract.IPresenter {
    private ListContract2.IView mListView;
    private Context mContext;
    private int pageNum = 1;
    private static final String pageSize = "50";
    private String url;
    private ListFragmentBaseAdapter mListAdapter;

    public ListPresenter2(ListContract2.IView view) {
        this.mListView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void loadRecommendUserList(int type) {
        switch (type) {
            case C.homepage.TYPE_GODDESS:
                url = ApiConstant.URL_HOMEPAGE_INCOME;
                break;
            case C.homepage.TYPE_ACTIVE_ANCHOR:
                url = ApiConstant.URL_HOMEPAGE_ACTIVE;
                break;
            case C.homepage.TYPE_NEW:
                url = ApiConstant.URL_HOMEPAGE_NEW;
                break;
            case C.homepage.TYPE_ACTIVE_FRIEND:
                url = ApiConstant.URL_HOMEPAGE_ACTIVE;
                break;
            case C.homepage.TYPE_NEW_FRIEND:
                url = ApiConstant.URL_HOMEPAGE_NEW;
                break;
        }
        mListAdapter = new ListFragmentBaseAdapter(mContext, R.layout.list_fragment2_item);
        mListAdapter.setFragmentManager(mListView.obtainFragmentManager());
//        mListAdapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
//                SearchUser searchUser = mListAdapter.getItemByPosition(position);
//                if (null != searchUser) {
//                    LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
//                            new UserDetailParcelable(String.valueOf(searchUser.getUesrId())));
//                }
//            }
//        });
        // 设置加载更多
        mListView.setAdapter(mListAdapter, R.layout.common_load_more);
//        ListDataSave listDataSave = new ListDataSave(mContext,"ReMenDate");
//        List<SearchUser> reMenDate = listDataSave.getDataList("ReMenDate");
//        if(reMenDate!=null){
//            mListAdapter.replaceAll(reMenDate);
//        }else {
            load();
//        }

    }

    @Override
    public void refresh() {
        pageNum = 1;
        load();
    }

    @Override
    public void sayHelloStatus() {

    }

    @Override
    public void start(RecyclerView recyclerView) {

    }

    /**
     * 上拉加载更多
     */
    public void loadMore() {
        mListView.showLoadMore();
        pageNum++;
        load();
    }

    private void load() {
        // 设置搜索条件
        Map<String, SearchCriteria> criteria = new HashMap<>();
        SearchCriteria searchCriteria = SearchPreference.getSearchCriteria();
        if (null != searchCriteria) {
            criteria.put("criteria", searchCriteria);
        }
        ApiManager.getHomepageRecommend(ApiConstant.URL_HOMEPAGE_HOT, pageNum, pageSize, new Gson().toJson(criteria), new IGetDataListener<SearchUserList>() {
            @Override
            public void onResult(SearchUserList searchUserList, boolean isEmpty) {
                mListView.hideView();
                if (isEmpty) {
                    if (pageNum == 1) {
                        mListView.toggleShowEmpty(true, null);
                    } else if (pageNum > 1) {
                        mListView.showNoMore();
                    }
                } else {
                    if (null != searchUserList) {
                        List<SearchUser> list = searchUserList.getSearchUserList();
                        if (!Utils.isListEmpty(list)) {
                            for (int i = 0; i < list.size(); i++) {
                                String iconUrl=list.get(i).getIconUrlMiddle();
                                String iconUrl1 = list.get(i).getIconUrl();
                                new getImageCacheAsyncTask(mContext).execute(iconUrl);
                                new getImageCacheAsyncTask(mContext).execute(iconUrl1);
                            }
                            if (pageNum == 1) {
                                mListAdapter.replaceAll(list);
                            } else if (pageNum > 1) {
                                mListAdapter.appendToList(list);
                            }
                            mListView.hideLoadMore();
                        }
                    }
                }
                mListView.hideRefresh(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mListView.hideRefresh(1);
                if (!isNetworkError) {
                    mListView.toggleShowError(true, msg);
                }
            }

        });
    }
   public void removeItem(int pos){
       if (mListAdapter != null) {
           mListAdapter.removeItem(0);
       }
   }
    public static class getImageCacheAsyncTask extends AsyncTask<String, Void, File> {
        private final Context context;
        private final WeakReference<Context> weakReference;
        private String imgUrl;

        public getImageCacheAsyncTask(Context context) {
            this.context = context;
            weakReference = new WeakReference<>(context);
        }

        @Override
        protected File doInBackground(String... params) {
            imgUrl =  params[0];
            try {
                return Glide.with(context)
                        .load(imgUrl)
                        .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                        .get();
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(File result) {
            MainActivity mainActivity = (MainActivity) weakReference.get();
            if(mainActivity!=null){
                if (result == null) {
                    return;
                }
                //此path就是对应文件的缓存路径
                String path = result.getPath();
                Log.e("path", path);
                UserPreference.setCacheIg(imgUrl,path);
            }


//            SharedPreferenceUtil.setStringValue(context,"cacheImg",imgUrl,path);

//            Bitmap bmp= BitmapFactory.decodeFile(path);
//            img.setImageBitmap(bmp);

        }
    }
}
