package com.wiscomwis.facetoface.ui.video;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tianzhentao on 2018/7/20.
 */

public class VideoMessageActivity extends Activity {

    @BindView(R.id.video_invite_preview)
    SurfaceView videoInvitePreview;
    @BindView(R.id.video_invite_avatar_big)
    ImageView videoInviteAvatarBig;
    @BindView(R.id.video_invite_mask)
    View videoInviteMask;
    @BindView(R.id.video_invite_avatar)
    ImageView videoInviteAvatar;
    @BindView(R.id.video_invite_nickname)
    TextView videoInviteNickname;
    @BindView(R.id.video_invite_tip)
    TextView videoInviteTip;
    @BindView(R.id.video_reject)
    TextView videoReject;
    @BindView(R.id.video_cancel)
    TextView videoCancel;
    @BindView(R.id.video_accept)
    TextView videoAccept;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("tag", "onCreate:启动了消息内容的activity ");
        //四个标志位顾名思义，分别是锁屏状态下显示，解锁，保持屏幕长亮，打开屏幕。这样当Activity启动的时候，它会解锁并亮屏显示。
        Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED //锁屏状态下显示
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD //解锁
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON //保持屏幕长亮
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON); //打开屏幕
//        Drawable wallPaper = WallpaperManager.getInstance( this).getDrawable();
//        win.setBackgroundDrawable(wallPaper);
        setContentView(R.layout.activity_video_invite);
        ButterKnife.bind(this);
        videoAccept.setVisibility(View.VISIBLE);
        videoCancel.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}