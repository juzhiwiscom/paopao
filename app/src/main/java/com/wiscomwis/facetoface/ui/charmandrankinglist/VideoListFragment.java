package com.wiscomwis.facetoface.ui.charmandrankinglist;

import android.content.Context;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragment;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.UserTagVideoEvent;
import com.wiscomwis.facetoface.ui.charmandrankinglist.contract.VideoListContract;
import com.wiscomwis.facetoface.ui.charmandrankinglist.presenter.VideoListPresenter;
import com.wiscomwis.library.widget.tab.NewVerticalViewPager;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class VideoListFragment extends BaseFragment implements VideoListContract.IView {
    @BindView(R.id.vvp_video_list)
    NewVerticalViewPager viewpager;
    @BindView(R.id.rl_more)
    RelativeLayout rl_more;
    VideoListPresenter videoListPresenter;
    private int mPostion = 1;
    private int i=0;
    private boolean isposition=true;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_video_list_layout;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected View getNoticeView() {
        viewpager.setOffscreenPageLimit(2);
        return viewpager;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        videoListPresenter = new VideoListPresenter(this);
        videoListPresenter.start();
    }

    @Override
    protected void setListeners() {
        rl_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoListPresenter.reportShow(viewpager.getCurrentItem());
            }
        });
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int position1 = position;
                float positionOffset1 = positionOffset;
                int positionOffsetPixels1 = positionOffsetPixels;
                Log.v("HHHHH==","position1="+position1+"--positionOffset1="+positionOffset1+"--positionOffsetPixels1="+positionOffsetPixels1);
                if(position==0&&positionOffsetPixels1==0){
                    i++;
                }
                if(i>10&&isposition){
                    isposition=false;
                    i=0;
                    if(videoListPresenter.videoList!=null){
                        if(videoListPresenter.videoList.getVideoSquareList()!=null) {
                            if (videoListPresenter.videoList.getVideoSquareList().size() > 2) {
                                videoListPresenter.loadVideoShowList(100);
                            }
                        }
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isposition=true;
                        }
                    },5000);
                }
//                EventBus.getDefault().post(new OpenPhotoEvent());

            }

            @Override
            public void onPageSelected(int position) {

                if (viewpager.getAdapter().getCount() - 1 == position) {
                    videoListPresenter.loadVideoShowList(mPostion);
                }
                position=position%5;
//                埋点：广场浏览视频的监测
                if(videoListPresenter.videoList!=null){
                    if(videoListPresenter.videoList.getVideoSquareList()!=null){
                        if(videoListPresenter.videoList.getVideoSquareList().size()>position){
                            if(videoListPresenter.videoList.getVideoSquareList().get(position)!=null&&videoListPresenter.videoList.getVideoSquareList().get(position).getUserBase()!=null){
                                String userId = videoListPresenter.videoList.getVideoSquareList().get(position).getUserBase().getGuid()+"";
                                ApiManager.userActivityTag(UserPreference.getId(), userId, "18", "1", new IGetDataListener<BaseModel>() {
                                    @Override
                                    public void onResult(BaseModel baseModel, boolean isEmpty) {
                                    }

                                    @Override
                                    public void onError(String msg, boolean isNetworkError) {
                                    }
                                });
                            }
                        }
                    }

                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void setPostion() {
        mPostion++;
    }
    @Override
    public void clearPosition() {
        mPostion=1;
    }

    @Override
    protected void loadData() {
        videoListPresenter.loadVideoShowList(1);
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            if (viewpager.getAdapter().getCount() == 0) {
                videoListPresenter.loadVideoShowList(1);
            }
        }
    }

    @Override
    public void showTip(String msg) {

    }

    @Override
    public void setAdapter(FragmentStatePagerAdapter adapter) {
        if(viewpager!=null)
          viewpager.setAdapter(adapter);
    }

    @Override
    public void setEmptyView(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                toggleShowEmpty(false, null, null);
                videoListPresenter.loadVideoShowList(1);
            }
        });
    }

    @Override
    public void setErrorView(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                toggleShowError(false, null, null);
                videoListPresenter.loadVideoShowList(1);
            }
        });
    }
    @Override
    public Fragment getFragment() {
        return VideoListFragment.this;
    }



    @Subscribe
    public void onEvent(UserTagVideoEvent event) {
//                埋点：广场浏览视频的监测
        if(videoListPresenter.videoList!=null){
            String userId = videoListPresenter.videoList.getVideoSquareList().get(0).getUserBase().getGuid()+"";
            ApiManager.userActivityTag(UserPreference.getId(), userId, "18", "1", new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel baseModel, boolean isEmpty) {
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                }
            });
        }
    }
}
