package com.wiscomwis.facetoface.ui.personalcenter.contract;

import android.support.v4.app.FragmentManager;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;

/**
 * Created by Administrator on 2017/7/14.
 */

public interface WithdrawContract {

    interface IView extends BaseView {
        FragmentManager getFragment();
    }

    interface IPresenter extends BasePresenter {
        /**
         * 设置账号姓名钱数信息
         * @param name
         * @param money
         * @param account
         */
        void setInfo(String name,String money,String type,String account);
    }
}
