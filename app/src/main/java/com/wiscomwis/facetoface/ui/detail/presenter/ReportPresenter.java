package com.wiscomwis.facetoface.ui.detail.presenter;

import android.content.Context;

import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.ui.detail.ReportSuccessActivity;
import com.wiscomwis.facetoface.ui.detail.contract.ReportContract;
import com.wiscomwis.library.util.LaunchHelper;

/**
 * Created by Administrator on 2017/6/14.
 */
public class ReportPresenter implements ReportContract.IPresenter {
   private Context mContext;
   private ReportContract.IView mReportIview;

    public ReportPresenter(ReportContract.IView mReportIview) {
        this.mReportIview = mReportIview;
        mContext=mReportIview.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void startReport(String userId,String resonCode) {
        ApiManager.report(userId, resonCode,new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel aSwitch, boolean isEmpty) {
                LaunchHelper.getInstance().launchFinish(mContext, ReportSuccessActivity.class);
            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }
}
