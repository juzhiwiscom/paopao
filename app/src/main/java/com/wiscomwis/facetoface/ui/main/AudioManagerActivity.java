package com.wiscomwis.facetoface.ui.main;

import android.app.Activity;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.Util;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by tianzhentao on 2018/7/26.
 */

public class AudioManagerActivity extends Activity {
    private Button play;
    private Button stop;
    private SeekBar soundValue;
    private AudioTrack at;
    private AudioManager am;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_manager);
        am = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        play = (Button)findViewById(R.id.main_sk_play);
        stop = (Button)findViewById(R.id.main_sk_stop);
        soundValue = (SeekBar)findViewById(R.id.skbVolume);
        setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
        Util.playMp3(AudioManagerActivity.this);
        play.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(am.isSpeakerphoneOn()){
                    am.setSpeakerphoneOn(false);
                }
                //setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
                am.setMode(AudioManager.MODE_IN_CALL);
                System.out.println(am.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL));
                System.out.println("&&&&&&&&&&&&&");
                System.out.println(am.getStreamVolume(AudioManager.STREAM_VOICE_CALL));
                //am.setStreamVolume(streamType, index, flags)
                int bufferSizeInBytes = AudioTrack.getMinBufferSize(44100, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
                if(at==null){
                    at = new AudioTrack(AudioManager.STREAM_VOICE_CALL, 44100, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSizeInBytes, AudioTrack.MODE_STREAM);
                    System.out.println("22222");
                    //at.setStereoVolume(100f, 100f);
                    at.setStereoVolume(0.7f, 0.7f);//设置当前音量大小
                    new AudioTrackThread().start();
                }else{
                    if(at.getPlayState()==AudioTrack.PLAYSTATE_PLAYING){
                        System.out.println("111111111");
                    }else{
                        System.out.println("33333");
                        at = new AudioTrack(AudioManager.STREAM_VOICE_CALL, 44100, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSizeInBytes, AudioTrack.MODE_STREAM);
                        new AudioTrackThread().start();
                    }
                }
            }
        });
        stop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(at.getPlayState()==AudioTrack.PLAYSTATE_PLAYING){
                    try{
                        at.stop();
                    }catch (IllegalStateException e)
                    {
                        e.printStackTrace();
                    }
                    at.release();
                    am.setMode(AudioManager.MODE_NORMAL);
                }
            }
        });
//    soundValue.setMax(100);//音量调节的极限
//    soundValue.setProgress(70);//设置seekbar的位置值
        soundValue.setMax(am.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL));
        soundValue.setProgress(am.getStreamVolume(AudioManager.STREAM_VOICE_CALL));
        soundValue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
//       float vol=(float)(seekBar.getProgress())/(float)(seekBar.getMax());
//       System.out.println(vol);
//       at.setStereoVolume(vol, vol);//设置音量
                am.setStreamVolume(AudioManager.STREAM_VOICE_CALL, seekBar.getProgress(), AudioManager.FLAG_PLAY_SOUND);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub
            }
        });
    }
    class AudioTrackThread extends Thread{
        @Override
        public void run() {
            byte[] out_bytes = new byte[44100];
            InputStream is = getResources().openRawResource(R.raw.start);
            int length ;
            try{
                at.play();
            }catch (IllegalStateException e)
            {
                e.printStackTrace();
            }
            try {
                while((length = is.read(out_bytes))!=-1){
                    //System.out.println(length);
                    at.write(out_bytes, 0, length);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(at.getPlayState()==AudioTrack.PLAYSTATE_PLAYING){
                try{
                    at.stop();
                }catch (IllegalStateException e)
                {
                    e.printStackTrace();
                }
                at.release();
                am.setMode(AudioManager.MODE_NORMAL);
            }
        }
    }
}