package com.wiscomwis.facetoface.ui.charmandrankinglist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragment;
import com.wiscomwis.facetoface.common.CustomDialogAboutOther;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.common.VideoHelper;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.CheckStatus;
import com.wiscomwis.facetoface.data.model.VideoSquare;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.GiftSendEvent;
import com.wiscomwis.facetoface.event.IsFollow;
import com.wiscomwis.facetoface.event.PauseEvent;
import com.wiscomwis.facetoface.event.StartEvent;
import com.wiscomwis.facetoface.event.StartVideoEvent;
import com.wiscomwis.facetoface.event.UpdataFollowUser;
import com.wiscomwis.facetoface.parcelable.ChatParcelable;
import com.wiscomwis.facetoface.parcelable.UserDetailParcelable;
import com.wiscomwis.facetoface.parcelable.VideoInviteParcelable;
import com.wiscomwis.facetoface.parcelable.VideoShowParcelable;
import com.wiscomwis.facetoface.ui.chat.ChatActivity;
import com.wiscomwis.facetoface.ui.detail.UserDetailActivity;
import com.wiscomwis.facetoface.ui.main.MainActivity;
import com.wiscomwis.facetoface.ui.personalcenter.AuthenticationActivity;
import com.wiscomwis.facetoface.ui.personalcenter.VideoShowActivity;
import com.wiscomwis.library.dialog.AlertDialog;
import com.wiscomwis.library.dialog.OnDialogClickListener;
import com.wiscomwis.library.image.CropCircleTransformation;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.LogUtil;
import com.wiscomwis.library.util.SharedPreferenceUtil;
import com.wiscomwis.library.util.ToastUtil;
import com.wiscomwis.playerlibrary.EmptyControlVideo;
import com.wiscomwis.playerlibrary.gsyvideoplayer.GSYVideoManager;
import com.wiscomwis.playerlibrary.gsyvideoplayer.listener.GSYSampleCallBack;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 待整理，优化
 * Created by xuzhaole on 2018/3/20.
 */

public class VideoRoomFragment extends BaseFragment implements View.OnClickListener {
    @BindView(R.id.player_empty_room)
    EmptyControlVideo player_empty_room;
    @BindView(R.id.iv_avatar)
    ImageView ivAvatar;
    @BindView(R.id.rl_avatar)
    RelativeLayout rlAvatar;
    @BindView(R.id.ll_chat)
    LinearLayout llChat;
    @BindView(R.id.iv_video)
    ImageView ivVideo;
    @BindView(R.id.rl_video)
    RelativeLayout rlVideo;
    @BindView(R.id.iv_send_gift)
    ImageView ivSendGift;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_account)
    TextView tvAccount;
    @BindView(R.id.iv_cover_photo)
    ImageView iv_cover_photo;
    @BindView(R.id.iv_follow)
    ImageView iv_follow;
    @BindView(R.id.tv_chat)
    TextView tvChat;
    Unbinder unbinder;
    @BindView(R.id.iv_bg_show)
    ImageView ivBgShow;
    @BindView(R.id.progress_empty)
    ProgressBar progressEmpty;
    @BindView(R.id.fl_empty)
    FrameLayout flEmpty;
    private VideoSquare mData;
    protected boolean isInit = false;
    protected boolean isLoad = false;
    private long lastClickTime = 0;
    private String cacheImg;
    private Bitmap bmp;
    private boolean isRelease=false;


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Log.v("播放视频==", "setUserVisibleHint-1=" + isVisibleToUser);
            initView();
        } else {
            if (iv_cover_photo != null && player_empty_room != null && !TextUtils.isEmpty(cacheImg)) {
                Log.v("播放视频==", "setUserVisibleHint-1=执行隐藏功能");
                flEmpty.setVisibility(View.VISIBLE);

            }

        }
    }

    private void playVideoShow() {
            LogUtil.v("cacheImg","准备播放视频=="+cacheImg);
            player_empty_room.setLooping(true);
            player_empty_room.setUp(mData.gettUserVideoShow().getVideoUrl(), true, "123");
            player_empty_room.startPlayLogic();
            player_empty_room.setVideoAllCallBack(new GSYSampleCallBack() {
                //加载成功，
                @Override
                public void onPrepared(String url, Object... objects) {
                    super.onPrepared(url, objects);
                    if((!getUserVisibleHint()||getActivityPosition()!=2)&&player_empty_room != null)
                        GSYVideoManager.instance().onPause();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(flEmpty!=null)
                                flEmpty.setVisibility(View.GONE);
                            LogUtil.v("cacheImg","开始播放视频=="+cacheImg);
                            Log.v("播放视频==", "加载成功progressEmpty=");
                        }
                    },200);


                }
            });
    }
    private int getActivityPosition() {
        MainActivity mainActivity = (MainActivity) getActivity();
        return mainActivity.getPosition();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() && getActivityPosition() == 2) {
            Log.v("播放视频==", "onResume-=");
            if(isRelease){
                isRelease=false;
                initViews();
            }else {
                playVideoShow();
            }

        }
    }

    @Override
    public void onPause() {
        if (getUserVisibleHint()&&getActivityPosition() == 2) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.v("播放视频==", "onPause-=");
                    GSYVideoManager.instance().onPause();
                    GSYVideoManager.instance().setNeedMute(true);
                    if(player_empty_room!=null)
                        player_empty_room.onVideoPause();
                    isRelease=true;
                }
            },200);

        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isLoad = false;
        isInit = false;
        unbinder.unbind();
    }

    @Override
    protected void initViews() {
        Log.v("播放视频==", "initViews-=");
////                判断奇偶测试，是否隐藏送礼物按钮
//        String abTest = DataPreference.getABTest();
//        if (!TextUtils.isEmpty(abTest)) {
//            if (abTest.equals("1")) {
//                String account = UserPreference.getAccount();
//                String c = account.charAt(account.length() - 1) + "";
//                boolean b = Integer.parseInt(c) % 2 == 0;
//                if ((!UserPreference.isVip()) && (b)) {
//                    ivSendGift.setVisibility(View.GONE);
//                }
//            } else if (abTest.equals("3")) {
//                if (!UserPreference.isVip()) {
//                    ivSendGift.setVisibility(View.GONE);
//                }
//            }
//        } else {
            if (!UserPreference.isVip()) {
                ivSendGift.setVisibility(View.GONE);
            }
//        }


        isInit = true;
        if (mData != null) {
            String thumbnailUrl = mData.gettUserVideoShow().getThumbnailUrl();
            String iconUrl = mData.gettUserVideoShow().getVideoUrl();
            if (!TextUtils.isEmpty(iconUrl)) {
                cacheImg = SharedPreferenceUtil.getStringValue(mContext, "cacheImg", iconUrl, "url");
                LogUtil.v("cacheImg","获取视频第一帧=="+cacheImg);
                if (!cacheImg.equals("url")) {
                    bmp = BitmapFactory.decodeFile(cacheImg);

                    iv_cover_photo.setImageBitmap(bmp);
                    progressEmpty.setVisibility(View.VISIBLE);
                    iv_cover_photo.setVisibility(View.VISIBLE);
                }else {
                     cacheImg = UserPreference.getCacheIg(thumbnailUrl);
                    if(!TextUtils.isEmpty(cacheImg)){
                        bmp = BitmapFactory.decodeFile(cacheImg);

                        iv_cover_photo.setImageBitmap(bmp);
                        progressEmpty.setVisibility(View.VISIBLE);
                        iv_cover_photo.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
        initView();
    }

    protected void initView() {
        if (!isInit) {
            return;
        }
        if (getUserVisibleHint()) {
            if (mData != null && mData.getUserBase() != null) {
                tvName.setText(mData.getUserBase().getNickName());
                tvAccount.setText(mData.getUserBase().getOwnWords());
                if (mData != null && mData.gettUserVideoShow() != null) {
                    if (mData.getIsFollow() == 1) {
                        iv_follow.setVisibility(View.GONE);
                    } else {
                        iv_follow.setVisibility(View.VISIBLE);
                    }
                    String cacheIg = UserPreference.getCacheIg(mData.getUserBase().getIconUrlMininum());
                    if(!TextUtils.isEmpty(cacheIg)){
                        Bitmap bitmap = BitmapFactory.decodeFile(cacheIg);
                        Bitmap bitmap1 = setCircle(bitmap);
                        if(bitmap1!=null)
                          ivAvatar.setImageBitmap(bitmap1);
                    }else {
                        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder()
                                .url(mData.getUserBase().getIconUrlMininum())
                                .transform(new CropCircleTransformation(mContext))
                                .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(ivAvatar).build());
                    }

                    playVideoShow();
                    isLoad = true;
                }
            }
        }
    }

    private Bitmap setCircle(Bitmap resource) {
        if(resource!=null){
            //获取图片的宽度
            int width = resource.getWidth();
            Paint paint = new Paint();
            //设置抗锯齿
            paint.setAntiAlias(true);

            //创建一个与原bitmap一样宽度的正方形bitmap
            Bitmap circleBitmap = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888);
            //以该bitmap为低创建一块画布
            Canvas canvas = new Canvas(circleBitmap);
            //以（width/2, width/2）为圆心，width/2为半径画一个圆
            canvas.drawCircle(width/2, width/2, width/2, paint);
            //设置画笔为取交集模式
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            //裁剪图片
            canvas.drawBitmap(resource, 0, 0, paint);
            return circleBitmap;
        }
        return null;



    }

    private boolean isUserBase(){
        if(mData!=null&&mData.getUserBase()!=null){
            return true;
        }
        return false;
    }



    @Override
    protected void setListeners() {
        if (!UserPreference.isVip()&&isUserBase()) {
            rlVideo.setVisibility(View.GONE);
            boolean isSayHello = SharedPreferenceUtil.getBooleanValue(mContext, String.valueOf(mData.getUserBase().getGuid()), UserPreference.getId(), false);
            if (mData.getIsSayHello() == 1 || isSayHello) {
                tvChat.setText("已打招呼");
                llChat.setClickable(false);
            } else {
                tvChat.setText("打招呼");
                llChat.setClickable(true);
                llChat.setOnClickListener(this);
            }

        } else {
            llChat.setOnClickListener(this);
            llChat.setClickable(true);
            tvChat.setText("聊天");
        }
        rlAvatar.setOnClickListener(this);
        rlVideo.setOnClickListener(this);
        ivSendGift.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }


    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_video_show;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    public void setData(VideoSquare t) {
        this.mData = t;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_avatar://头像
                if (!UserPreference.isMale()) {
                    ToastUtil.showShortToast(mContext, "快去上传视频，提高曝光");
                } else {
                    if (mData == null) {
                        return;
                    }
                    if (UserPreference.isAnchor()) {
                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                        return;
                    }
                    if (mData.getIsFollow() == 1&&isUserBase()) {//已关注，进入到详情
                        LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                                new UserDetailParcelable(String.valueOf(mData.getUserBase().getGuid())));
                        Util.setDetailMsgFlag(String.valueOf(mData.getUserBase().getGuid()),"8");
                    } else {//未关注，直接关注
                        follow();
                    }
                }

                break;
            case R.id.rl_video://视频
                if (!UserPreference.isMale()) {
                    ToastUtil.showShortToast(mContext, "快去上传视频，提高曝光");
                } else {
                    if (System.currentTimeMillis() - lastClickTime > 1000) {
                        lastClickTime = System.currentTimeMillis();
                        if (UserPreference.isAnchor()) {
                            ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                            return;
                        }
                        goToVideoInvite();
                        if (UserPreference.isVip()) {//付费
                            getClickNum(String.valueOf(mData.getUserBase().getGuid()), "1", "12");
                        } else {//未付费
                            getClickNum(String.valueOf(mData.getUserBase().getGuid()), "2", "12");
                        }
                    }
                }

                break;
            case R.id.ll_chat://聊天
                if (!UserPreference.isMale()||mData.getUserBase()==null) {
                    ToastUtil.showShortToast(mContext, "快去上传视频，提高曝光");
                } else {
                    if (rlVideo.getVisibility() == View.GONE) {
//                    调打招呼接口
                        tvChat.setText("已打招呼");
                        SharedPreferenceUtil.setBooleanValue(mContext, String.valueOf(mData.getUserBase().getGuid()), UserPreference.getId(), true);
                        ToastUtil.showShortToast(mContext, "打招呼成功");
                        llChat.setClickable(false);
                        ApiManager.sayHello(String.valueOf(mData.getUserBase().getGuid()), "1", new IGetDataListener<BaseModel>() {
                            @Override
                            public void onResult(BaseModel baseModel, boolean isEmpty) {

                            }

                            @Override
                            public void onError(String msg, boolean isNetworkError) {

                            }
                        });
                    } else {
                        if (System.currentTimeMillis() - lastClickTime > 1000) {
                            lastClickTime = System.currentTimeMillis();
                            if (UserPreference.isAnchor()) {
                                ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                                return;
                            }

                            //跳转写信的界面
                            goToWriteMessage(true);
                            if (UserPreference.isVip()) {//付费
                                getClickNum(String.valueOf(mData.getUserBase().getGuid()), "1", "12");
                            } else {//未付费
                                getClickNum(String.valueOf(mData.getUserBase().getGuid()), "2", "14");
                            }
                        }
                    }

                }

                break;
            case R.id.iv_send_gift://送礼物
                if (!UserPreference.isMale()) {
                    ToastUtil.showShortToast(mContext, "快去上传视频，提高曝光");
                } else {
                    if (System.currentTimeMillis() - lastClickTime > 1000) {
                        lastClickTime = System.currentTimeMillis();
                        if (UserPreference.isAnchor()) {
                            ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                            return;
                        }
                        if(mData.getUserBase()!=null) {
                            CustomDialogAboutOther.giveGiftShow(mContext, mData.getUserBase().getGuid() + "", mData.getUserBase().getAccount(), mData.getUserBase().getIconUrlMininum(), mData.getUserBase().getNickName(), 1, false, 2);
                            if (UserPreference.isVip()) {//付费
                                getClickNum(String.valueOf(mData.getUserBase().getGuid()), "1", "14");
                            } else {//未付费
                                getClickNum(String.valueOf(mData.getUserBase().getGuid()), "2", "14");
                            }
                        }

                    }
                }

                break;
        }
    }

    private void follow() {
        if(mData.getUserBase()!=null){
            ApiManager.follow(mData.getUserBase().getGuid() + "", new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel baseModel, boolean isEmpty) {
                    EventBus.getDefault().post(new IsFollow(mData.getUserBase().getAccount()));
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                    ToastUtil.showShortToast(mContext, mContext.getString(R.string.follow_fail));
                }
            });
        }

    }

    private void goToVideoInvite() {
        if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
            getStatus();
        } else {
            if (UserPreference.getStatus().equals("3")) {
                Toast.makeText(mContext, mContext.getString(R.string.quiet_hours_me), Toast.LENGTH_SHORT).show();
            } else {
                if (mData.getUserBase() != null) {
                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, mData.getUserBase().getGuid(), mData.getUserBase().getAccount()
                            , mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0, 0), mContext, "1");
                }
            }
        }
    }

    private void getStatus() {
        ApiManager.getAuthorCheckStatus(new IGetDataListener<CheckStatus>() {
            @Override
            public void onResult(CheckStatus checkStatus, boolean isEmpty) {
                if (checkStatus != null) {
                    if (checkStatus.getAuditStatus() == 0) {//审核中
                        if (checkStatus.getShowStatus() == 1) {//完整
                            Toast.makeText(mContext, "认证审核中.....", Toast.LENGTH_SHORT).show();
                        } else if (checkStatus.getShowStatus() == -1) {//不完整
                            AlertDialog.showNoCanceled(getFragmentManager(), "", "视频聊天需要通过真人认证哦",
                                    "去认证", "取消", new OnDialogClickListener() {
                                        @Override
                                        public void onNegativeClick(View view) {
                                        }

                                        @Override
                                        public void onPositiveClick(View view) {
                                            LaunchHelper.getInstance().launch(mContext, VideoShowActivity.class, new VideoShowParcelable("", "", ""));
                                        }
                                    }
                            );
                        }
                    } else {
                        AlertDialog.showNoCanceled(getFragmentManager(), "", "视频聊天需要通过真人认证哦",
                                "去认证", "取消", new OnDialogClickListener() {
                                    @Override
                                    public void onNegativeClick(View view) {
                                    }

                                    @Override
                                    public void onPositiveClick(View view) {
                                        LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                                    }
                                }
                        );
                    }

                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    private void goToWriteMessage(boolean b) {
        if (b) {
            if (mData.getUserBase() != null) {
                if (UserPreference.isAnchor()) {
                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
                            mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));
                } else if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
                    getStatus();
                } else {
                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
                            mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));

//                if (UserPreference.isVip() || UserPreference.isAnchor()) {
//                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
//                            mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));
//                } else {
//                    LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON, 0, 7));
//
//                }
                }
            }
        } else {
            if (mData.getUserBase() != null) {
                if (UserPreference.isAnchor()) {
//                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
//                            mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));
                } else if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
                    getStatus();
                } else {
//                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
//                            mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));

//                if (UserPreference.isVip() || UserPreference.isAnchor()) {
//                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
//                            mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));
//                } else {
//                    LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON, 0, 7));
//
//                }
                }
            }
        }

    }

    @Subscribe
    public void onEvent(IsFollow follow) {
        if (mData.getUserBase()!=null&&follow.getAccount().equals(mData.getUserBase().getAccount())) {
            mData.setIsFollow(mData.getIsFollow() == 1 ? 0 : 1);
            if (mData.getIsFollow() == 1) {
                iv_follow.setVisibility(View.GONE);
            } else {
                iv_follow.setVisibility(View.VISIBLE);
            }
        }
    }

    @Subscribe
    public void onEvent(UpdataFollowUser follow) {

        if (mData.getUserBase()!=null&&follow.getGuid().equals(String.valueOf(mData.getUserBase().getGuid()))) {
            mData.setIsFollow(mData.getIsFollow() == 1 ? 0 : 1);
            if (mData.getIsFollow() == 1) {
                iv_follow.setVisibility(View.GONE);
            } else {
                iv_follow.setVisibility(View.VISIBLE);
            }
        }
    }

    @Subscribe
    public void onEvent(GiftSendEvent giftSendEvent) {
        if (getUserVisibleHint() && player_empty_room.isInPlayingState()) {
            goToWriteMessage(false);
        }
    }

    @Subscribe
    public void onEvent(StartEvent start) {
        if (getUserVisibleHint()) {
            GSYVideoManager.instance().onResume();
        }
    }
    @Subscribe
    public void onEvent(StartVideoEvent start) {
        if (getUserVisibleHint()&&getActivityPosition()==2) {
            if(player_empty_room!=null)
                player_empty_room.release();
            initViews();
            GSYVideoManager.instance().onResume();
        }
    }


    @Subscribe
    public void onEvent(PauseEvent pause) {
        if (player_empty_room != null) {
            GSYVideoManager.instance().onPause();
        }
    }


    //为点击事件埋点
    private void getClickNum(String remoteId, String extendTag, String baseTag) {
        ApiManager.userActivityTag(UserPreference.getId(), remoteId, baseTag, extendTag, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }
    public class getVideoCacheAsyncTask extends AsyncTask<String, Void, Bitmap> {
        private final Context context;
        private String imgUrl;

        public getVideoCacheAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            imgUrl =  params[0];
            //MediaMetadataRetriever 是android中定义好的一个类，提供了统一
            //的接口，用于从输入的媒体文件中取得帧和元数据；
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            try {
                //根据文件路径获取缩略图
                retriever.setDataSource(imgUrl, new HashMap());
                //获得第一帧图片
                return retriever.getFrameAtTime();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } finally {
                retriever.release();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result == null) {
                return;
            }
            String path = saveBitmap(mContext, result);
            SharedPreferenceUtil.setStringValue(context,"cacheImg",imgUrl,path);
            LogUtil.v("cacheImg","视频第一帧成功=="+path);


//            Bitmap bmp= BitmapFactory.decodeFile(path);
//            img.setImageBitmap(bmp);

        }
    }
    private static final String SD_PATH = "/sdcard/dskqxt/pic/";
    private static final String IN_PATH = "/dskqxt/pic/";

    /**
     * 随机生产文件名
     *
     * @return
     */
    private static String generateFileName() {
        return UUID.randomUUID().toString();
    }
    /**
     * 保存bitmap到本地
     *
     * @param context
     * @param mBitmap
     * @return
     */
    public static String saveBitmap(Context context, Bitmap mBitmap) {
        String savePath;
        File filePic;
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            savePath = SD_PATH;
        } else {
            savePath = context.getApplicationContext().getFilesDir()
                    .getAbsolutePath()
                    + IN_PATH;
        }
        try {
            filePic = new File(savePath + generateFileName() + ".jpg");
            if (!filePic.exists()) {
                filePic.getParentFile().mkdirs();
                filePic.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(filePic);
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

        return filePic.getPath();
    }

}







//import android.content.Context;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.media.MediaPlayer;
//import android.net.Uri;
//import android.os.Bundle;
//import android.os.Parcelable;
//import android.text.TextUtils;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.WindowManager;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ProgressBar;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.wiscomwis.facetoface.R;
//import com.wiscomwis.facetoface.base.BaseFragment;
//import com.wiscomwis.facetoface.common.CustomDialogAboutOther;
//import com.wiscomwis.facetoface.common.Util;
//import com.wiscomwis.facetoface.common.VideoHelper;
//import com.wiscomwis.facetoface.data.api.ApiManager;
//import com.wiscomwis.facetoface.data.api.IGetDataListener;
//import com.wiscomwis.facetoface.data.model.BaseModel;
//import com.wiscomwis.facetoface.data.model.CheckStatus;
//import com.wiscomwis.facetoface.data.model.VideoSquare;
//import com.wiscomwis.facetoface.data.preference.DataPreference;
//import com.wiscomwis.facetoface.data.preference.UserPreference;
//import com.wiscomwis.facetoface.event.GiftSendEvent;
//import com.wiscomwis.facetoface.event.IsFollow;
//import com.wiscomwis.facetoface.event.OpenPhotoEvent;
//import com.wiscomwis.facetoface.event.PauseEvent;
//import com.wiscomwis.facetoface.event.StartEvent;
//import com.wiscomwis.facetoface.event.UpdataFollowUser;
//import com.wiscomwis.facetoface.parcelable.ChatParcelable;
//import com.wiscomwis.facetoface.parcelable.UserDetailParcelable;
//import com.wiscomwis.facetoface.parcelable.VideoInviteParcelable;
//import com.wiscomwis.facetoface.parcelable.VideoShowParcelable;
//import com.wiscomwis.facetoface.ui.chat.ChatActivity;
//import com.wiscomwis.facetoface.ui.detail.UserDetailActivity;
//import com.wiscomwis.facetoface.ui.main.MainActivity;
//import com.wiscomwis.facetoface.ui.personalcenter.AuthenticationActivity;
//import com.wiscomwis.facetoface.ui.personalcenter.VideoShowActivity;
//import com.wiscomwis.library.dialog.AlertDialog;
//import com.wiscomwis.library.dialog.OnDialogClickListener;
//import com.wiscomwis.library.image.CropCircleTransformation;
//import com.wiscomwis.library.image.ImageLoader;
//import com.wiscomwis.library.image.ImageLoaderUtil;
//import com.wiscomwis.library.util.LaunchHelper;
//import com.wiscomwis.library.util.SharedPreferenceUtil;
//import com.wiscomwis.library.util.ToastUtil;
//import com.wiscomwis.library.widget.PlayView;
//
//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.Subscribe;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
//
///**
// * 待整理，优化
// * Created by xuzhaole on 2018/3/20.
// */
//
//public class VideoRoomFragment extends BaseFragment implements View.OnClickListener {
//    @BindView(R.id.player_empty_room)
//    PlayView playView;
//    @BindView(R.id.iv_avatar)
//    ImageView ivAvatar;
//    @BindView(R.id.rl_avatar)
//    RelativeLayout rlAvatar;
//    @BindView(R.id.ll_chat)
//    LinearLayout llChat;
//    @BindView(R.id.iv_video)
//    ImageView ivVideo;
//    @BindView(R.id.rl_video)
//    RelativeLayout rlVideo;
//    @BindView(R.id.iv_send_gift)
//    ImageView ivSendGift;
//    @BindView(R.id.tv_name)
//    TextView tvName;
//    @BindView(R.id.tv_account)
//    TextView tvAccount;
//    @BindView(R.id.iv_cover_photo)
//    ImageView iv_cover_photo;
//    @BindView(R.id.iv_follow)
//    ImageView iv_follow;
//    @BindView(R.id.tv_chat)
//    TextView tvChat;
//    Unbinder unbinder;
//    @BindView(R.id.iv_bg_show)
//    ImageView ivBgShow;
//    @BindView(R.id.progress_empty)
//    ProgressBar progressEmpty;
//    private VideoSquare mData;
//    protected boolean isInit = false;
//    protected boolean isLoad = false;
//    private long lastClickTime = 0;
//    private long playPostion = -1;
//    private long duration = -1;
//
//    public VideoRoomFragment(int position) {
//
//    }
////    private PowerManager.WakeLock wakeLock;
//
//
//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if(isVisibleToUser&&getActivityPosition()==2){
//            startVideo("setUserVisibleHint");
//        }else {
//            pauseVideo();
//        }
//
//    }
//
//    private void playVideoShow() {
//        if (!TextUtils.isEmpty(mData.gettUserVideoShow().getVideoUrl())) {
//            playView.setVideoURI(Uri.parse(mData.gettUserVideoShow().getVideoUrl()));
//            playView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                @Override
//                public void onCompletion(MediaPlayer mp) {
//                    playView.seekTo(1);
//                    if(getUserVisibleHint()&&getActivityPosition()==2){
//                        startVideo("setOnCompletionListener");
//                    }else {
//                        pauseVideo();
//                    }
//                }
//            });
//            playView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                @Override
//                public void onPrepared(MediaPlayer mp) {
//                    mp.setOnInfoListener(new MediaPlayer.OnInfoListener() {
//                        @Override
//                        public boolean onInfo(MediaPlayer mp, int what, int extra) {
//                            if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
//                                progressEmpty.setVisibility(View.GONE);
//                                iv_cover_photo.setVisibility(View.GONE);
//                                ivBgShow.setVisibility(View.GONE);
//                                if(getUserVisibleHint()&&getActivityPosition()==2){
//                                    startVideo("setOnPreparedListener");
//                                }else {
//                                    pauseVideo();
//                                }
//                            }
//                            return true;
//                        }
//                    });
//                    WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
//                    DisplayMetrics dm = new DisplayMetrics();
//                    wm.getDefaultDisplay().getMetrics(dm);
//
//                    playView.setSizeH(dm.heightPixels);
//                    playView.setSizeW(dm.widthPixels);
//                    playView.requestLayout();
//                    duration = mp.getDuration();
//                }
//            });
////            play();
//        }
//    }
//    private void startVideo(String log) {
//        if(playView!=null&&!playView.isPlaying()&&getActivityPosition()==2){
//            String url="0000";
//            if(mData!=null&&mData.gettUserVideoShow()!=null){
//                url=mData.gettUserVideoShow().getVideoUrl();
//            }
//            Log.v("播放视频：：","开始播放="+log+"=="+url);
//            playView.start();
//        }
//
//
//    }
//
//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//        if(hidden){
//            pauseVideo();
//        }else {
//            startVideo("onHiddenChanged");
//        }
//    }
//
//    /**
//     * 播放
//     */
//    private void play() {
//        if (playView.isPlaying()) {
//            pauseVideo();
//        } else {
//            if (playView.getCurrentPosition() == playView.getDuration()) {
//                playView.seekTo(0);
//            }
//            startVideo("play");
//        }
//    }
//    private void pauseVideo() {
//        if(playView!=null){
//            Log.v("播放视频：：","------暂停--------");
//            playView.pause();
//
//        }
//    }
//
//    private int getActivityPosition() {
//        MainActivity mainActivity = (MainActivity) getActivity();
//        if(mainActivity!=null){
//            return mainActivity.getPosition();
//        }else {
//            return 0;
//        }
//
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        if(getActivityPosition() == 2){
//            if(getUserVisibleHint()){
//                startVideo("onResume");
//            }else {
//                pauseVideo();
//            }
//        }
////        if (getUserVisibleHint() && getActivityPosition() == 2) {
//
////        startVideo();
//    }
//
//    @Override
//    public void onPause() {
////        if (getUserVisibleHint() && getActivityPosition() == 2) {
////            GSYVideoManager.instance().onPause();
////        }
//        pauseVideo();
//        super.onPause();
//
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        isLoad = false;
//        isInit = false;
//        unbinder.unbind();
//        if(playView!=null)
//        playView.stopPlayback();
////        //释放锁，屏幕熄灭。
////        wakeLock.release();
//    }
//
//    @Override
//    protected void initViews() {
////                判断奇偶测试，是否隐藏送礼物按钮
//        String abTest = DataPreference.getABTest();
//        if (!TextUtils.isEmpty(abTest)) {
//            if (abTest.equals("1")) {
//                String account = UserPreference.getAccount();
//                String c = account.charAt(account.length() - 1) + "";
//                boolean b = Integer.parseInt(c) % 2 == 0;
//                if ((!UserPreference.isVip()) && (b)) {
//                    ivSendGift.setVisibility(View.GONE);
//                }
//            } else if (abTest.equals("3")) {
//                if (!UserPreference.isVip()) {
//                    ivSendGift.setVisibility(View.GONE);
//                }
//            }
//        } else {
//            if (!UserPreference.isVip()) {
//                ivSendGift.setVisibility(View.GONE);
//            }
//        }
//
//
//        isInit = true;
////        //        屏幕持续点亮
////        PowerManager pm = (PowerManager) getActivity().getSystemService(Context.POWER_SERVICE);
////        wakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "TAG");
//////屏幕会持续点亮
////        wakeLock.acquire();
//        initView();
//    }
//
//    protected void initView() {
//        if (!isInit) {
//            return;
//        }
////        if (getUserVisibleHint()) {
//            if (mData != null && mData.getUserBase() != null) {
//                tvName.setText(mData.getUserBase().getNickName());
//                tvAccount.setText(mData.getUserBase().getOwnWords());
//            }
//            if (mData != null && mData.gettUserVideoShow() != null) {
//                if (mData.getIsFollow() == 1) {
//                    iv_follow.setVisibility(View.GONE);
//                } else {
//                    iv_follow.setVisibility(View.VISIBLE);
//                }
//                ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder()
//                        .url(mData.getUserBase().getIconUrlMininum())
//                        .transform(new CropCircleTransformation(mContext))
//                        .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(ivAvatar).build());
//                String iconUrl = mData.gettUserVideoShow().getIconUrl();
//                if(!TextUtils.isEmpty(iconUrl)){
//                    String cacheImg = SharedPreferenceUtil.getStringValue(mContext, "cacheImg", iconUrl, "url");
//                    if(!cacheImg.equals("url")){
//                        Bitmap bmp= BitmapFactory.decodeFile(cacheImg);
//                        iv_cover_photo.setImageBitmap(bmp);
//                        ivBgShow.setVisibility(View.GONE);
//                        progressEmpty.setVisibility(View.GONE);
//                    }
//
////                        player_empty_room.setPhoto(cacheImg);
////                    ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder()
////                            .url(mData.gettUserVideoShow().getIconUrl())
////                            .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_cover_photo).build());
////                    iv_cover_photo.setVisibility(View.VISIBLE);
////                    new Handler().postDelayed(new Runnable() {
////                        @Override
////                        public void run() {
////                            iv_cover_photo.setVisibility(View.GONE);
////                        }
////                    },3000);
//                }
//
//
////                playVideoShow();
//                if (getActivityPosition() == 2) {
//                    playVideoShow();
//                    playView.seekTo((int) ((playPostion > 0 && playPostion < duration) ? playPostion : 1));
//
//                }
//
//                isLoad = true;
//            }
////        }
//    }
//
//
//    @Override
//    protected void setListeners() {
//        if (!UserPreference.isVip()) {
//            rlVideo.setVisibility(View.GONE);
//            boolean isSayHello = SharedPreferenceUtil.getBooleanValue(mContext, String.valueOf(mData.getUserBase().getGuid()), "isSayHello", false);
//            if (mData.getIsSayHello() == 1 || isSayHello) {
//                tvChat.setText("已打招呼");
//                llChat.setClickable(false);
//            } else {
//                tvChat.setText("打招呼");
//                llChat.setClickable(true);
//                llChat.setOnClickListener(this);
//
//            }
//
//        } else {
//            llChat.setOnClickListener(this);
//            llChat.setClickable(true);
//            tvChat.setText("聊天");
//        }
//        rlAvatar.setOnClickListener(this);
//        rlVideo.setOnClickListener(this);
//        ivSendGift.setOnClickListener(this);
//    }
//
//    @Override
//    protected void loadData() {
//
//    }
//
//
//    @Override
//    protected int getLayoutResId() {
//        return R.layout.fragment_video_show;
//    }
//
//    @Override
//    protected boolean isRegistEventBus() {
//        return true;
//    }
//
//    @Override
//    protected View getNoticeView() {
//        return null;
//    }
//
//    @Override
//    protected void getArgumentParcelable(Parcelable parcelable) {
//
//    }
//
//    public void setData(VideoSquare t) {
//        this.mData = t;
//    }
//
//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.rl_avatar://头像
//                if (!UserPreference.isMale()) {
//                    ToastUtil.showShortToast(mContext, "快去上传视频，提高曝光");
//                } else {
//                    if (mData == null) {
//                        return;
//                    }
//                    if (UserPreference.isAnchor()) {
//                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
//                        return;
//                    }
//                    if (mData.getIsFollow() == 1) {//已关注，进入到详情
//                        LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
//                                new UserDetailParcelable(String.valueOf(mData.getUserBase().getGuid())));
//                    } else {//未关注，直接关注
//                        follow();
//                    }
//                }
//
//                break;
//            case R.id.rl_video://视频
//                if (!UserPreference.isMale()) {
//                    ToastUtil.showShortToast(mContext, "快去上传视频，提高曝光");
//                } else {
//                    if (System.currentTimeMillis() - lastClickTime > 1000) {
//                        lastClickTime = System.currentTimeMillis();
//                        if (UserPreference.isAnchor()) {
//                            ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
//                            return;
//                        }
//                        goToVideoInvite();
//                        if (UserPreference.isVip()) {//付费
//                            getClickNum(String.valueOf(mData.getUserBase().getGuid()), "1", "12");
//                        } else {//未付费
//                            getClickNum(String.valueOf(mData.getUserBase().getGuid()), "2", "12");
//                        }
//                    }
//                }
//
//                break;
//            case R.id.ll_chat://聊天
//                if (!UserPreference.isMale()) {
//                    ToastUtil.showShortToast(mContext, "快去上传视频，提高曝光");
//                } else {
//                    if (rlVideo.getVisibility() == View.GONE) {
////                    调打招呼接口
//                        tvChat.setText("已打招呼");
//                        SharedPreferenceUtil.setBooleanValue(mContext, String.valueOf(mData.getUserBase().getGuid()), "isSayHello", true);
//                        ToastUtil.showShortToast(mContext, "打招呼成功");
//                        llChat.setClickable(false);
//                        ApiManager.sayHello(String.valueOf(mData.getUserBase().getGuid()), "1", new IGetDataListener<BaseModel>() {
//                            @Override
//                            public void onResult(BaseModel baseModel, boolean isEmpty) {
//
//                            }
//
//                            @Override
//                            public void onError(String msg, boolean isNetworkError) {
//
//                            }
//                        });
//                    } else {
//                        if (System.currentTimeMillis() - lastClickTime > 1000) {
//                            lastClickTime = System.currentTimeMillis();
//                            if (UserPreference.isAnchor()) {
//                                ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
//                                return;
//                            }
//
//                            //跳转写信的界面
//                            goToWriteMessage(true);
//                            if (UserPreference.isVip()) {//付费
//                                getClickNum(String.valueOf(mData.getUserBase().getGuid()), "1", "12");
//                            } else {//未付费
//                                getClickNum(String.valueOf(mData.getUserBase().getGuid()), "2", "14");
//                            }
//                        }
//                    }
//
//                }
//
//                break;
//            case R.id.iv_send_gift://送礼物
//                if (!UserPreference.isMale()) {
//                    ToastUtil.showShortToast(mContext, "快去上传视频，提高曝光");
//                } else {
//                    if (System.currentTimeMillis() - lastClickTime > 1000) {
//                        lastClickTime = System.currentTimeMillis();
//                        if (UserPreference.isAnchor()) {
//                            ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
//                            return;
//                        }
//                        CustomDialogAboutOther.giveGiftShow(mContext, mData.getUserBase().getGuid() + "", mData.getUserBase().getAccount(), mData.getUserBase().getIconUrlMininum(), mData.getUserBase().getNickName(), 1, false, 2);
//                        if (UserPreference.isVip()) {//付费
//                            getClickNum(String.valueOf(mData.getUserBase().getGuid()), "1", "14");
//                        } else {//未付费
//                            getClickNum(String.valueOf(mData.getUserBase().getGuid()), "2", "14");
//                        }
//
//                    }
//                }
//
//                break;
//        }
//    }
//
//    private void follow() {
//        ApiManager.follow(mData.getUserBase().getGuid() + "", new IGetDataListener<BaseModel>() {
//            @Override
//            public void onResult(BaseModel baseModel, boolean isEmpty) {
//                EventBus.getDefault().post(new IsFollow(mData.getUserBase().getAccount()));
//            }
//
//            @Override
//            public void onError(String msg, boolean isNetworkError) {
//                ToastUtil.showShortToast(mContext, mContext.getString(R.string.follow_fail));
//            }
//        });
//    }
//
//    private void goToVideoInvite() {
//        if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
//            getStatus();
//        } else {
//            if (UserPreference.getStatus().equals("3")) {
//                Toast.makeText(mContext, mContext.getString(R.string.quiet_hours_me), Toast.LENGTH_SHORT).show();
//            } else {
//                if (mData.getUserBase() != null) {
//                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, mData.getUserBase().getGuid(), mData.getUserBase().getAccount()
//                            , mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0, 0), mContext, "1");
//                }
//            }
//        }
//    }
//
//    private void getStatus() {
//        ApiManager.getAuthorCheckStatus(new IGetDataListener<CheckStatus>() {
//            @Override
//            public void onResult(CheckStatus checkStatus, boolean isEmpty) {
//                if (checkStatus != null) {
//                    if (checkStatus.getAuditStatus() == 0) {//审核中
//                        if (checkStatus.getShowStatus() == 1) {//完整
//                            Toast.makeText(mContext, "认证审核中.....", Toast.LENGTH_SHORT).show();
//                        } else if (checkStatus.getShowStatus() == -1) {//不完整
//                            AlertDialog.showNoCanceled(getFragmentManager(), "", "视频聊天需要通过真人认证哦",
//                                    "去认证", "取消", new OnDialogClickListener() {
//                                        @Override
//                                        public void onNegativeClick(View view) {
//                                        }
//
//                                        @Override
//                                        public void onPositiveClick(View view) {
//                                            LaunchHelper.getInstance().launch(mContext, VideoShowActivity.class, new VideoShowParcelable("", "", ""));
//                                        }
//                                    }
//                            );
//                        }
//                    } else {
//                        AlertDialog.showNoCanceled(getFragmentManager(), "", "视频聊天需要通过真人认证哦",
//                                "去认证", "取消", new OnDialogClickListener() {
//                                    @Override
//                                    public void onNegativeClick(View view) {
//                                    }
//
//                                    @Override
//                                    public void onPositiveClick(View view) {
//                                        LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
//                                    }
//                                }
//                        );
//                    }
//
//                }
//            }
//
//            @Override
//            public void onError(String msg, boolean isNetworkError) {
//
//            }
//        });
//    }
//
//    private void goToWriteMessage(boolean b) {
//        if (b) {
//            if (mData.getUserBase() != null) {
//                if (UserPreference.isAnchor()) {
//                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
//                            mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));
//                } else if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
//                    getStatus();
//                } else {
//                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
//                            mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));
//
////                if (UserPreference.isVip() || UserPreference.isAnchor()) {
////                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
////                            mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));
////                } else {
////                    LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON, 0, 7));
////
////                }
//                }
//            }
//        } else {
//            if (mData.getUserBase() != null) {
//                if (UserPreference.isAnchor()) {
////                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
////                            mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));
//                } else if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
//                    getStatus();
//                } else {
////                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
////                            mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));
//
////                if (UserPreference.isVip() || UserPreference.isAnchor()) {
////                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
////                            mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));
////                } else {
////                    LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON, 0, 7));
////
////                }
//                }
//            }
//        }
//
//    }
//
//    @Subscribe
//    public void onEvent(IsFollow follow) {
//        if (follow.getAccount().equals(mData.getUserBase().getAccount())) {
//            mData.setIsFollow(mData.getIsFollow() == 1 ? 0 : 1);
//            if (mData.getIsFollow() == 1) {
//                iv_follow.setVisibility(View.GONE);
//            } else {
//                iv_follow.setVisibility(View.VISIBLE);
//            }
//        }
//    }
//
//    @Subscribe
//    public void onEvent(UpdataFollowUser follow) {
//        if (follow.getGuid().equals(String.valueOf(mData.getUserBase().getGuid()))) {
//            mData.setIsFollow(mData.getIsFollow() == 1 ? 0 : 1);
//            if (mData.getIsFollow() == 1) {
//                iv_follow.setVisibility(View.GONE);
//            } else {
//                iv_follow.setVisibility(View.VISIBLE);
//            }
//        }
//    }
//
//    @Subscribe
//    public void onEvent(GiftSendEvent giftSendEvent) {
//        if (getUserVisibleHint() && playView.isPlaying()) {
////            if (getUserVisibleHint() && player_empty_room.isInPlayingState()) {
//            goToWriteMessage(false);
//        }
//    }
//
//    @Subscribe
//    public void onEvent(StartEvent start) {
//        if (getUserVisibleHint()&&playView!=null) {
//            startVideo("StartEvent");
////            GSYVideoManager.instance().onResume();
//        }
//    }
//
//    @Subscribe
//    public void onEvent(OpenPhotoEvent start) {
//        iv_cover_photo.setVisibility(View.VISIBLE);
//    }
//
//    @Subscribe
//    public void onEvent(PauseEvent pause) {
//        pauseVideo();
////        if (player_empty_room != null) {
////            GSYVideoManager.instance().onPause();
////        }
//    }
//
//    //为点击事件埋点
//    private void getClickNum(String remoteId, String extendTag, String baseTag) {
//        ApiManager.userActivityTag(UserPreference.getId(), remoteId, baseTag, extendTag, new IGetDataListener<BaseModel>() {
//            @Override
//            public void onResult(BaseModel baseModel, boolean isEmpty) {
//
//            }
//
//            @Override
//            public void onError(String msg, boolean isNetworkError) {
//
//            }
//        });
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        // TODO: inflate a fragment view
//        View rootView = super.onCreateView(inflater, container, savedInstanceState);
//        unbinder = ButterKnife.bind(this, rootView);
//        return rootView;
//    }
//}
