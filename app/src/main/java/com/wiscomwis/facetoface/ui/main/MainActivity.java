package com.wiscomwis.facetoface.ui.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Ringtone;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.google.gson.Gson;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;
import com.meizu.cloud.pushsdk.PushManager;
import com.meizu.cloud.pushsdk.util.MzSystemUtils;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseAppCompatActivity;
import com.wiscomwis.facetoface.base.BaseApplication;
import com.wiscomwis.facetoface.common.AgoraHelper;
import com.wiscomwis.facetoface.common.CustomDialogAboutPay;
import com.wiscomwis.facetoface.common.HyphenateHelper;
import com.wiscomwis.facetoface.common.LocationUtil;
import com.wiscomwis.facetoface.common.TimeUtils;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.GoogleGPS;
import com.wiscomwis.facetoface.data.model.HuanXinUser;
import com.wiscomwis.facetoface.data.model.LocationInfo;
import com.wiscomwis.facetoface.data.model.NettyMessage;
import com.wiscomwis.facetoface.data.model.SearchUser;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.event.AgoraEvent;
import com.wiscomwis.facetoface.event.CancelVideoEvent;
import com.wiscomwis.facetoface.event.CloseRingtoneEvent;
import com.wiscomwis.facetoface.event.FinishEvent;
import com.wiscomwis.facetoface.event.FinishVideoToChatActivityEvent;
import com.wiscomwis.facetoface.event.FollowEvent;
import com.wiscomwis.facetoface.event.MiPushEvevnt;
import com.wiscomwis.facetoface.event.OpenVideoEvent;
import com.wiscomwis.facetoface.event.PauseEvent;
import com.wiscomwis.facetoface.event.RegisterAndLoginFinish;
import com.wiscomwis.facetoface.event.RingtoneEvent;
import com.wiscomwis.facetoface.event.SingleLoginFinishEvent;
import com.wiscomwis.facetoface.event.StartEvent;
import com.wiscomwis.facetoface.event.UnReadMsgEvent;
import com.wiscomwis.facetoface.event.UnreadMsgChangedEvent;
import com.wiscomwis.facetoface.event.UserInfoChangedEvent;
import com.wiscomwis.facetoface.event.UserTagVideoEvent;
import com.wiscomwis.facetoface.parcelable.ChatParcelable;
import com.wiscomwis.facetoface.parcelable.VideoInviteParcelable;
import com.wiscomwis.facetoface.receiver.HomeWatcherReceiver;
import com.wiscomwis.facetoface.service.CheckAcitivityService;
import com.wiscomwis.facetoface.ui.charmandrankinglist.VideoListFragment;
import com.wiscomwis.facetoface.ui.chat.ChatActivity;
import com.wiscomwis.facetoface.ui.dialog.DialogUtil;
import com.wiscomwis.facetoface.ui.dialog.OnDoubleDialogClickListener;
import com.wiscomwis.facetoface.ui.follow.FollowFragment;
import com.wiscomwis.facetoface.ui.homepage.HomepageFragment;
import com.wiscomwis.facetoface.ui.main.contract.MainActivityContract;
import com.wiscomwis.facetoface.ui.main.presenter.MainActivityPresenter;
import com.wiscomwis.facetoface.ui.message.MessageFragment;
import com.wiscomwis.facetoface.ui.personalcenter.PersonFragment;
import com.wiscomwis.facetoface.ui.video.VideoInviteActivity;
import com.wiscomwis.facetoface.ui.video.VideoInviteActivity2;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.DeviceUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.NotificationsUtils;
import com.wiscomwis.library.widget.tab.ItemBadge;
import com.wiscomwis.library.widget.tab.OnTabSelectedListener;
import com.wiscomwis.library.widget.tab.TabLayout;
import com.wiscomwis.library.widget.tab.TabView;
import com.xiaomi.mipush.sdk.MiPushClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 主Activity，包含所有tab
 * Created by zhangdroid on 2017/5/12.
 */
public class MainActivity extends BaseAppCompatActivity implements MainActivityContract.IView {
    public static final boolean FROM_NOTIFICATION = false;
    private static final int UNREAD_MSG_EVENT = 0;
    private static final int CANCEL_VIDEO_INVITE = 1;
    private static final int FINISH_VIDEO_TO_CHAT = 2;
    private static HomeWatcherReceiver mHomeKeyReceiver;
    @BindView(R.id.main_tab)
    TabLayout mTabLayout;
    @BindView(R.id.main_activity_tv_tab)
    TextView tv_tab;
    @BindView(R.id.rl_all)
    RelativeLayout rl_all;
    @BindView(R.id.rl_root)
    RelativeLayout rl_root;
    @BindView(R.id.fl_main)
    FrameLayout fl_main;
    @BindView(R.id.tv_unread_info)
    TextView tvUnreadInfo;
    @BindView(R.id.iv_unread_exit)
    ImageView ivUnreadExit;
    @BindView(R.id.ll_unread_info)
    LinearLayout llUnreadInfo;
    private int position = 0;

    // 消息tab角标
    private ItemBadge mTabMessageBadge;
    private int loginNum = 0;
    private static boolean flag = false;
    private boolean isVipUser = false;
    private MainActivityPresenter mainActivityPresenter;
    private boolean isCanShowPop = true;
    private boolean firstIntoVideo = true;
    private boolean unReadHide=false;
    private boolean fromReadEvent=false;
    private boolean clicktab=false;
    private boolean isResume=false;
    private Ringtone ringtone;
    public static final String INTENT_KEY_COMMON = "intent_key_common_parcelable";
    private boolean isVideoMsg=false;
    private VideoInviteParcelable videoInviteParcelable;
//    private NotificationManager manager;
    private boolean isViteActivity=true;
    private boolean isCancelVideo=false;
    private LocationManager mLocationManager;
    private static final int PERMISSION_CODE_ACCESS_FINE_LOCATION = 0;
    private boolean getCountry=false;
    private String jingdu;
    private String weidu;


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        videoInviteParcelable = (VideoInviteParcelable) parcelable;
        if(getIntent()!=null)
          isViteActivity = getIntent().getBooleanExtra("inviteActivity", true);
    }

    @Override
    protected View getNoticeView() {
        return fl_main;
    }

    @Override
    protected void initViews() {
//        获取百度定位
        msgHandler.sendEmptyMessageDelayed(3,5*1000);
        //        上传位置信息
//        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        requestLocation();
//        获取魅族推送
        if(MzSystemUtils.isBrandMeizu(BaseApplication.getGlobalContext())){
            String pushId = PushManager.getPushId(BaseApplication.getGlobalContext());
            ApiManager.getMiPushToken(pushId,new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel baseModel, boolean isEmpty) {
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                }
            });
            UserPreference.setRegId(pushId);

        }
        //        Util.initPlayMp3();
//        Intent intent = new Intent(MainActivity.this, HomeWatcherReceiver.class);
//        PendingIntent sender = PendingIntent.getBroadcast(context,
//                REQUEST_CODE, intent, PendingIntent.FLAG_CANCEL_CURRENT);
//
//        // Schedule the alarm!
//        AlarmManager am = (AlarmManager) MainActivity.this
//                .getSystemService(Context.ALARM_SERVICE);
//
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.HOUR_OF_DAY, 21);
//        calendar.set(Calendar.MINUTE, 30);
//        calendar.set(Calendar.SECOND, 10);
//        calendar.set(Calendar.MILLISECOND, 0);
//
//        am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
//                5*1000, sender);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean notificationIsOpen = NotificationsUtils.notificationIsOpen(MainActivity.this);
                notificationIsOpenTag(notificationIsOpen);
            }
        },30*1000);
        NotificationsUtils.checkNotificationOpend(MainActivity.this, new NotificationsUtils.NotificationCheckResultListener() {
            @Override
            public void checkResult(boolean hasOpenNotification) {
            }
        },true);
        Log.i("Main===","MainActivity==initViews="+ringtone);
        registerHomeKeyReceiver(BaseApplication.getGlobalContext());

        EMClient.getInstance().logout(true);
        HyLogin();
        setMarginTop(1);
        // 设置消息tab角标
        mTabMessageBadge = new ItemBadge(Color.parseColor("#ff0000"), 12, Gravity.TOP | Gravity.RIGHT).hide();
        mTabMessageBadge.setTextColor(Color.WHITE);
        // 设置tab
        mTabLayout.addTabView(new TabView(getString(R.string.tab_homepage), R.drawable.tab_homepage_normal, R.drawable.tab_homepage_selected))
                .addTabView(new TabView(getString(R.string.tab_message), R.drawable.tab_message_normal, R.drawable.tab_message_selected))
                .addTabView(new TabView(getString(R.string.tab_squarelist), R.drawable.tab_square_normal, R.drawable.tab_square_selected))
                .addTabView(new TabView(getString(R.string.tab_follow), R.drawable.tab_follow_normal, R.drawable.tab_follow_selected))
                .addTabView(new TabView(getString(R.string.tab_person), R.drawable.tab_person_normal, R.drawable.tab_person_selected))
                .setFirstSelectedPosition(0)
                .initialize();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fl_main, new HomepageFragment(), "homepage");
        fragmentTransaction.add(R.id.fl_main, new MessageFragment(), "message");
        fragmentTransaction.add(R.id.fl_main, new VideoListFragment(), "video");
        fragmentTransaction.add(R.id.fl_main, new FollowFragment(), "follow");
        fragmentTransaction.add(R.id.fl_main, new PersonFragment(), "person");
        fragmentTransaction.commit();
        HyphenateHelper.getInstance().setOnClick();
        EventBus.getDefault().post(new RegisterAndLoginFinish());
        mainActivityPresenter = new MainActivityPresenter(this);
        mainActivityPresenter.loadData();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (UserPreference.isRegister() && UserPreference.isMale()) {
                    mainActivityPresenter.makeFreeVideo();
                }
            }
        }, 2000);
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<HuanXinUser> allAcount = DbModle.getInstance().getUserAccountDao().getAllAcount();
                if (allAcount != null && allAcount.size() != 0) {
                    for (HuanXinUser user : allAcount) {
                        int converUnreadMsgCount = HyphenateHelper.getInstance().getConverUnreadMsgCount(user.getAccount());
                        if (converUnreadMsgCount > 0) {
                            List<EMMessage> emMessages = HyphenateHelper.getInstance().getUnReadMsg(user.getAccount());
                            if (emMessages != null && emMessages.size() > 0) {
                                user.setMsgNum(converUnreadMsgCount);
                                String lastMsg = getString(R.string.how_are_you);
                                if (emMessages.get(emMessages.size() - 1).getType() == EMMessage.Type.TXT) {
                                    NettyMessage nettyMessage = setMsgContent(emMessages.get(emMessages.size() - 1).getStringAttribute("msg", ""));
                                    if (nettyMessage != null) {
                                        EMTextMessageBody emTextMessageBody = (EMTextMessageBody) emMessages.get(emMessages.size() - 1).getBody();
                                        if (nettyMessage.getExtendType() == 2 || emTextMessageBody.getMessage().equals("msgCall")) {
                                            lastMsg = getString(R.string.video_invitations);
                                        } else if (nettyMessage.getExtendType() == 3) {
                                            lastMsg = getString(R.string.one_private_message);
                                        } else if (nettyMessage.getExtendType() == 5) {
                                            lastMsg = getString(R.string.gift);
                                        } else if (nettyMessage.getExtendType() == 19) {
                                            lastMsg = getString(R.string.voice_message);
                                        } else {
                                            if (emTextMessageBody.getMessage().contains(".mp3")) {
                                                lastMsg = getString(R.string.voice_message);
                                            } else {
                                                lastMsg = emTextMessageBody.getMessage();
                                            }
                                        }
                                    }
                                } else if (emMessages.get(emMessages.size() - 1).getType() == EMMessage.Type.VOICE) {
                                    lastMsg = getString(R.string.voice_message);
                                } else if (emMessages.get(emMessages.size() - 1).getType() == EMMessage.Type.IMAGE) {
                                    lastMsg = getString(R.string.photo_message);
                                }
                                user.setLastMsg(lastMsg);
                                user.setMsgTime(String.valueOf(emMessages.get(emMessages.size() - 1).getMsgTime()));
                                DbModle.getInstance().getUserAccountDao().addAccount(user);
                            }
                        }
                    }
                }
            }
        }).start();

        if(DbModle.getInstance().getUserAccountDao().queryMsgNum()>0){
            llUnreadInfo.setVisibility(View.VISIBLE);
            tvUnreadInfo.setText("你有"+DbModle.getInstance().getUserAccountDao().queryMsgNum()+"条未读消息，请查看");
        }else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(DbModle.getInstance().getUserAccountDao().queryMsgNum()>0){
                        llUnreadInfo.setVisibility(View.VISIBLE);
                        tvUnreadInfo.setText("你有"+DbModle.getInstance().getUserAccountDao().queryMsgNum()+"条未读消息，请查看");
                    }
                }
            },2000);
        }
        setUnreadHeight();

    }


    private void notificationIsOpenTag(boolean notificationIsOpen) {
        String tag;
        if(notificationIsOpen){
            tag="1";
        }else {
            tag="0";
        }
        ApiManager.userActivityTag(UserPreference.getId(), "", "29", tag, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("Main===","MainActivity==onStart="+ringtone);
        changeFragment(position);
        if(fromReadEvent)
            mTabLayout.setFirstSelectedPosition(1).initialize();
        fromReadEvent=false;

    }

    private void changeFragment(int i) {
        if(i==1||i==2||unReadHide){
            llUnreadInfo.setVisibility(View.GONE);
        }else {
            if(DbModle.getInstance().getUserAccountDao().queryMsgNum()>0)
               llUnreadInfo.setVisibility(View.VISIBLE);
        }
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        HomepageFragment homepageFragment = (HomepageFragment) manager.findFragmentByTag("homepage");
        MessageFragment messageFragment = (MessageFragment) manager.findFragmentByTag("message");
        VideoListFragment videoListFragment = (VideoListFragment) manager.findFragmentByTag("video");
        FollowFragment followFragment = (FollowFragment) manager.findFragmentByTag("follow");
        PersonFragment personFragment = (PersonFragment) manager.findFragmentByTag("person");
        if (i == 0) {
            position = 0;
            transaction.show(homepageFragment)
                    .hide(messageFragment).hide(videoListFragment).hide(followFragment).hide(personFragment);
            EventBus.getDefault().post(new PauseEvent());
        } else if (i == 1) {
            position = 1;
            transaction.show(messageFragment)
                    .hide(homepageFragment).hide(videoListFragment).hide(followFragment).hide(personFragment);
            EventBus.getDefault().post(new PauseEvent());

        } else if (i == 2) {
            position = 2;
            transaction.show(videoListFragment)
                    .hide(messageFragment).hide(homepageFragment).hide(followFragment).hide(personFragment);
            EventBus.getDefault().post(new StartEvent());

        } else if (i == 3) {
            position = 3;
            transaction.show(followFragment)
                    .hide(messageFragment).hide(videoListFragment).hide(homepageFragment).hide(personFragment);
            EventBus.getDefault().post(new PauseEvent());

        } else {
            position = 4;
            transaction.show(personFragment)
                    .hide(messageFragment).hide(videoListFragment).hide(followFragment).hide(homepageFragment);
            EventBus.getDefault().post(new PauseEvent());
            EventBus.getDefault().post(new UserInfoChangedEvent());

        }

        transaction.commit();
    }
    private static void registerHomeKeyReceiver(Context context) {
        try {
            mHomeKeyReceiver = new HomeWatcherReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction(Intent.ACTION_SCREEN_ON);
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            filter.addAction(Intent.ACTION_USER_PRESENT);
//        filter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            context.registerReceiver(mHomeKeyReceiver, filter);
        }catch (Exception e){

        }

    }
    private static void unregisterHomeKeyReceiver(Context context) {
        if (null != mHomeKeyReceiver) {
            try{
                context.unregisterReceiver(mHomeKeyReceiver);
            }catch (Exception e){

            }
        }
    }

    public int getPosition() {
        return position;
    }


    private void setMarginTop(int top) {
        if (top == 0) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup
                    .LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(0, 0, 0, 0);
            rl_root.setLayoutParams(layoutParams);
        } else {
            int statusBarHeight = DeviceUtil.getStatusBarHeight(mContext);
            int barHeight = DeviceUtil.px2dip(mContext, statusBarHeight);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup
                    .LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(0, barHeight, 0, 0);
            rl_root.setLayoutParams(layoutParams);
        }

    }

    //解析发过来的数据
    private NettyMessage setMsgContent(String msgContent) {
        if (!TextUtils.isEmpty(msgContent)) {
            NettyMessage nettyMessage = new Gson().fromJson(msgContent, NettyMessage.class);
            return nettyMessage;
        } else {
            return null;
        }
    }

    @Override
    protected void setListeners() {
        mTabLayout.setOnTabSelectedListener(new OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position) {
                clicktab=true;
                if (HyphenateHelper.getInstance().getUnreadMsgCount() == 0) {
                    mTabMessageBadge.setText("").hide();
                    llUnreadInfo.setVisibility(View.GONE);
                    unReadHide=true;
                }
                if (position == 2) {
                    mTabLayout.setBackgroundColor(Color.TRANSPARENT);
                    rl_all.setBackgroundColor(Color.TRANSPARENT);
                    setMarginTop(0);
                    if (firstIntoVideo) {
                        firstIntoVideo = false;
                        EventBus.getDefault().post(new UserTagVideoEvent());
                    }
                } else {
                    mTabLayout.setBackgroundColor(Color.WHITE);
                    rl_all.setBackgroundColor(getResources().getColor(R.color.main_color));
//                    rl_all.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.main_color));
                    setMarginTop(1);
                }
                changeFragment(position);
//                EventBus.getDefault().post(new RefreshEvent());
            }
        });
    }

    @Override
    protected void loadData() {
        // 登陆声网信令系统
//        AgoraHelper.getInstance().login();
        HyphenateHelper.getInstance().isNotChatActivity();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        flag = isFinishing();
        unregisterHomeKeyReceiver(this);
        serviceStop();
        HyphenateHelper.getInstance().logout();
        super.onDestroy();
        Log.i("Main===","MainActivity==onDestroy="+ringtone);
    }

    @Override
    public void onBackPressed() {
    }

    // 用来计算返回键的点击间隔时间
    private long exitTime = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                //弹出提示，可以有多种方式
                Toast.makeText(getApplicationContext(), getString(R.string.exit), Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            } else {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addCategory(Intent.CATEGORY_HOME);
                startActivity(intent);
            }
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Subscribe
    public void onEvent(UnreadMsgChangedEvent event) {
        //判断，如果存在没有未读消息，却存在未读数量的情况清空
        if (DbModle.getInstance().getUserAccountDao().selectSqlit() == 0) {
            HyphenateHelper.getInstance().clearAllUnReadMsg();
        }
        Message msg = Message.obtain();
        msg.obj = event;
        msg.what=UNREAD_MSG_EVENT;
        handler.sendMessage(msg);
    }
    @Subscribe
    public void onEvent(OpenVideoEvent event) {
        Log.i("Main==","获取OpenVideoEvent");
        islockScreen(videoInviteParcelable);
    }


    @Subscribe
    public void onEvent(FinishEvent event) {
        finish();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Main===","onResume+isViteActivity="+isViteActivity);
        if(UserPreference.isClickNotification()){
            UserPreference.setClickNotification(false);
            isVideoMsg=false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
//                    if(manager!=null)
//                        manager.cancel(2);
                    boolean videoInvite = Util.isTopActivity("VideoInvite");
                    if(!videoInvite&&videoInviteParcelable!=null){
                        if(isViteActivity){
                            LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity.class, videoInviteParcelable);
                        }else {
                            LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity2.class, videoInviteParcelable);
                        }
                    }
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            if (ringtone != null && ringtone.isPlaying()) {
//                                ringtone.stop();
//                                ringtone = null;
//                            }
//                        }
//                    },1000);
                }
            },1000);
        }

        serviceStop();
        Log.i("HomeReceiver", "onResume:setReception==false ");
        isResume=true;
        //        在线状态，开始小米推送
//        MiPushClient.resumePush(MainActivity.this,null);
        //        清除小米推送的消息
        MiPushClient.clearNotification(MainActivity.this);
        flag = isFinishing();
        if (DbModle.getInstance().getUserAccountDao().queryMsgNum() > 0) {
            if(!clicktab){
                llUnreadInfo.setVisibility(View.VISIBLE);
            }
            if (DbModle.getInstance().getUserAccountDao().queryMsgNum() >= 99) {
                tv_tab.setText("99+");
                tvUnreadInfo.setText("你有99+条未读消息，请查看");
            } else {
                tv_tab.setText(String.valueOf(DbModle.getInstance().getUserAccountDao().queryMsgNum()));
                tvUnreadInfo.setText("你有"+DbModle.getInstance().getUserAccountDao().queryMsgNum()+"条未读消息，请查看");
            }
            tv_tab.setVisibility(View.VISIBLE);
        } else {
            tv_tab.setVisibility(View.GONE);

        }
        // 登陆声网信令系统
        AgoraHelper.getInstance().login();
        // 登录过环信，加载
        if (HyphenateHelper.getInstance().isLoggedIn()) {
            // 登录成功后加载聊天会话
            new Thread(new Runnable() {
                @Override
                public void run() {
                    HyphenateHelper.getAllMsgFromHuanXin();
//                    EMClient.getInstance().chatManager().loadAllConversations();
                }
            }).start();
        } else {
            HyLogin();
        }
        if (TimeUtils.timeIsPast()) {//判断页脚是否过期
            if (isCanShowPop) {
                msgHandler.sendEmptyMessage(1);
            }
        } else if (!TimeUtils.timeIsPast()) {
            msgHandler.sendEmptyMessage(2);
        }

//        判读是否来自与通知栏
        Intent intent = getIntent();
        boolean booleanExtra = intent.getBooleanExtra(LaunchHelper.INTENT_KEY_COMMON, false);
        if(booleanExtra){
            fromReadEvent=true;
            llUnreadInfo.setVisibility(View.GONE);
            changeFragment(1);
            mTabLayout.setFirstSelectedPosition(1).initialize();
        }

        // 个推配置
//        PushManager.getInstance().initialize(this.getApplicationContext(), com.wiscomwis.facetoface.service.GeTuiService.class);
//        PushManager.getInstance().registerPushIntentService(this.getApplicationContext(), com.wiscomwis.facetoface.service.GetTuiIntentService.class);
    }

    private void HyLogin() {
        String account = UserPreference.getAccount();
        String password = UserPreference.getPassword();
        HyphenateHelper.getInstance().login(UserPreference.getAccount(), UserPreference.getPassword(), new HyphenateHelper.OnLoginCallback() {
            public void onSuccess() {
                Log.e("AAAAAAA", "onSussess: 登录成功---------======登录次数" + loginNum);

            }

            @Override
            public void onFailed() {
                Log.e("AAAAAAA", "onFailed: 登录失败---------======" + loginNum);
                ++loginNum;
                if (loginNum >= 4) {
                    return;
                }
                HyLogin();//登录失败5次之后不再登录
            }
        });
    }
    @Override
    public void onActionModeFinished(ActionMode mode) {
        super.onActionModeFinished(mode);
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case UNREAD_MSG_EVENT:
                    int msgNum = DbModle.getInstance().getUserAccountDao().queryMsgNum();
                    if (msgNum == 0) {
                        tv_tab.setVisibility(View.GONE);
                        mTabMessageBadge.setText("").hide();
                        llUnreadInfo.setVisibility(View.GONE);
                        unReadHide=true;
                    } else {
                        if(!clicktab){
                            llUnreadInfo.setVisibility(View.VISIBLE);
                        }
                        tv_tab.setVisibility(View.VISIBLE);
//                llUnreadInfo.setVisibility(View.VISIBLE);
                        unReadHide=false;
                        if (msgNum >= 99) {
                            tv_tab.setText("99+");
                            tvUnreadInfo.setText("你有99+条未读消息，请查看");

                        } else {
                            tv_tab.setText(String.valueOf(msgNum));
                            tvUnreadInfo.setText("你有"+msgNum+"条未读消息，请查看");

                        }
                        mTabMessageBadge.setText(String.valueOf(msgNum));
                    }
                    break;
                case CANCEL_VIDEO_INVITE:
                    UserPreference.setCancelInvite((String)msg.obj, false);
                    Log.e("AAAAAAA", "关闭取消邀请");
                    break;
                case FINISH_VIDEO_TO_CHAT:
                    FinishVideoToChatActivityEvent event = (FinishVideoToChatActivityEvent) msg.obj;
                    if (DataPreference.getChatActivityStatus() == 0) {
//                if (!event.getAccount().equals(DataPreference.getChatActivityAccount())) {
                        LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(event.getGuid(),
                                event.getAccount(), event.getNickname(), event.getImageUrl(), 0));
//                }
                    } else {
                        LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(event.getGuid(),
                                event.getAccount(), event.getNickname(), event.getImageUrl(), 0));
                    }
                    break;
            }

        }
    };

    public static boolean nowIsFinish() {
        return flag;
    }


    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }

    @Override
    public void sayHellAll(List<SearchUser> searchUserList1) {
        CustomDialogAboutPay.sayHelloShow(MainActivity.this, searchUserList1);
    }

    @Override
    public void isVipUser() {
        isVipUser = true;
    }

    @Subscribe
    public void onEvent(NettyMessage event) {
        msgHandler.sendEmptyMessage(1);
        isCanShowPop = false;//只让悬浮页脚显示一次
    }
    @Subscribe
    public void onEvent(CloseRingtoneEvent event) {
//        manager.cancel(2);
        if(event!=null&&!event.isNotificationClick()){
//            String nickname="有人";
//            if(videoInviteParcelable!=null)
//                nickname = videoInviteParcelable.nickname;
//            Notification.Builder mNotifyBuilder = new Notification.Builder(this);
//            Notification notification = mNotifyBuilder.setContentTitle("泡泡")
//                    .setContentText(nickname+"已取消视频通话")
//                    .setSmallIcon(R.mipmap.vivologo)
//                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.vivologo))
//                    .build();
//            Intent intent = new Intent(MainActivity.this, MainActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
//            intent.putExtra(INTENT_KEY_COMMON, true);
//            PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0x123,intent,PendingIntent.FLAG_UPDATE_CURRENT);
//            notification.contentIntent = pendingIntent;
//            manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//            manager.notify(2,notification);
            isVideoMsg=false;
            Util.stopMp3();
            videoInviteParcelable=null;
        }else {
            videoInviteParcelable=null;
        }

    }
    @Subscribe
    public void onEvent(MiPushEvevnt event) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!isResume){
//                    MiPushClient.pausePush(MainActivity.this,null);
                    Log.i("onActivityStopped","MiPushClient.pausePush");
                }
            }
        },10000);
    }

    private int num=0;
    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    isCanShowPop(true);
                    break;
                case 2:
                    closeShowPop();
                    break;
                case 3:
                    LocationUtil.MLocation baseLocation = LocationUtil.getInstance(BaseApplication.getGlobalContext()).getBaseLocation();
                    BDLocation location = LocationUtil.getInstance(BaseApplication.getGlobalContext()).getLocation();
                    if(baseLocation==null||location==null||baseLocation.latitude==0||baseLocation.longitude==0||TextUtils.isEmpty(location.getCity())||TextUtils.isEmpty(location.getProvince())){
                        num++;
                        if(num<10)
                         msgHandler.sendEmptyMessageDelayed(3,5*1000);
                    }else {
                        getLocation(baseLocation.latitude+"", baseLocation.longitude+"", location.getCountry(), location.getProvince(),location.getCity());
                        Log.e("AAAAA","百度定位：baseLocation=latitude="+baseLocation.latitude+"--baseLocation="+baseLocation.longitude+"=====location=getCity="+location.getCity()+"--getProvince="+location.getProvince()+"--num="+num);
                    }
                    break;
            }
        }
    };

    @Override
    protected void isCanShowPop(boolean flag) {
        super.isCanShowPop(flag);
    }

    @Override
    protected void closeShowPop() {
        super.closeShowPop();
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }

    @Subscribe
    public void onEvent(FollowEvent event) {
        changeFragment(0);
        mTabLayout.setFirstSelectedPosition(0).initialize();
    }
    @Subscribe
    public void onEvent(UnReadMsgEvent event) {
        fromReadEvent=true;
        llUnreadInfo.setVisibility(View.GONE);
        changeFragment(1);
        mTabLayout.setFirstSelectedPosition(1).initialize();
    }

    @Subscribe
    public void onEvent(FinishVideoToChatActivityEvent event) {
        if (event != null && mContext != null) {
            Message msg = new Message();
            msg.obj = event;
            msg.what=FINISH_VIDEO_TO_CHAT;
            handler.sendMessage(msg);
        }
    }
    @Subscribe
    public void onEvent(RingtoneEvent event) {
        isVideoMsg=true;
        Util.playMp3(MainActivity.this);
//        if(ringtone!=null){
//            if(!ringtone.isPlaying())
//                ringtone.play();
//        }else {
//            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
//            ringtone = RingtoneManager.getRingtone(MainActivity.this, notification);
//            ringtone.play();
//        }
        if(event!=null&&event.getmVideoInviteParcelable()!=null){
             videoInviteParcelable = event.getmVideoInviteParcelable();
             isViteActivity = event.isInviteActivity();
            sendNotification(event.getmVideoInviteParcelable(),event.isInviteActivity());

        }
    }
    private void sendNotification(final VideoInviteParcelable videoInviteParcelable, boolean inviteActivity){
//        String nickname = videoInviteParcelable.nickname;
//        Notification.Builder mNotifyBuilder = new Notification.Builder(this);
//        Notification notification = mNotifyBuilder.setContentTitle("泡泡")
//                .setContentText(nickname+"邀请你视频通话")
//                .setSmallIcon(R.mipmap.vivologo)
//                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.vivologo))
//                .build();
//        Intent intent =new Intent (MainActivity.this,NotificationClickReceiver.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
//        intent.putExtra("intent_key_main_parcelable",videoInviteParcelable);
//        intent.putExtra("inviteActivity",inviteActivity);
//        PendingIntent pendingIntent =PendingIntent.getBroadcast(MainActivity.this, 0x123, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//        notification.contentIntent = pendingIntent;
//        manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//        manager.notify(2,notification);
        UserPreference.setLockVideoId(videoInviteParcelable.uId+"");
//        判断是否锁屏，没有锁屏的话，直接打开
        Log.i("Main==","获取sendNotification");
        islockScreen(videoInviteParcelable);
    }

    private void islockScreen(VideoInviteParcelable videoInviteParcelable) {
        boolean lockScreen = UserPreference.isLockScreen();
        String phonename = Build.MANUFACTURER;
        Log.i("Main==","获取OppO手机=phonename="+phonename+"=videoInviteParcelable="+videoInviteParcelable);
        if (phonename != null && phonename != null && phonename != "") {
            if (Util.isOPPO()) {
                boolean b = (videoInviteParcelable != null);
                Log.i("Main==","获取OppO手机=videoInviteParcelable不为null="+b);
                if(videoInviteParcelable!=null){
                    boolean videoInvit = Util.isTopActivity("VideoInvite");
                    Log.i("Main==","nosetOppoVideo=videoInvit="+videoInvit);
                    if(!videoInvit){
                        if(isViteActivity){
                            LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity.class, videoInviteParcelable);
                        }else {
                            LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity2.class, videoInviteParcelable);
                        }
                        setOppoVideo(videoInviteParcelable);
                    }
                }
            }else {
                if(!lockScreen){
                    if(videoInviteParcelable!=null){
                        if(isViteActivity){
                            LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity.class, videoInviteParcelable);
                        }else {
                            LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity2.class, videoInviteParcelable);
                        }
                    }
                }
            }
        }else {
            if(!lockScreen){
                if(videoInviteParcelable!=null){
                    if(isViteActivity){
                        LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity.class, videoInviteParcelable);
                    }else {
                        LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity2.class, videoInviteParcelable);
                    }
                    videoInviteParcelable=null;
                }
            }
        }


    }
    private Handler handler1 = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            boolean videoInvit = Util.isTopActivity("VideoInvit");
            Log.i("Main==","setOppoVideo=videoInvit="+videoInvit+"=videoInviteParcelable="+videoInviteParcelable);
            if(!videoInvit){
                setOppoVideo(videoInviteParcelable);
                if(isViteActivity){
                    LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity.class, videoInviteParcelable);
                }else {
                    LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity2.class, videoInviteParcelable);
                }
            }

        }

    };
    private void setOppoVideo(final VideoInviteParcelable videoInviteParcelable) {
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
            //执行耗时操作
            try {
                Thread.sleep(1000);
                handler1.sendEmptyMessage(0);
             } catch (InterruptedException e) {
                e.printStackTrace();
             }
            }
        };
        new Thread() {
       public void run() {
          new Handler(Looper.getMainLooper()).post(runnable);//在子线程中直接去new 一个handler
         //这种情况下，Runnable对象是运行在主线程中的，不可以进行联网操作，但是可以更新UI
      }
        }.start();
//
//
//
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
////                boolean intoVideo = UserPreference.isIntoVideo();
//                boolean videoInvit = Util.isTopActivity("VideoInvit");
//                Log.i("Main==","setOppoVideo=videoInvit="+videoInvit+"=videoInviteParcelable="+videoInviteParcelable);
//                if(!videoInvit){
//                    setOppoVideo(videoInviteParcelable);
//                }else {
//                    if (ringtone != null && ringtone.isPlaying()) {
//                        ringtone.stop();
//                        ringtone = null;
//                    }
//                    manager.cancel(2);
//                }
//                if(isViteActivity){
//                    LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity.class, videoInviteParcelable);
//                }else {
//                    LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity2.class, videoInviteParcelable);
//                }
//            }
//        },1000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        Log.i("Main===","MainActivity==onCreate="+ringtone);
    }

    @OnClick({R.id.tv_unread_info, R.id.iv_unread_exit, R.id.ll_unread_info})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_unread_info:
                EventBus.getDefault().post(new UnReadMsgEvent());
//                changeFragment(1);
//                mTabLayout.setFirstSelectedPosition(1).initialize();

                break;
            case R.id.iv_unread_exit:
                llUnreadInfo.setVisibility(View.GONE);
                break;
            case R.id.ll_unread_info:
                break;
        }
    }
    private void setUnreadHeight() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(llUnreadInfo.getLayoutParams());
        int screenHeight = DataPreference.getScreenHeight();
        Log.e("AAAAAAA", "convert: "+screenHeight);
        if(screenHeight==5760){//小米4
            params.setMargins(0,DeviceUtil.dip2px(MainActivity.this, 67),0,0);
        }else if(screenHeight==2560){//小米3x
            String phonename = Build.MANUFACTURER;
            if (phonename != null && phonename != null && phonename != ""&&phonename.equals("OPPO")) {
                    params.setMargins(0,DeviceUtil.dip2px(MainActivity.this, 70),0,0);
                }else {
                params.setMargins(0,DeviceUtil.dip2px(MainActivity.this, 73),0,0);
            }
        }else if(screenHeight==4709){//谷歌
            params.setMargins(0,DeviceUtil.dip2px(MainActivity.this, 67),0,0);
        }else if(screenHeight==5040){
            params.setMargins(0,DeviceUtil.dip2px(MainActivity.this, 67),0,0);
        }else if(screenHeight<2416){//华为
            params.setMargins(0,DeviceUtil.dip2px(MainActivity.this, 73),0,0);
        }else if(screenHeight==2416){
            params.setMargins(0,DeviceUtil.dip2px(MainActivity.this, 73),0,0);
        }else if(screenHeight>2416&&screenHeight<2560){
            params.setMargins(0,DeviceUtil.dip2px(MainActivity.this, 71),0,0);
        }else if(screenHeight>2560&&screenHeight<4709){
            params.setMargins(0,DeviceUtil.dip2px(MainActivity.this, 68),0,0);
        }else if(screenHeight>4709&&screenHeight<5040){
            params.setMargins(0,DeviceUtil.dip2px(MainActivity.this, 67),0,0);
        }else if(screenHeight>5040&&screenHeight<5760){
            params.setMargins(0,DeviceUtil.dip2px(MainActivity.this, 67),0,0);
        }else if(screenHeight>5760){
            params.setMargins(0,DeviceUtil.dip2px(MainActivity.this, 65),0,0);
        }else {
            params.setMargins(0,DeviceUtil.dip2px(MainActivity.this, 67),0,0);
        }
        llUnreadInfo.setLayoutParams(params);
    }

    public void serviceStart(){
        Intent intent=new Intent(this,CheckAcitivityService.class);
        startService(intent);
    }
    private  void serviceStop(){
        Intent intent=new Intent(this,CheckAcitivityService.class);
        stopService(intent);
    }
    @Override
    protected void onStop() {
        super.onStop();
        serviceStart();
        Log.i("Main===","MainActivity==onStop="+ringtone);
    }



    @Override
    protected void onPause() {
        super.onPause();
        isResume=false;
        Log.i("Main===","MainActivity==onPause="+ringtone);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("Main===","MainActivity==onRestart="+ringtone);
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(final AgoraEvent event) {
        if(event.eventCode==AgoraHelper.EVENT_CODE_INVITE_END_PEER&&!isCancelVideo){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    AgoraEvent agoraEvent = new AgoraEvent();
                    agoraEvent.setEventCode(AgoraHelper.EVENT_CODE_INVITE_END_PEER);
                    agoraEvent.setChannelId(event.channelId);
                    agoraEvent.setAccount(event.account);
                    EventBus.getDefault().post(agoraEvent);
                    Log.e("AAAAA","MainActivity==发送取消的事件");
                }
            },1000);
        }
    }
    @Subscribe
    public void onEvent(CancelVideoEvent event) {
        isCancelVideo=true;
    }


    /**
     * 请求定位信息
     */
    private void requestLocation() {
        if (checkGPS()) {// GPS打开
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_CODE_ACCESS_FINE_LOCATION);
                } else {
                    if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
                        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, mLocationListener);
                    } else if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
                        Location lastKnownLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if(lastKnownLocation==null)
                            lastKnownLocation= mLocationManager.getLastKnownLocation(mLocationManager.getProvider(LocationManager.GPS_PROVIDER).getName());
                        getCountryByLoaction(lastKnownLocation);
                    }
                }
            } else {
                if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, mLocationListener);
                }
                if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
                    getCountryByLoaction(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                }
            }
//            }
        } else {// GPS关闭
            // 跳转到设置页，打开定位开关
            DialogUtil.showDoubleBtnDialog(getSupportFragmentManager(), null, getString(R.string.dialog_location_tip), getString(R.string.dialog_set), null, true, new OnDoubleDialogClickListener() {
                @Override
                public void onPositiveClick(View view) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, 0);
                }

                @Override
                public void onNegativeClick(View view) {
                }
            });
        }
    }

    /**
     * 检查GPS定位开关
     *
     * @return
     */
    private boolean checkGPS() {
        if (mLocationManager != null) {
            return (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
        }
        return false;
    }

    private LocationListener mLocationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
           getCountryByLoaction(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    /**
     * 根据位置信息（经纬度）获得所在国家
     *
     * @param location 定位信息
     */
    private void getCountryByLoaction(Location location) {
        if (location != null && !getCountry) {
            LocationInfo locationInfo = new LocationInfo();
            locationInfo.setLatitude(String.valueOf(location.getLatitude()));
            locationInfo.setLongitude(String.valueOf(location.getLongitude()));
            jingdu = String.valueOf(location.getLatitude());
            weidu = String.valueOf(location.getLongitude());
            getAddress(jingdu, weidu);
        }
    }

    private void getAddress(String getLatitude, String getLongitude) {
        //		本地
        String url = "http://ditu.google.cn/maps/api/geocode/json?latlng=" + getLatitude + "," + getLongitude + "&&sensor=false&&language=";
//		线上
//		String url = "http://ditu.google.com/maps/api/geocode/json?latlng="+ lat + "," + lng + "&&sensor=false&&language=";
//        美国地址
//        String url = "http://ditu.google.cn/maps/api/geocode/json?latlng=31.543114,-97.154185&&sensor=false&&language=en_US";

        //获取国际化参数
//        String localInfo = "en_US";
        String localInfo="zh_CN";
        url += localInfo;    //拼接url地址

        String charset = "UTF-8";

        OkHttpClient okHttpClient = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .build();
        Call call = okHttpClient.newCall(request);


        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (request != null) {
                    String str = response.body().string();
                    Gson gson = new Gson();
                    GoogleGPS googleGPS = gson.fromJson(str, GoogleGPS.class);
                    if (googleGPS.getResults() != null) {
                        String province = "";
                        String city = "";
                        String country = null;
                        for (int i = 0; i < googleGPS.getResults().size(); i++) {

                            if (googleGPS.getResults().size() > 0 && googleGPS.getResults().get(i).getTypes().get(0).equals("country")) {

                                country = googleGPS.getResults().get(i).getAddress_components().get(0).getLong_name();
                            }

                            if (googleGPS.getResults().size() > 0 && googleGPS.getResults().get(i).getTypes().get(0).equals("administrative_area_level_1")) {
                                province = googleGPS.getResults().get(i).getAddress_components().get(0).getLong_name();
                            }

                            if (googleGPS.getResults().size() > 0 && googleGPS.getResults().get(i).getTypes().get(0).equals("administrative_area_level_2")) {
                                city = googleGPS.getResults().get(i).getAddress_components().get(0).getLong_name();
                            }

                        }
//                        if (TextUtils.isEmpty(province)) {
//                            province = city;     //没有一级城市，使用二级城市
//                        }
                        if (country != null) {
                            getCountry = true;
                            getLocation(jingdu, weidu, country, province,city);
                        }
                    }

                }
            }
        });
    }


    private void getLocation(String latitude, String longitude, String country, String province,String city) {
        LocationInfo locationInfo = new LocationInfo();
        locationInfo.setAddrStr("");
        locationInfo.setProvince(province);
        locationInfo.setCity(city);
        locationInfo.setCityCode("");
        locationInfo.setDistrict("");
        locationInfo.setStreet("");
        locationInfo.setStreetNumber("");
        locationInfo.setLatitude(latitude);
        locationInfo.setLongitude(longitude);
        locationInfo.setLanguage(Util.getLacalLanguage());
        locationInfo.setCountry(country);
        ApiManager.setLocation(locationInfo, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                String isSucceed = baseModel.getIsSucceed();
                if (!TextUtils.isEmpty(isSucceed) && isSucceed.equals("1")) {
                    Log.e("AAAAAA", "上传位置信息成功");
//                    更新用户资料
//                    ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
//                        @Override
//                        public void onResult(MyInfo myInfo, boolean isEmpty) {
//                            // 更新用户信息
//                            if (null != myInfo) {
//                                UserBase userBase = myInfo.getUserDetail().getUserBase();
//                                if (null != userBase) {
//                                    UserPreference.saveUserInfo(userBase);
////                                    发送给个人信息，更新消息
////                                    EventBus.getDefault().post(new UpadateLocationEvent());
//                                }
//                            }
//                        }
//
//                        @Override
//                        public void onError(String msg, boolean isNetworkError) {
//                        }
//
//                    });
                }

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }
}
