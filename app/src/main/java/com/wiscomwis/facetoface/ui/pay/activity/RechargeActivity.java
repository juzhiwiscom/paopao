package com.wiscomwis.facetoface.ui.pay.activity;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseAppCompatActivity;
import com.wiscomwis.facetoface.common.TimeUtils;
import com.wiscomwis.facetoface.data.model.NettyMessage;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.event.SingleLoginFinishEvent;
import com.wiscomwis.facetoface.event.UnReadMsgEvent;
import com.wiscomwis.facetoface.parcelable.PayParcelable;
import com.wiscomwis.facetoface.ui.pay.adapter.PayAdapter;
import com.wiscomwis.facetoface.ui.pay.contract.RechargeContract;
import com.wiscomwis.facetoface.ui.pay.presenter.RechargePresenter;
import com.wiscomwis.library.dialog.AlertDialog;
import com.wiscomwis.library.dialog.OnDialogClickListener;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.SnackBarUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 支付页面
 * Created by zhangdroid on 2017/5/25.
 */
public class RechargeActivity extends BaseAppCompatActivity implements RechargeContract.IView, View.OnClickListener {
    @BindView(R.id.recharge_root)
    LinearLayout mLlRoot;
    @BindView(R.id.recharge_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.recharge_activity_viewpager)
    ViewPager mViewPager;
    @BindView(R.id.tv_unread_info)
    TextView tvUnreadInfo;
    @BindView(R.id.iv_unread_exit)
    ImageView ivUnreadExit;
    @BindView(R.id.ll_unread_info)
    LinearLayout llUnreadInfo;
    private boolean isVipUser=false;
    private RechargePresenter mRechargePresenter;
    private PayParcelable mPayParcelable;
    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    isCanShowPop(true);
                    break;
                case 2:
                    closeShowPop();
                    break;
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_recharge;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mPayParcelable = (PayParcelable) parcelable;

    }

    @Override
    protected View getNoticeView() {
        return mLlRoot;
    }

    @Override
    protected void initViews() {
        mViewPager.setOffscreenPageLimit(1);
        mRechargePresenter = new RechargePresenter(this);
        mRechargePresenter.addTabs();
        if (TimeUtils.timeIsPast()) {//判断页脚是否过期
            msgHandler.sendEmptyMessage(1);
        } else if (!TimeUtils.timeIsPast()) {
            msgHandler.sendEmptyMessage(2);
        }
        setUnreadMsg();
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
        mRechargePresenter.getPayWay(mPayParcelable.fromTag);
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
        mRechargePresenter.getPayWay(mPayParcelable.fromTag);
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void isCanShowPop(boolean flag) {
        super.isCanShowPop(flag);
    }

    @Override
    protected void closeShowPop() {
        super.closeShowPop();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void showLoading() {
        toggleShowLoading(true, null);
    }

    @Override
    public void dismissLoading() {
        toggleShowLoading(false, null);
    }

    @Override
    public void setAdapter(PagerAdapter pagerAdapter) {
        mViewPager.setAdapter(pagerAdapter);
        if (mPayParcelable.type == 1) {
            mViewPager.setCurrentItem(2);
        }
    }

    @Override
    public FragmentManager getManager() {
        return getSupportFragmentManager();
    }

    @Override
    public void setAdapter(PayAdapter payAdapter) {

    }

    @Override
    public void setTextDetail(String detial) {

    }

    @Override
    public void getKeyNum(String num) {

    }

    @Override
    public void getNowMoney(String money) {

    }

    @Override
    public int getPaySource() {
        if (mPayParcelable != null) {
            return mPayParcelable.paySource;
        } else {
            return 6;
        }
    }

    @Override
    public void isVipUser() {
        isVipUser=true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.recharge_activity_rl_back:
                if(isVipUser){
                 finish();
                }else{
                    AlertDialog.showNoCanceled(getSupportFragmentManager(), "", "美女不等人，请三思而行",
                            "我再想想", "去意已决", new OnDialogClickListener() {
                                @Override
                                public void onNegativeClick(View view) {
                                    finish();
                                }

                                @Override
                                public void onPositiveClick(View view) {
                                }
                            }
                    );
                }
                break;
        }
    }

    private void back() {
        AlertDialog.showNoCanceled(getSupportFragmentManager(), "", "美女不等人，请三思而行",
                "我再想想", "去意已决", new OnDialogClickListener() {
                    @Override
                    public void onNegativeClick(View view) {
                        finish();
                    }

                    @Override
                    public void onPositiveClick(View view) {
                    }
                }
        );
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }

    @Subscribe
    public void onEvent(NettyMessage event) {
        msgHandler.sendEmptyMessage(1);
    }

    @Override
    public void onBackPressed() {
        if(isVipUser){
            finish();
        }else{
            AlertDialog.showNoCanceled(getSupportFragmentManager(), "", "美女不等人，请三思而行",
                    "我再想想", "去意已决", new OnDialogClickListener() {
                        @Override
                        public void onNegativeClick(View view) {
                            finish();
                        }

                        @Override
                        public void onPositiveClick(View view) {
                        }
                    }
            );
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRechargePresenter.getMyInfo();
    }
    private void setUnreadMsg() {
        int unReadInfo = DbModle.getInstance().getUserAccountDao().queryMsgNum();
        if(unReadInfo>0){
            llUnreadInfo.setVisibility(View.GONE);
            tvUnreadInfo.setText("你有"+unReadInfo+"条未读消息，请查看");
            tvUnreadInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new UnReadMsgEvent());
                    finish();
                }
            });
            ivUnreadExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    llUnreadInfo.setVisibility(View.GONE);
                }
            });
        }else{
            llUnreadInfo.setVisibility(View.GONE);
        }
    }

}
