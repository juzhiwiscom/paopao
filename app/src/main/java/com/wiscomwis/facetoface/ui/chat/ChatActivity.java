package com.wiscomwis.facetoface.ui.chat;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.meizu.cloud.pushsdk.PushManager;
import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseAppCompatActivity;
import com.wiscomwis.facetoface.base.BaseApplication;
import com.wiscomwis.facetoface.common.CustomDialogAboutOther;
import com.wiscomwis.facetoface.common.CustomDialogAboutPay;
import com.wiscomwis.facetoface.common.HyphenateHelper;
import com.wiscomwis.facetoface.common.TimeUtils;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.CustomCmd;
import com.wiscomwis.facetoface.data.model.HuanXinUser;
import com.wiscomwis.facetoface.data.model.NettyMessage;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.PlatformPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.event.FinishChatActivityEvent;
import com.wiscomwis.facetoface.event.MessageArrive;
import com.wiscomwis.facetoface.event.PaySuccessEvent;
import com.wiscomwis.facetoface.event.QAEvent;
import com.wiscomwis.facetoface.event.SeeDetailEvent;
import com.wiscomwis.facetoface.event.SingleLoginFinishEvent;
import com.wiscomwis.facetoface.event.UnreadMsgChangedEvent;
import com.wiscomwis.facetoface.event.UpdateChatDataEvent;
import com.wiscomwis.facetoface.parcelable.ChatParcelable;
import com.wiscomwis.facetoface.parcelable.PayParcelable;
import com.wiscomwis.facetoface.parcelable.ReportParcelable;
import com.wiscomwis.facetoface.parcelable.UserDetailParcelable;
import com.wiscomwis.facetoface.ui.detail.ReportActivity;
import com.wiscomwis.facetoface.ui.detail.UserDetailActivity;
import com.wiscomwis.facetoface.ui.main.MainActivity;
import com.wiscomwis.facetoface.ui.pay.activity.RechargeActivity;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.NotificationHelper;
import com.wiscomwis.library.util.SharedPreferenceUtil;
import com.wiscomwis.library.util.SnackBarUtil;
import com.wiscomwis.library.util.ToastUtil;
import com.wiscomwis.library.util.Utils;
import com.wiscomwis.library.widget.AutoSwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 聊天页面
 * Created by zhangdroid on 2017/5/27.
 */
public class ChatActivity extends BaseAppCompatActivity implements ChatContract.IView, View.OnClickListener {
    @BindView(R.id.chat_root)
    RelativeLayout mRlRoot;
    @BindView(R.id.chat_refresh)
    AutoSwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.chat_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.chat_switch)
    ImageView mIvSwitch;
    @BindView(R.id.chat_activity_btn_receive)
    Button btn_receive;
    @BindView(R.id.chat_activity_btn_wite_report)
    Button btn_wite_report;
    @BindView(R.id.chat_text_input)
    EditText mEtInput;
    @BindView(R.id.chat_voice)
    Button mBtnVoiceSend;
    @BindView(R.id.chat_send)
    Button mBtnSend;
    @BindView(R.id.chat_more)
    ImageView mIvMore;
    @BindView(R.id.chat_more_container)
    LinearLayout mLlMore;
    @BindView(R.id.chat_album)
    TextView mTvAlbum;
    @BindView(R.id.chat_camera)
    TextView mTvCamera;
    @BindView(R.id.chat_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.chat_activity_tv_nickname)
    TextView tv_name;
    @BindView(R.id.chat_bottom)
    LinearLayout ll_bottom;
    @BindView(R.id.chat_activity_rl_set)
    RelativeLayout rl_set;
    @BindView(R.id.chat_activity_iv_send_gifts)
    ImageView iv_send_gifts;
    @BindView(R.id.chat_activity_bottom_view)
    TextView view;
    @BindView(R.id.chat_activity_tv_writting)
    TextView tv_writting;
    @BindView(R.id.iv_more)
    ImageView ivMore;
    @BindView(R.id.tv_report)
    TextView tvReport;
    @BindView(R.id.ic_caht_report_exit)
    ImageView ivCahtReportExit;
    @BindView(R.id.rl_caht_report)
    RelativeLayout rlCahtReport;
    private TimerTask timerInput;
    // 录音框
    private PopupWindow mRecordPopupWindow;
    private int mRecordDuration = 0;
    // 标记是否初次加载
    private boolean mIsFirstLoad;
    private boolean mIsBlock = false;
    private LinearLayoutManager mLinearLayoutManager;
    private ChatAdapter mChatAdapter;
    private ChatParcelable mChatParcelable;
    private ChatPresenter mChatPresenter;
    AnimationDrawable animationDrawable;
    private long lastClickTime = 0;
    private int indexText = 0;
    public int time = 5000;//按照自己的需求自定义时间间隔发送状态消息（单位：ms）
    public long firstTime = System.currentTimeMillis();//文本框第一次输入内容变化的时间

    // 异步任务
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == C.message.MSG_TYPE_VOICE_UI_TIME) {
                mRecordDuration++;
                mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_VOICE_UI_TIME, 1_000);
            } else {
                if (mChatPresenter != null) {
                    mChatPresenter.handleAsyncTask(msg);

                }
            }
        }
    };
    private boolean isQAMsg = false;
    private boolean getIsQAMsg;

    private void startAnimation(ImageView iv_animation) {
        List<Drawable> drawableList = new ArrayList<Drawable>();
//        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_1));
//        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_2));
//        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_3));
//        drawableList.add(getResources().getDrawable(R.drawable.msg_record_voice_4));

        drawableList.add(ContextCompat.getDrawable(mContext, R.drawable.msg_record_voice_1));
        drawableList.add(ContextCompat.getDrawable(mContext, R.drawable.msg_record_voice_2));
        drawableList.add(ContextCompat.getDrawable(mContext, R.drawable.msg_record_voice_3));
        drawableList.add(ContextCompat.getDrawable(mContext, R.drawable.msg_record_voice_4));

        animationDrawable = Utils.getFrameAnim(drawableList, true, 150);
        iv_animation.setImageDrawable(animationDrawable);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_chat;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }


    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mChatParcelable = (ChatParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return mSwipeRefreshLayout;
    }


    @Override
    protected void initViews() {

        //        删除魅族的通知栏消息
        PushManager.clearNotification(BaseApplication.getGlobalContext());

        // 登录过环信，加载
        if (HyphenateHelper.getInstance().isLoggedIn()) {
        } else {
            HyLogin();
        }


//        DbModle.getInstance().getUserAccountDao().deleteData("149369","1530618928391");
        //                判断奇偶测试，是否隐藏送礼物按钮
//        String abTest = DataPreference.getABTest();
//        if(!TextUtils.isEmpty(abTest)){
//            if(abTest.equals("1")){
//                String account = UserPreference.getAccount();
//                String c = account.charAt(account.length() - 1)+"";
//                boolean b = Integer.parseInt(c) % 2 == 0;
//                if((!UserPreference.isVip())&&(b)){
//                    iv_send_gifts.setVisibility(View.GONE);
//                }
//            }else if(abTest.equals("3")){
//                if(!UserPreference.isVip()){
//                    iv_send_gifts.setVisibility(View.GONE);
//                }
//            }
//        }else{
        if (!UserPreference.isVip()&&!UserPreference.isAnchor()) {
            iv_send_gifts.setVisibility(View.GONE);
        }
//        }


        // 设置Notice替换View
        mSwipeRefreshLayout.setColorSchemeResources(R.color.main_color);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.WHITE);
        mLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        if (mChatParcelable != null) {
            //        华为渠道展示举报对话框。。。。。判断是否第一次进入这个聊天页面
            boolean b = !UserPreference.isFirstIntoChat(mChatParcelable.guid + "");
            boolean equals = PlatformPreference.getKeyFid().equals(C.CN_HUAWEI);
            boolean b1 = mChatParcelable.guid != 10000;
            Log.e("AAAAAA","isFirstIntoChat="+b+equals+b1);
            if (!UserPreference.isFirstIntoChat(mChatParcelable.guid + "") && PlatformPreference.getKeyFid().equals(C.CN_HUAWEI)&&mChatParcelable.guid!=10000) {
                UserPreference.setFirstIntoChatActivity(mChatParcelable.guid + "");
                ivMore.setVisibility(View.GONE);
                tvReport.setVisibility(View.VISIBLE);
                rlCahtReport.setVisibility(View.VISIBLE);
            }
            mChatAdapter = new ChatAdapter(this, mChatParcelable.imageUrl, mChatParcelable.account, mChatParcelable.nickname, mChatParcelable.guid + "", getSupportFragmentManager());
            mRecyclerView.setAdapter(mChatAdapter);
            mChatPresenter = new ChatPresenter(this, mChatParcelable.account, mChatParcelable.guid, mChatParcelable.nickname, mChatParcelable.imageUrl, 1);
            mChatPresenter.start();
            tv_name.setText(mChatParcelable.nickname);
            if ( mChatParcelable.guid == 10000) {
                iv_send_gifts.setVisibility(View.GONE);
            }
        }
        if (mChatParcelable != null) {

            NotificationHelper.getInstance(ChatActivity.this).cancel((int) mChatParcelable.guid);
            //判断，如果存在没有未读消息，却存在未读数量的情况清空
            if (DbModle.getInstance().getUserAccountDao().selectSqlit() == 0) {
                HyphenateHelper.getInstance().clearAllUnReadMsg();
            }
            EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
            List<HuanXinUser> allAcount = DbModle.getInstance().getUserAccountDao().getAllAcount();
            if (allAcount != null && allAcount.size() > 0) {
                for (HuanXinUser huanXinUser : allAcount) {
                    if (huanXinUser != null) {
                        if (huanXinUser.getAccount().equals(mChatParcelable.account)) {
                            HuanXinUser accountByHyID = DbModle.getInstance().getUserAccountDao().getAccountByHyID(huanXinUser.getHxId());
                            if (accountByHyID.getMsgNum() > 0) {
                                mChatPresenter.msgRead(String.valueOf(mChatParcelable.guid));
                            }
                            DbModle.getInstance().getUserAccountDao().setMsgNum(huanXinUser);//把消息数量至为空
                            DbModle.getInstance().getUserAccountDao().setState(huanXinUser, true);//把标记设置为已经读取
                        }
                    }
                }
            }
            if (mChatParcelable.guid == 10000) {
                rl_set.setVisibility(View.GONE);
            }
        }
        mIsFirstLoad = true;
        if (TimeUtils.timeIsPast()) {//判断页脚是否过期
            msgHandler.sendEmptyMessage(1);
        } else if (!TimeUtils.timeIsPast()) {
            msgHandler.sendEmptyMessage(2);
        }
        mChatPresenter.isBlock();//进入界面先判断是否拉黑或者被拉黑
        saveActivityStatus();//保存activity的状态
        setMsgFlag(mChatParcelable.guid + "", "24");
    }

    private void saveActivityStatus() {
        DataPreference.saveChatActivityAccount(mChatParcelable.account);
        DataPreference.saveChatActivityStatus(0);
    }

    @Override
    protected void setListeners() {
        mIvSwitch.setOnClickListener(this);
        mBtnSend.setOnClickListener(this);
        mIvMore.setOnClickListener(this);
        mTvAlbum.setOnClickListener(this);
        mTvCamera.setOnClickListener(this);
        mRlRoot.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        rl_set.setOnClickListener(this);
        btn_receive.setOnClickListener(this);
        iv_send_gifts.setOnClickListener(this);
        ivCahtReportExit.setOnClickListener(this);
        // 下拉刷新，加载之前的聊天记录
        mSwipeRefreshLayout.setDistanceToTriggerSync(250);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mChatPresenter.setRefresh2();
            }
        });
        mEtInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mEtInput.getText().length() > 0) {
                    mBtnSend.setVisibility(View.VISIBLE);
                    mIvMore.setVisibility(View.GONE);
                    mBtnSend.setEnabled(true);
//                    mBtnSend.setTextColor(ContextCompat.getDrawable(mContext,R.color.main_color));
                    mBtnSend.setTextColor(getResources().getColor(R.color.main_color));
                    mBtnSend.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shape_round_rectangle_gray_border2));
//                    mBtnSend.setBackground(getResources().getDrawable(R.drawable.shape_round_rectangle_gray_border2));
                } else {
                    mBtnSend.setEnabled(false);
                    mBtnSend.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shape_round_rectangle_gray_border));
//                    mBtnSend.setBackground(getResources().getDrawable(R.drawable.shape_round_rectangle_gray_border));
                    mBtnSend.setTextColor(getResources().getColor(R.color.drak_gray));
                }
                if (System.currentTimeMillis() - firstTime > time) {
                    HyphenateHelper.getInstance().sendCmdMessage(mChatParcelable.account, "writting", 1);//正在输入中
                    firstTime = System.currentTimeMillis();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mBtnVoiceSend.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:// 按下时开始录音
                        mBtnVoiceSend.setText(getString(R.string.chat_loose_to_end));
                        mChatPresenter.handleTouchEventDown();
                        // 开始计时
                        mRecordDuration = 0;
                        // 播放动画
                        if (animationDrawable != null) {
                            animationDrawable.start();
                        }
                        mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_VOICE_UI_TIME, 1000);
                        break;

                    case MotionEvent.ACTION_MOVE:// 判断是否上滑取消
                        if (animationDrawable != null) {
                            if (animationDrawable.isRunning()) {
                                animationDrawable.stop();
                            }
                        }
                        mChatPresenter.handleTouchEventMove(event);
                        break;

                    case MotionEvent.ACTION_UP:// 松开时停止录音并发送
                        mBtnVoiceSend.setText(getString(R.string.chat_press_to_speak));
                        mChatPresenter.handleTouchEventUp();
                        if (animationDrawable != null) {
                            if (animationDrawable.isRunning()) {
                                animationDrawable.stop();
                            }
                        }
                        // 结束计时
                        mHandler.removeMessages(C.message.MSG_TYPE_VOICE_UI_TIME);
                        break;
                }
                return true;
            }
        });
        // 处理滑动冲突
        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // 触摸聊天列表时若键盘是打开的，则隐藏键盘
                Util.hideKeyboard(ChatActivity.this, mEtInput);
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        // 禁用下拉刷新
                        mSwipeRefreshLayout.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                        // 恢复下拉刷新
                        mSwipeRefreshLayout.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });
        mEtInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                sendMessage(250, 100);
                return false;
            }
        });
        // 处理滑动冲突
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // 判断是否滑动到顶部
                int scrolledPosition = (mRecyclerView == null || mRecyclerView.getChildCount() == 0) ? 0 : mRecyclerView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(scrolledPosition >= 0);
            }
        });
    }

    @Override
    public void setAQShow(int size) {
        if (UserPreference.isVip() || UserPreference.isAnchor() || mChatParcelable.guid == 10000) {
        } else {
            if (size > 2) {
                btn_receive.setVisibility(View.VISIBLE);
                btn_wite_report.setVisibility(View.GONE);
                ll_bottom.setVisibility(View.GONE);
            } else {
                getIsQAMsg = SharedPreferenceUtil.getBooleanValue(ChatActivity.this, "isQAMsg", mChatParcelable.guid + "", false);
                if (getIsQAMsg) {
                    isQAMsg = true;
                    btn_receive.setVisibility(View.GONE);
                    btn_wite_report.setVisibility(View.VISIBLE);
                    ll_bottom.setVisibility(View.VISIBLE);
                }
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chat_switch:// 切换文字/语音
                if (mChatParcelable.guid == 10000) {
                    Toast.makeText(ChatActivity.this, getString(R.string.not_chat_kefu_voice), Toast.LENGTH_SHORT).show();
                } else {
                    mBtnSend.setVisibility(View.GONE);
                    mIvMore.setVisibility(View.VISIBLE);
                    mLlMore.setVisibility(View.GONE);
                    // 触摸聊天列表时若键盘是打开的，则隐藏键盘
                    Util.hideKeyboard(ChatActivity.this, mEtInput);
                    mIvSwitch.setSelected(!mIvSwitch.isSelected());
                    if (mIvSwitch.isSelected()) { // 显示发送语音
                        mBtnVoiceSend.setText(getString(R.string.chat_press_to_speak));
                        mBtnVoiceSend.setVisibility(View.VISIBLE);
                        mEtInput.setVisibility(View.GONE);
                        mBtnSend.setEnabled(false);
                        mBtnSend.setTextColor(getResources().getColor(R.color.drak_gray));
                    } else { // 显示输入框
                        mBtnVoiceSend.setVisibility(View.GONE);
                        mEtInput.setVisibility(View.VISIBLE);
                        if (mEtInput.getText().length() > 0) {// 已经输入文字
                            mBtnSend.setEnabled(true);
                            mBtnSend.setTextColor(getResources().getColor(R.color.main_color));
                        } else {
                            mBtnSend.setEnabled(false);
                            mBtnSend.setTextColor(getResources().getColor(R.color.drak_gray));
                        }
                    }
                }

                break;

            case R.id.chat_send:// 发送
                mBtnSend.setVisibility(View.GONE);
                mIvMore.setVisibility(View.VISIBLE);
                mChatPresenter.sendTextMessage();
                break;

            case R.id.chat_more:// 显示更多
                // 触摸聊天列表时若键盘是打开的，则隐藏键盘
                Util.hideKeyboard(ChatActivity.this, mEtInput);
                if (mLlMore.isShown()) {
                    mLlMore.setVisibility(View.GONE);
                    mEtInput.requestFocus();
                } else {
                    mEtInput.clearFocus();
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            mLlMore.setVisibility(View.VISIBLE);
                        }
                    }, 300);
                }
                break;

            case R.id.chat_album:// 相册
                mLlMore.setVisibility(View.GONE);
                mChatPresenter.sendImageMessage(false);
                break;

            case R.id.chat_camera:// 拍照
                mLlMore.setVisibility(View.GONE);
                mChatPresenter.sendImageMessage(true);
                break;
            case R.id.chat_root:
                mEtInput.clearFocus();
                break;
            case R.id.chat_activity_rl_back:
                if (mChatParcelable != null) {
                    if (isQAMsg) {
                        SharedPreferenceUtil.setBooleanValue(ChatActivity.this, "isQAMsg", mChatParcelable.guid + "", true);
                    } else {
                        if (getIsQAMsg) {
                            SharedPreferenceUtil.setBooleanValue(ChatActivity.this, "isQAMsg", mChatParcelable.guid + "", false);
                        }
                    }
                }
                if (MainActivity.nowIsFinish()) {//Activity已经销毁了
                    LaunchHelper.getInstance().launchFinish(ChatActivity.this, MainActivity.class);
                } else {
                    finish();
                }
                break;
            case R.id.chat_activity_btn_receive:
                setMsgFlag(mChatParcelable.guid + "", "26");
                LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON, 0, 6));
                break;
            case R.id.chat_activity_rl_set:
                if (mChatParcelable != null) {
                    if(tvReport.getVisibility()==View.VISIBLE){
                        LaunchHelper.getInstance().launch(ChatActivity.this, ReportActivity.class, new ReportParcelable(String.valueOf(mChatParcelable.guid)));
                    }else {
                        CustomDialogAboutPay.reportShow(ChatActivity.this, String.valueOf(mChatParcelable.guid));
                    }
                }
                break;
            case R.id.chat_activity_iv_send_gifts:
                if (mChatParcelable != null) {
                    if (System.currentTimeMillis() - lastClickTime > 1000) {
                        lastClickTime = System.currentTimeMillis();
                        CustomDialogAboutOther.giveGiftShow(ChatActivity.this, String.valueOf(mChatParcelable.guid), mChatParcelable.account, mChatParcelable.imageUrl, mChatParcelable.nickname, 0, false, 3);
                    }
                }
                break;
            case R.id.ic_caht_report_exit:
                rlCahtReport.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    protected void loadData() {
        mChatPresenter.initChatConversation();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DataPreference.saveChatActivityStatus(1);//activity已经销毁了
        mChatPresenter.finish();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mRlRoot, msg);
    }

    @Override
    public void showRecordPopupWindow(int state) {
        if (null == mRecordPopupWindow) {
            View contentView = LayoutInflater.from(mContext).inflate(R.layout.popup_chat_record, null);
            mRecordPopupWindow = new PopupWindow(contentView, WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT, true);
            // 设置背景透明
            mRecordPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            if (null != mRecordPopupWindow) {
                ImageView iv_animation = (ImageView) mRecordPopupWindow.getContentView().findViewById(R.id.popup_record_iv_duration);
                startAnimation(iv_animation);
            }
        }

        View view = mRecordPopupWindow.getContentView();
        if (null != view) {
            // 录音中
            LinearLayout llRecording = (LinearLayout) view.findViewById(R.id.popup_recording_container);
            // 上滑取消
            LinearLayout ll_cancle = (LinearLayout) view.findViewById(R.id.popup_recording_container_cancle);
            switch (state) {
                case C.message.STATE_RECORDING: // 正在录音
                    llRecording.setVisibility(View.VISIBLE);
                    ll_cancle.setVisibility(View.GONE);
                    break;

                case C.message.STATE_CANCELED: // 取消录音
                    llRecording.setVisibility(View.GONE);
                    ll_cancle.setVisibility(View.VISIBLE);
                    break;

                case C.message.STATE_IDLE:// 录音结束
                    llRecording.setVisibility(View.GONE);
                    ll_cancle.setVisibility(View.GONE);
                    break;
            }
        }
        // 居中显示
        mRecordPopupWindow.showAtLocation(mRlRoot, Gravity.CENTER, 0, 0);

    }

    @Override
    public void dismissPopupWindow() {
        if (null != mRecordPopupWindow && mRecordPopupWindow.isShowing()) {
            mRecordPopupWindow.dismiss();
        }
    }

    @Override
    public String getInputText() {
        return mEtInput.getText().toString();
    }

    @Override
    public void clearInput() {
        mEtInput.setText("");
    }

    @Override
    public void hideRefresh() {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChatPresenter.refresh();
            }
        });
    }

    @Override
    public void scroollToBottom() {
        mLlMore.setVisibility(View.GONE);
        mLinearLayoutManager.scrollToPosition(mChatAdapter.getItemCount() - 1);
    }

    @Override
    public ChatAdapter getChatAdapter() {
        return mChatAdapter;
    }

    @Override
    public void sendMessage(long delayedMills, int msgType) {
        mHandler.sendEmptyMessageDelayed(msgType, delayedMills);
    }

    @Override
    public void sendMessage(Message message) {
        mHandler.sendMessage(message);
    }

    @Override
    public void showInterruptDialog(String tip) {
        ToastUtil.showShortToast(mContext, tip);
    }

    @Subscribe
    public void onEvent(UpdateChatDataEvent event) {
        if (event != null && !TextUtils.isEmpty(event.getMessage()) && mChatParcelable != null) {
            mChatPresenter.updateDate(event.getMessage());
        }

    }

    @Subscribe
    public void onEvent(FinishChatActivityEvent event) {
        finish();
    }

    @Subscribe
    public void onEvent(SeeDetailEvent event) {
        if (mChatParcelable != null) {
            if (mChatParcelable.guid == 10000) {

            } else {
                LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                        new UserDetailParcelable(String.valueOf(mChatParcelable.guid)));
                Util.setDetailMsgFlag(mChatParcelable.guid + "", "1");
            }
        }
    }

    @Override
    public void isGoneButtong(boolean flag) {
        if (flag) {
            isFlag = flag;
            if (isQAMsg) {
                btn_receive.setVisibility(View.GONE);
                ll_bottom.setVisibility(View.VISIBLE);
            } else {
                ll_bottom.setVisibility(View.GONE);
                btn_receive.setVisibility(View.VISIBLE);

            }
        }
    }

    @Subscribe
    public void onEvent(MessageArrive event) {
        if (event.getHunxinid().equals(String.valueOf(mChatParcelable.guid))) {
            mChatPresenter.initChatConversation();
            mChatPresenter.msgRead(String.valueOf(mChatParcelable.guid));//把消息置为已读
            DbModle.getInstance().getUserAccountDao().setMsgNum(DbModle.getInstance().getUserAccountDao().getAccountByHyID(event.getHunxinid()));//把消息数量至为空
            tv_name.setVisibility(View.VISIBLE);
            tv_writting.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {//当离开聊天界面的时候消息提醒以通知的形式展示
        super.onResume();
        HyphenateHelper.getInstance().isChatActivity();
        if (mChatParcelable != null) {
            mChatPresenter.isHaveIntercept(mChatParcelable.guid);
        }
    }

    @Override
    protected void onPause() {//当离开聊天界面打开通知的形式展示
        super.onPause();
        HyphenateHelper.getInstance().isNotChatActivity();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (MainActivity.nowIsFinish()) {//Activity已经销毁了
            LaunchHelper.getInstance().launchFinish(ChatActivity.this, MainActivity.class);
        } else {
            finish();
        }
    }

    @Subscribe
    public void onEvent(QAEvent qaEvent) {
        if (!isFinishing()) {
            btn_receive.setVisibility(View.GONE);
            ll_bottom.setVisibility(View.VISIBLE);
            isQAMsg = true;
            SharedPreferenceUtil.setBooleanValue(ChatActivity.this, "isQAMsg", mChatParcelable.guid + "", true);
            CustomDialogAboutOther.qaMessageShow(ChatActivity.this, qaEvent.getQaMessage(), qaEvent.getUserAccount(), qaEvent.getGuid(), qaEvent.getQaMsg(), view);
        }

    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }

    @Subscribe
    public void onEvent(NettyMessage event) {
        Message msg = Message.obtain();
        msg.obj = event;
        msg.what = 1;
        msgHandler.sendMessage(msg);
    }

    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    isCanShowPop(true);
                    break;
                case 2:
                    closeShowPop();
                    break;
            }
        }
    };

    @Override
    protected void closeShowPop() {
        super.closeShowPop();
    }


    @Override
    protected void isCanShowPop(boolean flag) {
        super.isCanShowPop(flag);
    }

    @Subscribe
    public void onEvent(CustomCmd event) {
        if (event != null) {
            Message msg = new Message();
            msg.obj = event;
            handl.sendMessage(msg);
        }
    }

    private boolean isFlag = true;
    private Handler handl = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            CustomCmd event = (CustomCmd) msg.obj;
            if (event.getSendUserAccount().equals(mChatParcelable.account)) {
                tv_writting.setVisibility(View.VISIBLE);
                tv_name.setVisibility(View.GONE);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        tv_name.setVisibility(View.VISIBLE);
                        tv_writting.setVisibility(View.GONE);
                    }
                }, 5000);
            }
        }
    };

    @Subscribe
    public void onEvent(PaySuccessEvent event) {
        payHandler.sendEmptyMessage(1);
    }

    private Handler payHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            isFlag = false;
            btn_receive.setVisibility(View.GONE);
            ll_bottom.setVisibility(View.VISIBLE);
        }
    };

    //重写onKeyDown方法,对按键(不一定是返回按键)监听
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {//当返回按键被按下
            if (mChatParcelable != null) {
                if (isQAMsg) {
                    SharedPreferenceUtil.setBooleanValue(ChatActivity.this, "isQAMsg", mChatParcelable.guid + "", true);
                } else {
                    if (getIsQAMsg) {
                        SharedPreferenceUtil.setBooleanValue(ChatActivity.this, "isQAMsg", mChatParcelable.guid + "", false);
                    }
                }
            }
            finish();
        }
        return false;
    }

    private static void setMsgFlag(String guid, String tag) {
        ApiManager.userActivityTag(UserPreference.getId(), guid, tag, "1", new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    private int loginNum = 0;

    private void HyLogin() {
        String account = UserPreference.getAccount();
        String password = UserPreference.getPassword();
        HyphenateHelper.getInstance().login(UserPreference.getAccount(), UserPreference.getPassword(), new HyphenateHelper.OnLoginCallback() {
            public void onSuccess() {
                Log.e("AAAAAAA", "onSussess: 登录成功---------======登录次数" + loginNum);

            }

            @Override
            public void onFailed() {
                Log.e("AAAAAAA", "onFailed: 登录失败---------======" + loginNum);
                ++loginNum;
                if (loginNum >= 4) {
                    return;
                }
                HyLogin();//登录失败5次之后不再登录
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
