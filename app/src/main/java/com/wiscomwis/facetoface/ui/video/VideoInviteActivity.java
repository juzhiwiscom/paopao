package com.wiscomwis.facetoface.ui.video;

import android.content.Context;
import android.media.Ringtone;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.Vibrator;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragmentActivity;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.AgoraParams;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.AgoraEvent;
import com.wiscomwis.facetoface.event.CloseRingtoneEvent;
import com.wiscomwis.facetoface.event.FinishVideoToChatActivityEvent;
import com.wiscomwis.facetoface.parcelable.VideoInviteParcelable;
import com.wiscomwis.facetoface.ui.video.contract.VideoInviteContract;
import com.wiscomwis.facetoface.ui.video.presenter.VideoInvitePresenter;
import com.wiscomwis.library.image.CropCircleTransformation;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;

/**
 * 视频邀请页面（邀请/被邀请）
 * Created by zhangdroid on 2017/5/27.
 */
public class VideoInviteActivity extends BaseFragmentActivity implements View.OnClickListener,
        SurfaceHolder.Callback, VideoInviteContract.IView {
    @BindView(R.id.video_invite_avatar)
    ImageView mIvAvatar;
    @BindView(R.id.video_invite_nickname)
    TextView mTvNickname;
    @BindView(R.id.video_invite_tip)
    TextView mTvVideoTip;
    @BindView(R.id.video_cancel)
    TextView mTvVideoCancel;
    @BindView(R.id.video_reject)
    TextView mTvVideoReject;
    @BindView(R.id.video_accept)
    TextView mTvVideoAccept;
    // 遮罩层
    @BindView(R.id.video_invite_avatar_big)
    ImageView mIvAvatarBig;
    @BindView(R.id.video_invite_mask)
    View mMaskView;
    // 相机预览
    @BindView(R.id.video_invite_preview)
    SurfaceView mSurfaceView;
    private VideoInviteParcelable mVideoInviteParcelable;
    private VideoInvitePresenter mVideoInvitePresenter;
    private SurfaceHolder mSurfaceHolder;
    // 发起视频时的频道id，生成规则：邀请人guid+被邀请人guid
    private String mChannelId;
    Ringtone ringtone;
    private long lastClickTime = 0;
    private boolean flag = true;
    private boolean flag2 = false;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 3:
                    if (flag2) {
                        Util.stopMp3();
                    }
                    break;
                case 1:
                    if (flag) {
                            flag = false;
                            flag2 = true;
                        Util.playMp3(VideoInviteActivity.this);

                    }
                    break;
            }
        }
    };
    private boolean isClick=true;
    private boolean isInto=true;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_video_invite;
    }

    @Override
    protected void setWakeAndLack() {
        super.setWakeAndLack();
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        Log.e("BBBBBBB","进入VideoInviteActivity2==Parcelable");
        mVideoInviteParcelable = (VideoInviteParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        UserPreference.setIntoVideo(false);
        isInto=true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isInto=false;
                EventBus.getDefault().post(new CloseRingtoneEvent(true));
            }
        },3000);
        Log.e("Main===","进入VideoInviteActivity==initViews");
        if (null != mVideoInviteParcelable) {
            ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(mVideoInviteParcelable.imgageUrl).transform(new CropCircleTransformation(mContext))
                    .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(mIvAvatar).build());
            mTvNickname.setText(mVideoInviteParcelable.nickname);
            if (mVideoInviteParcelable.type == 1) {
                mTvVideoTip.setText(mVideoInviteParcelable.isInvited ? getString(R.string.video_invited2) : getString(R.string.video_invite2));
            } else {
                mTvVideoTip.setText(mVideoInviteParcelable.isInvited ? getString(R.string.video_invited) : getString(R.string.video_invite));
            }
            mTvVideoCancel.setVisibility(mVideoInviteParcelable.isInvited ? View.GONE : View.VISIBLE);
            mTvVideoReject.setVisibility(mVideoInviteParcelable.isInvited ? View.VISIBLE : View.GONE);
            mTvVideoAccept.setVisibility(mVideoInviteParcelable.isInvited ? View.VISIBLE : View.GONE);
            if (mVideoInviteParcelable.isInvited) {
                ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(mVideoInviteParcelable.imgageUrl)
                        .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(mIvAvatarBig).build());
                mIvAvatarBig.setVisibility(View.VISIBLE);
                mMaskView.setVisibility(View.VISIBLE);
                mSurfaceView.setVisibility(View.GONE);
                // 被邀请加入的频道id
                mChannelId = mVideoInviteParcelable.channelId;
                handler.sendEmptyMessage(1);
            } else {
                mIvAvatarBig.setVisibility(View.GONE);
                mMaskView.setVisibility(View.GONE);
                mSurfaceView.setVisibility(View.VISIBLE);
                // 生成channelId
                mChannelId = UserPreference.getId() + mVideoInviteParcelable.uId;
            }
            mVideoInvitePresenter = new VideoInvitePresenter(this, mVideoInviteParcelable.uId, mVideoInviteParcelable.account,
                    mVideoInviteParcelable.nickname, "", mVideoInviteParcelable.type, mVideoInviteParcelable.imgageUrl);
            if (!mVideoInviteParcelable.isInvited) {
                if (mVideoInviteParcelable.type == 1) {
                    mVideoInvitePresenter.iosPush(String.valueOf(mVideoInviteParcelable.uId), "1", "2");
                } else {
                    mVideoInvitePresenter.iosPush(String.valueOf(mVideoInviteParcelable.uId), "1", "1");
                }
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!TextUtils.isEmpty(DataPreference.getYejiaoString())) {
                        DataPreference.saveYeJiaoTime(System.currentTimeMillis() - 60 * 1000);//修改页脚的时间
                        mTvVideoTip.setText(DataPreference.getYejiaoString());
                    }
                }
            }, 1000);
        }

    }

    @Override
    protected void setListeners() {
        mTvVideoCancel.setOnClickListener(this);
        mTvVideoReject.setOnClickListener(this);
        mTvVideoAccept.setOnClickListener(this);
        if (!mVideoInviteParcelable.isInvited) {
            // 主动邀请视频时开启相机预览
            mSurfaceHolder = mSurfaceView.getHolder();
            mSurfaceHolder.addCallback(this);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mVideoInvitePresenter.startPreView();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mVideoInvitePresenter.stopPreview();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.video_cancel:// 取消
                if(isClick){
                    isClick=false;
                    if (System.currentTimeMillis() - lastClickTime > 1000) {
                        Util.stopMp3();
                        lastClickTime = System.currentTimeMillis();
                        mVideoInvitePresenter.cancelInVite(mChannelId, mVideoInviteParcelable.account, String.valueOf(mVideoInviteParcelable.uId));
                        if (mVideoInviteParcelable.type == 1) {
                            mVideoInvitePresenter.iosPush(String.valueOf(mVideoInviteParcelable.uId), "2", "2");
                        } else {
                            mVideoInvitePresenter.iosPush(String.valueOf(mVideoInviteParcelable.uId), "2", "1");
                        }
                        shake(VideoInviteActivity.this);
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isClick=true;

//                            AgoraHelper.getInstance().leaveChannel(mChannelId);
                            finishActivity(0);
                        }
                    },3000);
                }

                break;

            case R.id.video_reject:// 拒绝
                isInto=false;
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    handler.sendEmptyMessage(3);
                    setMsgFlag(mVideoInviteParcelable.uId+"","2");
                    mVideoInvitePresenter.refuseInvite(mChannelId, mVideoInviteParcelable.account, String.valueOf(mVideoInviteParcelable.uId));
                    EventBus.getDefault().post(new FinishVideoToChatActivityEvent(mVideoInviteParcelable.uId,mVideoInviteParcelable.account,mVideoInviteParcelable.nickname,mVideoInviteParcelable.imgageUrl,0));
                    shake(VideoInviteActivity.this);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                            Util.stopMp3();
                        }
                    },2000);
                }
                break;

            case R.id.video_accept:// 接受
                isInto=false;
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    setMsgFlag(mVideoInviteParcelable.uId+"","1");
//                    handler.sendEmptyMessage(3);
                    mVideoInvitePresenter.acceptInvite(mChannelId, mVideoInviteParcelable.account);
                    shake(VideoInviteActivity.this);
                }
                break;
        }
    }
    @Override
    protected void loadData() {
        if (!mVideoInviteParcelable.isInvited) {
            String account = mVideoInviteParcelable.account;
            int uid = Integer.parseInt(UserPreference.getId());
            String nickname = UserPreference.getNickname();
            String smallImage = UserPreference.getSmallImage();
            int type = mVideoInviteParcelable.type;
            int inviteType = mVideoInviteParcelable.inviteType;
            long currentTime = Util.getCurrentTime();
//            long currentTime = System.currentTimeMillis();
            AgoraParams agoraParams = new AgoraParams(uid,nickname,smallImage,type,inviteType,currentTime);
            String s = new Gson().toJson(agoraParams);
            // 发起视频邀请
            mVideoInvitePresenter.startInvite(mChannelId,account,s);
        }
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        UserPreference.setIntoVideo(true);
        if(!isInto){
            Util.stopMp3();
            EventBus.getDefault().post(new CloseRingtoneEvent(true));
        }
        super.onDestroy();
//        if (ringtone != null && ringtone.isPlaying()) {
//            ringtone.stop();
//        }

        Log.e("Main===","进入VideoInviteActivity==onDestroy");
//        handler.sendEmptyMessage(3);
    }

    @Override
    public void onBackPressed() {
        // do nothing,屏蔽返回键
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        ToastUtil.showShortToast(mContext, msg);
    }

    @Override
    public void hideSurfaceView() {
        mSurfaceView.setVisibility(View.GONE);
    }

    @Override
    public SurfaceHolder getHolder() {
        return mSurfaceHolder;
    }

    @Override
    public void changeInviteState(String msg) {
        if (!TextUtils.isEmpty(msg)) {
            mTvVideoTip.setText(msg);
        }
    }

    // 这是内陆城市的代表
    @Override
    public void finishActivity(int seconds) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                EventBus.getDefault().post(new FinishVideoToChatActivityEvent(mVideoInviteParcelable.uId,mVideoInviteParcelable.account,mVideoInviteParcelable.nickname,mVideoInviteParcelable.imgageUrl,0));
                finish();

            }
        }, seconds * 1000);
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return getSupportFragmentManager();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AgoraEvent event) {
        Log.e("AAAAAA","进入VideoInviteActivity==收到声网消息");
        mVideoInvitePresenter.handleAgoraEvent(event);
    }

    /**
     * 接通或者挂断震动一下
     *
     * @param context
     */
    public static void shake(Context context) {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(88);
    }

    @Override
    protected void onPause() {
        super.onPause();
        DataPreference.saveYejiaoString(0);
        DataPreference.saveNoSeeVideo(true);//显示悬浮框

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("Main===","进入VideoInviteActivity==stop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("AAAAAA","进入VideoInviteActivity==onResume");

    }

    @Override
    public boolean onKeyDown(int keyCode,KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK)
            return true;//不执行父类点击事件
        return super.onKeyDown(keyCode, event);//继续执行父类其他点击事件
    }
    private static void setMsgFlag(String guid,String tag){
        ApiManager.userActivityTag(UserPreference.getId(),guid, "27", tag, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }
}
