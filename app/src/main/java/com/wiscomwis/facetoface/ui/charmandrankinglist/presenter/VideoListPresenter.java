package com.wiscomwis.facetoface.ui.charmandrankinglist.presenter;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.CustomDialogAboutPay;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.VideoSquare;
import com.wiscomwis.facetoface.data.model.VideoSquareList;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.StartEvent;
import com.wiscomwis.facetoface.event.StartVideoEvent;
import com.wiscomwis.facetoface.ui.charmandrankinglist.adapter.VideoListAdapter;
import com.wiscomwis.facetoface.ui.charmandrankinglist.contract.VideoListContract;
import com.wiscomwis.facetoface.ui.main.MainActivity;
import com.wiscomwis.library.util.LogUtil;
import com.wiscomwis.library.util.SharedPreferenceUtil;
import com.wiscomwis.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by xuzhaole on 2018/3/20.
 */

public class VideoListPresenter implements VideoListContract.IPresenter {
    private VideoListAdapter adapter;
    private VideoListContract.IView mVideoListView;
    private Context mContext;
    public VideoSquareList videoList;
    private boolean isRefish=false;


    public VideoListPresenter(VideoListContract.IView videoListView) {
        this.mVideoListView = videoListView;
        mContext = videoListView.obtainContext();
    }

    @Override
    public void start() {
        adapter = new VideoListAdapter(mVideoListView.getFragment().getChildFragmentManager());
        mVideoListView.setAdapter(adapter);
    }

    @Override
    public void loadVideoShowList(int num) {
        Log.e("AAAAAA","loadVideoShowList=="+num);
        if(num==100){
            num=1;
            isRefish=true;
            mVideoListView.clearPosition();
        }
        final int finalNum = num;
        final int finalNum1 = num;
        ApiManager.VideoShowList(num + "", "5", new IGetDataListener<VideoSquareList>() {
            @Override
            public void onResult(VideoSquareList videoSquareList, boolean isEmpty) {
                videoList=videoSquareList;
                if (videoSquareList.getVideoSquareList() == null || videoSquareList.getVideoSquareList().size() == 0) {
                    if (finalNum == 1) {
                        mVideoListView.setEmptyView(true, null);
                    } else {
                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.tip_no_more));
                    }
                } else {
//                    将图片缓存
                    cachePhoto(videoSquareList.getVideoSquareList(), finalNum1);
                    mVideoListView.setPostion();
                    if (finalNum == 1) {
                        if(isRefish){
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    EventBus.getDefault().post(new StartVideoEvent());
                                }
                            },500);
                            EventBus.getDefault().post(new StartEvent());
                            isRefish=false;
                            start();
                        }
                        adapter.setData(videoSquareList.getVideoSquareList());
                    } else {
                        adapter.addData(videoSquareList.getVideoSquareList());
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mVideoListView.setErrorView(true, msg);
            }
        });
    }

    private void cachePhoto(List<VideoSquare> videoSquareList, int finalNum1) {
        for (int i = 0; i < videoSquareList.size(); i++) {
//            String iconUrl = videoSquareList.get(i).gettUserVideoShow().getIconUrl();
            String videoUrl = videoSquareList.get(i).gettUserVideoShow().getVideoUrl();
            String thumbnailUrl = videoSquareList.get(i).gettUserVideoShow().getThumbnailUrl();
            String iconUrlMininum = videoSquareList.get(i).getUserBase().getIconUrlMininum();
            if(!TextUtils.isEmpty(iconUrlMininum)){
                getImg(iconUrlMininum);
            }
            if(!TextUtils.isEmpty(videoUrl)){
                String cacheImg = SharedPreferenceUtil.getStringValue(mContext, "cacheImg", videoUrl, "url");
                if (cacheImg.equals("url")) {
                    new getVideoCacheAsyncTask(mContext).execute(videoUrl,thumbnailUrl);
                }
            }

            if(i==videoSquareList.size()-1&&finalNum1!=0&&videoSquareList.size()>2){
//                继续加载数据
                uploadPhoto(finalNum1+1);
            }
        }
    }

    private void uploadPhoto(final int finalNum1) {
        ApiManager.VideoShowList(finalNum1 + 1+"", "5", new IGetDataListener<VideoSquareList>() {
            @Override
            public void onResult(VideoSquareList videoSquareList, boolean isEmpty) {
//                videoList=videoSquareList;
                if (videoSquareList.getVideoSquareList() == null || videoSquareList.getVideoSquareList().size() == 0) {
                    if (finalNum1 == 1) {
//                        mVideoListView.setEmptyView(true, null);
                    } else {
//                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.tip_no_more));
                    }
                } else {
//                    将图片缓存
                    cachePhoto(videoSquareList.getVideoSquareList(),0);
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mVideoListView.setErrorView(true, msg);
            }
        });
    }

    @Override
    public void reportShow(int currentPosition) {
        List<VideoSquare> data = adapter.getData();
        if (data != null && currentPosition < data.size()) {
            VideoSquare videoSquare = data.get(currentPosition);
            long guid = videoSquare.getUserBase().getGuid();
            CustomDialogAboutPay.reportShow(mContext, guid + "");
        }
    }

    public class getVideoCacheAsyncTask extends AsyncTask<String, Void, Bitmap> {
        private final Context context;
        private String imgUrl;

        public getVideoCacheAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            imgUrl =  params[0];
            //MediaMetadataRetriever 是android中定义好的一个类，提供了统一
            //的接口，用于从输入的媒体文件中取得帧和元数据；
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            //创建FFmpegMediaMetadataRetriever对象
            try {
                if(Build.VERSION.SDK_INT >= 14){
                    //根据文件路径获取缩略图
                    retriever.setDataSource(imgUrl, new HashMap());
                }else {
                    retriever.setDataSource(imgUrl);
                }
                //获得第一帧图片
                return retriever.getFrameAtTime();

            } catch (Exception e) {
                getImg(params[1]);
                e.printStackTrace();
            } finally {
                retriever.release();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result == null) {
                return;
            }
            String path = saveBitmap(mContext, result);
            SharedPreferenceUtil.setStringValue(context,"cacheImg",imgUrl,path);
            LogUtil.v("cacheImg","视频第一帧成功=="+path);


//            Bitmap bmp= BitmapFactory.decodeFile(path);
//            img.setImageBitmap(bmp);

        }
    }

    private void getImg(String param) {
        String thumbnailUrl= param;
        if(!TextUtils.isEmpty(thumbnailUrl)){
            String cacheIg = UserPreference.getCacheIg(thumbnailUrl);
            if(TextUtils.isEmpty(cacheIg))
                new getImageCacheAsyncTask(mContext).execute(thumbnailUrl);
        }
    }

    private static final String SD_PATH = "/sdcard/dskqxt/pic/";
    private static final String IN_PATH = "/dskqxt/pic/";

    /**
     * 随机生产文件名
     *
     * @return
     */
    private static String generateFileName() {
        return UUID.randomUUID().toString();
    }
    /**
     * 保存bitmap到本地
     *
     * @param context
     * @param mBitmap
     * @return
     */
    public static String saveBitmap(Context context, Bitmap mBitmap) {
        String savePath;
        File filePic;
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            savePath = SD_PATH;
        } else {
            savePath = context.getApplicationContext().getFilesDir()
                    .getAbsolutePath()
                    + IN_PATH;
        }
        try {
            filePic = new File(savePath + generateFileName() + ".jpg",".nomedia");//加.nomedia为了在系统系统相册中隐藏缓存图片
            if (!filePic.exists()) {
                filePic.getParentFile().mkdirs();
                filePic.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(filePic);
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

        return filePic.getPath();
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private Bitmap createVideoThumbnail(String url) {
        Bitmap bitmap = null;
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        int kind = MediaStore.Video.Thumbnails.MINI_KIND;
        try {
            if (Build.VERSION.SDK_INT >= 14) {
                retriever.setDataSource(url, new HashMap<String, String>());
            } else {
                retriever.setDataSource(url);
            }
            bitmap = retriever.getFrameAtTime();
        } catch (IllegalArgumentException ex) {
            // Assume this is a corrupt video file
        } catch (RuntimeException ex) {
            // Assume this is a corrupt video file.
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException ex) {
                // Ignore failures while cleaning up.
            }
        }
        return bitmap;
    }

    public static class getImageCacheAsyncTask extends AsyncTask<String, Void, File> {
        private final Context context;
        private final WeakReference<Context> weakReference;
        private String imgUrl;

        public getImageCacheAsyncTask(Context context) {
//            使用软引用防止ANR
            weakReference = new WeakReference<>(context);
            this.context = context;
        }

        @Override
        protected File doInBackground(String... params) {
            imgUrl =  params[0];
            try {
                return Glide.with(context)
                        .load(imgUrl)
                        .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                        .get();
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(File result) {
            MainActivity activity = (MainActivity) weakReference.get();
            if (activity != null) {
                if (result == null) {
                    return;
                }
                //此path就是对应文件的缓存路径
                String path = result.getPath();
                Log.e("path", path);
                UserPreference.setCacheIg(imgUrl,path);
            }


        }
    }

}
