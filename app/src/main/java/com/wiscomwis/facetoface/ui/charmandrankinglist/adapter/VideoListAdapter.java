package com.wiscomwis.facetoface.ui.charmandrankinglist.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.wiscomwis.facetoface.data.model.VideoSquare;
import com.wiscomwis.facetoface.ui.charmandrankinglist.VideoRoomFragment;

import java.util.List;

/**
 * Created by xuzhaole on 2018/3/20.
 */

public class VideoListAdapter extends FragmentStatePagerAdapter {
    private List<VideoSquare> mList;

    public VideoListAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        if(mList.size()>position){
            VideoRoomFragment videoRoomFragment = new VideoRoomFragment();
            videoRoomFragment.setData(mList.get(position));
            return videoRoomFragment;
        }
        return null;

    }

    @Override
    public int getCount() {
        return mList == null || mList.size() == 0 ? 0 : mList.size();
    }


    public void setData(List<VideoSquare> list) {
        if(mList!=null){
            mList.clear();
        }
        this.mList = list;
        notifyDataSetChanged();
    }

    public void addData(List<VideoSquare> list) {
        this.mList.addAll(list);
        notifyDataSetChanged();
    }

    public List<VideoSquare> getData() {
        return mList;
    }
}
