package com.wiscomwis.facetoface.ui.homepage.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.LayoutRes;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.model.SearchUser;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.library.adapter.base.BaseQuickAdapter;
import com.wiscomwis.library.adapter.base.BaseViewHolder;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;

import java.util.List;

/**
 * Created by xuzhaole on 2018/1/9.
 */

public class HomeListAdapter extends BaseQuickAdapter<SearchUser, BaseViewHolder> {
    int i=0;

    public HomeListAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position, List<Object> payloads) {

        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else {
            holder.getView(R.id.item_homepage_big_iv_sayhello).setBackgroundResource(R.drawable.say_helloed);
        }
    }

    @Override
    protected void convert(final BaseViewHolder holder, final SearchUser searchUser) {
        if (searchUser != null) {

            String uesrId = searchUser.getUesrId()+"";
            Util.addUserId(uesrId);
            ImageView imageView = holder.getView(R.id.item_homepage_iv_avatar);
            String cacheIg = UserPreference.getCacheIg(searchUser.getIconUrlMiddle());
            if(!TextUtils.isEmpty(cacheIg)){
                Bitmap bitmap = BitmapFactory.decodeFile(cacheIg);
                imageView.setImageBitmap(bitmap);
            }else {
//                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(searchUser.getIconUrlMiddle())
//                        .placeHolder(Util. getDefaultImage()).error(Util.getDefaultImage()).imageView(imageView).build());
                getImage(searchUser, i, imageView,holder);
            }
            TextView tv_distance = (TextView) holder.getView(R.id.item_homepage_tv_distance);
            tv_distance.setText(searchUser.getDistance()+"km");
            holder.setText(R.id.item_homepage_tv_nickname, searchUser.getNickName());
            holder.setText(R.id.item_homepage_tv_age_and_height, searchUser.getAge() + mContext.getString(R.string.edit_info_years_old) + "  " + mContext.getString(R.string.distance));
            TextView tv_state = holder.getView(R.id.item_homepage_tv_state);
            TextView tv_age = holder.getView(R.id.item_homepage_tv_age);
            TextView tv_online = holder.getView(R.id.item_homepage_big_tv_online);
            TextView tv_heard_word = holder.getView(R.id.item_homepage_tv_heard_word);
            RelativeLayout rl_sex = holder.getView(R.id.item_homepage_rl_age_sex);
            TextView tv_big_state = holder.getView(R.id.item_homepage_big_tv_state);
            ImageView iv_vip = holder.getView(R.id.item_homepage_iv_vip);
            // 视频
            ImageView iBVideo = holder.getView(R.id.item_homepage_iv_state);
//            final ImageView iv_sayhello = holder.getView(R.id.item_homepage_big_iv_sayhello);
            if (searchUser.getVipDays()>0||searchUser.getVipDays()==-1) {
                iv_vip.setVisibility(View.VISIBLE);
            }else{
                iv_vip.setVisibility(View.GONE);
            }
            if(searchUser.getGender().equals("0")){
                rl_sex.setBackgroundResource(R.drawable.icon_male);
            }else{
                rl_sex.setBackgroundResource(R.drawable.icon_famle);
            }
            tv_heard_word.setText(searchUser.getOwnWords());
            tv_age.setText(searchUser.getAge());
            final int status = searchUser.getStatus();
            int onlineStatus = searchUser.getOnlineStatus();
            if (onlineStatus == 3) {
                tv_online.setBackgroundResource(R.drawable.linear_circle_red_bg);
                tv_online.setVisibility(View.VISIBLE);
                tv_big_state.setText(mContext.getString(R.string.not_online));
            } else if(onlineStatus==2){
                tv_online.setBackgroundResource(R.drawable.linear_circle_green_bg);
                tv_online.setVisibility(View.VISIBLE);
                tv_big_state.setText(mContext.getString(R.string.online_status_chat));
            }else{
                tv_online.setBackgroundResource(R.drawable.linear_circle_green_bg);
                tv_online.setVisibility(View.VISIBLE);
                tv_big_state.setText(mContext.getString(R.string.edit_info_status));
            }
//            if (onlineStatus == -1) {
//                tv_big_state.setText(mContext.getString(R.string.not_online));
//                tv_online.setVisibility(View.GONE);
//            } else {
//                tv_big_state.setText(mContext.getString(R.string.edit_info_status));
//                tv_online.setVisibility(View.VISIBLE);
//            }
            if (searchUser.getVipDays() > 0) {
                holder.getView(R.id.iv_vip).setVisibility(View.VISIBLE);
            } else {
                holder.getView(R.id.iv_vip).setVisibility(View.GONE);
            }
//            if (UserPreference.isAnchor()) {
//                iv_sayhello.setVisibility(View.VISIBLE);
//                iv_sayhello.setBackgroundResource(R.drawable.say_hello);
//            } else {
//                iv_sayhello.setVisibility(View.GONE);
//            }
//            int isSayHello = searchUser.getIsSayHello();
//            if (isSayHello == 1) {
//                iv_sayhello.setBackgroundResource(R.drawable.say_helloed);
//            } else {
//                iv_sayhello.setBackgroundResource(R.drawable.say_hello);
//            }
//            iv_sayhello.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Toast.makeText(mContext, mContext.getString(R.string.hello_success), Toast.LENGTH_SHORT).show();
//                    searchUser.setIsSayHello(1);
//                    iv_sayhello.setBackgroundResource(R.drawable.say_helloed);
//                    notifyItemChanged(holder.getAdapterPosition(), searchUser);
//                    ApiManager.sayHello(String.valueOf(searchUser.getUesrId()), new IGetDataListener<BaseModel>() {
//                        @Override
//                        public void onResult(BaseModel baseModel, boolean isEmpty) {
//
//                        }
//
//                        @Override
//                        public void onError(String msg, boolean isNetworkError) {
//
//                        }
//                    });
//                }
//            });
            switch (status) {
                case C.homepage.STATE_FREE:// 空闲
                    iBVideo.setVisibility(View.VISIBLE);
                    tv_state.setVisibility(View.VISIBLE);
                    iBVideo.setImageResource(R.drawable.ic_video_free);
                    tv_state.setText(mContext.getString(R.string.invitable));
                    break;
                case C.homepage.STATE_BUSY:// 忙线中
                    iBVideo.setVisibility(View.VISIBLE);
                    tv_state.setVisibility(View.VISIBLE);
                    iBVideo.setImageResource(R.drawable.ic_video_busy);
                    tv_state.setText(mContext.getString(R.string.talking));
                    break;
                case C.homepage.STATE_NO_DISTRUB:// 勿扰
                    iBVideo.setVisibility(View.GONE);
                    tv_state.setVisibility(View.GONE);
                    tv_big_state.setText(mContext.getString(R.string.message_state_nodistrub));
                    break;
                default:
                    iBVideo.setVisibility(View.VISIBLE);
                    tv_state.setVisibility(View.VISIBLE);
                    iBVideo.setImageResource(R.drawable.ic_video_free);
                    tv_state.setText(mContext.getString(R.string.invitable));
                    break;
            }
        }
    }

    private void getImage(final SearchUser searchUser, final int i, final ImageView imageView, final BaseViewHolder holder) {
        ImageLoaderUtil.getInstance().load(Glide.with(mContext), new ImageLoader.Builder().url(searchUser.getIconUrlMiddle())
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(imageView).build(), new ImageLoaderUtil.OnImageLoadListener() {
            @Override
            public void onCompleted() {
//                Log.e("AAAAAA","图片加载成功");
            }

            @Override
            public void onFailed() {
                Log.e("AAAAAA","图片加载失败--i="+i);
                int ii=i;
                ii++;
                if(ii<2){
                    getImage(searchUser, ii, imageView,holder);
                }else if(ii<20){
                    Log.e("AAAAAA","图片加载失败--convert="+i);
                    convert(holder,searchUser);
                }
            }
        });
    }
}
