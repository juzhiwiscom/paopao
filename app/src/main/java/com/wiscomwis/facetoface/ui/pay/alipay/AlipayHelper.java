package com.wiscomwis.facetoface.ui.pay.alipay;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.alipay.sdk.app.PayTask;
import com.wiscomwis.facetoface.common.AgoraHelper;
import com.wiscomwis.facetoface.common.CustomDialogAboutPay;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.PayResult;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.PaySuccessEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

/**
 * Created by WangYong on 2017/9/7.
 */
public class AlipayHelper {
    private static AlipayHelper sInstance;
    private static final int SDK_PAY_FLAG=1;
    private  static Context context1;
    private static int type=1;//1钻石 2 vip 3 钥匙
    private static String serviceName="";
    private static String price="";
    private static int paySource=6;
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case SDK_PAY_FLAG:
                    PayResult payResult = new PayResult((Map<String, String>) msg.obj);
                    if(payResult.getResultStatus().equals("9000")){
                        if(context1!=null){
                            EventBus.getDefault().post(new PaySuccessEvent());
                            setMsgFlag(String.valueOf(paySource));
                            //支付成功之后直接重新访问个人的数据
                            CustomDialogAboutPay.paySucceedShow(context1,serviceName,price,type,paySource);
                        }
                    }else{
//                        if(paySource==11){
//                            String videoCallAccount = DataPreference.getVideoCallAccount();
//                            String videoCallId = DataPreference.getVideoCallId();
//                            String videoCallImageUrl = DataPreference.getVideoCallImageUrl();
//                            String videoCallName = DataPreference.getVideoCallName();
//                            if (!videoCallAccount.equals("0")) {
//                                EventBus.getDefault().post(new FinishVideoToChatActivityEvent(Long.parseLong(videoCallId),videoCallAccount,videoCallName,videoCallImageUrl,0));
//                            }
//                        }
                        if(context1!=null){
                            CustomDialogAboutPay.payFaildShow(context1,1);
                        }
                    }
                    break;
            }
        }
    };
    public static AlipayHelper getInstance() {
        if (null == sInstance) {
            synchronized (AgoraHelper.class) {
                if (null == sInstance) {
                    sInstance = new AlipayHelper();
                }
            }
        }
        return sInstance;
    }
    //调用支付宝支付的api
    public void callAlipayApi(Context context, final String orderInfo,String serviceName1,String price1,int type1,int paySource2){
        context1=context;
        type=type1;
        serviceName=serviceName1;
        price=price1;
        paySource=paySource2;
        Runnable payRunnable = new Runnable() {

            @Override
            public void run() {
                PayTask alipay = new PayTask((Activity) context1);
                Map<String, String> result = alipay.payV2(orderInfo,true);
                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                handler.sendMessage(msg);
            }
        };
        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }
    //埋点
    private static void setMsgFlag(String extendTag){
        ApiManager.userActivityTag(UserPreference.getId(), "", "16", extendTag, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }
}
