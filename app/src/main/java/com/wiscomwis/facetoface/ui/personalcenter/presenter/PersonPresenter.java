package com.wiscomwis.facetoface.ui.personalcenter.presenter;

import android.text.TextUtils;
import android.view.View;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.CheckStatus;
import com.wiscomwis.facetoface.data.model.HostInfo;
import com.wiscomwis.facetoface.data.model.MyInfo;
import com.wiscomwis.facetoface.data.model.Switch;
import com.wiscomwis.facetoface.data.model.UpLoadMyPhoto;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.data.model.UserBean;
import com.wiscomwis.facetoface.data.model.UserDetail;
import com.wiscomwis.facetoface.data.model.UserPhoto;
import com.wiscomwis.facetoface.data.preference.AnchorPreference;
import com.wiscomwis.facetoface.data.preference.BeanPreference;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.SwitchPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.parcelable.BigPhotoParcelable;
import com.wiscomwis.facetoface.ui.detail.adapter.AlbumPhotoAdapter;
import com.wiscomwis.facetoface.ui.personalcenter.contract.PersonContract;
import com.wiscomwis.facetoface.ui.photo.BigPhotoActivity;
import com.wiscomwis.library.adapter.MultiTypeRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.util.LaunchHelper;

import java.io.File;
import java.util.List;

import static com.wiscomwis.facetoface.common.TimeUtils.mContext;

/**
 * Created by zhangdroid on 2017/5/27.
 */
public class PersonPresenter implements PersonContract.IPresenter {
    private PersonContract.IView mPersonView;
    private AlbumPhotoAdapter mAlbumPhotoAdapter;

    public PersonPresenter(PersonContract.IView view) {
        this.mPersonView = view;
    }

    private boolean isCanLoad = true;

    @Override
    public void start() {
        mAlbumPhotoAdapter = new AlbumPhotoAdapter(mContext, R.layout.album_recyclerview_item, 0);
        mPersonView.setAdapter(mAlbumPhotoAdapter);
        mAlbumPhotoAdapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                final List<UserPhoto> adapterDataList = mAlbumPhotoAdapter.getAdapterDataList();
                if (adapterDataList != null && adapterDataList.size() > 0) {
                    // 点击图片查看大图
                    LaunchHelper.getInstance().launch(mContext, BigPhotoActivity.class, new BigPhotoParcelable(position,
                            Util.convertPhotoUrl(adapterDataList)));
                }
            }
        });
    }

    @Override
    public void getUserInfo() {
        ApiManager.kaiguan(new IGetDataListener<Switch>() {
            @Override
            public void onResult(Switch aSwitch, boolean isEmpty) {
                SwitchPreference.saveSwitch(aSwitch);
                if (aSwitch != null) {
                    if (aSwitch.getAllpaySwitch() == -1) {
                        mPersonView.switchAllPay();
                        isCanLoad = false;
                    } else {
                        isCanLoad = true;
                    }
                    if (aSwitch.getAllpaySwitch() == -1) {
                        mPersonView.switchAllPay();
                    }

                    if (aSwitch.getBeanpaySwitch() == -1) {
                        mPersonView.switchDionmads(true);
                    }else {
                        mPersonView.switchDionmads(false);
                    }
                    if(aSwitch.getVippaySwitch()==-1){
                        mPersonView.switchVip(true);
                    }else {
                        mPersonView.switchVip(false);
                    }
                    if (aSwitch.getWalletSwitch() == -1) {
                        mPersonView.switchWallet();
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });

    }

    @Override
    public void updateBalance() {
        // 更新当前余额（充值/消费操作后）
//        mPersonView.setBalance(String.valueOf(BeanPreference.getBeanCount()));
    }

    @Override
    public void updatePrice(String newPrice) {
        // 更新当前主播价格
        //     mPersonView.setPrice(newPrice);
    }

    @Override
    public void getUploadInfo() {
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if (myInfo != null) {
                    UserDetail userDetail = myInfo.getUserDetail();
                    String vipDays = userDetail.getVipDays();
                    List<UserPhoto> userPhotos = userDetail.getUserPhotos();
                    if (userPhotos != null && userPhotos.size() > 0) {
                        mAlbumPhotoAdapter.bind(userPhotos);
                        mPersonView.picGone();
                    }
                    if (!TextUtils.isEmpty(vipDays) && vipDays.length() > 0) {
                        int i = Integer.parseInt(vipDays);
                        mPersonView.vipDays(i);
                    } else {
                        mPersonView.vipDays(0);
                    }
                    if (userDetail != null) {
                        UserBean userBean = userDetail.getUserBean();
                        if (userBean != null) {
                            BeanPreference.saveUserBean(userBean);
                            mPersonView.setUserPrecent("100", "0");
                        }
                        UserBase userBase = userDetail.getUserBase();
                        if (userBase != null) {
                            mPersonView.setUserAvatar(userBase.getIconUrlMininum(), userBase.getIconStatus());
                            mPersonView.setUserName(userBase.getNickName());
                            mPersonView.setUserId(String.valueOf(userBase.getAccount()));
                            UserPreference.saveUserInfo(userBase);
                            int userType = userBase.getUserType();
                            if (isCanLoad) {
                                if (userType == 0) {
                                    mPersonView.setIsAnchor(false);
                                } else {
                                    mPersonView.setIsAnchor(true);

                                }
                            }
                        }
                        HostInfo hostInfo = userDetail.getHostInfo();
                        if (hostInfo != null) {
                            AnchorPreference.saveHostInfo(hostInfo);
                            mPersonView.setAccumulatedIncome(String.valueOf(hostInfo.getMonthlyIncome()));
                            mPersonView.setAnchorCurrentIncome(String.valueOf(hostInfo.getIncomeBalance()));
                            mPersonView.setPrice(String.valueOf((int) hostInfo.getPrice()), hostInfo.getAudioPrice());
                            mPersonView.setBalance(hostInfo.getBalanceCash());
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
//      ApiManager.getDiamondPayWay("", new IGetDataListener<String>() {
//          @Override
//          public void onResult(String s, boolean isEmpty) {
//              PayPreference.saveDiamondInfo(s);
//          }
//
//          @Override
//          public void onError(String msg, boolean isNetworkError) {
//          }
//      });
//        ApiManager.getKeyPayWay("", new IGetDataListener<String>() {
//            @Override
//            public void onResult(String s, boolean isEmpty) {
//                PayPreference.saveKeyInfo(s);
//            }
//
//            @Override
//            public void onError(String msg, boolean isNetworkError) {
//
//            }
//        });
    }

    @Override
    public void getCheckStatus() {
        ApiManager.getAuthorCheckStatus(new IGetDataListener<CheckStatus>() {
            @Override
            public void onResult(CheckStatus checkStatus, boolean isEmpty) {
                if (checkStatus != null) {
                    mPersonView.getCheckStatus(checkStatus.getAuditStatus(),checkStatus.getShowStatus());
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    public void loadPersonInfor() {
        ApiManager.getFaceBookAccount(new IGetDataListener<String>() {
            @Override
            public void onResult(String faceBookAccount, boolean isEmpty) {
                if (faceBookAccount != null) {
                    DataPreference.saveDataJson(faceBookAccount);
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }


    @Override
    public void upLoadAvator(File file, boolean photoType) {
        mPersonView.showLoading();
        ApiManager.upLoadMyPhotoOrAvator(file, photoType, new IGetDataListener<UpLoadMyPhoto>() {
            @Override
            public void onResult(UpLoadMyPhoto upLoadMyPhoto, boolean isEmpty) {
                mPersonView.hideLoading();
                if (upLoadMyPhoto != null) {
                    final UserPhoto userPhoto = upLoadMyPhoto.getUserPhoto();
                    if (userPhoto != null) {
                        final String stringFile = userPhoto.getFileUrlMiddle();
                        String fileUrl = userPhoto.getFileUrl();
                        String fileUrlMinimum = userPhoto.getFileUrlMinimum();
                        if (!TextUtils.isEmpty(stringFile)) {
                            UserPreference.setOriginalImage(fileUrl);
                            UserPreference.setMiddleImage(stringFile);
                            UserPreference.setSmallImage(fileUrlMinimum);
                            mPersonView.setUserAvatar(userPhoto.getFileUrlMinimum(), userPhoto.getStatus());
                        }
                    }
                }
                mPersonView.showTip(mContext.getString(R.string.person_picture_check));
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mPersonView.hideLoading();
                mPersonView.showTip(msg);
            }
        });
    }
    @Override
    public void toggleNoDistrub(final boolean toggle) {
        if ((toggle && "3".equals(UserPreference.getStatus())) || (!toggle && !"3".equals(UserPreference.getStatus()))) {
            return;
        }
        ApiManager.modifyUserStatus(toggle ? "3" : "1", new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                UserPreference.setStatus(toggle ? "3" : "1");
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }
    @Override
    public void getNoDistrubState() {
        mPersonView.toggleNoDistrub("3".equals(UserPreference.getStatus()));
    }

}
