package com.wiscomwis.facetoface.ui.message.adapter;

import android.support.annotation.LayoutRes;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.TimeUtils;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.model.HuanXinUser;
import com.wiscomwis.facetoface.parcelable.UserDetailParcelable;
import com.wiscomwis.facetoface.ui.detail.UserDetailActivity;
import com.wiscomwis.library.adapter.base.BaseQuickAdapter;
import com.wiscomwis.library.adapter.base.BaseViewHolder;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.LaunchHelper;

/**
 * Created by xuzhaole on 2018/1/22.
 */

public class HelloPeopleAdapter extends BaseQuickAdapter<HuanXinUser, BaseViewHolder> {

    public HelloPeopleAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder holder, final HuanXinUser user) {
        if (user != null) {
            String hxId = user.getHxId();
            if (!TextUtils.isEmpty(hxId)) {
                ImageView ivAvatar = holder.getView(R.id.item_chat_history_avatar);
                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(user.getHxIcon())
                        .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(ivAvatar).build());
                holder.setText(R.id.item_chat_history_nickname, user.getHxName());
                holder.setText(R.id.item_chat_history_message, String.valueOf(user.getLastMsg()));
                TextView tv_msgNum = holder.getView(R.id.item_chat_history_tv_msgnum);
                if (user.getMsgNum() > 0) {
                    tv_msgNum.setVisibility(View.VISIBLE);
                    tv_msgNum.setText(user.getMsgNum() + "");
                } else {
                    tv_msgNum.setVisibility(View.GONE);
                }
//
                holder.setText(R.id.item_chat_is_tv_time, TimeUtils.getLocalTime(mContext, System.currentTimeMillis(), Long.parseLong(user.getMsgTime())));
                ivAvatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                                new UserDetailParcelable(user.getHxId()));
                        Util.setDetailMsgFlag(user.getHxId(),"5");
                    }
                });
            }
        }
    }
}
