package com.wiscomwis.facetoface.ui.register;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Parcelable;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragmentActivity;
import com.wiscomwis.facetoface.base.CommonWebActivity;
import com.wiscomwis.facetoface.data.api.ApiConstant;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.NickName;
import com.wiscomwis.facetoface.data.model.Register;
import com.wiscomwis.facetoface.data.model.SearchCriteria;
import com.wiscomwis.facetoface.data.model.SearchUser;
import com.wiscomwis.facetoface.data.model.SearchUserList;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.data.preference.SearchPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.event.FinishEvent;
import com.wiscomwis.facetoface.event.RegisterAndLoginFinish;
import com.wiscomwis.facetoface.parcelable.WebParcelable;
import com.wiscomwis.facetoface.ui.login.LoginActivity;
import com.wiscomwis.library.dialog.LoadingDialog;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.SharedPreferenceUtil;
import com.wiscomwis.library.util.ToastUtil;
import com.wiscomwis.library.util.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * Created by Administrator on 2017/8/26.
 */

public class FirstPageActivity extends BaseFragmentActivity implements View.OnClickListener {
    @BindView(R.id.firstpage_activity_iv_bg)
    ImageView iv_bg;
    @BindView(R.id.register_login)
    TextView tv_login;
    @BindView(R.id.register)
    Button btn_register;
    @BindView(R.id.iv_male)
    ImageView iv_male;
    @BindView(R.id.rl_male)
    RelativeLayout rl_male;
    @BindView(R.id.iv_female)
    ImageView iv_female;
    @BindView(R.id.rl_female)
    RelativeLayout rl_female;
    @BindView(R.id.register_xieyi)
    TextView tv_xieyi;
    @BindView(R.id.tv_clip)
    TextView tv_clip;
    @BindView(R.id.et_clip)
    EditText et_clip;
    private boolean isMale = true;
    private String mNickName;
    private boolean isFirstClick=true;


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_firstpage;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        setAnimation();
        String text = tv_xieyi.getText().toString();
        if(!TextUtils.isEmpty(text)&&text.length()>0){
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
            spannableStringBuilder.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    LaunchHelper.getInstance().launch(FirstPageActivity.this, CommonWebActivity.class,
                            new WebParcelable(getString(R.string.user_agreement), ApiConstant.URL_AGREEMENT_USER, false));
                }
                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(getResources().getColor(R.color.main_color));
                }

            }, text.indexOf("《"), text.indexOf("》") + 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            spannableStringBuilder.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    LaunchHelper.getInstance().launch(FirstPageActivity.this, CommonWebActivity.class,
                            new WebParcelable(getString(R.string.privacy_agreement), ApiConstant.URL_AGREEMENT_PRIVACY, false));
                }
                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(getResources().getColor(R.color.main_color));
                }
            }, text.lastIndexOf("《"), text.lastIndexOf("》") + 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            tv_xieyi.setMovementMethod(new LinkMovementMethod());
            tv_xieyi.setText(spannableStringBuilder);
        }


        et_clip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!TextUtils.isEmpty(charSequence)&& charSequence.length()> 0){
                    et_clip.setSelection(charSequence.length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    protected void setListeners() {
        btn_register.setOnClickListener(this);
        tv_login.setOnClickListener(this);
        rl_male.setOnClickListener(this);
        rl_female.setOnClickListener(this);
        tv_clip.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
        ApiManager.nickName(new IGetDataListener<NickName>() {

            @Override
            public void onResult(NickName nickName, boolean isEmpty) {
                if (nickName != null) {
                    mNickName = nickName.getNickName();
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_male://男
                if (!isMale) {
                    rl_male.setBackgroundResource(R.drawable.checked_first_page);
                    rl_female.setBackgroundResource(R.drawable.uncheck_first_page);
                    isMale = true;
                }
                break;
            case R.id.rl_female://女
                if (isMale) {
                    rl_female.setBackgroundResource(R.drawable.checked_first_page);
                    rl_male.setBackgroundResource(R.drawable.uncheck_first_page);
                    isMale = false;
                }
                break;
            case R.id.register://开启泡泡之旅
                if(isFirstClick){
                    isFirstClick=false;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isFirstClick=true;
                        }
                    },1000);
                    LoadingDialog.show(getSupportFragmentManager());
                    if (!TextUtils.isEmpty(mNickName)){
                        register();
                    }else{
                        ApiManager.nickName(new IGetDataListener<NickName>() {

                            @Override
                            public void onResult(NickName nickName, boolean isEmpty) {
                                if (nickName != null) {
                                    mNickName = nickName.getNickName();
                                    register();
                                }
                            }

                            @Override
                            public void onError(String msg, boolean isNetworkError) {
                                LoadingDialog.hide();
                                ToastUtil.showShortToast(mContext,TextUtils.isEmpty(msg)?getString(R.string.tip_error):msg);
                            }
                        });
                    }
                }


                break;
            case R.id.register_login://已有账号
                LaunchHelper.getInstance().launch(mContext, LoginActivity.class);
                break;
            case R.id.tv_clip:
                if (!TextUtils.isEmpty(Utils.getClipboardText(mContext))){
                    if (TextUtils.isDigitsOnly(Utils.getClipboardText(mContext))){
                        et_clip.setText(Utils.getClipboardText(mContext));
                    }
                }
                break;
        }
    }

    private void register() {
        ApiManager.register(mNickName, isMale, "0",
                new IGetDataListener<Register>() {
                    @Override
                    public void onResult(Register register, boolean isEmpty) {
                        LoadingDialog.hide();
                        // 保存用户信息到本地
                        UserBase userBase = register.getUserBase();
                        if (null != userBase) {
                            UserPreference.saveUserInfo(userBase);
                        }
                        DbModle.getInstance().getUserAccountDao().deleteAllData();//删除数据库中的所有数据
                        DbModle.getInstance().getUserAccountDao().deleteQaAllData();//删除QA

                        // 关闭之前打开的页面
                        EventBus.getDefault().post(new FinishEvent());
                        // 跳转主页面
                        LaunchHelper.getInstance().launch(mContext, CompleteInfoActivity.class);
                        UserPreference.registered();//注册成功的标记
                        UserPreference.guided();//第一次使用时的标记
//                        缓存主界面的图片数据
                        cacheMainIcon();
                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {
                        LoadingDialog.hide();
                        if (isNetworkError) {
                            ToastUtil.showShortToast(FirstPageActivity.this, getString(R.string.tip_network_error));
                        } else {
                            ToastUtil.showShortToast(FirstPageActivity.this, TextUtils.isEmpty(msg) ? getString(R.string.tip_empty) : msg);
                        }
                    }
                });
    }



    @Subscribe
    public void onEvent(RegisterAndLoginFinish event) {
        finish();
    }

    private void setAnimation() {
        AnimationSet animationSet = new AnimationSet(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1, 1.2f, 1, 1.2f,
                Animation.RELATIVE_TO_SELF, 0.1f, Animation.RELATIVE_TO_SELF, 0.1f);
//        //3秒完成动画
        scaleAnimation.setDuration(7000);
//        //将AlphaAnimation这个已经设置好的动画添加到 AnimationSet中
        scaleAnimation.setRepeatMode(ScaleAnimation.REVERSE);
        //设置动画播放次数
        scaleAnimation.setRepeatCount(ScaleAnimation.INFINITE);
        animationSet.addAnimation(scaleAnimation);
        //从当前位置，向下和向右各平移300px
        TranslateAnimation animation = new TranslateAnimation(0.0f, -100f, 0.0f, 0.0f);
        animation.setDuration(7000);
        animation.setRepeatMode(TranslateAnimation.REVERSE);
        //设置动画播放次数
        animation.setRepeatCount(TranslateAnimation.INFINITE);
        animationSet.addAnimation(animation);
        //启动动画
        iv_bg.startAnimation(animationSet);
    }
    private void cacheMainIcon() {
        // 设置搜索条件
        Map<String, SearchCriteria> criteria = new HashMap<>();
        SearchCriteria searchCriteria = SearchPreference.getSearchCriteria();
        if (null != searchCriteria) {
            criteria.put("criteria", searchCriteria);
        }
        ApiManager.getHomepageRecommend(ApiConstant.URL_HOMEPAGE_HOT, 1, "10", new Gson().toJson(criteria), new IGetDataListener<SearchUserList>() {
            @Override
            public void onResult(SearchUserList searchUserList, boolean isEmpty) {
                List<SearchUser> list = searchUserList.getSearchUserList();
                if (!Utils.isListEmpty(list)) {
//                    ListDataSave listDataSave = new ListDataSave(FirstPageActivity.this,"ReMenDate");
//                    listDataSave.setDataList("ReMenDate",list);
                    for (int i = 0; i < list.size(); i++) {
                        String iconUrl=list.get(i).getIconUrlMiddle();
                        String iconUrl1 = list.get(i).getIconUrl();

                        String cacheIg = UserPreference.getCacheIg(iconUrl);
                        String cacheIg1 = UserPreference.getCacheIg(iconUrl1);
                        if(TextUtils.isEmpty(cacheIg)){
                            if(i==0){
                                SharedPreferenceUtil.setStringValue(FirstPageActivity.this,"ReMenIcon","imgUrl",iconUrl);
                            }
                            new getImageCacheAsyncTask(FirstPageActivity.this).execute(iconUrl);
                        }
                        if(TextUtils.isEmpty(cacheIg1)){
                            new getImageCacheAsyncTask(FirstPageActivity.this).execute(iconUrl1);
                        }

                    }

                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
        ApiManager.getHomepageRecommend(ApiConstant.URL_HOMEPAGE_AUTHOR_RECOMMEND, 1, "10", new Gson().toJson(criteria), new IGetDataListener<SearchUserList>() {
            @Override
            public void onResult(SearchUserList searchUserList, boolean isEmpty) {
                List<SearchUser> list = searchUserList.getSearchUserList();
                if (!Utils.isListEmpty(list)) {
                    for (int i = 0; i < list.size(); i++) {
                        String iconUrl=list.get(i).getIconUrlMiddle();
                        new getImageCacheAsyncTask(FirstPageActivity.this).execute(iconUrl);
                    }

                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }
    public static class getImageCacheAsyncTask extends AsyncTask<String, Void, File> {
        private final Context context;
        private final WeakReference<Context> weakReference;
        private String imgUrl;

        public getImageCacheAsyncTask(Context context) {
//            使用软引用防止ANR
            weakReference = new WeakReference<>(context);
            this.context = context;
        }

        @Override
        protected File doInBackground(String... params) {
            imgUrl =  params[0];
            try {
                return Glide.with(context)
                        .load(imgUrl)
                        .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                        .get();
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(File result) {
            FirstPageActivity activity = (FirstPageActivity) weakReference.get();
            if (activity != null) {
                if (result == null) {
                    return;
                }
                //此path就是对应文件的缓存路径
                String path = result.getPath();
                Log.e("path", path);
                UserPreference.setCacheIg(imgUrl,path);
            }


        }
    }
//    @Subscribe
//    public void onEvent(FinishFirstPageEvent event) {
//        finish();
//    }
}
