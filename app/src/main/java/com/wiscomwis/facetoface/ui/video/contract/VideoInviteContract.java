package com.wiscomwis.facetoface.ui.video.contract;

import android.support.v4.app.FragmentManager;
import android.view.SurfaceHolder;

import com.wiscomwis.facetoface.event.AgoraEvent;
import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/6/7.
 */
public interface VideoInviteContract {

    interface IView extends BaseView {

        void hideSurfaceView();

        SurfaceHolder getHolder();

        /**
         * 视频呼叫成功后，改变状态
         */
        void changeInviteState(String msg);

        void finishActivity(int seconds);

        FragmentManager obtainFragmentManager();

    }

    interface IPresenter extends BasePresenter {

        /**
         * 主动发起视频时开启摄像头预览
         */
        void startPreView();

        /**
         * 停止预览
         */
        void stopPreview();

        /**
         * 发起呼叫
         *
         * @param channelId 频道id
         * @param account   对方account
         * @param userInfo  对方信息
         */
        void startInvite(String channelId, String account, String userInfo);

        /**
         * 取消呼叫
         *
         * @param channelId 频道id
         * @param account   对方account
         */
        void cancelInVite(String channelId, String account,String guid);

        /**
         * 接受邀请
         *
         * @param channelId 频道id
         * @param account   己方account
         */
        void acceptInvite(String channelId, String account);

        /**
         * 拒绝邀请
         *
         * @param channelId 频道id
         * @param account   己方account
         */
        void refuseInvite(String channelId, String account,String guid);

        /**
         * 处理声网信令系统事件
         *
         * @param agoraEvent
         */
        void handleAgoraEvent(AgoraEvent agoraEvent);

    }

}
