package com.wiscomwis.facetoface.ui.register;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseTopBarActivity;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.customload.MutilRadioGroup;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.ui.main.MainActivity;
import com.wiscomwis.facetoface.ui.register.contract.CompleteSelfhoodInfoContract;
import com.wiscomwis.facetoface.ui.register.presenter.CompleteSelfInfoPresenter;
import com.wiscomwis.library.image.CropCircleTransformation;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.net.NetUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by WangYong on 2018/4/18.
 */

public class CompleteSelfhoodInfoActivity extends BaseTopBarActivity implements View.OnClickListener, CompleteSelfhoodInfoContract.IView, MutilRadioGroup.OnCheckedChangeListener {
    @BindView(R.id.activity_complete_selfhood_info_rl_male)
    RelativeLayout rl_male;
    @BindView(R.id.complete_selfhood_info_tv_right0)
    TextView tv_right0;
    @BindView(R.id.complete_selfhood_info_tv_right1)
    TextView tv_right1;
    @BindView(R.id.complete_selfhood_info_tv_right2)
    TextView tv_right2;
    @BindView(R.id.complete_selfhood_info_tv_right3)
    TextView tv_right3;
    @BindView(R.id.complete_selfhood_info_tv_right4)
    TextView tv_right4;
//    @BindView(R.id.complete_selfhood_info_tv_right5)
//    TextView tv_right5;

    @BindView(R.id.complete_selfhood_info_tv_left0)
    TextView tv_left0;
    @BindView(R.id.complete_selfhood_info_tv_left1)
    TextView tv_left1;
    @BindView(R.id.complete_selfhood_info_tv_left2)
    TextView tv_left2;
    @BindView(R.id.complete_selfhood_info_tv_left3)
    TextView tv_left3;
    @BindView(R.id.complete_selfhood_info_tv_left4)
    TextView tv_left4;
//    @BindView(R.id.complete_selfhood_info_tv_left5)
//    TextView tv_left5;
//    @BindView(R.id.complete_selfhood_info_tv_left6)
//    TextView tv_left6;
//    @BindView(R.id.complete_selfhood_info_tv_left7)
//    TextView tv_left7;
    @BindView(R.id.activity_complete_selfhood_info_iv_avator)
    ImageView iv_avator;

    @BindView(R.id.activity_complete_selfhood_info_ll_female)
    LinearLayout ll_female;
    @BindView(R.id.activity_comeplete_selfhood_info_rg1)
    MutilRadioGroup rg1;
    @BindView(R.id.activity_comeplete_selfhood_info_rg2)
    MutilRadioGroup rg2;
    @BindView(R.id.activity_comeplete_selfhood_info_rg3)
    MutilRadioGroup rg3;


    @BindView(R.id.complete_info_first_rb0)
    RadioButton rb_first_rb0;
    @BindView(R.id.complete_info_first_rb1)
    RadioButton rb_first_rb1;
    @BindView(R.id.complete_info_first_rb2)
    RadioButton rb_first_rb2;
    @BindView(R.id.complete_info_first_rb3)
    RadioButton rb_first_rb3;
    @BindView(R.id.complete_info_first_rb4)
    RadioButton rb_first_rb4;
    @BindView(R.id.complete_info_first_rb5)
    RadioButton rb_first_rb5;
    @BindView(R.id.complete_info_first_rb6)
    RadioButton rb_first_rb6;
    @BindView(R.id.complete_info_first_rb7)
    RadioButton rb_first_rb7;

    @BindView(R.id.complete_info_second_rb0)
    RadioButton rb_second_rb0;
    @BindView(R.id.complete_info_second_rb1)
    RadioButton rb_second_rb1;
    @BindView(R.id.complete_info_second_rb2)
    RadioButton rb_second_rb2;
    @BindView(R.id.complete_info_second_rb3)
    RadioButton rb_second_rb3;


    @BindView(R.id.complete_info_third_rb0)
    RadioButton rb_third_rb0;
    @BindView(R.id.complete_info_third_rb1)
    RadioButton rb_third_rb1;
    @BindView(R.id.complete_info_third_rb2)
    RadioButton rb_third_rb2;
    @BindView(R.id.complete_info_third_rb3)
    RadioButton rb_third_rb3;
    @BindView(R.id.complete_info_third_rb4)
    RadioButton rb_third_rb4;
    @BindView(R.id.complete_info_third_rb5)
    RadioButton rb_third_rb5;
    @BindView(R.id.complete_info_third_rb6)
    RadioButton rb_third_rb6;
    @BindView(R.id.complete_info_third_rb7)
    RadioButton rb_third_rb7;

    @BindView(R.id.complete_selfhood_info_btn_finish)
    Button btn_finish;
    private CompleteSelfInfoPresenter mCompleteSelfInfoPresenter;
    private List<TextView> tv_list = new ArrayList<TextView>();
    private String female_label1, female_label2, female_label3;


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_complete_selfhood_info_layout;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return null;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
    }

    @Override
    protected void initViews() {
        mCompleteSelfInfoPresenter = new CompleteSelfInfoPresenter(CompleteSelfhoodInfoActivity.this);
        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(UserPreference.getMiddleImage()).transform(new CropCircleTransformation(mContext))
                .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(iv_avator).build());
       TextView[] tv_array = {tv_left0, tv_left1, tv_left4, tv_left3, tv_left2, tv_right0, tv_right1, tv_right2, tv_right3, tv_right4};
        RadioButton[] rb_array = {rb_first_rb0, rb_first_rb1, rb_first_rb2, rb_first_rb3, rb_first_rb4, rb_first_rb5,rb_first_rb6,rb_first_rb7, rb_second_rb0, rb_second_rb1, rb_second_rb2, rb_second_rb3, rb_third_rb0, rb_third_rb1, rb_third_rb2, rb_third_rb3, rb_third_rb4, rb_third_rb5, rb_third_rb6};
        if (UserPreference.isMale()) {
            rl_male.setVisibility(View.VISIBLE);
            ll_female.setVisibility(View.GONE);
            List<String> manUserTagValue = DataPreference.getManUserTagValue();
            if(tv_array!=null&&tv_array.length>0){
               int num =manUserTagValue.size()-tv_array.length;
                if (num==0||num>0) {
                    for (int i = 0; i < tv_array.length; i++) {
                        if (manUserTagValue!=null&&manUserTagValue.size()>0) {
                            tv_array[i].setText(manUserTagValue.get(i));
                        }
                    }
                }else if(num<0){
                    for (int i = 0; i < tv_array.length-num; i++) {
                        if (manUserTagValue!=null&&manUserTagValue.size()>0) {
                            tv_array[i].setText(manUserTagValue.get(i));
                        }
                    }
                }
            }
        }else{
            List<String> womanUserTagValue = DataPreference.getWomanUserTagValue();
            ll_female.setVisibility(View.VISIBLE);
            rl_male.setVisibility(View.GONE);
            int num=DataPreference.getWomanUserTagValue().size()-rb_array.length;
            if (num==0||num>0) {
                    for (int i = 0; i < rb_array.length; i++) {
                        if (womanUserTagValue!=null&&womanUserTagValue.size()>0) {
                            rb_array[i].setText(womanUserTagValue.get(i));
                        }
                    }
            }else if(num<0){
                for (int i = 0; i < rb_array.length-num; i++) {
                    if (womanUserTagValue!=null&&womanUserTagValue.size()>0) {
                        rb_array[i].setText(womanUserTagValue.get(i));
                    }
                }
            }


        }
    }

    @Override
    protected void setListeners() {
        btn_finish.setOnClickListener(this);
        tv_right0.setOnClickListener(this);
        tv_right1.setOnClickListener(this);
        tv_right2.setOnClickListener(this);
        tv_right3.setOnClickListener(this);
        tv_right4.setOnClickListener(this);
//        tv_right5.setOnClickListener(this);

        tv_left0.setOnClickListener(this);
        tv_left1.setOnClickListener(this);
        tv_left2.setOnClickListener(this);
        tv_left3.setOnClickListener(this);
        tv_left4.setOnClickListener(this);
//        tv_left5.setOnClickListener(this);
//        tv_left6.setOnClickListener(this);
//        tv_left7.setOnClickListener(this);

        rg1.setOnCheckedChangeListener(this);
        rg2.setOnCheckedChangeListener(this);
        rg3.setOnCheckedChangeListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.complete_selfhood_info_tv_right0:
                setListTextView(tv_right0, 2);
                break;
            case R.id.complete_selfhood_info_tv_right1:
                setListTextView(tv_right1, 2);
                break;
            case R.id.complete_selfhood_info_tv_right2:
                setListTextView(tv_right2, 2);
                break;
            case R.id.complete_selfhood_info_tv_right3:
                setListTextView(tv_right3, 2);
                break;
            case R.id.complete_selfhood_info_tv_right4:
                setListTextView(tv_right4, 2);
                break;
//            case R.id.complete_selfhood_info_tv_right5:
//                setListTextView(tv_right5, 2);
//                break;


            case R.id.complete_selfhood_info_tv_left0:
                setListTextView(tv_left0, 1);
                break;
            case R.id.complete_selfhood_info_tv_left1:
                setListTextView(tv_left1, 1);
                break;
            case R.id.complete_selfhood_info_tv_left2:
                setListTextView(tv_left2, 1);
                break;
            case R.id.complete_selfhood_info_tv_left3:
                setListTextView(tv_left3, 1);
                break;
            case R.id.complete_selfhood_info_tv_left4:
                setListTextView(tv_left4, 1);
                break;
//            case R.id.complete_selfhood_info_tv_left5:
//                setListTextView(tv_left5, 1);
//                break;
//            case R.id.complete_selfhood_info_tv_left6:
//                setListTextView(tv_left6, 1);
//                break;
//            case R.id.complete_selfhood_info_tv_left7:
//                setListTextView(tv_left7, 1);
//                break;


            case R.id.complete_selfhood_info_btn_finish:
                if (UserPreference.isMale()) {
                    if (tv_list.size()<=3) {
                        if (tv_list.size()==1) {
                            mCompleteSelfInfoPresenter.commitMaleData(tv_list.get(0).getText().toString(),null,null);
                        } else if (tv_list.size()==2) {
                            mCompleteSelfInfoPresenter.commitMaleData(tv_list.get(0).getText().toString(),tv_list.get(1).getText().toString(),null);
                        }else if(tv_list.size()==3){
                            mCompleteSelfInfoPresenter.commitMaleData(tv_list.get(0).getText().toString(),tv_list.get(1).getText().toString(),tv_list.get(2).getText().toString());
                        }
                        Intent intent = new Intent(mContext, MainActivity.class);
                        startActivity(intent);
                        finish();

//                        LaunchHelper.getInstance().launchFinish(mContext, MainActivity.class);

                    }else{
                        Toast.makeText(CompleteSelfhoodInfoActivity.this, "请选择1~3个标签", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    mCompleteSelfInfoPresenter.commitFemaleData();
                }
                break;
        }
    }

    private void setListTextView(TextView tv, int flag) {
        if (tv_list != null) {
            if (tv_list.size() <= 2) {
                if (tv_list.contains(tv)) {
                    tv_list.remove(tv);
                    if (flag == 1) {
                        tv.setBackgroundResource(R.drawable.complete_info_polygon_left_icon);
                    } else {
                        tv.setBackgroundResource(R.drawable.complete_info_polygon_right_icon);
                    }
                } else {
                    tv_list.add(tv);
                    if (flag == 1) {
                        tv.setBackgroundResource(R.drawable.complete_info_polygon_left_blue_icon);
                    } else {
                        tv.setBackgroundResource(R.drawable.complete_info_polygon_right_blue_icon);
                    }
                }

            } else {
                if (tv_list != null && tv_list.size() > 0) {
                    if (tv_list.size() == 3) {
                        if (tv_list.contains(tv)) {
                            tv_list.remove(tv);
                            if (flag == 1) {
                                tv.setBackgroundResource(R.drawable.complete_info_polygon_left_icon);
                            } else {
                                tv.setBackgroundResource(R.drawable.complete_info_polygon_right_icon);
                            }
                        } else {
                            Toast.makeText(CompleteSelfhoodInfoActivity.this, "最多只能选择三个标签", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }

    @Override
    public void onCheckedChanged(MutilRadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.complete_info_first_rb0:
                female_label1 = (String) rb_first_rb0.getText();

                break;
            case R.id.complete_info_first_rb1:
                female_label1 = (String) rb_first_rb1.getText();

                break;
            case R.id.complete_info_first_rb2:
                female_label1 = (String) rb_first_rb2.getText();

                break;
            case R.id.complete_info_first_rb3:
                female_label1 = (String) rb_first_rb3.getText();

                break;
            case R.id.complete_info_first_rb4:
                female_label1 = (String) rb_first_rb4.getText();

                break;
            case R.id.complete_info_first_rb5:
                female_label1 = (String) rb_first_rb5.getText();

                break;
            case R.id.complete_info_first_rb6:
                female_label1 = (String) rb_first_rb6.getText();

                break;
            case R.id.complete_info_first_rb7:
                female_label1 = (String) rb_first_rb7.getText();

                break;


            case R.id.complete_info_second_rb0:
                female_label2 = (String) rb_second_rb0.getText();

                break;
            case R.id.complete_info_second_rb1:
                female_label2 = (String) rb_second_rb1.getText();

                break;
            case R.id.complete_info_second_rb2:
                female_label2 = (String) rb_second_rb2.getText();

                break;
            case R.id.complete_info_second_rb3:
                female_label2 = (String) rb_second_rb3.getText();

                break;


            case R.id.complete_info_third_rb0:
                female_label3 = (String) rb_third_rb0.getText();

                break;
            case R.id.complete_info_third_rb1:
                female_label3 = (String) rb_third_rb1.getText();

                break;
            case R.id.complete_info_third_rb2:
                female_label3 = (String) rb_third_rb2.getText();

                break;
            case R.id.complete_info_third_rb3:
                female_label3 = (String) rb_third_rb3.getText();

                break;
            case R.id.complete_info_third_rb4:
                female_label3 = (String) rb_third_rb4.getText();

                break;
            case R.id.complete_info_third_rb5:
                female_label3 = (String) rb_third_rb5.getText();

                break;
            case R.id.complete_info_third_rb6:
                female_label3 = (String) rb_third_rb6.getText();

                break;
            case R.id.complete_info_third_rb7:
                female_label3 = (String) rb_third_rb7.getText();

                break;
        }
    }

    @Override
    public String getFemale1() {
        return female_label1;
    }

    @Override
    public String getFemale2() {
        return female_label2;
    }

    @Override
    public String getFemale3() {
        return female_label3;
    }
}
