package com.wiscomwis.facetoface.ui.auto.fragment;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragment;
import com.wiscomwis.facetoface.common.AutoReplyDialog;
import com.wiscomwis.facetoface.event.ReplyEvent;
import com.wiscomwis.facetoface.ui.auto.AutoReplyActivity;
import com.wiscomwis.facetoface.ui.auto.contract.ReplyContract;
import com.wiscomwis.facetoface.ui.auto.presenter.ReplyPresenter;
import com.wiscomwis.library.util.ToastUtil;
import com.wiscomwis.library.widget.AutoSwipeRefreshLayout;
import com.wiscomwis.library.widget.ItemTouchHelperCallback;
import com.wiscomwis.library.widget.RefreshRecyclerView;
import com.wiscomwis.library.widget.XRecyclerView;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/11/2.
 */

public class AutoReplyFragment extends BaseFragment implements ReplyContract.IView {
    @BindView(R.id.fragment_auto_reply_ll)
    LinearLayout ll_reply;
    @BindView(R.id.fragment_auto_reply_btn)
    Button btn_reply;
    @BindView(R.id.ranking_list_recyclerview)
    XRecyclerView recyclerView;
    @BindView(R.id.ranking_list_swiperefresh)
    AutoSwipeRefreshLayout mSwiperefresh;

    private ReplyPresenter mReplyPresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_auto_reply;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        // 设置下拉刷新样式
        mSwiperefresh.setColorSchemeResources(R.color.main_color);
        mSwiperefresh.setProgressBackgroundColorSchemeColor(Color.WHITE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        mReplyPresenter = new ReplyPresenter(this);
    }

    @Override
    protected void setListeners() {
        btn_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AutoReplyDialog autoReplyDialog = new AutoReplyDialog();
                autoReplyDialog.setContext(mContext);
                autoReplyDialog.recordAndMsgShow();
            }
        });

        // 下拉刷新
        mSwiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mReplyPresenter.refresh();

            }
        });
    }

    @Override
    protected void loadData() {
        mReplyPresenter.loadReplyList();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }

    @Override
    public void hideRefresh(int delaySeconds) {
        mSwiperefresh.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSwiperefresh.isRefreshing()) {
                    mSwiperefresh.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                mReplyPresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        ll_reply.setVisibility(View.VISIBLE);
    }


    @Override
    public void setAdapter(RecyclerView.Adapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void deleteItem(int position) {
        ToastUtil.showShortToast(mContext, mContext.getString(R.string.pic_delete_success));
    }

    @Override
    public void hindEmpty(boolean b) {
        if (b) {
            ll_reply.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Subscribe
    public void onEvent(ReplyEvent adapter) {
        mReplyPresenter.refresh();
    }
}
