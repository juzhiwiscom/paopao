package com.wiscomwis.facetoface.ui.pay.presenter;

import android.content.Context;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.PayDict;
import com.wiscomwis.facetoface.data.model.PayWay;
import com.wiscomwis.facetoface.data.model.UserBean;
import com.wiscomwis.facetoface.data.model.UserDetail;
import com.wiscomwis.facetoface.data.model.UserDetailforOther;
import com.wiscomwis.facetoface.data.preference.PayPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.ui.pay.adapter.MyDiamondAdapter;
import com.wiscomwis.facetoface.ui.pay.contract.MyDiamondContract;
import com.wiscomwis.library.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WangYong on 2017/8/30.
 */

public class MyDiamondPresenter implements MyDiamondContract.IPresenter {
     private MyDiamondContract.IView mPurchaseKey;
     private Context mContext;
     private MyDiamondAdapter mPayAdapter;

    public MyDiamondPresenter(MyDiamondContract.IView mPurchaseKey) {
        this.mPurchaseKey = mPurchaseKey;
        this.mContext = mPurchaseKey.obtainContext();
    }

    @Override
    public void start() {
     ApiManager.getUserInfo(UserPreference.getId(), new IGetDataListener<UserDetailforOther>() {
         @Override
         public void onResult(UserDetailforOther userDetailforOther, boolean isEmpty) {
             if(userDetailforOther!=null){
                 UserDetail userDetail = userDetailforOther.getUserDetail();
                 if(userDetail!=null){
                     UserBean userBean = userDetail.getUserBean();
                     if(userBean!=null){
                         mPurchaseKey.getDionmads(String.valueOf(userBean.getCounts()));
                         PayPreference.saveDionmadsNum(userBean.getCounts());
                     }
                 }
             }
         }

         @Override
         public void onError(String msg, boolean isNetworkError) {

         }
     });
    }


    @Override
    public void getPayWay(String fromTag) {
        mPurchaseKey.showLoading();
        ApiManager.getPayWay(fromTag,"1","1",new IGetDataListener<PayWay>() {
            @Override
            public void onResult(PayWay payWay, boolean isEmpty) {
                if (null != payWay) {
                    mPayAdapter = new MyDiamondAdapter(mContext, R.layout.mydiamond_list_item,
                            checkProductValid(payWay.getDictPayList()));
                    mPurchaseKey.setAdapter(mPayAdapter);
                }
                mPurchaseKey.dismissLoading();
            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
                if (isNetworkError) {
                    mPurchaseKey.showNetworkError();
                }
                mPurchaseKey.dismissLoading();
            }
        });
    }

    /**
     * 检测后台商品是否可用
     *
     * @param list 后台返回的商品信息列表
     * @return 可用的商品列表
     */
    private List<PayDict> checkProductValid(List<PayDict> list) {
        List<PayDict> validList = new ArrayList<>();
        if (!Utils.isListEmpty(list)) {
            for (PayDict item : list) {
                if ("1".equals(item.getIsvalid())) {
                    validList.add(item);
                }
            }
        }
        return validList;
    }

}
