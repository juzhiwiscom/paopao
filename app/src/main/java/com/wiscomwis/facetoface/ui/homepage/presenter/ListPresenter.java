package com.wiscomwis.facetoface.ui.homepage.presenter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.Gson;
import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.api.ApiConstant;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.SearchCriteria;
import com.wiscomwis.facetoface.data.model.SearchUser;
import com.wiscomwis.facetoface.data.model.SearchUserList;
import com.wiscomwis.facetoface.data.preference.SearchPreference;
import com.wiscomwis.facetoface.parcelable.UserDetailParcelable;
import com.wiscomwis.facetoface.ui.detail.UserDetailActivity;
import com.wiscomwis.facetoface.ui.homepage.adapter.HomeListAdapter;
import com.wiscomwis.facetoface.ui.homepage.contract.ListContract;
import com.wiscomwis.library.adapter.base.BaseQuickAdapter;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangdroid on 2017/6/2.
 */
public class ListPresenter implements ListContract.IPresenter {
    private ListContract.IView mListView;
    private Context mContext;
    private int pageNum = 1;
    private static final String pageSize = "20";
    private String url;
    private HomeListAdapter mListAdapter;
    private int mCurrentPosition = 0;

    public ListPresenter(ListContract.IView view) {
        this.mListView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void start(RecyclerView recyclerView) {
        mListAdapter = new HomeListAdapter(R.layout.item_homepage_list);
        mListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                SearchUser searchUser = mListAdapter.getItem(position);
                if (null != searchUser) {
                    mCurrentPosition = position;
                    LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                            new UserDetailParcelable(String.valueOf(searchUser.getUesrId())));
                    Util.setDetailMsgFlag(String.valueOf(searchUser.getUesrId()),"7");

                }
            }
        });

        // 设置加载更多
        mListView.setAdapter(mListAdapter);
        mListAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                load(false);
            }
        }, recyclerView);
    }

    @Override
    public void loadRecommendUserList(int type) {
        switch (type) {
            case C.homepage.TYPE_GODDESS:
                url = ApiConstant.URL_HOMEPAGE_HOT;
                break;
            case C.homepage.TYPE_ACTIVE_ANCHOR:
                url = ApiConstant.URL_HOMEPAGE_ACTIVE;
                break;
            case C.homepage.TYPE_NEW:
                url = ApiConstant.URL_HOMEPAGE_NEW;
                break;
            case C.homepage.TYPE_ACTIVE_FRIEND:
                url = ApiConstant.URL_HOMEPAGE_ACTIVE;
                break;
            case C.homepage.TYPE_NEW_FRIEND:
                url = ApiConstant.URL_HOMEPAGE_NEW;
                break;
            case C.homepage.TYPE_AUTHOR_SEE_MAN:
                url = ApiConstant.URL_HOMEPAGE_AUTHOR_SEE_MAN;
                break;
            case C.homepage.TYPE_AUTHOR_REMMEND:
                url = ApiConstant.URL_HOMEPAGE_AUTHOR_RECOMMEND;
                break;
        }
        load(true);
    }

    @Override
    public void refresh() {
        pageNum = 1;
        load(true);
    }

    @Override
    public void sayHelloStatus() {
        if (mCurrentPosition >= 0 && mListAdapter != null) {
            SearchUser searchUser = mListAdapter.getItem(mCurrentPosition);
            if (searchUser != null) {
                searchUser.setIsSayHello(1);
                mListAdapter.notifyItemChanged(mCurrentPosition, searchUser);
            }
        }
    }

    private void load(final boolean isRefresh) {
        // 设置搜索条件
        Map<String, SearchCriteria> criteria = new HashMap<>();
        SearchCriteria searchCriteria = SearchPreference.getSearchCriteria();
        if (null != searchCriteria) {
            criteria.put("criteria", searchCriteria);
        }
        ApiManager.getHomepageRecommend(url, pageNum, pageSize, new Gson().toJson(criteria), new IGetDataListener<SearchUserList>() {
            @Override
            public void onResult(SearchUserList searchUserList, boolean isEmpty) {
                setData(searchUserList, isEmpty, isRefresh);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mListView.hideRefresh(1);
                if (isRefresh){
                    mListAdapter.setEnableLoadMore(true);
                }else{
                    mListAdapter.loadMoreFail();
                }
            }
        });
    }

    private void setData(SearchUserList searchUserList, boolean isEmpty, boolean isRefresh) {
        pageNum++;
        if (isEmpty) {
            mListAdapter.setEmptyView(R.layout.common_empty);
        } else {
            if (null != searchUserList) {
                List<SearchUser> list = searchUserList.getSearchUserList();
                if (!Utils.isListEmpty(list)) {
                    if (isRefresh) {
                        mListAdapter.setNewData(list);
                    } else {
                        if (list.size() > 0) {
                            mListAdapter.addData(list);
                        }
                    }
                    if (list.size() < Integer.parseInt(pageSize)) {
                        mListAdapter.loadMoreEnd(isRefresh);
                    } else {
                        mListAdapter.loadMoreComplete();
                    }
                }
            }
        }
        mListView.hideRefresh(1);
    }

}
