package com.wiscomwis.facetoface.ui.login;

import android.content.Context;
import android.text.TextUtils;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.HyphenateHelper;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.FindPassword;
import com.wiscomwis.facetoface.data.model.HostInfo;
import com.wiscomwis.facetoface.data.model.Login;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.data.model.UserBean;
import com.wiscomwis.facetoface.data.model.UserDetail;
import com.wiscomwis.facetoface.data.preference.AnchorPreference;
import com.wiscomwis.facetoface.data.preference.BeanPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.event.FinishEvent;
import com.wiscomwis.facetoface.ui.main.MainActivity;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by zhangdroid on 2017/6/1.
 */
public class LoginPresenter implements LoginContract.IPresenter {
    private LoginContract.IView mLoginView;
    private Context mContext;

    public LoginPresenter(LoginContract.IView view) {
        this.mLoginView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
        // 获取本地保存的账号密码并设置
        String account = UserPreference.getAccount();
        String password = UserPreference.getPassword();
        if (!TextUtils.isEmpty(account) && !TextUtils.isEmpty(password)) {
            mLoginView.setAccount(account);
            mLoginView.setPassword(password);
        }
    }

    @Override
    public void login() {
        mLoginView.showLoading();
        ApiManager.login(mLoginView.getAccount(), mLoginView.getPassword(), new IGetDataListener<Login>() {
            @Override
            public void onResult(Login login, boolean isEmpty) {
                UserDetail userDetail = login.getUserDetail();
                if (null != userDetail) {
                    UserPreference.setRegisterSign(true);
                    if(!mLoginView.getAccount().equals(UserPreference.getAccount())){
                        HyphenateHelper.getInstance().clearAllUnReadMsg();//清空所有未读消息
                        DbModle.getInstance().getUserAccountDao().deleteAllData();//删除数据库中的所有数据
                        DbModle.getInstance().getUserAccountDao().deleteQaAllData();//删除QA
                    }
                    // 保存用户相关信息
                    UserBean userBean = userDetail.getUserBean();
                    if (null != userBean) {
                        BeanPreference.saveUserBean(userBean);
                    }
                    HostInfo hostInfo = userDetail.getHostInfo();
                    if (null != hostInfo) {
                        AnchorPreference.saveHostInfo(hostInfo);
                    }
                    UserBase userBase = userDetail.getUserBase();
                    if (null != userBase) {
                        // 保存用户信息到本地
                        UserPreference.saveUserInfo(userBase);
                        // 登录环信
                        ToastUtil.showShortToast(mContext, login.getMsg());
                        handleHyphenateLoginResult();
                    } else {
                        ToastUtil.showShortToast(mContext, login.getMsg());
                        handleHyphenateLoginResult();
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mLoginView.dismissLoading();
                if (isNetworkError) {
                    mLoginView.showNetworkError();
                } else {
                    mLoginView.showTip(msg);
                }
            }
        });
    }

    @Override
    public void findPassword() {
        mLoginView.showLoading();
        ApiManager.findPassword(new IGetDataListener<FindPassword>() {
            @Override
            public void onResult(FindPassword findPassword, boolean isEmpty) {
                mLoginView.dismissLoading();
                if(!TextUtils.isEmpty(findPassword.getAccount())){
                    mLoginView.showFindPwdDialog(findPassword.getAccount(), findPassword.getPassword());
                }else{
                    mLoginView.showFindPwdDialog(mContext.getString(R.string.nothing), mContext.getString(R.string.nothing));
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mLoginView.dismissLoading();
            }

        });
    }

    private void handleHyphenateLoginResult() {
        mLoginView.dismissLoading();
        // 关闭之前打开的页面
        EventBus.getDefault().post(new FinishEvent());
        // 跳转主页面
        LaunchHelper.getInstance().launchFinish(mContext, MainActivity.class);
    }

}
