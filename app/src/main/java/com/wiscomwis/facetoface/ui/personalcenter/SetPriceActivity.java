package com.wiscomwis.facetoface.ui.personalcenter;

import android.content.Context;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseTopBarActivity;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.SingleLoginFinishEvent;
import com.wiscomwis.facetoface.parcelable.ListParcelable;
import com.wiscomwis.facetoface.parcelable.TvPriceParcelable;
import com.wiscomwis.facetoface.ui.personalcenter.contract.SetPriceConteact;
import com.wiscomwis.facetoface.ui.personalcenter.presenter.SetPricePresenter;
import com.wiscomwis.facetoface.ui.video.MassVideoOrVoiceActivity;
import com.wiscomwis.library.dialog.LoadingDialog;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.LaunchHelper;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 价格设置页面
 * Created by zhangdroid on 2017/5/27.
 */
public class SetPriceActivity extends BaseTopBarActivity implements SetPriceConteact.IView, View.OnClickListener {
    @BindView(R.id.setting_range_seekBar)
    SeekBar settingRangeSeekBar;
    @BindView(R.id.setting_range_seekBar2)
    SeekBar settingRangeSeekBar2;
    @BindView(R.id.set_price_tv_sure)
    TextView tv_sure;
    @BindView(R.id.setting_range_seekBar_voice)
    SeekBar seekBarVoice;
    @BindView(R.id.setting_range_seekBar_voice2)
    SeekBar seekBarVoice2;
    @BindView(R.id.set_price_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.activity_set_price_ll_video_zhuanqian)
    LinearLayout ll_video_zhuanqian;
    @BindView(R.id.activity_set_price_tv_video_price)
    TextView tv_video_price;
    @BindView(R.id.activity_set_price_ll_voice_zhuanqian)
    LinearLayout ll_voice_zhuanqian;
    @BindView(R.id.activity_set_price_tv_voice_price)
    TextView tv_voice_price;
    @BindView(R.id.activity_set_price_ll_gifts_zhuanqian)
    LinearLayout ll_gifts_zhunaqian;
    private SetPricePresenter mSetPricePresenter;
    private TvPriceParcelable priceParcelable;
    private boolean videoCanClick = true;
    private boolean voiceCanClick = true;
    private String setTime;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_set_price;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.person_price_set);
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        priceParcelable = (TvPriceParcelable) parcelable;
    }

    @Override
    protected void initViews() {
        if (!TextUtils.isEmpty(priceParcelable.price)) {
            settingRangeSeekBar.setProgress((int) (Double.parseDouble(priceParcelable.price) - 10));
            settingRangeSeekBar2.setProgress((int) (Double.parseDouble(priceParcelable.price) - 10));
            seekBarVoice.setProgress(priceParcelable.audioPrice - 10);
            seekBarVoice2.setProgress(priceParcelable.audioPrice - 10);
            tv_video_price.setText(priceParcelable.price);
            tv_voice_price.setText(String.valueOf(priceParcelable.audioPrice));
        }
        mSetPricePresenter = new SetPricePresenter(this);
        tv_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSetPricePresenter.savePrice();

            }
        });
    }

    @Override
    protected void setListeners() {
        settingRangeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                settingRangeSeekBar2.setProgress(progress);
                tv_video_price.setText((10 + progress) + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        settingRangeSeekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                settingRangeSeekBar.setProgress(progress);
                tv_video_price.setText((10 + progress) + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        seekBarVoice.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarVoice2.setProgress(progress);
                tv_voice_price.setText((10 + progress) + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        seekBarVoice2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarVoice.setProgress(progress);
                tv_voice_price.setText((10 + progress) + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        rl_back.setOnClickListener(this);
        ll_video_zhuanqian.setOnClickListener(this);
        ll_voice_zhuanqian.setOnClickListener(this);
        ll_gifts_zhunaqian.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }


    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }


    @Override
    public String getVideoPrice() {
        return tv_video_price.getText().toString();
    }


    @Override
    public void modificationPrice() {
        Toast.makeText(SetPriceActivity.this, "价格修改成功", Toast.LENGTH_SHORT).show();

    }

    @Override
    public String getAudioPrice() {
        return tv_voice_price.getText().toString();
    }

    @Override
    public void getVideoTime(long countDown) {
         setTime = Util.convertTimeToString(countDown);
        videoCanClick = false;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.set_price_rl_back:
                finish();
                break;
            case R.id.activity_set_price_ll_video_zhuanqian:
                if (videoCanClick) {
                    String video_price = tv_video_price.getText().toString();
                    if (!TextUtils.isEmpty(video_price) && video_price.length() > 0) {
                        //群发视频
                        long currentTime1 = DataPreference.getCurrentTime();
                        if (currentTime1 == 0) {
                            //群发视频邀请
                            massVideoInvite();
                            DataPreference.saveCurrentTime(System.currentTimeMillis());
                        } else {
                            if (System.currentTimeMillis() - currentTime1 < 120 * 1000) {
                                Toast.makeText(SetPriceActivity.this, getString(R.string.sending_voice), Toast.LENGTH_SHORT).show();
                            } else {
                                //群发视频邀请
                                massVideoInvite();
                                DataPreference.saveCurrentTime(0);
                            }
                        }
                    }
                } else {
                    Toast.makeText(SetPriceActivity.this, "请在"+setTime+"后拨打", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.activity_set_price_ll_voice_zhuanqian:
                if (voiceCanClick) {
                    String voice_price = tv_voice_price.getText().toString();
                    if (!TextUtils.isEmpty(voice_price) && voice_price.length() > 0) {
                        //群发语音
                        long currentTime2 = DataPreference.getCurrentTime();
                        if (currentTime2 == 0) {
                            //群发语音邀请
                            massVoiceInvite();
                            DataPreference.saveCurrentTime(System.currentTimeMillis());
                        } else {
                            if (System.currentTimeMillis() - currentTime2 < 120 * 1000) {
                                Toast.makeText(SetPriceActivity.this, getString(R.string.sending_voice), Toast.LENGTH_SHORT).show();
                            } else {
                                //群发语音邀请
                                massVoiceInvite();
                                DataPreference.saveCurrentTime(0);
                            }
                        }
                    }
                } else {
                    Toast.makeText(SetPriceActivity.this, "半小时内限制发一次，请稍后再试", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.activity_set_price_ll_gifts_zhuanqian:
                LaunchHelper.getInstance().launch(mContext, AskGiftsActivity.class);
                break;
        }
    }

    //群发语音邀请
    private void massVoiceInvite() {
        UserPreference.setQuanFa(1);
        mSetPricePresenter.startInvite("2");
        DataPreference.saveTime(0);
        //ListParcelable是用来回调fragment,数据类型是一样的这里就用这个吧
        LaunchHelper.getInstance().launch(this, MassVideoOrVoiceActivity.class, new ListParcelable(2));
    }

    //群发视频邀请
    private void massVideoInvite() {
        UserPreference.setQuanFa(1);
        mSetPricePresenter.startInvite("1");
        DataPreference.saveTime(0);
        //ListParcelable是用来回调fragment,数据类型是一样的这里就用这个吧
        LaunchHelper.getInstance().launch(this, MassVideoOrVoiceActivity.class, new ListParcelable(1));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSetPricePresenter.getVideoCountDown();
        mSetPricePresenter.getVoiceCountDown();
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }
}
