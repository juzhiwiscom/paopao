package com.wiscomwis.facetoface.ui.personalcenter.adapter;

import android.content.Context;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.data.model.IncomeRecord;
import com.wiscomwis.library.adapter.CommonRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.util.DateTimeUtil;

import java.util.List;

/**
 * Created by Administrator on 2017/7/14.
 */

public class IncomeRecordAdapter extends CommonRecyclerViewAdapter<IncomeRecord> {
    public IncomeRecordAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public IncomeRecordAdapter(Context context, int layoutResId, List<IncomeRecord> dataList) {
        super(context, layoutResId, dataList);
    }

    @Override
    public void convert(IncomeRecord incomeRecord, int position, RecyclerViewHolder holder) {
        if(incomeRecord!=null){
              holder.setText(R.id.income_record_tv_time, DateTimeUtil.convertTimeMillis2String(incomeRecord.getAddTime()));
              holder.setText(R.id.income_record_tv_money,String.valueOf(incomeRecord.getBeanAmount()));
              switch (incomeRecord.getType()){
                  case 1:
                      holder.setText(R.id.income_record_tv_tujing,mContext.getString(R.string.tab_message));
                      break;
                  case 2:
                      holder.setText(R.id.income_record_tv_tujing,mContext.getString(R.string.video_video));
                      break;
                  case 3:
                      holder.setText(R.id.income_record_tv_tujing,mContext.getString(R.string.gift));
                      break;
                  case 4:
                      holder.setText(R.id.income_record_tv_tujing,mContext.getString(R.string.user_detail_voice));
                      break;
              }
          }

    }
}
