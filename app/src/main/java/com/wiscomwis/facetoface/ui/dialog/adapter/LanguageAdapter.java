package com.wiscomwis.facetoface.ui.dialog.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.View;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.library.adapter.CommonRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 语言选择（多选）适配器
 * Created by zhangdroid on 2017/6/9.
 */
public class LanguageAdapter extends CommonRecyclerViewAdapter<String> {
    // 标记item是否选中的集合
    private SparseBooleanArray mSelectArray;

    /**
     * 初始化列表中Item的选中状态
     *
     * @param list 已经选中的Item集合
     */
    public void initSelectState(List<String> list) {
        if (!Utils.isListEmpty(list)) {
            List<String> dataList = getAdapterDataList();
            for (int i = 0; i < dataList.size(); i++) {
                boolean isContained = false;
                for (int j = 0; j < list.size(); j++) {
                    if (dataList.get(i).equals(list.get(j))) {
                        isContained = true;
                        break;
                    }
                }
                mSelectArray.put(i, isContained);
            }
        } else {
            // 默认所有item都不选中
            for (int i = 0; i < getSize(); i++) {
                mSelectArray.put(i, false);
            }
        }
    }

    public LanguageAdapter(Context context, int layoutResId, List<String> dataList) {
        super(context, layoutResId, dataList);
        mSelectArray = new SparseBooleanArray(dataList.size());
    }

    @Override
    public void convert(String s, final int position, final RecyclerViewHolder holder) {
        if (!TextUtils.isEmpty(s)) {
            holder.setText(R.id.item_dialog_language, s);
        }

        holder.setOnClickListener(R.id.item_dialog_language, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelected = isItemSelected(position);
                setItemSelectState(position, !isSelected);

            }
        });
    }

    private boolean isItemSelected(int position) {
        return mSelectArray.get(position);
    }

    private void setItemSelectState(int position, boolean selected) {
        mSelectArray.put(position, selected);
    }

    /**
     * @return 返回所有选中的item
     */
    public List<String> getSelectedItems() {
        List<String> selectedList = new ArrayList<>();
        if (mSelectArray.size() > 0) {
            for (int i = 0; i < getSize(); i++) {
                if (mSelectArray.get(i)) {
                    selectedList.add(getAdapterDataList().get(i));
                }
            }
        }
        return selectedList;
    }

}
