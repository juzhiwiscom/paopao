package com.wiscomwis.facetoface.ui.chat;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMVoiceMessageBody;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.HyphenateHelper;
import com.wiscomwis.facetoface.common.RecordUtil;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.SeeDetailEvent;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.adapter.provider.ItemViewProvider;
import com.wiscomwis.library.image.CropCircleTransformation;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.DateTimeUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * 聊天语音消息（接收方）
 * Created by zhangdroid on 2017/6/29.
 */
public class VoiceMessageLeftProvider implements ItemViewProvider<EMMessage> {
    private Context mContext;
    private String mAvatarUrl;
    private List<EMMessage> messageList;


    public VoiceMessageLeftProvider(Context context, String url, List<EMMessage> messageList) {
        this.mContext = context;
        this.mAvatarUrl = url;
        this.messageList = messageList;
    }

    @Override
    public int getItemViewLayoutResId() {
        return R.layout.item_chat_voice_left;
    }

    @Override
    public boolean isViewType(EMMessage item, int position) {
        return (item.getType() == EMMessage.Type.VOICE && item.direct() == EMMessage.Direct.RECEIVE);
    }

    @Override
    public void convert(final EMMessage emMessage, int position, final RecyclerViewHolder holder) {
        // 头像
        ImageView ivAvatar = (ImageView) holder.getView(R.id.item_chat_voice_avatar_left);
        String cacheIg = UserPreference.getCacheIg(mAvatarUrl);
        if(!TextUtils.isEmpty(cacheIg)){
            Bitmap bitmap = BitmapFactory.decodeFile(cacheIg);
            Bitmap bitmap1 = Util.setCircle(bitmap);
            if(bitmap1!=null)
              ivAvatar.setImageBitmap(bitmap1);
        }else {
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().transform(new CropCircleTransformation(mContext)).placeHolder(Util.getDefaultImageCircle())
                    .error(Util.getDefaultImageCircle()).url(mAvatarUrl).imageView(ivAvatar).build());
        }
        ivAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 发送事件，查看用户详情
                EventBus.getDefault().post(new SeeDetailEvent());
            }
        });
        if (null != emMessage) {
            if (position > 0) {
                long msgTime = messageList.get(position - 1).getMsgTime();
                if ((emMessage.getMsgTime()-msgTime) / (1000 * 60) > 5) {
                    holder.getView(R.id.item_chat_voice_time_left).setVisibility(View.VISIBLE);
                    holder.setText(R.id.item_chat_voice_time_left, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
                } else {
                    holder.getView(R.id.item_chat_voice_time_left).setVisibility(View.GONE);
                }
            } else {
                holder.getView(R.id.item_chat_voice_time_left).setVisibility(View.VISIBLE);
                holder.setText(R.id.item_chat_voice_time_left, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
            }

            final EMVoiceMessageBody emVoiceMessageBody = (EMVoiceMessageBody) emMessage.getBody();
            if (null != emVoiceMessageBody) {
                // 如果语音文件本地路径不存在，则下载
                if (TextUtils.isEmpty(emVoiceMessageBody.getLocalUrl())) {
                    HyphenateHelper.getInstance().downloadAttachment(emMessage);
                }
                final ImageView iv = (ImageView) holder.getView(R.id.item_chat_voice_left);
                // 语音时间
                holder.setText(R.id.item_chat_voice_left_duration, TextUtils.concat(String.valueOf(emVoiceMessageBody.getLength()), "'").toString());
                holder.setOnClickListener(R.id.item_chat_voice_left_ll, new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        RecordUtil.getInstance().play4(emVoiceMessageBody.getLocalUrl(),iv,mContext,new RecordUtil.OnPlayerListener(){
                            @Override
                            public void onCompleted() {
                                iv.setImageResource(R.drawable.sound_wave_left3);
                            }

                            @Override
                            public void onPaused() {
                                iv.setImageResource(R.drawable.sound_wave_left3);
                            }
                        });
                    }
                });
            }
        }
    }

}
