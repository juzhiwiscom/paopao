package com.wiscomwis.facetoface.ui.charmandrankinglist.adapter;

import android.content.Context;
import android.widget.ImageView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.model.RankList;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.data.model.UserMend;
import com.wiscomwis.library.adapter.CommonRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;

/**
 * Created by WangYong on 2017/10/31.
 */

public class CharmandAndDrankingAdapter extends CommonRecyclerViewAdapter<RankList>{
    private int pos=0;
    public CharmandAndDrankingAdapter(Context context, int layoutResId,int pos) {
        super(context, layoutResId);
        this.pos=pos;
    }

    @Override
    public void convert(RankList rankList, int position, RecyclerViewHolder holder) {
         if(rankList!=null){
             holder.setText(R.id.charmand_ranking_item_tv_pos,String.valueOf(position+4));
             UserBase userBase = rankList.getUserBase();
             ImageView iv_avatar = (ImageView) holder.getView(R.id.charmand_ranking_item_iv_avatar);
             if(userBase!=null){
                   holder.setText(R.id.charmand_ranking_item_tv_nickname,userBase.getNickName());
                 ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(userBase.getIconUrlMiddle())
                         .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_avatar).build());
             }
             UserMend userMend = rankList.getUserMend();
             if(userMend!=null){
                 if(pos==0){
                     holder.setText(R.id.charmand_ranking_item_tv_gift_num,mContext.getString(R.string.receive)+userMend.getReceiveGiftCount()+mContext.getString(R.string.number_gifts));
                 }else{
                     holder.setText(R.id.charmand_ranking_item_tv_gift_num,mContext.getString(R.string.send)+userMend.getSendGiftCount()+mContext.getString(R.string.number_gifts));

                 }
             }

         }
    }
}
