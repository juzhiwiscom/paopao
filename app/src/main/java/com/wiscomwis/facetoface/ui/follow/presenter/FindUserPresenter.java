package com.wiscomwis.facetoface.ui.follow.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.data.model.UserDetail;
import com.wiscomwis.facetoface.data.model.UserDetailforOther;
import com.wiscomwis.facetoface.parcelable.UserDetailParcelable;
import com.wiscomwis.facetoface.ui.detail.UserDetailActivity;
import com.wiscomwis.facetoface.ui.follow.contract.FindUserContract;
import com.wiscomwis.library.util.LaunchHelper;

/**
 * Created by Administrator on 2017/6/15.
 */

public class FindUserPresenter implements FindUserContract.IPresenter{
    private FindUserContract.IView mFildUserView;
    private Context mContext;

    public FindUserPresenter(FindUserContract.IView mFildUserView) {
        this.mFildUserView = mFildUserView;
        this.mContext=mFildUserView.obtainContext();
    }

    @Override
    public void start() {

    }


    @Override
    public void goToUserDetailActivity(String id) {
        LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class, new UserDetailParcelable(id));
        Util.setDetailMsgFlag(id,"3");

    }

    @Override
    public void setOnclick() {
        final String remoteId = mFildUserView.getRemoteId();
        if(!TextUtils.isEmpty(remoteId)){
            ApiManager.findUserInfo(remoteId, new IGetDataListener<UserDetailforOther>() {
                @Override
                public void onResult(UserDetailforOther userDetailforOther, boolean isEmpty) {
                    if(userDetailforOther!=null){
                        String isSucced = userDetailforOther.getIsSucced();
                        if (isSucced.equals("1")) {
                            UserDetail userDetail = userDetailforOther.getUserDetail();
                            if(userDetail!=null){
                                UserBase userBase = userDetail.getUserBase();
                                if(userBase!=null){
                                    int gender = userBase.getGender();
                                        mFildUserView.canGoToUserDetailActivity(String.valueOf(userBase.getGuid()));
                                }
                            }
                        }else{
                            mFildUserView.showIdOrMessageError(mContext.getString(R.string.find_account_miss));
                        }
                    }
                }
                @Override
                public void onError(String msg, boolean isNetworkError) {
                    mFildUserView.showIdOrMessageError(mContext.getString(R.string.find_account_miss));
                }
            });
        }else{
          mFildUserView.showTip(mContext.getString(R.string.find_edit_id));
        }
    }
}
