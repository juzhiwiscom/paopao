package com.wiscomwis.facetoface.ui.chat;

import android.content.ClipboardManager;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;
import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.HyphenateHelper;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.model.HuanXinUser;
import com.wiscomwis.facetoface.data.model.NettyMessage;
import com.wiscomwis.facetoface.data.model.PicInfo;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.ui.dialog.CustomOperateDialog;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.adapter.provider.ItemViewProvider;
import com.wiscomwis.library.image.CropCircleTransformation;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.DateTimeUtil;
import com.wiscomwis.library.util.ToastUtil;

import java.util.List;

/**
 * 聊天文字消息（发送方）
 * Created by zhangdroid on 2017/6/29.
 */
public class TextMessageRightProvider implements ItemViewProvider<EMMessage> {
    private Context mContext;
    private List<EMMessage> messageList;
    private String mAccount;//接收方的账户
    private String sendTime;
    private TextView textSendTime;
    private PicInfo picInfo;
    private String nickname;
    private String uid;
    private String url;

    public TextMessageRightProvider(Context context, List<EMMessage> messageList,String account,String nickname,String uid,String url) {
        this.mContext = context;
        this.messageList = messageList;
        this.mAccount=account;
        this.nickname=uid;
        this.url=url;
    }

    @Override
    public int getItemViewLayoutResId() {
        return R.layout.item_chat_text_right;
    }

    @Override
    public boolean isViewType(EMMessage item, int position) {
        return (item.getType() == EMMessage.Type.TXT && item.direct() == EMMessage.Direct.SEND);
    }

    @Override
    public void convert(final EMMessage emMessage, int position, final RecyclerViewHolder holder) {
        if (null != emMessage) {
            String msgContent = emMessage.getStringAttribute("msg", "");
            final NettyMessage nettyMessage = setMsgContent(msgContent);
            if (nettyMessage != null) {
                 sendTime = nettyMessage.getSendTime();
                 textSendTime = (TextView) holder.getView(R.id.item_chat_text_revocation);
                textSendTime.setText(sendTime);
            }
            if (position > 0) {
                long msgTime = messageList.get(position - 1).getMsgTime();
                if ((emMessage.getMsgTime()-msgTime) /(1000 * 60) > 5) {
                    holder.getView(R.id.item_chat_text_time_right).setVisibility(View.VISIBLE);
                    holder.setText(R.id.item_chat_text_time_right, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
                } else {
                    holder.getView(R.id.item_chat_text_time_right).setVisibility(View.GONE);
                }
            }else{
                holder.getView(R.id.item_chat_text_time_right).setVisibility(View.VISIBLE);
                holder.setText(R.id.item_chat_text_time_right, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
            }
            EMTextMessageBody emTextMessageBody = (EMTextMessageBody) emMessage.getBody();
            if (null != emTextMessageBody) {
                boolean deleteMsg = UserPreference.isDeleteMsg(emMessage.getMsgId());
                boolean withdrawMsg = UserPreference.isNativeWithdraw(emMessage.getMsgId());
                if (!TextUtils.isEmpty(emTextMessageBody.getMessage())&&emTextMessageBody.getMessage().contains("对方撤回了一条消息")) {
//           本地消息撤回处理（隐藏掉执行撤回的这条语句)
            holder.setVisibility(R.id.item_chat_text_avatar_right,false);
            holder.setVisibility(R.id.item_chat_tv_right,false);
            holder.setVisibility(R.id.item_chat_ll_right_send_gifts,false);
            holder.setVisibility(R.id.item_chat_text_revocation,false);
            holder.getView(R.id.item_chat_text_time_right).setVisibility(View.GONE);
                }else if(deleteMsg){
                    //           本地消息删除处理
                    holder.setVisibility(R.id.item_chat_text_avatar_right,false);
                    holder.setVisibility(R.id.item_chat_tv_right,false);
                    holder.setVisibility(R.id.item_chat_ll_right_send_gifts,false);
                    holder.setVisibility(R.id.item_chat_text_revocation,true);
                    holder.setText(R.id.item_chat_text_revocation,"你删除了一条消息");
                }else if(withdrawMsg) {
                    //           本地消息撤回处理
                    holder.setVisibility(R.id.item_chat_text_avatar_right,false);
                    holder.setVisibility(R.id.item_chat_tv_right,false);
                    holder.setVisibility(R.id.item_chat_ll_right_send_gifts,false);
                    holder.setVisibility(R.id.item_chat_text_revocation,true);
                    holder.setText(R.id.item_chat_text_revocation,mContext.getString(R.string.withdraw_msg));
                }else {
                    ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().transform(new CropCircleTransformation(mContext)).placeHolder(Util.getDefaultImageCircle())
                            .error(Util.getDefaultImageCircle()).url(UserPreference.getSmallImage()).imageView((ImageView) holder.getView(R.id.item_chat_text_avatar_right)).build());
                    final TextView tv_right = (TextView) holder.getView(R.id.item_chat_tv_right);
                    LinearLayout ll_send_gifts = (LinearLayout) holder.getView(R.id.item_chat_ll_right_send_gifts);
                    ImageView iv_gifts = (ImageView) holder.getView(R.id.item_chat_iv_right_gifts);
                    TextView tv_male = (TextView) holder.getView(R.id.item_chat_tv_right_meinv_or_shuaige);
                    TextView tv_gift_num = (TextView) holder.getView(R.id.item_chat_tv_right_gifts_num);
                    holder.setVisibility(R.id.item_chat_text_revocation,false);
                    holder.setVisibility(R.id.item_chat_text_avatar_right,true);
                    if (emTextMessageBody.getMessage().contains("{") && emTextMessageBody.getMessage().contains("}")) {
                        tv_right.setVisibility(View.GONE);
                        ll_send_gifts.setVisibility(View.VISIBLE);
                         picInfo = new Gson().fromJson(emTextMessageBody.getMessage(), PicInfo.class);
                        if (picInfo != null) {
                            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(picInfo.getPicIcon())
                                    .placeHolder(0).error(0).imageView(iv_gifts).build());
                            if (UserPreference.isMale()) {
                                tv_male.setText(mContext.getString(R.string.hi_girl));
                            } else {
                                tv_male.setText(mContext.getString(R.string.hi_boy));
                            }
                            tv_gift_num.setText(picInfo.getPicNum() + "个" + picInfo.getPicName());
                        }
                    } else {
                        tv_right.setVisibility(View.VISIBLE);
                        ll_send_gifts.setVisibility(View.GONE);
                        tv_right.setText(emTextMessageBody.getMessage());
                    }
                    tv_right.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            final CustomOperateDialog codDialog = new CustomOperateDialog(mContext)
                                    .setClickedView(tv_right);
                            codDialog.setClickListener(new CustomOperateDialog.OnClickCustomButtonListener() {
                                @Override
                                public void onClick(String str) {
                                    if(str.equals("撤回")){
                                        if(isTime()){
                                            final NettyMessage nettyMessage = setMsgContent(emMessage.getStringAttribute("msg", ""));
                                            if (nettyMessage != null) {
                                                sendTime = nettyMessage.getSendTime();
                                            }else {
                                                sendTime=tv_right.getText()+"";
                                            }
                                            holder.setVisibility(R.id.item_chat_text_revocation,true);
                                            holder.setText(R.id.item_chat_text_revocation,mContext.getString(R.string.withdraw_msg));
                                            holder.setVisibility(R.id.item_chat_text_avatar_right,false);
                                            holder.setVisibility(R.id.item_chat_tv_right,false);
                                            holder.setVisibility(R.id.item_chat_ll_right_send_gifts,false);
                                            UserPreference.nativeWithdraw(emMessage.getMsgId());
                                            codDialog.dismiss();
                                            HyphenateHelper.getInstance().sendTextMessage(mAccount, sendTime+ C.withdraw_msg,true,
                                                    new HyphenateHelper.OnMessageSendListener() {
                                                        @Override
                                                        public void onSuccess(EMMessage emMessage) {
                                                            if(nettyMessage!=null){
                                                                HuanXinUser user = new HuanXinUser(nettyMessage.getSendUserId()+"", nettyMessage.getSendUserName(),nettyMessage.getSendUserIcon(), mAccount, "1", mContext.getString(R.string.withdraw_msg), 0, String.valueOf(System.currentTimeMillis()), 1);
                                                                DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
                                                            }
                                                                }

                                                        @Override
                                                        public void onError() {
                                                        }
                                                    });
                                        }else {
                                            codDialog.dismiss();
                                            ToastUtil.showShortToast(mContext, "消息已经超过两分钟，不能撤回");
                                        }
                                    }else if(str.equals("删除")){
                                        holder.setVisibility(R.id.item_chat_text_revocation,true);
                                        holder.setText(R.id.item_chat_text_revocation,"你删除了一条消息");
                                        holder.setVisibility(R.id.item_chat_text_avatar_right,false);
                                        holder.setVisibility(R.id.item_chat_tv_right,false);
                                        holder.setVisibility(R.id.item_chat_ll_right_send_gifts,false);
                                        UserPreference.setDeleteMsg(emMessage.getMsgId());
                                        codDialog.dismiss();
                                    }else if(str.equals("取消")){
                                        codDialog.dismiss();
                                    }else if(str.equals("复制")){
                                        ClipboardManager cm = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                                        // 将文本内容放到系统剪贴板里。
                                        cm.setText(tv_right.getText());
                                        ToastUtil.showShortToast(mContext, "复制成功");
                                    }
                                }
                            });
                            codDialog.show();
                            return false;
                        }

                        private boolean isTime() {
                            long l = Util.getCurrentTime() - emMessage.getMsgTime();
                            if(l<1000*60*2){
                                return true;
                            }
                            return false;
                        }
                    });
                }
            }
        }
    }
    //解析发过来的数据
    private NettyMessage setMsgContent(String msgContent) {
        if (!TextUtils.isEmpty(msgContent)) {
            NettyMessage nettyMessage = new Gson().fromJson(msgContent, NettyMessage.class);
            return nettyMessage;
        } else {
            return null;
        }
    }



}
