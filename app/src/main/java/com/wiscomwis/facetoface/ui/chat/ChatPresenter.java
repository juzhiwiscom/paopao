package com.wiscomwis.facetoface.ui.chat;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.text.TextUtils;
import android.view.MotionEvent;

import com.hyphenate.chat.EMMessage;
import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.HyphenateHelper;
import com.wiscomwis.facetoface.common.RecordUtil;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.BlockBean;
import com.wiscomwis.facetoface.data.model.CheckTextModel;
import com.wiscomwis.facetoface.data.model.HuanXinUser;
import com.wiscomwis.facetoface.data.model.ResultMessage;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.parcelable.PayParcelable;
import com.wiscomwis.facetoface.ui.pay.activity.RechargeActivity;
import com.wiscomwis.facetoface.ui.photo.GetPhotoActivity;
import com.wiscomwis.library.util.FileUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.LogUtil;
import com.wiscomwis.library.util.Utils;

import java.io.File;
import java.util.List;

/**
 * Created by zhangdroid on 2017/6/27.
 */
public class ChatPresenter implements ChatContract.IPresenter {
    private static final String TAG = ChatPresenter.class.getSimpleName();
    private ChatContract.IView mChatView;
    private Context mContext;
    // 接收方用户account
    private String mAccount;
    // 接收方用户guid
    private long mGuid;
    // 消息业务扩展类型
    private int mExtendType;
    // 录音文件存放目录
    private String mRecordDirectory;
    // 录音文件路径
    private String mRecordOutputPath;
    // 是否正在录音
    private boolean mIsRecording;
    // 录音是否被取消
    private boolean mIsRecordCanceled;
    // 录音时长（毫秒）
    private long mRecordDuration;
    // 录音计时刷新间隔（毫秒）
    private final long mRefreshInterval = 250;
    // 每页消息数
    private final int mPageSize = 20;
    private boolean canSendMsg = true;
    private String userName = "";
    private String userPic = "";
    private boolean isBlock = true;
    private String checkText;

    public ChatPresenter(ChatContract.IView view, String account, long guid, String name, String pic, int extendType) {
        this.mChatView = view;
        this.mContext = view.obtainContext();
        this.mAccount = account;
        this.mGuid = guid;
        this.userName = name;
        this.userPic = pic;
        this.mExtendType = extendType;
    }

    @Override
    public void isHaveIntercept(long id) {
        if (UserPreference.isVip() || UserPreference.isAnchor() || mGuid == 10000) {
            canSendMsg = true;
        } else {
            canSendMsg=false;
            mChatView.isGoneButtong(true);
        }
    }

    @Override
    public void start() {
        // 录音文件临时存放目录，内部存储，不需要权限
        mRecordDirectory = FileUtil.getExternalFilesDir(mContext, Environment.DIRECTORY_MUSIC) + File.separator;
    }

    @Override
    public void finish() {
//        mChatView = null;
//        mContext = null;
        // 释放录音和播放器资源
        RecordUtil.getInstance().releaseRecord();
        if (RecordUtil.getInstance().isPlaying()) {// 如果正在播放语音，需要停止
            RecordUtil.getInstance().stop();
        }
        RecordUtil.getInstance().release();
    }

    @Override
    public void msgRead(String guid) {
        ApiManager.msgRead(guid, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    @Override
    public void isBlock() {
        ApiManager.getIsBlock(String.valueOf(mGuid), new IGetDataListener<BlockBean>() {
            @Override
            public void onResult(BlockBean blockBean, boolean isEmpty) {
                if (blockBean != null) {
                    if (blockBean.getIsBlock() == 0) {//未拉黑
                    } else {//已拉黑
                        isBlock = false;
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    @Override
    public void handleAsyncTask(Message message) {
        switch (message.what) {
            case C.message.MSG_TYPE_TIMER:// 计时
                if (mIsRecording) {
                    mRecordDuration += mRefreshInterval;
                    // 录音超过1分钟自动发送
                    if (mRecordDuration > 60_000) {
                        stopRecordAndSend();
                    } else {
                        mChatView.sendMessage(mRefreshInterval, C.message.MSG_TYPE_TIMER);
                    }
                }
                break;
            case C.message.MSG_TYPE_DELAYED:// 延时结束录音
                RecordUtil.getInstance().stopRecord();
                mRecordDuration = 0;
                break;

            case C.message.MSG_TYPE_INIT:
                List<EMMessage> list = (List<EMMessage>) message.obj;
                if (Utils.isListEmpty(list)) {
//                    mChatView.toggleShowEmpty(true, null);
                } else {
                    if (mChatView != null) {
                        mChatView.getChatAdapter().bind(list);
                    }
                    if(list.size()>1)
                        mChatView.setAQShow(list.size());
                }
                mChatView.hideRefresh();
                mChatView.scroollToBottom();
                break;
            case C.message.MSG_TYPE_ADD:
                List<EMMessage> adapterDataList = mChatView.getChatAdapter().getAdapterDataList();
                if (adapterDataList != null && adapterDataList.size() > 0) {
                    String msgId = adapterDataList.get(0).getMsgId();
                    List<EMMessage> emMessages = HyphenateHelper.getInstance().addConversation(mAccount, msgId, mPageSize);
                    if (emMessages != null && emMessages.size() > 0) {
                        for (int i = emMessages.size() - 1; i >= 0; i--) {
                            mChatView.getChatAdapter().insertItem(0, emMessages.get(i));
                        }
                    }
                }

                mChatView.hideRefresh();
                break;
            case C.message.MSG_TYPE_LOAD_MORE:
                List<EMMessage> emMessagesMore = (List<EMMessage>) message.obj;
                if (!Utils.isListEmpty(emMessagesMore)) {

                    mChatView.getChatAdapter().appendToList(emMessagesMore);
                }
                mChatView.hideRefresh();
                break;
            case C.message.MSG_TYPE_SEND_TXT:
                if (!TextUtils.isEmpty(mAccount)) {
                    String text;
                    if(!TextUtils.isEmpty(checkText)){
                         text =checkText;
                    }else {
                         text=mChatView.getInputText();
                    }
                    EMMessage txtSendMessage = EMMessage.createTxtSendMessage(text, mAccount);
                    Message msg = Message.obtain();
                    msg.what = C.message.MSG_TYPE_UPDATE;
                    msg.obj = txtSendMessage;
                    mChatView.sendMessage(msg);
                    HyphenateHelper.getInstance().sendTextMessage(mAccount, text,
                            new HyphenateHelper.OnMessageSendListener() {
                                @Override
                                public void onSuccess(EMMessage emMessage) {
                                }

                                @Override
                                public void onError() {
                                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                                }
                            });
                } else {
                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                }
                mChatView.clearInput();
                break;
            case C.message.MSG_TYPE_SEND_VOICE:
                if (!TextUtils.isEmpty(mAccount)) {
                    EMMessage txtSendMessage = EMMessage.createVoiceSendMessage(mRecordOutputPath, (int) (mRecordDuration / 1000), mAccount);
                    Message msg = Message.obtain();
                    msg.what = C.message.MSG_TYPE_UPDATE;
                    msg.obj = txtSendMessage;
                    mChatView.sendMessage(msg);
                    HyphenateHelper.getInstance().sendVoiceMessage(mAccount, mRecordOutputPath, (int) (mRecordDuration / 1000),
                            new HyphenateHelper.OnMessageSendListener() {
                                @Override
                                public void onSuccess(EMMessage emMessage) {
                                }

                                @Override
                                public void onError() {
                                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                                }
                            });
                } else {
                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                }
                break;
            case C.message.MSG_TYPE_SEND_IMAGE:
                if (!TextUtils.isEmpty(mAccount)) {

                    String path = (String) message.obj;
                    EMMessage imageSendMessage = EMMessage.createImageSendMessage(path, false, mAccount);
                    Message msg = Message.obtain();
                    msg.what = C.message.MSG_TYPE_UPDATE;
                    msg.obj = imageSendMessage;
                    mChatView.sendMessage(msg);

                    HyphenateHelper.getInstance().sendImageMessage(mAccount, (String) message.obj,
                            new HyphenateHelper.OnMessageSendListener() {
                                @Override
                                public void onSuccess(EMMessage emMessage) {
                                }

                                @Override
                                public void onError() {
                                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                                }
                            });
                } else {
                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                }
                break;
            case C.message.MSG_TYPE_UPDATE:// 更新消息
                ChatAdapter chatAdapter = mChatView.getChatAdapter();
                if (chatAdapter.getItemCount() == 0) {
                    mChatView.toggleShowEmpty(false, null);
                }
                EMMessage emMessage = (EMMessage) message.obj;
                if (null != chatAdapter && null != emMessage) {
                    chatAdapter.insertItem(chatAdapter.getItemCount(), emMessage);
                }
                mChatView.scroollToBottom();
                break;
            case 100:
                mChatView.scroollToBottom();
                break;
        }
    }

    @Override
    public void initChatConversation() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message message = Message.obtain();
                message.what = C.message.MSG_TYPE_INIT;
                message.obj = HyphenateHelper.getInstance().initConversation(mGuid,mAccount, mPageSize);
                mChatView.sendMessage(message);
            }
        }).start();
    }

    @Override
    public void refresh() {
        // 加载当前消息列表之前的消息
        LogUtil.i(TAG, "refresh()");

        // 当前显示聊天记录列表条数
        final int listCount = mChatView.getChatAdapter().getItemCount();
        // 全部聊天记录条数
        final int allCount = HyphenateHelper.getInstance().getAllMsgCount(mAccount);
        if (listCount < allCount) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    int pageSize = mPageSize;
                    if (allCount - listCount < 20) {
                        pageSize = allCount - listCount;
                    }
                    Message message = Message.obtain();
                    message.what = C.message.MSG_TYPE_LOAD_MORE;
                    message.obj = HyphenateHelper.getInstance().loadMoreMessages(mAccount, pageSize);
                    mChatView.sendMessage(message);
                }
            }).start();
        } else {
            mChatView.hideRefresh();
        }
    }

    @Override
    public void sendTextMessage() {
        if (!TextUtils.isEmpty(mChatView.getInputText())) {
            if (canSendMsg) {
                ApiManager.checkText(mGuid, mChatView.getInputText(), new IGetDataListener<CheckTextModel>() {
                    @Override
                    public void onResult(CheckTextModel baseModel, boolean isEmpty) {
                        if (baseModel.getIsSucceed().equals("-31")) {

                            mChatView.showInterruptDialog(mContext.getString(R.string.chat_who_interrupt));
                        } else if (baseModel.getIsSucceed().equals("-32")) {
                            mChatView.showInterruptDialog(mContext.getString(R.string.chat_interrupt_who));
                        }
                        ResultMessage resultMessage = baseModel.getResultMessage();
                        if(resultMessage!=null&&!TextUtils.isEmpty(resultMessage.getContent())){
                            checkText = resultMessage.getContent();
                        }else{
                            checkText=mChatView.getInputText();
                        }
                        if (isBlock) {
                            mChatView.sendMessage(0, C.message.MSG_TYPE_SEND_TXT);
                            saveDataToSqlit(userName, String.valueOf(mGuid), mAccount, userPic, checkText, String.valueOf(System.currentTimeMillis()), mExtendType);
                        }
                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {
                        if (isBlock) {
                            mChatView.sendMessage(0, C.message.MSG_TYPE_SEND_TXT);
                            saveDataToSqlit(userName, String.valueOf(mGuid), mAccount, userPic, mChatView.getInputText(), String.valueOf(System.currentTimeMillis()), mExtendType);
                        }
//                    mChatView.showInterruptDialog(mContext.getString(R.string.message_interrupt));
                    }
                });

            } else {
                LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON,0,9));

            }

        }
    }

    @Override
    public void sendImageMessage(final boolean isTakePhoto) {
        GetPhotoActivity.toGetPhotoActivity(mContext, isTakePhoto, new GetPhotoActivity.OnGetPhotoPathListener() {
            @Override
            public void getSelectedPhotoPath(final String path) {

                if (!TextUtils.isEmpty(path)) {
                    if (canSendMsg) {
                        ApiManager.interruptImage(mGuid, new File(path), new IGetDataListener<BaseModel>() {
                            @Override
                            public void onResult(BaseModel baseModel, boolean isEmpty) {

                                if (baseModel.getIsSucceed().equals("-32")) {
                                    mChatView.showInterruptDialog(mContext.getString(R.string.chat_who_interrupt));

                                } else if (baseModel.getIsSucceed().equals("-31")) {

                                    mChatView.showInterruptDialog(mContext.getString(R.string.chat_interrupt_who));
                                }
                            }

                            @Override
                            public void onError(String msg, boolean isNetworkError) {
                            }
                        });
                        if (isBlock) {
                            Message message = Message.obtain();
                            message.what = C.message.MSG_TYPE_SEND_IMAGE;
                            message.obj = path;
                            mChatView.sendMessage(message);
                            saveDataToSqlit(userName, String.valueOf(mGuid), mAccount, userPic, mContext.getString(R.string.photo_message), String.valueOf(System.currentTimeMillis()), mExtendType);
                        }
                    } else {
                        LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON,0,8));

                    }
                } else {
                    mChatView.showTip(mContext.getString(R.string.chat_image_empty));
                }
            }
        });
    }

    @Override
    public void handleTouchEventDown() {
        mChatView.showRecordPopupWindow(C.message.STATE_RECORDING);
        startRecord();
        mIsRecordCanceled = false;
    }

    @Override
    public void handleTouchEventMove(MotionEvent event) {
        if (event.getY() < -100) { // 上滑取消发送
            mIsRecordCanceled = true;
            mChatView.showRecordPopupWindow(C.message.STATE_CANCELED);
        } else {
            mIsRecordCanceled = false;
            mChatView.showRecordPopupWindow(C.message.STATE_RECORDING);
        }
    }

    @Override
    public void handleTouchEventUp() {
        mChatView.showRecordPopupWindow(C.message.STATE_IDLE);
        mChatView.dismissPopupWindow();
        if (mIsRecordCanceled) {
            cancelRecord();
        } else {
            stopRecordAndSend();
        }
    }

    private void startRecord() {
        // 录音开始前震动一下
        Vibrator vibrator = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(88);
        // 根据系统时间生成文件名
        String recordFileName = FileUtil.createFileNameByTime() + ".aac";
        // 录音文件临时保存路径：data下包名music目录
        mRecordOutputPath = mRecordDirectory + recordFileName;
        // 开始录音
        RecordUtil.getInstance().startRecord(mRecordOutputPath);
        mIsRecording = true;
        mRecordDuration = 0;
        // 计时，间隔250毫秒刷新
        mChatView.sendMessage(mRefreshInterval, C.message.MSG_TYPE_TIMER);
    }

    private void stopRecordAndSend() {
        if (mIsRecording) {
            mIsRecording = false;
            if (mRecordDuration < 1000) {// 录音时长小于1秒的不发送
                // 删除小于1秒的文件
                FileUtil.deleteFile(mRecordOutputPath);
                // 延时500毫秒调用MediaRecorder#stop()，防止出现start()之后立马调用stop()的异常
                mChatView.sendMessage(500, C.message.MSG_TYPE_DELAYED);
            } else {// 发送语音
                RecordUtil.getInstance().stopRecord();
                sendVoice();
            }
        }
    }

    private void cancelRecord() {
        if (mIsRecordCanceled) {
            RecordUtil.getInstance().stopRecord();
            mIsRecordCanceled = false;
            FileUtil.deleteFile(mRecordOutputPath);
        }
    }

    private void sendVoice() {
        if (canSendMsg) {
            ApiManager.interruptVoice(mGuid, new File(mRecordOutputPath), String.valueOf(mRecordDuration / 1000), new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel baseModel, boolean isEmpty) {
                    if (baseModel.getIsSucceed().equals("-32")) {
                        mChatView.showInterruptDialog(mContext.getString(R.string.chat_who_interrupt));
                        FileUtil.deleteFile(mRecordOutputPath);

                    } else if (baseModel.getIsSucceed().equals("-31")) {
                        mChatView.showInterruptDialog(mContext.getString(R.string.chat_interrupt_who));
                        FileUtil.deleteFile(mRecordOutputPath);
                    }
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                    // 删除已经录制的音频文件
                    FileUtil.deleteFile(mRecordOutputPath);
                }
            });
            if (isBlock) {
                mChatView.sendMessage(0, C.message.MSG_TYPE_SEND_VOICE);
                saveDataToSqlit(userName, String.valueOf(mGuid), mAccount, userPic, mContext.getString(R.string.voice_message), String.valueOf(System.currentTimeMillis()), mExtendType);
            }
        } else {
            LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON,0,8));
        }
    }

    public void saveDataToSqlit(String name, String id, String account, String pic, String msg, String time, int extendType) {
        if (!TextUtils.isEmpty(id)) {
            HuanXinUser user = new HuanXinUser(id, name, pic, account, "1", msg, 0, time, extendType);
            DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
        }
    }

    @Override
    public void setRefresh2() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                addChatConversation();
            }
        }, 2000);
    }

    private void addChatConversation() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message message = Message.obtain();
                message.what = C.message.MSG_TYPE_ADD;
                mChatView.sendMessage(message);
            }
        }).start();
    }
    @Override
    public void updateDate(String message){
        if (!TextUtils.isEmpty(mAccount)) {
            EMMessage txtSendMessage = EMMessage.createTxtSendMessage(message, mAccount);
            Message msg = Message.obtain();
            msg.what = C.message.MSG_TYPE_UPDATE;
            msg.obj = txtSendMessage;
            mChatView.sendMessage(msg);
        }
    }
}
