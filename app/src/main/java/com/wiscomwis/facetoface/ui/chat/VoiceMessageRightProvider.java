package com.wiscomwis.facetoface.ui.chat;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMVoiceMessageBody;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.HyphenateHelper;
import com.wiscomwis.facetoface.common.RecordUtil;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.adapter.provider.ItemViewProvider;
import com.wiscomwis.library.image.CropCircleTransformation;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.DateTimeUtil;
import com.wiscomwis.library.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 聊天语音消息（发送方）
 * Created by zhangdroid on 2017/6/29.
 */
public class VoiceMessageRightProvider implements ItemViewProvider<EMMessage> {
    private Context mContext;
    private List<EMMessage> messageList;


    public VoiceMessageRightProvider(Context context,List<EMMessage> messageList) {
        this.mContext = context;
        this.messageList = messageList;
    }

    @Override
    public int getItemViewLayoutResId() {
        return R.layout.item_chat_voice_right;
    }

    @Override
    public boolean isViewType(EMMessage item, int position) {
        return (item.getType() == EMMessage.Type.VOICE && item.direct() == EMMessage.Direct.SEND);
    }

    @Override
    public void convert(final EMMessage emMessage, int position, final RecyclerViewHolder holder) {
        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().transform(new CropCircleTransformation(mContext)).placeHolder(Util.getDefaultImageCircle())
                .error(Util.getDefaultImageCircle()).url(UserPreference.getSmallImage()).imageView((ImageView) holder.getView(R.id.item_chat_voice_avatar_right)).build());
        final LinearLayout llRight = (LinearLayout) holder.getView(R.id.item_chat_voice_right_ll);
        if (null != emMessage) {
            if (position > 0) {
                long msgTime = messageList.get(position - 1).getMsgTime();
                if ((emMessage.getMsgTime()-msgTime) / (1000 * 60) > 5) {
                    holder.getView(R.id.item_chat_voice_time_right).setVisibility(View.VISIBLE);
                    holder.setText(R.id.item_chat_voice_time_right, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
                } else {
                    holder.getView(R.id.item_chat_voice_time_right).setVisibility(View.GONE);
                }
            }else{
                holder.getView(R.id.item_chat_voice_time_right).setVisibility(View.VISIBLE);
                holder.setText(R.id.item_chat_voice_time_right, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
            }


            final EMVoiceMessageBody emVoiceMessageBody = (EMVoiceMessageBody) emMessage.getBody();
            if (null != emVoiceMessageBody) {
                // 如果语音文件本地路径不存在，则下载
                if (TextUtils.isEmpty(emVoiceMessageBody.getLocalUrl())) {
                    HyphenateHelper.getInstance().downloadAttachment(emMessage);
                }
                // 语音时间
                holder.setText(R.id.item_chat_voice_right_duration, TextUtils.concat(String.valueOf(emVoiceMessageBody.getLength()), "'").toString());
                holder.setOnClickListener(R.id.item_chat_voice_right_ll, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 播放语音动画
                        List<Drawable> list = new ArrayList<Drawable>();
//                        list.add(mContext.getDrawable(R.drawable.sound_wave_right1));
//                        list.add(mContext.getDrawable(R.drawable.sound_wave_right2));
//                        list.add(mContext.getDrawable(R.drawable.sound_wave_right3));
                        list.add(ContextCompat.getDrawable(mContext,R.drawable.sound_wave_right1));
                        list.add(ContextCompat.getDrawable(mContext,R.drawable.sound_wave_right2));
                        list.add(ContextCompat.getDrawable(mContext,R.drawable.sound_wave_right3));
                        final AnimationDrawable animationDrawable = Utils.getFrameAnim(list, true, 200);
                        holder.setImageDrawable(R.id.item_chat_voice_right, animationDrawable);
                        animationDrawable.start();
                        if (RecordUtil.getInstance().isPlaying()) {
                            RecordUtil.getInstance().stop();
                        }
                        RecordUtil.getInstance().play(emVoiceMessageBody.getLocalUrl(), new RecordUtil.OnPlayerListener() {
                            @Override
                            public void onCompleted() {
                                animationDrawable.stop();
                                holder.setImageResource(R.id.item_chat_voice_right, R.drawable.sound_wave_right3);
                            }

                            @Override
                            public void onPaused() {
                                animationDrawable.stop();
                                holder.setImageResource(R.id.item_chat_voice_right, R.drawable.sound_wave_right3);
                            }
                        });
                    }
                });
            }
        }

//        llRight.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                final CustomOperateDialog codDialog = new CustomOperateDialog(mContext)
//                        .setClickedView(llRight);
//                codDialog.setClickListener(new CustomOperateDialog.OnClickCustomButtonListener() {
//                    @Override
//                    public void onClick(String str) {
//                        if(str.equals("撤回")){
//                            if(isTime()){
//                                final NettyMessage nettyMessage = setMsgContent(emMessage.getStringAttribute("msg", ""));
//                                if (nettyMessage != null) {
//                                    sendTime = nettyMessage.getSendTime();
//                                }else {
//                                    sendTime=tv_right.getText()+"";
//                                }
//                                holder.setVisibility(R.id.item_chat_text_revocation,true);
//                                holder.setText(R.id.item_chat_text_revocation,mContext.getString(R.string.withdraw_msg));
//                                holder.setVisibility(R.id.item_chat_text_avatar_right,false);
//                                holder.setVisibility(R.id.item_chat_tv_right,false);
//                                holder.setVisibility(R.id.item_chat_ll_right_send_gifts,false);
//                                UserPreference.nativeWithdraw(emMessage.getMsgId());
//                                codDialog.dismiss();
//                                HyphenateHelper.getInstance().sendTextMessage(mAccount, sendTime+ C.withdraw_msg,true,
//                                        new HyphenateHelper.OnMessageSendListener() {
//                                            @Override
//                                            public void onSuccess(EMMessage emMessage) {
//                                                if(nettyMessage!=null){
//                                                    HuanXinUser user = new HuanXinUser(nettyMessage.getSendUserId()+"", nettyMessage.getSendUserName(),nettyMessage.getSendUserIcon(), mAccount, "1", mContext.getString(R.string.withdraw_msg), 0, String.valueOf(System.currentTimeMillis()), 1);
//                                                    DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
//                                                }
//                                            }
//
//                                            @Override
//                                            public void onError() {
//                                            }
//                                        });
//                            }else {
//                                codDialog.dismiss();
//                                ToastUtil.showShortToast(mContext, "消息已经超过两分钟，不能撤回");
//                            }
//                        }else if(str.equals("删除")){
//                            holder.setVisibility(R.id.item_chat_text_revocation,true);
//                            holder.setText(R.id.item_chat_text_revocation,"你删除了一条消息");
//                            holder.setVisibility(R.id.item_chat_text_avatar_right,false);
//                            holder.setVisibility(R.id.item_chat_tv_right,false);
//                            holder.setVisibility(R.id.item_chat_ll_right_send_gifts,false);
//                            UserPreference.setDeleteMsg(emMessage.getMsgId());
//                            codDialog.dismiss();
//                        }else if(str.equals("取消")){
//                            codDialog.dismiss();
//                        }else if(str.equals("复制")){
//                            ClipboardManager cm = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
//                            // 将文本内容放到系统剪贴板里。
//                            cm.setText(tv_right.getText());
//                            ToastUtil.showShortToast(mContext, "复制成功");
//                        }
//                    }
//                });
//                codDialog.show();
//                return false;
//            }
//
//            private boolean isTime() {
//                long l = Util.getCurrentTime() - emMessage.getMsgTime();
//                if(l<1000*60*2){
//                    return true;
//                }
////                            return false;
//                return true;
//            }
//        });
    }

}
