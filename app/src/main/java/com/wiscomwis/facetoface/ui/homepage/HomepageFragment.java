package com.wiscomwis.facetoface.ui.homepage;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseTopBarFragment;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.FollowEvent;
import com.wiscomwis.facetoface.parcelable.ListParcelable;
import com.wiscomwis.facetoface.ui.charmandrankinglist.CharmandAndDrankingListActivity;
import com.wiscomwis.facetoface.ui.homepage.contract.HomepageContract;
import com.wiscomwis.facetoface.ui.homepage.presenter.HomepagePresenter;
import com.wiscomwis.library.util.LaunchHelper;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 首页
 * Created by zhangdroid on 2017/5/23.
 */
public class HomepageFragment extends BaseTopBarFragment implements HomepageContract.IView, View.OnClickListener {
    @BindView(R.id.homepage_framelayout_container)
    FrameLayout linear_container;
    @BindView(R.id.homepage_rl_search)
    RelativeLayout rl_search;
    @BindView(R.id.homepage_iv_remen)
    ImageView iv_remen;
    @BindView(R.id.homepage_iv_tuijian)
    ImageView iv_tuijian;
    @BindView(R.id.homepage_tv_remen)
    TextView tv_remmen;
    @BindView(R.id.homepage_tv_tuijian)
    TextView tv_tuijian;
    @BindView(R.id.homepage_rl_toolbar_selected)
    RelativeLayout rl_toobar_selected;
    @BindView(R.id.homepage_tv_search)
    TextView tv_search;
    @BindView(R.id.homepage_rl_chanking)
    RelativeLayout rl_chanking;
    @BindView(R.id.homepage_rl_guide)
    RelativeLayout rl_guide;
    @BindView(R.id.homepage_ll_guide_right)
    LinearLayout ll_guide_right;
    @BindView(R.id.homepage_ll_guide_left)
    LinearLayout ll_gudie_left;
    private HomepagePresenter mHomepagePresenter;
    private List<Fragment> list_fragment = new ArrayList<>();
    ListFragment listFragment;
    ListFragment2 listFragment2;
    private int current = 0;
    private long lastClickTime = 0;
    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_homepage;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.title_homepage);
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void initViews() {
        if (UserPreference.isAnchor() || !UserPreference.isMale()) {
            tv_search.setVisibility(View.VISIBLE);
            rl_toobar_selected.setVisibility(View.GONE);
            tv_remmen.setVisibility(View.GONE);
            tv_tuijian.setVisibility(View.GONE);
        }
        mHomepagePresenter = new HomepagePresenter(this);
        if (UserPreference.isAnchor() || !UserPreference.isMale()) {
            listFragment = ListFragment.newInstance(new ListParcelable(C.homepage.TYPE_AUTHOR_SEE_MAN));
        } else {
            listFragment = ListFragment.newInstance(new ListParcelable(C.homepage.TYPE_AUTHOR_REMMEND));
        }
        listFragment2 = ListFragment2.newInstance(new ListParcelable(C.homepage.TYPE_GODDESS));
        list_fragment.add(listFragment);
        list_fragment.add(listFragment2);
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.homepage_framelayout_container, listFragment);
        transaction.add(R.id.homepage_framelayout_container, listFragment2);
//        transaction.commit();
        transaction.show(listFragment).hide(listFragment2).commit();
    }

    @Override
    protected void setListeners() {
        tv_remmen.setOnClickListener(this);
        tv_tuijian.setOnClickListener(this);
        rl_chanking.setOnClickListener(this);
        rl_search.setOnClickListener(this);
        ll_guide_right.setOnClickListener(this);
        ll_gudie_left.setOnClickListener(this);
    }

    private void switchFragment(int position) {
        FragmentTransaction transaction = getChildFragmentManager()
                .beginTransaction();
        Fragment targetFragment = list_fragment.get(position);
        Fragment currentFragment = list_fragment.get(current);

        if (targetFragment.isAdded()) {
            transaction.show(targetFragment).hide(currentFragment).commit();
        } else {
            transaction.add(R.id.homepage_framelayout_container, targetFragment).hide(currentFragment).commit();
        }
        current = position;
    }


    @Override
    protected void loadData() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public FragmentManager getManager() {
        return getChildFragmentManager();
    }

    @Override
    public void setAdapter(PagerAdapter pagerAdapter) {
    }

    @Override
    public void showTip(String msg) {
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.homepage_tv_remen:
                if (current != 0) {
                    iv_tuijian.setVisibility(View.GONE);
                    iv_remen.setVisibility(View.VISIBLE);
                    switchFragment(0);
                    rl_search.setVisibility(View.VISIBLE);
                }
                rl_guide.setVisibility(View.GONE);
                break;
            case R.id.homepage_tv_tuijian:
                if (current != 1) {
                    iv_remen.setVisibility(View.GONE);
                    iv_tuijian.setVisibility(View.VISIBLE);
                    switchFragment(1);
                    rl_search.setVisibility(View.GONE);
                }
                if (UserPreference.isGuided() && UserPreference.isMale()) {
                    rl_guide.setVisibility(View.VISIBLE);
                    UserPreference.guidedModification();
                }
                break;
            case R.id.homepage_rl_chanking:
                LaunchHelper.getInstance().launch(mContext, CharmandAndDrankingListActivity.class);
                break;
            case R.id.homepage_ll_guide_right:
                rl_guide.setVisibility(View.GONE);
                break;
            case R.id.homepage_ll_guide_left:
                ll_guide_right.setVisibility(View.VISIBLE);
                ll_gudie_left.setVisibility(View.GONE);
                break;
            case R.id.homepage_rl_search:
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    mHomepagePresenter.isVipUser();
                }
                break;
        }
    }

    @Subscribe
    public void onEvent(FollowEvent event) {
        if (UserPreference.isMale()) {
            if (current != 1) {
                iv_remen.setVisibility(View.GONE);
                iv_tuijian.setVisibility(View.VISIBLE);
                rl_search.setVisibility(View.GONE);
                switchFragment(1);
            }
        }
    }
}
