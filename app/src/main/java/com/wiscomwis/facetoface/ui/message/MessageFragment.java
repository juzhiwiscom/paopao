package com.wiscomwis.facetoface.ui.message;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.util.ArraySet;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragment;
import com.wiscomwis.facetoface.common.HyphenateHelper;
import com.wiscomwis.facetoface.data.model.HuanXinUser;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.event.MessageArrive;
import com.wiscomwis.facetoface.event.UnReadMsgEvent;
import com.wiscomwis.facetoface.event.UnreadMsgChangedEvent;
import com.wiscomwis.facetoface.ui.message.adapter.ChatHistoryAdapter;
import com.wiscomwis.facetoface.ui.message.adapter.NewRecyclerAdapter;
import com.wiscomwis.facetoface.ui.message.contract.ChatHistoryContract;
import com.wiscomwis.facetoface.ui.message.presenter.ChatHistoryPresenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.Set;

import butterknife.BindView;

/**
 * 消息
 * Created by zhangdroid on 2017/5/23.
 */
public class MessageFragment extends BaseFragment implements ChatHistoryContract.IView, View.OnClickListener {

    @BindView(R.id.tv_select_all)
    TextView tv_select_all;
    @BindView(R.id.tv_edit)
    TextView tv_edit;
    @BindView(R.id.chat_history_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.tv_delete)
    TextView tv_delete;
    @BindView(R.id.tv_read)
    TextView tv_read;
    @BindView(R.id.ll_delete_read)
    LinearLayout ll_delete_read;

    private ChatHistoryPresenter mChatHistoryPresenter;
    // 初次进入时自动显示刷新，此时不调用刷新
    private boolean mIsFirstLoad;
    private boolean isAllSelected;
    private Set<String> checkSet = new ArraySet<>();
    NewRecyclerAdapter mAdapter;
    private boolean fromUnReadClick=false;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_message;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected View getNoticeView() {
        return mRecyclerView;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mChatHistoryPresenter = new ChatHistoryPresenter(this);
        mChatHistoryPresenter.start();

    }

    @Override
    protected void setListeners() {
        tv_edit.setOnClickListener(this);
        tv_select_all.setOnClickListener(this);
        tv_delete.setOnClickListener(this);
        tv_read.setOnClickListener(this);
        mAdapter.setOnSelectStateListener(new NewRecyclerAdapter.OnSelectStateListener() {
            @Override
            public void addHxId(String hxid) {
                checkSet.add(hxid);
                getDeleteState();
                if (checkSet.size() == mChatHistoryPresenter.getAllAcount().size()) {
                    tv_select_all.setText(getString(R.string.cancel_all));
                }
            }

            @Override
            public void removeHxId(String hxid) {
                if (checkSet.contains(hxid)) {
                    checkSet.remove(hxid);
                    getDeleteState();
                    if (tv_select_all.getText().toString().trim().equals(getString(R.string.cancel_all)))
                        tv_select_all.setText(getString(R.string.select_all));
                }
            }
        });
    }

    @Override
    protected void loadData() {
        mChatHistoryPresenter.refresh();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_edit://编辑
                if (ll_delete_read.getVisibility() == View.VISIBLE) {
                    tv_edit.setText(mContext.getString(R.string.edit));
                    ll_delete_read.setVisibility(View.GONE);
                    tv_select_all.setVisibility(View.GONE);
                    if (mAdapter != null)
                        mAdapter.setEditable(false);
                } else {
                    tv_edit.setText(mContext.getString(R.string.cancel));
                    ll_delete_read.setVisibility(View.VISIBLE);
                    tv_select_all.setVisibility(View.VISIBLE);
                    if (mAdapter != null)
                        mAdapter.setEditable(true);
                }
                break;
            case R.id.tv_select_all://全选
                //正选和反选
                if (mAdapter == null) return;
                List<HuanXinUser> allAcount = mChatHistoryPresenter.getAllAcount();
                if (allAcount == null || allAcount.size() <= 1) {
                    return;
                }
                if (checkSet.size() == allAcount.size() - 1) {
                    isAllSelected = true;
                } else {
                    isAllSelected = false;
                }
                if (isAllSelected) {//已全选
                    for (HuanXinUser user : allAcount) {
                        user.setSelect(false);
                        if (checkSet.contains(user.getHxId()))
                            checkSet.remove(user.getHxId());
                    }
                    tv_select_all.setText(getString(R.string.select_all));
                } else {//未全选
                    for (HuanXinUser user : allAcount) {
                        if (!user.getHxId().equals("10000")) {
                            user.setSelect(true);
                            checkSet.add(user.getHxId());
                        }
                    }
                    tv_select_all.setText(getString(R.string.cancel_all));
                }
                mAdapter.notifyDataSetChanged();
                getDeleteState();
                break;
            case R.id.tv_delete://删除
                if (checkSet != null) {
                    for (String id : checkSet) {
                        try {
                            HuanXinUser user = DbModle.getInstance().getUserAccountDao().getAccountByHyID(id);
                            HyphenateHelper.getInstance().clearCoversation(user.getAccount());
                            DbModle.getInstance().getUserAccountDao().setMsgNum(user);//把消息数量至为空
                            DbModle.getInstance().getUserAccountDao().deleteID(id);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
                    checkSet.clear();
                    getDeleteState();
                    handler.sendEmptyMessage(2);
                }
                break;
            case R.id.tv_read://全部设为已读
                if (mAdapter == null) return;
                List<HuanXinUser> allAcount1 = mChatHistoryPresenter.getAllAcount();
                if (allAcount1 == null || allAcount1.size() == 0) {
                    return;
                }
                for (HuanXinUser user : allAcount1) {
                    DbModle.getInstance().getUserAccountDao().setMsgNum(user);//把消息数量至为空
                }
                EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
                checkSet.clear();
                getDeleteState();
                handler.sendEmptyMessage(2);
                break;
        }
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChatHistoryPresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                mChatHistoryPresenter.refresh();
            }
        });
    }

    @Override
    public void hideRefresh(int delaySeconds) {
    }

    @Override
    public void showLoadMore() {

    }

    @Override
    public void hideLoadMore() {

    }

    @Override
    public void showNoMore() {

    }

    @Override
    public ChatHistoryAdapter getChatHistoryAdapter() {
        return null;
    }

    @Override
    public void setAdapter(NewRecyclerAdapter adapter) {
        //没有添加头布局的
        mAdapter = adapter;
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        mChatHistoryPresenter.refresh();
        if (ll_delete_read.getVisibility() == View.VISIBLE) {
            tv_select_all.setText(getString(R.string.select_all));
            if (checkSet != null && checkSet.size() != 0) {
                checkSet.clear();
                getDeleteState();
            }
        }
    }

    @Override
    public void onHiddenChanged(boolean hidd) {
        if(!hidd&&fromUnReadClick){//当fragment从隐藏到出现的时候
//            通过每个页面上方的通知栏进入的，刷新消息页
            fromUnReadClick=false;
            mChatHistoryPresenter.start();
            mChatHistoryPresenter.refresh();
        }
    }

    @Subscribe
    public void onEvent(UnReadMsgEvent event) {
       fromUnReadClick=true;
    }


    @Subscribe
    public void onEvent(MessageArrive arrive) {
        handler.sendEmptyMessage(1);
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            mChatHistoryPresenter.refresh();
        }
    };

    private void getDeleteState() {
        if (checkSet.size() != 0) {
            tv_delete.setClickable(true);
            tv_delete.setBackgroundResource(R.color.main_color);
        } else {
            tv_delete.setClickable(false);
            tv_delete.setBackgroundResource(R.color.bg_text_d4);
        }
    }
}
