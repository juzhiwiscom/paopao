package com.wiscomwis.facetoface.ui.chat;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;

import com.wiscomwis.facetoface.common.GetImageCacheAsyncTask;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.library.adapter.MultiTypeRecyclerViewAdapter;

/**
 * 聊天列表适配器
 * Created by zhangdroid on 2017/6/28.
 */
public class ChatAdapter extends MultiTypeRecyclerViewAdapter {
    private FragmentManager fragmentManager;
    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }
    public ChatAdapter(Context context, String url,String account,String nickname,String uid,FragmentManager fragmentManager ) {
        super(context);
        String cacheIg = UserPreference.getCacheIg(url);
        if(TextUtils.isEmpty(cacheIg)) {
            new GetImageCacheAsyncTask(mContext).execute(url);
        }
        // 文本消息
        addItemViewProvider(new TextMessageLeftProvider(context, url,fragmentManager,getAdapterDataList(),account));
        addItemViewProvider(new TextMessageRightProvider(context,getAdapterDataList(),account,nickname,nickname,url));
        // 语音消息
        addItemViewProvider(new VoiceMessageLeftProvider(context, url,getAdapterDataList()));
        addItemViewProvider(new VoiceMessageRightProvider(context,getAdapterDataList()));
        // 图片消息
        addItemViewProvider(new ImageMessageLeftProvider(context, url,getAdapterDataList()));
        addItemViewProvider(new ImageMessageRightProvider(context,getAdapterDataList()));
    }

}
