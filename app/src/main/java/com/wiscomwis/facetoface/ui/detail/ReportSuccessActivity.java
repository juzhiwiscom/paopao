package com.wiscomwis.facetoface.ui.detail;

import android.os.Parcelable;
import android.view.View;
import android.widget.RelativeLayout;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragmentActivity;
import com.wiscomwis.library.net.NetUtil;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/10/25.
 */

public class ReportSuccessActivity extends BaseFragmentActivity {
    @BindView(R.id.report_success_activity_rl_back)
    RelativeLayout rl_back;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_report_success;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }
}
