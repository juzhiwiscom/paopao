package com.wiscomwis.facetoface.ui.detail.contract;

import android.support.v4.app.FragmentManager;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;

/**
 * Created by Administrator on 2017/6/14.
 */
public interface ReportContract {

    interface IView extends BaseView {
        void showLoading();

        void dismissLoading();

        void showNetworkError();

        FragmentManager obtainFragmentManager();

    }

    interface IPresenter extends BasePresenter {
        void startReport(String userId,String resonCode);
    }

}
