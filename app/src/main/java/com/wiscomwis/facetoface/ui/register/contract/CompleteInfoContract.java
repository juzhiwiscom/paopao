package com.wiscomwis.facetoface.ui.register.contract;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;

import java.io.File;

/**
 * Created by zhangdroid on 2017/5/26.
 */
public interface CompleteInfoContract {

    interface IView extends BaseView {
        void showLoading();
        void dismissLoading();
        void showNetworkError();
        String getAge();
        String getNickname();
        void setUserAvator(String url);
        void canOnClick();
    }

    interface IPresenter extends BasePresenter {
           void completeInfo();
           void upLoadAvator(File file, boolean flag);
           void loadInfoData();
    }

}
