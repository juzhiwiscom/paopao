package com.wiscomwis.facetoface.ui.pay.activity;

import android.content.Intent;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragmentActivity;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.AlipayInfo;
import com.wiscomwis.facetoface.data.model.MyInfo;
import com.wiscomwis.facetoface.data.model.UserDetail;
import com.wiscomwis.facetoface.data.model.WeChatInfo;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.PaySuccessEvent;
import com.wiscomwis.facetoface.event.SingleLoginFinishEvent;
import com.wiscomwis.facetoface.parcelable.PayInfoParcelable;
import com.wiscomwis.facetoface.ui.dialog.DialogUtil;
import com.wiscomwis.facetoface.ui.dialog.OnDoubleDialogClickListener;
import com.wiscomwis.facetoface.ui.pay.alipay.AlipayHelper;
import com.wiscomwis.facetoface.wxapi.WXPayEntryActivity;
import com.wiscomwis.library.dialog.LoadingDialog;
import com.wiscomwis.library.net.NetUtil;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/8/30.
 */

public class PayActivity extends BaseFragmentActivity implements View.OnClickListener {
    @BindView(R.id.pay_activity_rl_alipay)
    RelativeLayout rl_alipay;
    @BindView(R.id.pay_activity_rl_wx_pay)
    RelativeLayout rl_wxpay;
    @BindView(R.id.pay_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.pay_activity_tv_purchase_product)
    TextView tv_service;
    @BindView(R.id.pay_activity_tv_purchase_money)
    TextView tv_money;
    private String payId="";
    private PayInfoParcelable payInfoParcelable;
    private boolean paySuccess=false;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_pay;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        payInfoParcelable= (PayInfoParcelable) parcelable;
        payId=payInfoParcelable.serviceId;
        tv_money.setText(payInfoParcelable.price);
        if(payInfoParcelable.type==2){
            tv_service.setText(getString(R.string.buy)+payInfoParcelable.serviceName+"VIP");
        }else if(payInfoParcelable.type==1){//钻石
            tv_service.setText(getString(R.string.buy)+payInfoParcelable.serviceName+getString(R.string.dialog_unit_ask_gift));
        }else if(payInfoParcelable.type==3){//钥匙
            tv_service.setText(getString(R.string.buy)+payInfoParcelable.serviceName+getString(R.string.key));
        }
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {

    }
    @Override
    protected void setListeners() {
        rl_alipay.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        rl_wxpay.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    protected void onResume() {
        super.onResume();
//        getMyInfo();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pay_activity_rl_alipay:
                LoadingDialog.show(getSupportFragmentManager());
                ApiManager.alipayOrderInfo(UserPreference.getId(), payId, new IGetDataListener<AlipayInfo>() {
                    @Override
                    public void onResult(AlipayInfo baseModel, boolean isEmpty) {
                        LoadingDialog.hide();
                        if(baseModel!=null){
//                                EnvUtils.setEnv(EnvUtils.EnvEnum.SANDBOX);//沙盒测试 上线时关闭
                            AlipayHelper.getInstance().callAlipayApi(mContext,baseModel.getAliPayOrder(),payInfoParcelable.serviceName,payInfoParcelable.price,payInfoParcelable.type,payInfoParcelable.paySource);
                        }
                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {
                        LoadingDialog.hide();
                    }
                });
                break;
            case R.id.pay_activity_rl_wx_pay:
                LoadingDialog.show(getSupportFragmentManager());
                ApiManager.wxOrderInfo(UserPreference.getId(), payId, new IGetDataListener<WeChatInfo>() {
                    @Override
                    public void onResult(WeChatInfo weChatInfo, boolean isEmpty) {
                        LoadingDialog.hide();
                        if (weChatInfo!=null) {
                            Intent intent=new Intent(PayActivity.this, WXPayEntryActivity.class);
                            intent.putExtra("type",payInfoParcelable.type);
                            intent.putExtra("serviceName",payInfoParcelable.serviceName);
                            intent.putExtra("price",payInfoParcelable.price);
                            intent.putExtra("orderInf",weChatInfo.getWeixinPayOrder());
                            intent.putExtra("paySource",payInfoParcelable.paySource);
                            PayActivity.this.startActivity(intent);
                        }
                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {
                        LoadingDialog.hide();
                    }});
                break;
            case R.id.pay_activity_rl_back:
                if(!paySuccess){
                    interDialog();
                }else {
                    finish();
                }
                break;
        }
    }

    private void interDialog() {
        DialogUtil.showPayDoubleBtnDialog(getSupportFragmentManager(), "确认取消支付", "您的订单在5分钟内未支付将被取消，请尽快完成支付", "继续支付", "确认离开", false, new OnDoubleDialogClickListener() {
            @Override
            public void onPositiveClick(View view) {

            }

            @Override
            public void onNegativeClick(View view) {
                finish();
            }
        });
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event){
        finish();//单点登录销毁的activity
    }
    public void getMyInfo() {
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if(myInfo!=null){
                    UserDetail userDetail = myInfo.getUserDetail();
                    if(userDetail!=null){
                        String vipDays = userDetail.getVipDays();
                        if(!TextUtils.isEmpty(vipDays)&&vipDays.length()>0){
                            int i = Integer.parseInt(vipDays);
                            if (i>0||i==-1) {
//                               isVip=true;
                            }
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }
    //重写onKeyDown方法,对按键(不一定是返回按键)监听
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {//当返回按键被按下
            if(!paySuccess){
                interDialog();
            }else {
                finish();
            }
        }
        return false;
    }

    public void onEvent(PaySuccessEvent event){
        paySuccess=true;
    }


}
