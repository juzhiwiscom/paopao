package com.wiscomwis.facetoface.ui.dialog.adapter;

import android.content.Context;
import android.widget.RelativeLayout;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.data.model.PayDict;
import com.wiscomwis.library.adapter.CommonRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;

import java.util.List;

/**
 * Created by WangYong on 2017/9/15.
 */

public class DialogKeyAndDiamondsAdapter extends CommonRecyclerViewAdapter<PayDict> {
    // 当前选中的Item索引
    private int mSelectedPosition;
    private boolean isDiamonds=false;
    public void setIsDiamonds(){
        this.isDiamonds=true;
    }
    public DialogKeyAndDiamondsAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public DialogKeyAndDiamondsAdapter(Context context, int layoutResId, List<PayDict> dataList) {
        super(context, layoutResId, dataList);
    }

    @Override
    public void convert(PayDict payDict, int position, RecyclerViewHolder holder) {
        if(isDiamonds){
            holder.setText(R.id.dialog_purchase_diamonds_item_tv_diamonds, payDict.getServiceName());
            holder.setText(R.id.dialog_purchase_diamonds_item_tv_money,  payDict.getPrice()+"元");
            RelativeLayout rl_diamonds_bg = (RelativeLayout) holder.getView(R.id.dialog_purchase_diamonds_item_rl_bg);
            rl_diamonds_bg.setBackgroundResource(R.drawable.purchase_diamond_un_selected);
            if(mSelectedPosition==position){
                rl_diamonds_bg.setBackgroundResource(R.drawable.purchase_diamond_selected);
            }
        }
    }
    public void setSelectedPosition(int selectedPosition) {
        this.mSelectedPosition = selectedPosition;
    }
}
