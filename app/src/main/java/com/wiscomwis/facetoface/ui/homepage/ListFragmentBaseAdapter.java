package com.wiscomwis.facetoface.ui.homepage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.common.VideoHelper;
import com.wiscomwis.facetoface.customload.BaseAdapterHelper;
import com.wiscomwis.facetoface.customload.CommonAbsListViewAdapter;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.SearchUser;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.SwitchPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.parcelable.ChatParcelable;
import com.wiscomwis.facetoface.parcelable.PayParcelable;
import com.wiscomwis.facetoface.parcelable.VideoInviteParcelable;
import com.wiscomwis.facetoface.ui.chat.ChatActivity;
import com.wiscomwis.facetoface.ui.pay.activity.RechargeActivity;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.ToastUtil;
import com.wiscomwis.library.util.Utils;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/8/18.
 */

public class ListFragmentBaseAdapter extends CommonAbsListViewAdapter<SearchUser> {
    private Context mContext;
    private FragmentManager mFragmentManager;
    private long prelongTim = 0;//定义上一次单击的时间

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.mFragmentManager = fragmentManager;
    }

    public void removeItem(int index) {
        List<SearchUser> list = getDataList();
        if (!Utils.isListEmpty(list)) {
            list.remove(index);
            notifyDataSetChanged();
        }
    }

    public ListFragmentBaseAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
        this.mContext = context;
    }
    @Override
    protected void convert(int position, BaseAdapterHelper helper, final SearchUser bean) {
        if (bean != null) {
            if(position==0){
                setMsgFlag(bean.getUesrId()+"");
            }
            ImageView iv_avatar = (ImageView) helper.getView(R.id.list_fragment_item_iv);
            ViewGroup.LayoutParams params = iv_avatar.getLayoutParams();
            params.width =ViewGroup.LayoutParams.MATCH_PARENT;

            int screenHeight = DataPreference.getScreenHeight();
//            Log.e("AAAAAAA", "convert: "+screenHeight);
            if(screenHeight==2416){
                params.height=520;
            }
            if(screenHeight==2560){//小米3x

            }
            if(screenHeight==4709){//谷歌
                params.height= 900;
            }
            if(screenHeight==5040){

            }
            if(screenHeight==5760){//小米4
                params.height= 900;
            }
            if(screenHeight<2416){//华为
                params.height=520;
            }else if(screenHeight>2416&&screenHeight<2560){
                params.height=550;
            }else if(screenHeight>2560&&screenHeight<4709){
                params.height=600;
            }else if(screenHeight>4709&&screenHeight<5040){

            }else if(screenHeight>5040&&screenHeight<5760){

            }else if(screenHeight>5760){

            }
            iv_avatar.setLayoutParams(params);
            String cacheIg = UserPreference.getCacheIg(bean.getIconUrlMiddle());
            if(!TextUtils.isEmpty(cacheIg)){
                Bitmap bitmap = BitmapFactory.decodeFile(cacheIg);
                iv_avatar.setImageBitmap(bitmap);
            }else {
//                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(bean.getIconUrlMiddle())
//                        .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_avatar).build());
                getImage(bean, 0, iv_avatar);
            }
            TextView tv_nickname = (TextView) helper.getView(R.id.list_fragment_item_tv_nickname);
            tv_nickname.setText(bean.getNickName());
            TextView tv_age_state = (TextView) helper.getView(R.id.list_fragment_item_tv_age_and_state);
            int status = bean.getStatus();
            int onlineStatus = bean.getOnlineStatus();
            if (onlineStatus == 3) {
                tv_age_state.setText(bean.getAge() + mContext.getString(R.string.edit_info_years_old) + "  " + mContext.getString(R.string.not_online));
            } else if(onlineStatus == 2){
                tv_age_state.setText(bean.getAge() + mContext.getString(R.string.edit_info_years_old) + "  " + mContext.getString(R.string.online_status_chat));
            }else{
                tv_age_state.setText(bean.getAge() + mContext.getString(R.string.edit_info_years_old) + "  " + mContext.getString(R.string.edit_info_status));
            }
//            if (status == 3) {
//                tv_age_state.setText(bean.getAge() + mContext.getString(R.string.edit_info_years_old) + "  " + mContext.getString(R.string.message_state_nodistrub));
//            }
            TextView tv_distance = (TextView) helper.getView(R.id.list_fragment_item_tv_distance);
            tv_distance.setText(bean.getDistance()+"km");

//            tv_age_state.setText(bean.get);
            //打招呼
            ImageView iv_sendMsg = (ImageView) helper.getView(R.id.list_fragment_item_iv_sendmessage);
            ImageView iv_sendgifts = (ImageView) helper.getView(R.id.list_fragment_item_iv_sendgifts);
            ImageView iv_sendVideo = (ImageView) helper.getView(R.id.list_fragment_item_iv_sendvideo);
            ImageView iv_sendVoice = (ImageView) helper.getView(R.id.list_fragment_item_iv_sendvoice);
            prelongTim = 0;
            iv_sendMsg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (prelongTim == 0) {//第一次单击，初始化为本次单击的时间
                        prelongTim = (new Date()).getTime();
                        goToWriteMsg(bean);
                    } else {
                        long curTime = (new Date()).getTime();//本地单击的时间
                        if (curTime - prelongTim > 1000) {
                            goToWriteMsg(bean);
                        }
                        prelongTim = curTime;//当前单击事件变为上次时间
                    }


                }
            });

            iv_sendVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (prelongTim == 0) {//第一次单击，初始化为本次单击的时间
                        prelongTim = (new Date()).getTime();
                        if (SwitchPreference.getAllPay() == -1) {
                            VideoHelper.startVideoInvite(new VideoInviteParcelable(false, bean.getUesrId(), bean.getAccount()
                                    , bean.getNickName(), bean.getIconUrlMininum(), 0, 0), mContext, "1");
                        } else {
                            goToViewPage(bean, 0);
                        }
                    } else {
                        long curTime = (new Date()).getTime();//本地单击的时间
                        if (curTime - prelongTim > 1000) {
                            if (SwitchPreference.getAllPay() == -1) {
                                VideoHelper.startVideoInvite(new VideoInviteParcelable(false, bean.getUesrId(), bean.getAccount()
                                        , bean.getNickName(), bean.getIconUrlMininum(), 0, 0), mContext, "1");
                            } else {
                                goToViewPage(bean, 0);
                            }
                        }
                        prelongTim = curTime;//当前单击事件变为上次时间
                    }

                }
            });

            iv_sendgifts.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (prelongTim == 0) {//第一次单击，初始化为本次单击的时间
                        prelongTim = (new Date()).getTime();
                        //发礼物
//                        CustomDialogAboutOther.giveGiftShow(mContext, String.valueOf(bean.getUesrId()), bean.getAccount(), bean.getIconUrlMininum(), bean.getNickName(), 1, false);
                    } else {
                        long curTime = (new Date()).getTime();//本地单击的时间
                        if (curTime - prelongTim > 1000) {
                            //发礼物
//                            CustomDialogAboutOther.giveGiftShow(mContext, String.valueOf(bean.getUesrId()), bean.getAccount(), bean.getIconUrlMininum(), bean.getNickName(), 1, false);
                        }
                        prelongTim = curTime;//当前单击事件变为上次时间
                    }

                }
            });

            iv_sendVoice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (prelongTim == 0) {//第一次单击，初始化为本次单击的时间
                        prelongTim = (new Date()).getTime();
                        if (SwitchPreference.getAllPay() == -1) {
                            VideoHelper.startVideoInvite(new VideoInviteParcelable(false, bean.getUesrId(), bean.getAccount()
                                    , bean.getNickName(), bean.getIconUrlMininum(), 1, 0), mContext, "1");
                        } else {
                            goToViewPage(bean, 1);
                        }
                    } else {
                        long curTime = (new Date()).getTime();//本地单击的时间
                        if (curTime - prelongTim > 1000) {
                            if (SwitchPreference.getAllPay() == -1) {
                                VideoHelper.startVideoInvite(new VideoInviteParcelable(false, bean.getUesrId(), bean.getAccount()
                                        , bean.getNickName(), bean.getIconUrlMininum(), 1, 0), mContext, "1");
                            } else {
                                goToViewPage(bean, 1);
                            }
                        }
                        prelongTim = curTime;//当前单击事件变为上次时间
                    }

                }
            });
        }

    }

    private void sayHello2(final SearchUser searchUser) {
        ApiManager.sayHello(String.valueOf(searchUser.getUesrId()), new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel videoStop, boolean isEmpty) {
                ToastUtil.showShortToast(mContext, mContext.getString(R.string.hello_success));
                searchUser.setIsSayHello(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    private void goToWriteMsg(final SearchUser searchUser) {
        //跳转写信的界面
        if (searchUser != null) {
            if (UserPreference.isVip() || UserPreference.isAnchor()) {
                LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(searchUser.getUesrId(),
                        searchUser.getAccount(), searchUser.getNickName(), searchUser.getIconUrlMininum(), 0));

            } else {
                LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON,0,7));

            }
        }
    }

    private void goToViewPage(final SearchUser searchUser, int type) {
        VideoHelper.startVideoInvite(new VideoInviteParcelable(false, searchUser.getUesrId(), searchUser.getAccount()
                , searchUser.getNickName(), searchUser.getIconUrlMininum(), type, 0), mContext, "1");
    }
    private static void setMsgFlag(String userId){
            ApiManager.userActivityTag(UserPreference.getId(), userId, "23", "1", new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel baseModel, boolean isEmpty) {
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                }
            });
        }
    private void getImage(final SearchUser searchUser, final int i, final ImageView imageView) {
        ImageLoaderUtil.getInstance().load(Glide.with(mContext), new ImageLoader.Builder().url(searchUser.getIconUrlMiddle())
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(imageView).build(), new ImageLoaderUtil.OnImageLoadListener() {
            @Override
            public void onCompleted() {
//                Log.e("AAAAAA","图片加载成功");
            }

            @Override
            public void onFailed() {
                Log.e("AAAAAA","图片加载失败--i="+i);
                int ii=i;
                ii++;
                if(ii<5)
                    getImage(searchUser, ii, imageView);
            }
        });
    }
}
