package com.wiscomwis.facetoface.ui.detail.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.widget.ImageView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.model.UserPhoto;
import com.wiscomwis.library.adapter.CommonRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;

public class UserAlbumPhotoAdapter extends CommonRecyclerViewAdapter<UserPhoto> {
    private FragmentManager fragmentManager;

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public UserAlbumPhotoAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    public void convert(final UserPhoto userPhoto, int position, final RecyclerViewHolder holder) {
        if (userPhoto != null) {
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(userPhoto.getFileUrlMinimum()).imageView((ImageView) holder.getView(R.id.item_photo_iv))
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).build());
        }
    }
}
