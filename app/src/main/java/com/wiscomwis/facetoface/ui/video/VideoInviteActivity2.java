package com.wiscomwis.facetoface.ui.video;

import android.content.Context;
import android.media.Ringtone;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragmentActivity;
import com.wiscomwis.facetoface.common.HyphenateHelper;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.common.VideoHelper;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.InviteEvent2;
import com.wiscomwis.facetoface.data.model.VideoStop;
import com.wiscomwis.facetoface.data.preference.DataPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.CloseRingtoneEvent;
import com.wiscomwis.facetoface.event.FinishVideoActivity2;
import com.wiscomwis.facetoface.event.FinishVideoToChatActivityEvent;
import com.wiscomwis.facetoface.parcelable.VideoInviteParcelable;
import com.wiscomwis.library.image.CropCircleTransformation;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.net.NetUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 视频邀请页面（邀请/被邀请）(群发视频/语音)
 * Created by zhangdroid on 2017/5/27.
 */
public class VideoInviteActivity2 extends BaseFragmentActivity implements View.OnClickListener {
    @BindView(R.id.video_invite_avatar)
    ImageView mIvAvatar;
    @BindView(R.id.video_invite_nickname)
    TextView mTvNickname;
    @BindView(R.id.video_invite_tip)
    TextView mTvVideoTip;
    @BindView(R.id.video_cancel)
    TextView mTvVideoCancel;
    @BindView(R.id.video_reject)
    TextView mTvVideoReject;
    @BindView(R.id.video_accept)
    TextView mTvVideoAccept;
    // 遮罩层
    @BindView(R.id.video_invite_avatar_big)
    ImageView mIvAvatarBig;
    @BindView(R.id.video_invite_mask)
    View mMaskView;
    // 相机预览
    @BindView(R.id.video_invite_preview)
    SurfaceView mSurfaceView;

    private VideoInviteParcelable mVideoInviteParcelable;
    private SurfaceHolder mSurfaceHolder;
    // 发起视频时的频道id，生成规则：邀请人guid+被邀请人guid
    private String mChannelId;
    Ringtone ringtone;
    private long lastClickTime = 0;
    private boolean flag = true;
    private boolean flag2 = false;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 3:
                    if (flag2) {
                        Util.stopMp3();
//                        if (ringtone != null && ringtone.isPlaying()) {
//                            ringtone.stop();
//                            ringtone = null;
//                        }
                    }
                    break;
                case 1:
                    if (flag) {
                        Util.playMp3(VideoInviteActivity2.this);
//                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
//                        ringtone = RingtoneManager.getRingtone(VideoInviteActivity2.this, notification);
//                        if(ringtone!=null)
//                            ringtone.play();
                        flag = false;
                        flag2 = true;
                    }
                    break;
                case 4:
                    if (flag2) {
                        Util.stopMp3();
//                        if (ringtone != null && ringtone.isPlaying()) {
//                            ringtone.stop();
//                            ringtone = null;
//                        }
                    }
                    finish();
                    break;
                case 5:
                    Toast.makeText(VideoInviteActivity2.this, "对方正在通话中，请稍等", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
    private boolean isInto=false;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_video_invite;
    }

    @Override
    protected void setWakeAndLack() {
        super.setWakeAndLack();
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mVideoInviteParcelable = (VideoInviteParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        Log.i("Main===","进入VideoInviteActivity2==initViews");
        isInto=true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isInto=false;
                EventBus.getDefault().post(new CloseRingtoneEvent(true));
            }
        },3000);
        if (null != mVideoInviteParcelable) {
            ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(mVideoInviteParcelable.imgageUrl).transform(new CropCircleTransformation(mContext))
                    .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(mIvAvatar).build());
            mTvNickname.setText(mVideoInviteParcelable.nickname);
            if (mVideoInviteParcelable.type == 1) {
                mTvVideoTip.setText(mVideoInviteParcelable.isInvited ? getString(R.string.video_invited2) : getString(R.string.video_invite2));
            } else {
                mTvVideoTip.setText(mVideoInviteParcelable.isInvited ? getString(R.string.video_invited) : getString(R.string.video_invite));
            }
            mTvVideoCancel.setVisibility(mVideoInviteParcelable.isInvited ? View.GONE : View.VISIBLE);
            mTvVideoReject.setVisibility(mVideoInviteParcelable.isInvited ? View.VISIBLE : View.GONE);
            mTvVideoAccept.setVisibility(mVideoInviteParcelable.isInvited ? View.VISIBLE : View.GONE);
            if (mVideoInviteParcelable.isInvited) {
                ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(mVideoInviteParcelable.imgageUrl)
                        .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(mIvAvatarBig).build());
                mIvAvatarBig.setVisibility(View.VISIBLE);
                mMaskView.setVisibility(View.VISIBLE);
                mSurfaceView.setVisibility(View.GONE);
                // 被邀请加入的频道id
                mChannelId = mVideoInviteParcelable.channelId;
                handler.sendEmptyMessage(1);
            } else {
                mIvAvatarBig.setVisibility(View.GONE);
                mMaskView.setVisibility(View.GONE);
                mSurfaceView.setVisibility(View.VISIBLE);
                // 生成channelId
                mChannelId = UserPreference.getId() + mVideoInviteParcelable.uId;
            }
        }
        handler.sendEmptyMessageDelayed(4, 50 * 1000);

    }

    @Override
    protected void setListeners() {
        mTvVideoCancel.setOnClickListener(this);
        mTvVideoReject.setOnClickListener(this);
        mTvVideoAccept.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.video_cancel:// 取消
                Util.stopMp3();
                isInto=false;

                break;

            case R.id.video_reject:// 拒绝
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    shake(VideoInviteActivity2.this);
                    handler.sendEmptyMessage(3);
                    setMsgFlag(mVideoInviteParcelable.uId+"","2");
                    //点击拒绝通知后台
                    clickRefuse(UserPreference.getId(),String.valueOf(mVideoInviteParcelable.uId));
                   EventBus.getDefault().post(new FinishVideoToChatActivityEvent(mVideoInviteParcelable.uId,mVideoInviteParcelable.account,mVideoInviteParcelable.nickname,mVideoInviteParcelable.imgageUrl,0));
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                            Util.stopMp3();
                            isInto=false;
//                            if (ringtone != null && ringtone.isPlaying()) {
//                                ringtone.stop();
//                                ringtone = null;
//                            }
                        }
                    },1000);
                }
                break;

            case R.id.video_accept:// 接受
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    shake(VideoInviteActivity2.this);
                    setMsgFlag(mVideoInviteParcelable.uId+"","1");
                    VideoHelper.immediatelyVideo(mChannelId, mVideoInviteParcelable.account, String.valueOf(mVideoInviteParcelable.uId), mVideoInviteParcelable.imgageUrl, mVideoInviteParcelable.type, VideoInviteActivity2.this,"2");
//                    handler.sendEmptyMessage(3);
                    isInto=false;
//                    DataPreference.saveVideoCallAccount(mVideoInviteParcelable.account);
//                    DataPreference.saveVideoCallId(String.valueOf(mVideoInviteParcelable.uId));
//                    DataPreference.saveVideoCallImageUrl(mVideoInviteParcelable.imgageUrl);
//                    DataPreference.saveVideoCallName(mVideoInviteParcelable.nickname);
                }
                break;
        }
    }

    @Override
    protected void loadData() {
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        HyphenateHelper.getInstance().isVideoInviteActivity2();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Main===","进入VideoInviteActivity2==onDestroy");
        handler.sendEmptyMessage(3);
        if(!isInto){
            Util.stopMp3();
            EventBus.getDefault().post(new CloseRingtoneEvent(true));
        }
    }

    @Subscribe
    public void onEvent(FinishVideoActivity2 event) {
        finish();
    }

    /**
     * 接通或者挂断震动一下
     *
     * @param context
     */
    public static void shake(Context context) {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(88);
    }
    @Subscribe
    public void onEvent(InviteEvent2 event){
        handler.sendEmptyMessage(5);
    }
    private void clickRefuse(String id, String guid) {
        ApiManager.videoCallRefuse(id, guid, new IGetDataListener<VideoStop>() {
            @Override
            public void onResult(VideoStop videoStop, boolean isEmpty) {
            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }
    @Override
    public boolean onKeyDown(int keyCode,KeyEvent event){
        if(keyCode==KeyEvent.KEYCODE_BACK)
            return true;//不执行父类点击事件
        return super.onKeyDown(keyCode, event);//继续执行父类其他点击事件
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("Main===","进入VideoInviteActivity2==onPause");

        DataPreference.saveNoSeeVideo(true);//显示悬浮框
        HyphenateHelper.getInstance().isNotVideoInviteActivity2();

    }

    @Override
    protected void onStop() {
        super.onStop();

//        if (ringtone != null && ringtone.isPlaying()) {
//            ringtone.stop();
//        }
    }

    private static void setMsgFlag(String guid,String tag){
        ApiManager.userActivityTag(UserPreference.getId(),guid, "27", tag, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }
}
