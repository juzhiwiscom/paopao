package com.wiscomwis.facetoface.ui.personalcenter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseTopBarActivity;
import com.wiscomwis.facetoface.customload.Withdrawal_PromptDialog;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.WithdrawBean;
import com.wiscomwis.facetoface.data.preference.BeanPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.ui.personalcenter.contract.WithdrawContract;
import com.wiscomwis.facetoface.ui.personalcenter.presenter.WithdrawPresenter;
import com.wiscomwis.library.dialog.OnDialogClickListener;
import com.wiscomwis.library.net.NetUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;

/**
 * 提现页面
 * Created by zhangdroid on 2017/5/27.
 */
public class WithdrawActivity extends BaseTopBarActivity implements View.OnClickListener, WithdrawContract.IView {
    @BindView(R.id.withdraw_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.withdraw_activity_rl_weixin_tixian)
    RelativeLayout rl_weixin_tixian;
    @BindView(R.id.withdraw_activity_rl_alipay_tixian)
    RelativeLayout rl_alipay_tixian;
    @BindView(R.id.rl_alipay_info)
    RelativeLayout rl_alipay_info;
    @BindView(R.id.rl_wechat_info)
    RelativeLayout rl_wechat_info;
    @BindView(R.id.withdraw_activity_iv_weixin)
    ImageView iv_weixin_selected;
    @BindView(R.id.withdraw_activity_iv_alipay)
    ImageView iv_alipay_selected;
    @BindView(R.id.withdraw_activity_tv_weixin_detail)
    TextView tv_weixin_detail;
    @BindView(R.id.withdraw_activity_tv_alipay_detail)
    TextView tv_alipay_detail;
    @BindView(R.id.withdraw_activity_tv_tixian_start)
    TextView tv_start_tixian;
    @BindView(R.id.withdraw_activity_et_tixian_money)
    EditText et_tixianmoney;
    @BindView(R.id.withdraw_activity_tv_tixian_detail)
    TextView tv_tixian_detail;
    private String authorName;
    private String authorAccount;
    private TvBalanceParcelable balanceParcelable;
    private boolean isSelected = true;
    private WithdrawPresenter mPresenter;
    private int isWeiXinOrAlipay = 1;//1为微信,2为支付宝
    InputMethodManager imm;
    private String payType = "3";

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_withdraw;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.person_withdraw);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        balanceParcelable = (TvBalanceParcelable) parcelable;
        if (isSelected) {
            iv_weixin_selected.setBackgroundResource(R.drawable.icon_selected);
        }
        tv_tixian_detail.setText(getString(R.string.balance) + balanceParcelable.balance + getString(R.string.withdraw_diamonds_nuit));
        et_tixianmoney.setText(String.valueOf(balanceParcelable.balance));
        et_tixianmoney.setSelection(String.valueOf(balanceParcelable.balance).length());

    }

    @Override
    protected void initViews() {
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mPresenter = new WithdrawPresenter(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(UserPreference.getAlipayName())) {
            tv_alipay_detail.setText(UserPreference.getAlipayName());
        }
        if (!TextUtils.isEmpty(UserPreference.getWechatName())) {
            tv_weixin_detail.setText(UserPreference.getWechatName());
        }
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(this);
        rl_weixin_tixian.setOnClickListener(this);
        rl_alipay_tixian.setOnClickListener(this);
        tv_start_tixian.setOnClickListener(this);
        rl_alipay_info.setOnClickListener(this);
        rl_wechat_info.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(Message message) {
        if (message.what == 1111) {
            String newMoney = (String) message.obj;
            double withdrawMoney = Double.parseDouble(newMoney) * 20;
//            double curentMoney= Double.parseDouble((String) tvWithdraw.getText())-withdrawMoney;
//            tvWithdraw.setText((int)curentMoney+"");
//            tvWithdraw.setText("5000");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.withdraw_activity_rl_back:
                finish();
                break;
            case R.id.withdraw_activity_rl_weixin_tixian:
                isSelected = true;
                setBackground();
                isWeiXinOrAlipay = 1;
                payType = "3";
                break;
            case R.id.withdraw_activity_rl_alipay_tixian:
                isSelected = false;
                setBackground();
                isWeiXinOrAlipay = 2;
                payType = "2";
                break;
            case R.id.withdraw_activity_tv_tixian_start:
                if (isWeiXinOrAlipay == 1) {//微信
                    if (TextUtils.isEmpty(UserPreference.getWechatName()) || TextUtils.isEmpty(UserPreference.getWechatAccount())) {
                        Toast.makeText(WithdrawActivity.this, getString(R.string.not_null_name), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(this, WithdrawToAlipayActivity.class);
                        intent.putExtra("isWeiXinOrAlipay", isWeiXinOrAlipay);
                        startActivity(intent);
                    } else {
                        String s = et_tixianmoney.getText().toString();
                        authorName = UserPreference.getWechatName();
                        authorAccount = UserPreference.getWechatAccount();
                        if (!TextUtils.isEmpty(s) && s.length() > 0) {
                            double i = Double.parseDouble(s);
                            if (i < 2000) {
                                tv_tixian_detail.setTextColor(Color.parseColor("#ff0000"));
                                tv_tixian_detail.setText(getString(R.string.diamonds_number_2000));
                            } else if (i > BeanPreference.getBeanCount()) {
                                tv_tixian_detail.setTextColor(Color.parseColor("#ff0000"));
                                tv_tixian_detail.setText(getString(R.string.withdraw_number_out));
                            } else {
                                if (!TextUtils.isEmpty(authorAccount) && !TextUtils.isEmpty(authorName) && !TextUtils.isEmpty(et_tixianmoney.getText().toString())) {
                                       if(i%100==0){
                                           setInfo(authorName, et_tixianmoney.getText().toString(), payType, authorAccount);
                                       }else{
                                           Toast.makeText(WithdrawActivity.this, "提现的钻石数量必须是100的整数倍", Toast.LENGTH_SHORT).show();
                                       }
                                } else {
                                    Toast.makeText(WithdrawActivity.this, getString(R.string.not_null_name), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                } else {//支付宝   //提现需要100的整数倍
                    if (TextUtils.isEmpty(UserPreference.getAlipayName()) || TextUtils.isEmpty(UserPreference.getAlipayAccount())) {
                        Toast.makeText(WithdrawActivity.this, getString(R.string.not_null_name), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(this, WithdrawToAlipayActivity.class);
                        intent.putExtra("isWeiXinOrAlipay", isWeiXinOrAlipay);
                        startActivity(intent);
                    } else {
                        String s = et_tixianmoney.getText().toString();
                        authorName = UserPreference.getAlipayName();
                        authorAccount = UserPreference.getAlipayAccount();
                        if (!TextUtils.isEmpty(s) && s.length() > 0) {
                            double i = Double.parseDouble(s);
                            if (i < 2000) {
                                tv_tixian_detail.setTextColor(Color.parseColor("#ff0000"));
                                tv_tixian_detail.setText(getString(R.string.diamonds_number_2000));
                            } else if (i > BeanPreference.getBeanCount()) {
                                tv_tixian_detail.setTextColor(Color.parseColor("#ff0000"));
                                tv_tixian_detail.setText(getString(R.string.withdraw_number_out));
                            } else {
                                if (!TextUtils.isEmpty(authorAccount) && !TextUtils.isEmpty(authorName) && !TextUtils.isEmpty(et_tixianmoney.getText().toString())) {
                                    if(i%100==0){
                                        setInfo(authorName, et_tixianmoney.getText().toString(), payType, authorAccount);
                                    }else{
                                        Toast.makeText(WithdrawActivity.this, "提现的钻石数量必须是100的整数倍", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(WithdrawActivity.this, getString(R.string.not_null_name), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                }
                break;
            case R.id.rl_alipay_info:
                Intent intent = new Intent(this, WithdrawToAlipayActivity.class);
                intent.putExtra("isWeiXinOrAlipay", 2);
                startActivity(intent);
                break;
            case R.id.rl_wechat_info:
                Intent it = new Intent(this, WithdrawToAlipayActivity.class);
                it.putExtra("isWeiXinOrAlipay", 1);
                startActivity(it);
                break;
        }
    }

    public void setBackground() {
        if (isSelected) {
            iv_weixin_selected.setBackgroundResource(R.drawable.icon_selected);
            iv_alipay_selected.setBackgroundResource(R.drawable.icon_un_selected);
        } else if (!isSelected) {
            iv_weixin_selected.setBackgroundResource(R.drawable.icon_un_selected);
            iv_alipay_selected.setBackgroundResource(R.drawable.icon_selected);
        }
    }


    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }

    @Override
    public FragmentManager getFragment() {
        return getSupportFragmentManager();
    }


    //触摸屏幕，可以让软键盘消失
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (this.getCurrentFocus() != null) {
                if (this.getCurrentFocus().getWindowToken() != null) {
                    imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        }
        return super.onTouchEvent(event);
    }

    public void setInfo(final String name, final String money, final String type, final String account) {
        Withdrawal_PromptDialog.show(getFragment(), "", name, money, account, new OnDialogClickListener() {
            @Override
            public void onNegativeClick(View view) {

            }

            @Override
            public void onPositiveClick(View view) {
                //确定按钮
                startWithdrawal(name, account, type, money);
            }
        });
    }

    private void startWithdrawal(String name, String account, String type, String money) {
        ApiManager.withdraw(money, account, name, type, new IGetDataListener<WithdrawBean>() {
            @Override
            public void onResult(WithdrawBean baseModel, boolean isEmpty) {
                Toast.makeText(mContext, mContext.getString(R.string.withdraw_success), Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                Toast.makeText(mContext, TextUtils.isEmpty(msg) ? mContext.getString(R.string.withdraw_fail) : msg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
