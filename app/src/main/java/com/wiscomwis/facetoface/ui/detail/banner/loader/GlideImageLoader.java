package com.wiscomwis.facetoface.ui.detail.banner.loader;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.wiscomwis.facetoface.common.Util;

/**
 * Created by xuzhaole on 2018/3/1.
 */

public class GlideImageLoader extends ImageLoader {
    @Override
    public void displayImage(Context context, Object path, final ImageView imageView, final OnImageLoadListener listener) {
        Glide.with(context.getApplicationContext())
                .load(path)
                .placeholder(Util.getDefaultImage())
                .listener(new RequestListener<Object, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, Object model, Target<GlideDrawable> target, boolean isFirstResource) {
//                        Log.e("AAAAAA","图片加载失败");
//                        i++;
//                        if(i<5)
//                           getImage(context, path, imageView);
                        if(listener!=null){
                            listener.onFailed();
                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, Object model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                        Log.e("AAAAAA","图片加载成功");
                        if(listener!=null){
                            listener.onCompleted();
                        }
                        return false;
                    }//这个用于监听图片是否加载完成
                }).error(Util.getDefaultImage())
                .into(imageView);
//        Glide.with(context.getApplicationContext())
//                .load(path)
//                .placeholder(Util.getDefaultImage())
//                .into(new SimpleTarget<GlideDrawable>() {
//                    @Override
//                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
//                        imageView.setImageDrawable(resource);
//                    }
//                });
    }
    /**
     * 图片加载监听器
     */
    public interface OnImageLoadListener {
        /**
         * 加载完成
         */
        void onCompleted();

        /**
         * 加载失败
         */
        void onFailed();
    }
}
