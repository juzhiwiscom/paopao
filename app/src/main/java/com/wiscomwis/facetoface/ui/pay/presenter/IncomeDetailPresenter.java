package com.wiscomwis.facetoface.ui.pay.presenter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.IncomeRecord;
import com.wiscomwis.facetoface.data.model.IncomeRecordList;
import com.wiscomwis.facetoface.data.model.SearchUser;
import com.wiscomwis.facetoface.ui.pay.adapter.IncomeDetailAdapter;
import com.wiscomwis.facetoface.ui.pay.contract.IncomeContract;
import com.wiscomwis.library.adapter.base.BaseQuickAdapter;
import com.wiscomwis.library.util.Utils;

import java.util.List;

/**
 * Created by xuzhaole on 2018/1/24.
 */

public class IncomeDetailPresenter implements IncomeContract.IPresenter {
    private IncomeContract.IView mIncomeView;
    private Context mContext;
    private IncomeDetailAdapter incomeDetailAdapter;
    private int pageNum = 1;
    private String pageSize = "20";

    public IncomeDetailPresenter(IncomeContract.IView view) {
        this.mIncomeView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void loadData(final boolean isRefresh) {
        ApiManager.getIncomeRecord(pageNum, pageSize, new IGetDataListener<IncomeRecordList>() {
            public void onResult(IncomeRecordList recordList, boolean isEmpty) {
                pageNum++;
                if (isEmpty) {
                    incomeDetailAdapter.setEmptyView(R.layout.common_empty);
                } else {
                    if (null != recordList) {
                        List<IncomeRecord> list = recordList.getIncomeFlowList();
                        if (!Utils.isListEmpty(list)) {
                            if (isRefresh) {
                                incomeDetailAdapter.setNewData(list);
                            } else {
                                if (list.size() > 0) {
                                    incomeDetailAdapter.addData(list);
                                }
                            }
                            if (list.size() < Integer.parseInt(pageSize)) {
                                incomeDetailAdapter.loadMoreEnd(isRefresh);
                            } else {
                                incomeDetailAdapter.loadMoreComplete();
                            }
                        }
                    }
                }
                mIncomeView.hideRefresh(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mIncomeView.hideRefresh(1);
                if (isNetworkError) {
                    mIncomeView.toggleShowError(false, TextUtils.isEmpty(msg) ? mContext.getString(R.string.tip_error) : msg);
                } else {
                    if (isRefresh) {
                        incomeDetailAdapter.setEnableLoadMore(true);
                    } else {
                        incomeDetailAdapter.loadMoreFail();
                    }
                }
            }
        });

    }

    @Override
    public void refresh() {
        pageNum = 1;
        loadData(true);
    }

    @Override
    public void start(RecyclerView recyclerView) {
        incomeDetailAdapter = new IncomeDetailAdapter(R.layout.item_income_record);
        mIncomeView.setAdapter(incomeDetailAdapter);
        incomeDetailAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                loadData(false);
            }
        }, recyclerView);
    }
}
