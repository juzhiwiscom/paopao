package com.wiscomwis.facetoface.ui.pay.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.data.model.PayDict;
import com.wiscomwis.facetoface.parcelable.PayInfoParcelable;
import com.wiscomwis.facetoface.ui.pay.activity.PayActivity;
import com.wiscomwis.facetoface.ui.pay.presenter.PurchaseKeyPresenter;
import com.wiscomwis.library.adapter.CommonRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.util.LaunchHelper;

import java.util.List;

/**
 * 支付项适配器
 * Created by zhangdroid on 2017/6/10.
 */
public class PayAdapter extends CommonRecyclerViewAdapter<PayDict> {
    private PurchaseKeyPresenter mRechargePresenter;
    private int paySource;

    public void setPayPresenter(PurchaseKeyPresenter rechargePresenter,int paySource) {
        this.mRechargePresenter = rechargePresenter;
        this.paySource=paySource;
    }

    public PayAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public PayAdapter(Context context, int layoutResId, List<PayDict> dataList) {
        super(context, layoutResId, dataList);
    }

    @Override
    public void convert(final PayDict payDict, final int position, RecyclerViewHolder holder) {
        if (null != payDict) {
            if (!"1".equals(payDict.getIsvalid())) {// 商品不可用
                holder.getConvertView().setVisibility(View.GONE);
                removeItem(position);
            } else {
                holder.setText(R.id.item_pay_name, payDict.getServiceName() + mContext.getString(R.string.number_keys));
                holder.setText(R.id.item_pay_purchase, TextUtils.concat("￥", String.valueOf(payDict.getPrice())).toString());
                holder.setText(R.id.item_pay_onekey_howmoney, payDict.getServiceDesc());
                holder.setOnClickListener(R.id.item_pay_purchase, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 购买
                        LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayInfoParcelable(payDict.getServiceId(), payDict.getServiceName(), 3, payDict.getPrice(),paySource));
                    }
                });
            }
        }
    }

}
