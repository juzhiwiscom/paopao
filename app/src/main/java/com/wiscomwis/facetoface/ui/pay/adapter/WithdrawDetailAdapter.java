package com.wiscomwis.facetoface.ui.pay.adapter;

import android.support.annotation.LayoutRes;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.data.model.IncomeRecord;
import com.wiscomwis.facetoface.data.model.WithdrawRecord;
import com.wiscomwis.library.adapter.base.BaseQuickAdapter;
import com.wiscomwis.library.adapter.base.BaseViewHolder;
import com.wiscomwis.library.util.DateTimeUtil;

/**
 * Created by xuzhaole on 2018/1/24.
 */

public class WithdrawDetailAdapter extends BaseQuickAdapter<WithdrawRecord, BaseViewHolder> {


    public WithdrawDetailAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder holder, WithdrawRecord withdrawRecord) {
        if (withdrawRecord != null) {
            holder.setText(R.id.withdraw_tv_money, String.valueOf(withdrawRecord.getWithdrawBeanAmount()));
            holder.setText(R.id.withdraw_tv_time, DateTimeUtil.convertTimeMillis2String(withdrawRecord.getRecordTime()));
            TextView tv_state = holder.getView(R.id.withdraw_tv_state);
            switch (withdrawRecord.getAuditStatus()) {
                case 1:
                    tv_state.setText(mContext.getString(R.string.under_review));
                    break;
                case 2:
                    tv_state.setText(mContext.getString(R.string.withdraw_fail));
                    break;
                case 3:
                    tv_state.setText(mContext.getString(R.string.withdraw_success));
                    break;
            }
        }
    }
}
