package com.wiscomwis.facetoface.mvp;

/**
 * Created by zhangdroid on 2017/5/20.
 */
public interface BasePresenter {
    void start();
}
