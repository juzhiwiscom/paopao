package com.wiscomwis.facetoface.mvp;

import android.content.Context;

/**
 * Created by zhangdroid on 2017/5/20.
 */
public interface BaseView {

    Context obtainContext();

    /**
     * Show tip message(Toast or SnackBar)
     *
     * @param msg
     * @see com.wiscomwis.library.util.ToastUtil
     * @see com.wiscomwis.library.util.SnackBarUtil
     */
    void showTip(String msg);

}
