package com.wiscomwis.facetoface.event;

import com.wiscomwis.facetoface.parcelable.VideoInviteParcelable;

/**
 * Created by tianzhentao on 2018/7/20.
 */

public class RingtoneEvent {
    private VideoInviteParcelable mVideoInviteParcelable;
    private boolean isInviteActivity;
    public RingtoneEvent(VideoInviteParcelable videoInviteParcelable, boolean isVideoInviteActivity) {
        this.mVideoInviteParcelable=videoInviteParcelable;
        this.isInviteActivity=isVideoInviteActivity;
    }

    public boolean isInviteActivity() {
        return isInviteActivity;
    }

    public void setInviteActivity(boolean inviteActivity) {
        isInviteActivity = inviteActivity;
    }

    public VideoInviteParcelable getmVideoInviteParcelable() {
        return mVideoInviteParcelable;
    }

    public void setmVideoInviteParcelable(VideoInviteParcelable mVideoInviteParcelable) {
        this.mVideoInviteParcelable = mVideoInviteParcelable;
    }
}
