package com.wiscomwis.facetoface.event;

/**
 * Created by Administrator on 2017/7/13.
 */

public class MessageQAArrive {
    private String hunxinid;
    private boolean isQAReport=false;

    public MessageQAArrive(String hunxinid) {
        this.hunxinid = hunxinid;
    }
    public MessageQAArrive(String hunxinid, boolean isQAReport) {
        this.hunxinid = hunxinid;
        this.isQAReport=isQAReport;
    }

    public boolean isQAReport() {
        return isQAReport;
    }

    public void setQAReport(boolean QAReport) {
        isQAReport = QAReport;
    }

    public String getHunxinid() {
        return hunxinid;
    }

    public void setHunxinid(String hunxinid) {
        this.hunxinid = hunxinid;
    }
}
