package com.wiscomwis.facetoface.event;

/**
 * Created by tianzhentao on 2018/7/23.
 */

public class CloseRingtoneEvent {
    private boolean isNotificationClick;
    public CloseRingtoneEvent(boolean isNotificationClick) {
        this.isNotificationClick=isNotificationClick;
    }

    public boolean isNotificationClick() {
        return isNotificationClick;
    }

    public void setNotificationClick(boolean notificationClick) {
        isNotificationClick = notificationClick;
    }
}
