package com.wiscomwis.facetoface.event;

/**
 * Created by tianzhentao on 2018/7/9.
 */

public class UpdateChatDataEvent {
    private String message;

    public UpdateChatDataEvent(String message) {
        this.message=message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
