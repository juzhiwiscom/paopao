package com.wiscomwis.facetoface.data.model;

/**
 * Created by WangYong on 2017/11/18.
 */

public class NickName extends BaseModel {

    String nickName;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
