package com.wiscomwis.facetoface.data.preference;

import android.content.Context;
import android.text.TextUtils;

import com.wiscomwis.facetoface.base.BaseApplication;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.library.util.SharedPreferenceUtil;

/**
 * 保存用户相关信息
 * Created by zhangdroid on 2017/5/31.
 */
public class UserPreference {
    private static Context mContext;

    static {
        mContext = BaseApplication.getGlobalContext();
    }

    private UserPreference() {
    }

    public static final String NAME = "pre_user";
    public static final String NAME_LIST = "user_list";
    public static final String NAME_CHECK = "name_check";
    /**
     * 是否激活过
     */
    private static final String ACTIVATION = "activation";
    /**
     * 是否显示计费提示
     */
    private static final String IS_SHOW_CHARGE = "is_show_charge";

    /**
     * 是否播主
     */
    private static final String IS_ANCHOR = "is_anchor";
    /**
     * 是否vip
     */
    private static final String IS_VIP = "is_vip";
    /**
     * 我的激活
     */
    private static final String My_ACTIVATION = "my_activation";
    /**
     * 账号
     */
    private static final String ACCOUNT = "account";
    /**
     * 密码
     */
    private static final String PASSWORD = "password";
    /**
     * 用户ID
     */
    private static final String USER_ID = "user_id";
    /**
     * 用户状态(1、空闲 2、在聊 3、勿扰)
     */
    private static final String STATUS = "status";
    /**
     * 头像-原图
     */
    private static final String IMG_ORIGINAL = "img_original";
    /**
     * 头像-中图
     */
    private static final String IMG_MIDDLE = "img_middle";
    /**
     * 头像-小图
     */
    private static final String IMG_SMALL = "img_small";
    /**
     * 昵称
     */
    private static final String NICKNAME = "nickname";
    /**
     * 年龄
     */
    private static final String AGE = "age";
    /**
     * 性别
     */
    private static final String GENDER = "gender";
    /**
     * 自我介绍
     */
    private static final String INTRODUCATION = "introducation";
    /**
     * 国家
     */
    private static final String COUNTRY = "country";
    /**
     * 州
     */
    private static final String STATE = "state";
    /**
     * 城市
     */
    private static final String CITY = "city";
    /**
     * 支持的语言
     */
    private static final String SPOKEN_LANGUAGE = "spoken_language";

    /**
     * 保存国家的ID
     */
    private static final String COUNTRY_ID = "country_id";
    /**
     * 判断是否进入上传头像的界面
     */
    private static final String IS_REGISTER = "is_register";
    /**
     * 判断是否从登陆界面退出的，
     */
    private static final String IS_LOGIN = "is_login";


    /**
     * 是否正在审核主播
     */
    public static final String CHECK_AUTHOR = "check_author";
    /**
     * 是否是第一次注册
     */
    public static final String IS_FIRST_REGISTER="is_first_register";
    /**
     * 视频聊天页面保存发送礼物的信息
     */
    public static final String SAVE_SEND_GIFT_MSG="save_send_gift_msg";
    /**
     * 判断是否是第一次使用，如果是就打开注册引导
     */
    public static final String REGISTER_GUIDE="register_guide";
    /**
     * 判断是否正在群发视频，
     */
    private static final String IS_QUN_FA = "is_qun_fa_video";
    /**
     * 支付时使用的服务id
     */
    private static final String SEVICE_ID_1 = "serviceId1";
    private static final String SEVICE_ID_2 = "serviceId2";
    private static final String SEVICE_ID_3 = "serviceId3";
    /**
     * 推送用到的regid
     */
    private static final String REGIND= "regid";


    // ********************************************* 保存标记 *********************************************
    //注册引导成功之后的标记
    public static void guided(){
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, REGISTER_GUIDE, true);
    }
    //标记注册引导只出现一次，下次调用注册接口时出现
    public static void guidedModification(){
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, REGISTER_GUIDE, false);
    }
    //判断注册引导是否已经打开
    public static boolean  isGuided(){
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, REGISTER_GUIDE, false);
    }


    //保存礼物的信息
    public static void saveGiftsContent(String json){
        SharedPreferenceUtil.setStringValue(mContext, NAME, SAVE_SEND_GIFT_MSG, json);
    }
    //获取保存礼物的信息
    public static String getGiftsContent(){
        return SharedPreferenceUtil.getStringValue(mContext,NAME,SAVE_SEND_GIFT_MSG,"0");
    }

   //第一次注册成功之后的标记(每次调用注册接口成功时调用)
    public static void registered(){
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, IS_FIRST_REGISTER, true);
    }
    //标记免费拨打电话只出现一次，下次调用注册接口时出现
    public static void registerModification(){
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, IS_FIRST_REGISTER, false);
    }
    //判断是否已经注册成功了
   public static boolean  isRegister(){
       return SharedPreferenceUtil.getBooleanValue(mContext, NAME, IS_FIRST_REGISTER, false);
   }
    public static void activation() {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, ACTIVATION, true);
    }

    public static boolean isActived() {
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, ACTIVATION, false);
    }

    //我自定义的激活
    public static void mActivation() {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, My_ACTIVATION, true);
    }

    public static boolean isMactived() {
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, My_ACTIVATION, false);
    }
    /**
     * 不再显示计费提示
     */
    public static void hideCharge() {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, IS_SHOW_CHARGE, false);
    }

    public static boolean isShowCharge() {
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, IS_SHOW_CHARGE, true);
    }

    // ********************************************* 保存/获取用户资料项信息 *********************************************

    private static void saveIfNotEmpty(String key, String value) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, key, value);//保存的信息不能做非空判断，因为这样有可能造成数据保存不上
    }

    private static String getValue(String key) {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, key, null);
    }

    public static void saveUserInfo(UserBase userBase) {
        if (null != userBase) {
            setAccount(userBase.getAccount());
            setPassword(userBase.getPassword());
            setIsAnchor(userBase.getUserType());
            setId(String.valueOf(userBase.getGuid()));
            setStatus(userBase.getStatus());
            setOriginalImage(userBase.getIconUrl());
            setMiddleImage(userBase.getIconUrlMiddle());
            setSmallImage(userBase.getIconUrlMininum());
            setNickname(userBase.getNickName());
            setAge(String.valueOf(userBase.getAge()));
            setGender(String.valueOf(userBase.getGender()));
            setIntroducation(userBase.getOwnWords());
            setCountry(userBase.getCountry());
            setState(userBase.getState());
            setCity(userBase.getCity());
            setSpokenLanguage(userBase.getSpokenLanguage());
            setUserList(userBase.getAccount(), userBase.getPassword());
        }
    }

    private static void setUserList(String account, String password) {
        if(!TextUtils.isEmpty(account)&&!TextUtils.isEmpty(password))
        DbModle.getInstance().getUserAccountDao().addAccountAndPwd(account, password);
    }


    public static void clearUserInfo() {//清空保存的数据
        setAccount(null);
        setPassword(null);
        setIsAnchor(-1);
        setId(null);
        setStatus(null);
        setOriginalImage(null);
        setMiddleImage(null);
        setSmallImage(null);
        setNickname(null);
        setAge(null);
        setGender(null);
        setIntroducation(null);
        setCountry(null);
        setState(null);
        setCity(null);
        setSpokenLanguage(null);
    }

    public static void setAlipayName(String alipayName) {
        saveIfNotEmpty("alipayname", alipayName);
    }

    public static String getAlipayName() {
        return getValue("alipayname");
    }

    public static void setAlipayAccount(String alipayAccount) {
        saveIfNotEmpty("alipayAccount", alipayAccount);
    }

    public static String getAlipayAccount() {
        return getValue("alipayAccount");
    }

    public static void setWechatName(String wechatName) {
        saveIfNotEmpty("wechatname", wechatName);
    }

    public static String getWechatName() {
        return getValue("wechatname");
    }

    public static void setWechatAccount(String wechatAccount) {
        saveIfNotEmpty("wechataccount", wechatAccount);
    }

    public static String getWechatAccount() {
        return getValue("wechataccount");
    }


    public static void setAccount(String value) {
        saveIfNotEmpty(ACCOUNT, value);
    }

    public static String getAccount() {
        return getValue(ACCOUNT);
    }

    public static void setPassword(String value) {
        saveIfNotEmpty(PASSWORD, value);
    }

    public static String getPassword() {
        return getValue(PASSWORD);
    }

    public static void setIsAnchor(int userType) {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, IS_ANCHOR, userType == 1);
    }

    public static boolean isAnchor() {
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, IS_ANCHOR, false);
    }

    public static void setIsVip(int vipdays) {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, IS_VIP, (vipdays == 0)?false:true);
    }
    public static void setIsVip(boolean isVip) {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, IS_VIP, isVip);
    }

    public static boolean isVip() {
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, IS_VIP, false);
    }

    public static void setId(String value) {
        saveIfNotEmpty(USER_ID, value);
    }

    public static String getId() {
        return getValue(USER_ID);
    }

    public static void setStatus(String value) {
        saveIfNotEmpty(STATUS, value);
    }

    public static String getStatus() {
        return getValue(STATUS);
    }

    public static void setOriginalImage(String value) {
        saveIfNotEmpty(IMG_ORIGINAL, value);
    }

    public static String getOriginalImage() {
        return getValue(IMG_ORIGINAL);
    }

    public static void setMiddleImage(String value) {
        saveIfNotEmpty(IMG_MIDDLE, value);
    }

    public static String getMiddleImage() {
        return getValue(IMG_MIDDLE);
    }

    public static void setSmallImage(String value) {
        saveIfNotEmpty(IMG_SMALL, value);
    }

    public static String getSmallImage() {
        return getValue(IMG_SMALL);
    }

    public static void setNickname(String value) {
        saveIfNotEmpty(NICKNAME, value);
    }

    public static String getNickname() {
        return getValue(NICKNAME);
    }

    public static void setAge(String value) {
        saveIfNotEmpty(AGE, value);
    }

    public static String getAge() {
        return getValue(AGE);
    }

    public static void setGender(String value) {
        saveIfNotEmpty(GENDER, value);
    }

    public static boolean isMale() {
        return "0".equals(SharedPreferenceUtil.getStringValue(mContext, NAME, GENDER, "0"));
    }

    public static void setIntroducation(String value) {
        saveIfNotEmpty(INTRODUCATION, value);
    }

    public static String getIntroducation() {
        return getValue(INTRODUCATION);
    }

    public static void setCountry(String value) {
        saveIfNotEmpty(COUNTRY, value);
    }

    public static String getCountry() {
        return getValue(COUNTRY);
    }

    public static void setState(String value) {
        saveIfNotEmpty(STATE, value);
    }

    public static String getState() {
        return getValue(STATE);
    }

    public static void setCity(String value) {
        saveIfNotEmpty(CITY, value);
    }

    public static String getCity() {
        return getValue(CITY);
    }

    public static void setSpokenLanguage(String value) {
        saveIfNotEmpty(SPOKEN_LANGUAGE, value);

    }

    public static String getSpokenLanguage() {
        return getValue(SPOKEN_LANGUAGE);
    }

    public static void setCountryId(String countryId) {

        SharedPreferenceUtil.setStringValue(mContext, COUNTRY_ID, "contry_key", countryId);
    }

    public static String getCountryId() {
        return SharedPreferenceUtil.getStringValue(mContext, COUNTRY_ID, "contry_key", null);
    }

    public static void setRegisterSign(boolean isRegister) {
        SharedPreferenceUtil.setBooleanValue(mContext, IS_REGISTER, "is_register_key", isRegister);
    }

    public static boolean isRegisterSign(){
        return SharedPreferenceUtil.getBooleanValue(mContext, IS_REGISTER, "is_register_key", false);
    }
//判断是否从登陆界面退出的应用，如果是的话，下次进入登陆界面
    public static void exitFromLoginView(boolean isExitFromLogin) {
        SharedPreferenceUtil.setBooleanValue(mContext, IS_LOGIN, "is_login_key", isExitFromLogin);
    }
    public static boolean isExitFromLoginView(){
        return SharedPreferenceUtil.getBooleanValue(mContext, IS_LOGIN, "is_login_key", false);
    }
    public static void setQuanFa(int isQuanFa){
        SharedPreferenceUtil.setIntValue(mContext, IS_QUN_FA, "is_qun_fa", isQuanFa);
    }
    //0非群发，1群发，2关闭群发
    public static int isQunFa(){
        return SharedPreferenceUtil.getIntValue(mContext, IS_QUN_FA, "is_qun_fa", 0);
    }
    public static void setServiceId1(String value){
        saveIfNotEmpty(SEVICE_ID_1,value);
    }
    public static String getServiceId1() {
        return getValue(SEVICE_ID_1);
    }
    public static void setServiceId2(String value){
        saveIfNotEmpty(SEVICE_ID_2,value);
    }
    public static String getServiceId2() {
        return getValue(SEVICE_ID_2);
    }
    public static void setServiceId3(String value){
        saveIfNotEmpty(SEVICE_ID_3,value);
    }
    public static String getServiceId3() {
        return getValue(SEVICE_ID_3);
    }

    public static void setCacheIg(String imgUrl, String path) {
        SharedPreferenceUtil.setStringValue(mContext, "cacheImg", imgUrl, path);
    }
    public static String getCacheIg(String imgUrl){
        return SharedPreferenceUtil.getStringValue(mContext, "cacheImg", imgUrl, null);
    }

    public static void setRegId(String value) {
        saveIfNotEmpty(REGIND, value);
    }
    public static String getRegid() {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, REGIND, "");
//        return getValue(REGIND);
    }

    public static void setReception(boolean isReception) {
        SharedPreferenceUtil.setBooleanValue(mContext, IS_LOGIN, "is_reception", isReception);
    }
    public static boolean isReception(){
        return SharedPreferenceUtil.getBooleanValue(mContext, IS_LOGIN, "is_reception", false);
    }
    public static void setLockScreen(boolean isLockScreen) {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, "is_Lock_Screen", isLockScreen);

    }
    public static boolean isLockScreen(){
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, "is_Lock_Screen", false);
    }

    public static boolean isFirstInfoChat(String guid) {
        return SharedPreferenceUtil.getBooleanValue(mContext,"is_first_into_caht",guid,true);
    }

    public static void setFirstIntoChat(String guid) {
        SharedPreferenceUtil.setBooleanValue(mContext, "is_first_into_caht", guid, false);
    }

    public static void setWithdrawMsg(String account,String withdrawId) {
        SharedPreferenceUtil.setBooleanValue(mContext, account, withdrawId, true);
    }
    public static boolean isWithdrawMsg(String account,String withdrawId) {
        return SharedPreferenceUtil.getBooleanValue(mContext,account,withdrawId,false);
    }

    public static void setDeleteMsg(String msgId) {
        SharedPreferenceUtil.setBooleanValue(mContext, "delete_msg", msgId, true);
    }
    public static boolean isDeleteMsg(String msgId) {
        return SharedPreferenceUtil.getBooleanValue(mContext,"delete_msg", msgId,false);
    }

    public static void nativeWithdraw(String msgId) {
        SharedPreferenceUtil.setBooleanValue(mContext, "withdraw_msg", msgId, true);
    }
    public static boolean isNativeWithdraw(String msgId) {
        return SharedPreferenceUtil.getBooleanValue(mContext,"withdraw_msg", msgId,false);
    }


    public static void setLockVideoId(String channelId) {
        saveIfNotEmpty("lock_video_id", channelId);
    }
    public static String getLockVideoId() {
        return getValue("lock_video_id");
    }

    public static void setClickNotification(boolean isClick) {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, "ClickNotification", isClick);
    }
    public static boolean isClickNotification() {
        return SharedPreferenceUtil.getBooleanValue(mContext,NAME, "ClickNotification",false);
    }

    public static void setIntoVideo(boolean isInto) {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, "isInto_video", isInto);
    }
    public static boolean isIntoVideo() {
        return SharedPreferenceUtil.getBooleanValue(mContext,NAME, "isInto_video",false);
    }

    public static void setCancelInvite(final String ChannelId, final boolean isCancel) {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, ChannelId, false);
    }
    public static boolean isCancelInvite(String ChannelId) {
        return SharedPreferenceUtil.getBooleanValue(mContext,NAME, ChannelId,false);
    }

    public static void setDetailFlag(String remoteId, boolean isInto) {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, remoteId, isInto);
    }
    public static boolean isIntoDetailFlag(String remoteId) {
        return SharedPreferenceUtil.getBooleanValue(mContext,NAME, remoteId,false);
    }
    public static void setFirstIntoChatActivity(String uid) {
        SharedPreferenceUtil.setBooleanValue(mContext, "FirstIntoChatActivity", uid, true);
    }
    public static boolean isFirstIntoChat(String uid) {
        return SharedPreferenceUtil.getBooleanValue(mContext,"FirstIntoChatActivity", uid,false);
    }
}
