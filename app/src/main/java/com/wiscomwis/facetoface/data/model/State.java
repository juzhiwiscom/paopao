package com.wiscomwis.facetoface.data.model;

/**
 * Created by Administrator on 2017/7/6.
 */

public class State {
      private String guid;
      private String name;
      private String country_id;
      private String language;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "State{" +
                "guid='" + guid + '\'' +
                ", name='" + name + '\'' +
                ", country_id='" + country_id + '\'' +
                ", language='" + language + '\'' +
                '}';
    }
}
