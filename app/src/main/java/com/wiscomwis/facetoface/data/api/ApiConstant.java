package com.wiscomwis.facetoface.data.api;

import com.wiscomwis.facetoface.data.preference.PlatformPreference;

/**
 * 该类保存项目中用到的所有接口定义常量
 * <li>1、该类中配置所有接口定义</li>
 * <li>2、接口配置时请放入相应模块注释下方，并写明接口作用，统一管理</li>
 * <li>3、接口命名规则请参照：URL_模块名_接口名</li>
 * Created by zhangdroid on 2017/5/17.
 */
public final class ApiConstant {

    // ***************************** 测试服 ***************************

//    public static final String URL_BASE = "http://121.42.144.2:9092/apollo";

//    public static final String URL_BASE = "http://192.168.199.128:8080/apollo";//大伟本地
//    public static final String URL_BASE = "http://47.93.32.12:9092/apolloplatform/";//大伟线上本地
//    public static final String URL_BASE = "http://59.110.167.200:9092/apolloplatform/";//大伟线上本地


    // ***************************** 正式服 ***************************
   public static final String URL_BASE = "http://andapo.ilove.ren:9092/apolloplatform";


    // ***************************** 支付 ***************************
//       public static final String PAYMENT = "http://47.89.208.14:10093/paycenter/totalBack/getIAPInApplePay.pay";
    public static final String PYMENT = "http://andapopay.ilove.ren:9093/apollopaycenter/totalBack/getGooglePay.pay";


    // ***************************** 订单接口***************************
    //支付宝测试

//   public static final String URL_ALIPAY_ORDERINFO = "http://121.42.144.2:9092/apollopaycenter/totalPay/getAlipayOrder.pay";

// public static final String URL_ALIPAY_ORDERINFO = "http://47.93.32.12:9093/apollopaycenter/totalPay/getAlipayOrder.pay";//大伟测试
// public static final String URL_ALIPAY_ORDERINFO = "http://59.110.167.200:9093/apollopaycenter/totalPay/getAlipayOrder.pay";//大伟测试

 //微信支付测试

// public static final String URL_WX_ORDERINFO = "http://121.42.144.2:9092/apollopaycenter/totalPay/getWeixinPayOrder.pay";
// public static final String URL_WX_ORDERINFO = "http://47.93.32.12:9093/apollopaycenter/totalPay/getWeixinPayOrder.pay";//大伟测试
// public static final String URL_WX_ORDERINFO = "http://59.110.167.200:9093/apollopaycenter/totalPay/getWeixinPayOrder.pay";//大伟测试

    //支付宝线上
    public static final String URL_ALIPAY_ORDERINFO = "http://andapopay.ilove.ren:9093/apollopaycenter/totalPay/getAlipayOrder.pay";

    //微信支付线上
    public static final String URL_WX_ORDERINFO = "http://andapopay.ilove.ren:9093/apollopaycenter/totalPay/getWeixinPayOrder.pay";
    // ***************************** 协议 ***************************

    /**
     * 用户协议
     */
    public static final String URL_AGREEMENT_USER = "http://www.easyplay.site/paopao/user_terms.jsp";

    public static String getAgreementUser() {
        String URL = "http://www.thunderdate.win/paopao/user_terms.jsp";
        return URL;
    }

    /**
     * 隐私协议
     */
    public static final String URL_AGREEMENT_PRIVACY = "http://www.easyplay.site/paopao/privacy.jsp";

    public static String getAgreementPrivacy() {
        String URL = "http://www.thunderdate.win/paopao/privacy.jsp";
        return URL;
    }

    /**
     * 播主协议
     */
    public static final String URL_AGREEMENT_ANCHOR = "http://www.easyplay.site/apollo/hosttermcn.html";

    public static String getUrlAgreementAnchor() {
        String URL = null;
        switch (PlatformPreference.getPlatformInfo().getFid()) {
            case "20108":
                URL = "http://www.easyplay.site/apollo/hostterm_tr.html";
                break;
            case "20101":
            case "20114":
            case "20112":
                URL = "http://www.easyplay.site/apollo/hostterm_en.html";
                break;
            default:
                URL = "http://www.easyplay.site/apollo/hostterm_cn.html";
                break;
        }
        return URL;
    }

    /**
     * 提现协议
     */
    public static final String URL_AGREEMENT_WITHDRAW = "http://www.easyplay.site/apollo/withdrawtermcn.html";

    public static String getUrlAgreementWithdraw() {
        String URL = null;
        switch (PlatformPreference.getPlatformInfo().getFid()) {
            case "20108":
                URL = "http://www.easyplay.site/apollo/withdrawterm_tr.html";
                break;
            case "20101":
            case "20114":
            case "20112":
                URL = "http://www.easyplay.site/apollo/withdrawterm_en.html";
                break;
            default:
                URL = "http://www.easyplay.site/apollo/withdrawterm_cn.html";
                break;
        }
        return URL;
    }
    // ***************************** 激活/注册/登陆 ***************************

    /**
     * 激活
     */
    public static final String URL_ACTIVATION = URL_BASE + "/sys/clientactivation.json";
    /**
     * 注册
     */
    public static final String URL_REGISTER = URL_BASE + "/user/register.json";
    /**
     * 登录
     */
    public static final String URL_LOGIN = URL_BASE + "/user/login.json";
    /**
     * 找回密码
     */
    public static final String URL_FIND_PWD = URL_BASE + "/user/getpwd.json";
    /**
     * 退出登录
     */
    public static final String URL_USER_SWITCH = URL_BASE + "/user/switch.json";

   /**
    * 获取小米推送的guid
    */
   public static final String URL_MI_PUSH_TOKEN = URL_BASE + "/user/receivepushtoken.json";
   /**
    * 位置信息
    */
   public static final String URL_LOCATION = URL_BASE + "/sys/uploadlocation.json";



   // ***************************** 首页 ***************************
    /**
     * 热门
     */
    public static final String URL_HOMEPAGE_HOT = URL_BASE + "/search/byhothost.json";
    /**
     * 女神（收入排行）
     */
    public static final String URL_HOMEPAGE_INCOME = URL_BASE + "/search/byincome.json";
    /**
     * 活跃播主/聊友（操作时间排行）
     */
    public static final String URL_HOMEPAGE_ACTIVE = URL_BASE + "/search/byactive.json";
    /**
     * 新晋聊友（注册时间排行）/新人（申请播主时间排行）
     */
    public static final String URL_HOMEPAGE_NEW = URL_BASE + "/search/bytime.json";
    /**
     * 女主播看到的男用户
     */
    public static final String URL_HOMEPAGE_AUTHOR_SEE_MAN = URL_BASE + "/search/searchcommen.json";
    /**
     * 推荐给男用户的女主播
     */
    public static final String URL_HOMEPAGE_AUTHOR_RECOMMEND = URL_BASE + "/search/byrecommend.json";
    /**
     * 每日推荐批量打招呼获取的数据
     */
    public static final String URL_MONEY_SAY_HELLO = URL_BASE + "/search/bydayhot.json";
    /**
     * 魅力榜
     */
    public static final String URL_CHARMAND_USER = URL_BASE + "/search/bycharm.json";
    /**
     * 富豪榜
     */
    public static final String URL_RICH_USER = URL_BASE + "/search/byrich.json";

    // ***************************** 消息 ***************************

    /**
     * 视频通话记录
     */
    public static final String URL_MESSAGE_VIDEO_RECORD = URL_BASE + "/chat/getcallrecords.json";
    /**
     * 敏感词过滤（文字）
     */
    public static final String URL_MESSAGE_CHECK_TEXT = URL_BASE + "/sys/checkword.json";
    /**
     * 聊天支付拦截（文字）
     */
    public static final String URL_MESSAGE_INTERRUPT_TEXT = URL_BASE + "/msg/sendtextmessage.json";
    /**
     * 聊天支付拦截（语音）
     */
    public static final String URL_MESSAGE_INTERRUPT_VOICE = URL_BASE + "/msg/sendaudiomessage.json";
    /**
     * 聊天支付拦截（图片）
     */
    public static final String URL_MESSAGE_INTERRUPT_IMAGE = URL_BASE + "/msg/sendimagemessage.json";

    /**
     * 主动拨打电话的拦截
     */
    public static final String URL_MESSAGE_INTERRUPT_CALL_VIDEO = URL_BASE + "/chat/calldial.json";
    /**
     * 视频邀请信接通视频电话
     */
    public static final String URL_MESSAGE_INTERRUPT_CALL_VIDEO_MSG = URL_BASE + "/chat/callanswer.json";
    /**
     * 主动拨打电话时为ios的推送
     */
    public static final String URL_MESSAGE_INTERRUPT_CALL_FOR_ISO = URL_BASE + "/chat/callpush.json";

    // ***************************** 关注 ***************************

    /**
     * 关注
     */
    public static final String URL_FOLLOW = URL_BASE + "/follow/dofollow.json";
    /**
     * 取消关注
     */
    public static final String URL_UN_FOLLOW = URL_BASE + "/follow/unfollow.json";
    /**
     * 关注列表
     */
    public static final String URL_FOLLOW_LIST = URL_BASE + "/follow/getfollows.json";//这个适用于1v1视频
    /**
     * 关注列表2
     */
    public static final String URL_FOLLOWPAGE_LIST = URL_BASE + "/follow/getfollowpage.json";//这个适用于1v1视频
    /**
     * 是否关注某个用户
     */
    public static final String URL_IS_FOLLOW = URL_BASE + "/follow/isfollow.json";
    /**
     * 删除图片
     */
    public static final String URL_DELETE_IMAGE = URL_BASE + "/photo/deleteimg.json";

    // ***************************** 个人中心 ***************************

    /**
     * 获得个人信息
     */
    public static final String URL_PERSON_MY_INFO = URL_BASE + "/setting/myinfo.json";

    /**
     * 获得支付渠道信息
     */
    public static final String URL_GET_PAY_WAY = URL_BASE + "/pay/payway.json";
    /**
     * 修改个人资料
     */
    public static final String URL_UPLOAD_MYINFO = URL_BASE + "/setting/updatemyinfo.json";
    /**
     * 上传头像或者图片
     */
    public static final String URL_UPLOAD_PHOTO = URL_BASE + "/photo/uploadimg.json";
    /**
     * 提现记录
     */
    public static final String URL_WITHDRAW_RECORD = URL_BASE + "/pay/getwithdrawrecords.json";
    /**
     * 提现申请
     */
    public static final String URL_WITHDRAW_APPLY = URL_BASE + "/pay/withdraw.json";
    /**
     * 收入记录
     */
    public static final String URL_INCOME = URL_BASE + "/pay/getincomerecords.json";
    /**
     * 礼物字典
     */
    public static final String URL_GIFTS_DIC = URL_BASE + "/gift/getgifts.json";
    /**
     * 发送礼物
     */
    public static final String URL_SEND_GIFTS = URL_BASE + "/gift/sendgift.json";
    /**
     * 解锁礼物
     */
    public static final String URL_UNLOCK_GIFTS = URL_BASE + "/gift/unlockmessage.json";
    // ***************************** 公用 ***************************

    /**
     * 获取某个用户详情
     */
    public static final String URL_GET_USER_INFO = URL_BASE + "/space/userinfo.json";
    /**
     * 查找某一个用户
     */
    public static final String URL_GET_FIND_USER = URL_BASE + "/search/byaccount.json";
    /**
     * 修改用户状态
     */
    public static final String URL_MODIFFY_USER_STATUS = URL_BASE + "/setting/modifyuserstatus.json";

    // ***************************** 视频 ***************************

    /**
     * 获取声网信令系统token
     */
    public static final String URL_GET_TOKEN = URL_BASE + "/sys/getsignalingkey.json";
    /**
     * 获取声网channel key
     */
    public static final String URL_GET_CHANNEL_KEY = URL_BASE + "/sys/getchannelkey.json";
    /**
     * 发起/接受视频呼叫前通知服务器
     */
    public static final String URL_VIDEO_CALL = URL_BASE + "/chat/callcreate.json";
    /**
     * 视频接通后心跳（50秒）
     */
    public static final String URL_VIDEO_HEART_BEAT = URL_BASE + "/chat/callheartbeat.json";
    /**
     * 视频结束后通知服务器
     */
    public static final String URL_VIDEO_STOP = URL_BASE + "/chat/callclose.json";
    /**
     * 视频邀请
     */
    public static final String URL_VIDEO_INVITE = URL_BASE + "/chat/sendcallmsg.json";

    // ***************************** 视频上传与主播认证 ***************************

    /**
     * 获取facebook的账号
     */
    public static final String URL_GET_FACEBOOK_ACCOUNT = URL_BASE + "/sys/init.json";
    /**
     * 提交审核
     */
    public static final String URL_SUBMIT = URL_BASE + "/setting/submitapply.json";
    /**
     * 调整主播价格
     **/
    public static final String URL_UPDATA_PRICE = URL_BASE + "/setting/updatehostprice.json";
    /**
     * 打招呼
     **/
    public static final String URL_SAY_HELLO = URL_BASE + "/msg/sayhello.json";
    /**
     * 群打招呼
     **/
    public static final String URL_SAY_HELLO_GROUP = URL_BASE + "/msg/batchsayhello.json";
    /**
     * 开关
     **/
    public static final String URL_KAIGUAN = URL_BASE + "/sys/getswitchinfo.json";
    /**
     * 举报
     **/
    public static final String URL_REPORT = URL_BASE + "/space/report.json";
    /**
     * 拉黑
     **/
    public static final String URL_LAHEI = URL_BASE + "/space/block.json";
    /**
     * 取消拉黑
     **/
    public static final String URL_CANCEL_BLOCK = URL_BASE + "/space/unblock.json";
    /**
     * 获取是否拉黑
     **/
    public static final String URL_GET_IS_BLOCK = URL_BASE + "/space/isblock.json";
    /**
     * 获取主播自动回复
     */
    public static final String URL_HOST_AUTOREPLY = URL_BASE + "/setting/getHostAutoReply.json";
    /**
     * 上传文本类型的自动回复/打招呼主播
     */
    public static final String URL_UPLOAD_TEXT_AUTOREPLAY = URL_BASE + "/setting/uploadTextAutoReply.json";
    /**
     * 上传文本类型的自动回复/打招呼主播
     */
    public static final String URL_UPLOAD_SOUND_AUTOREPLAY = URL_BASE + "/setting/uploadAudioAutoReply.json";
    /**
     * 修改自动回复/主动打招呼 状态
     */
    public static final String URL_UPLOAD_STATUS_AUTOREPLAY = URL_BASE + "/setting/updateAutoReplyStatus.json";

    /**
     * 所要礼物（文字）
     */
    public static final String URL_ASK_FOR_GIFTS_TEXT = URL_BASE + "/gift/asktext.json";

    /**
     * 所要礼物（语音）
     */
    public static final String URL_ASK_FOR_GIFTS_SOUND = URL_BASE + "/gift/askaudio.json";
    /**
     * 所要礼物（图片）
     */
    public static final String URL_SEND_PRIVATE_PHOTO = URL_BASE + "/gift/askphoto.json";
    /**
     * 所要礼物（视频）
     */
    public static final String URL_SEND_PRIVATE_VIDEO = URL_BASE + "/gift/askvideo.json";
    /**
     * 支付钥匙，解锁聊天
     */
    public static final String URL_PAY_MSG_KEY = URL_BASE + "/msg/paymsgkey.json";
    /**
     * 群发视频，群发语音
     */
    public static final String URL_MASS_VIDEO_OR_VOICE = URL_BASE + "/chat/sendmasscallmsg.json";
    /**
     * 获取群发视频群发语音的倒计时
     */
    public static final String URL_MASS_VIDEO_OR_VOICE_COUNTDOWN = URL_BASE + "/chat/getmassmsg.json";
    /**
     * QA消息
     */
    public static final String URL_QA = URL_BASE + "/msg/sendanswermessage.json";

    /**
     * 随机昵称
     */
    public static final String URL_NICKNAME = URL_BASE + "/sys/nickname.json";

    /**
     * 聊天列表置为已读状态
     */
    public static final String URL_MSG_READ = URL_BASE + "/msg/messageread.json";

    /**
     * 客户端收到消息后，向服务器回执，确认已送达
     */
    public static final String URL_MSG_RECEIVED = URL_BASE + "/msg/messagereceived.json";
    /**
     * 虚拟实体用户接口
     */
    public static final String URL_VIRTUAL_VIDOE = URL_BASE + "/chat/callvideo.json";
    /**
     * 应用打点回传
     */
    public static final String URL_USER_ACTIVITY_TAG = URL_BASE + "/sys/useractivetag.json";
    /**
     * 判断主播审核的状态
     */
    public static final String URL_USER_CHECK_STATUS = URL_BASE + "/setting/applyinfo.json";
    /**
     * 修改密码
     */
    public static final String URL_CHANGE_PASSWORD = URL_BASE + "/setting/modifypwd.json";
    /**
     * 上传视频秀
     */
    public static final String URL_UPLOAD_VIDEO_SHOW = URL_BASE + "/setting/uploadvideoshow.json";
    /**
     * 相册重排序
     */
    public static final String URL_RECORD_IMAGE = URL_BASE + "/photo/reorderimage.json";
    /**
     * 上传语音秀
     */
    public static final String URL_UPLOAD_VOICE_SHOW = URL_BASE + "/setting/uploadaudioshow.json";
    /**
     * 视频秀列表
     */
    public static final String URL_VIDEO_SHOW_LIST = URL_BASE + "/search/videosquare.json";
    /**
     * 视频秀列表
     */
    public static final String URL_UPLOAD_ALL_SHOW = URL_BASE + "/setting/uploadallshow.json";
    /**
     * 获取是否有一次机会去聊天的状态
     */
    public static final String URL_MSG_KEY_STATUS = URL_BASE + "/msg/getmsgkeystatus.json";
    /**
     * 用户通话拒接对方
     */
    public static final String URL_CALL_REFUSE = URL_BASE + "/chat/callbarring.json";
    /**
     * 用户上传标签
     */
    public static final String URL_UPLOAD_USER_TAG = URL_BASE + "/setting/uploadusertag.json";

}
