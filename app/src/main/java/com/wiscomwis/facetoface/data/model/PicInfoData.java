package com.wiscomwis.facetoface.data.model;

/**
 * Created by WangYong on 2018/2/3.
 */

public class PicInfoData {
    private int extentType;
    private PicInfo data;

    public PicInfoData(int extentType, PicInfo data) {
        this.extentType = extentType;
        this.data = data;
    }

    public int getExtentType() {
        return extentType;
    }

    public void setExtentType(int extentType) {
        this.extentType = extentType;
    }

    public PicInfo getData() {
        return data;
    }

    public void setData(PicInfo data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PicInfoData{" +
                "extentType=" + extentType +
                ", data=" + data +
                '}';
    }
}
