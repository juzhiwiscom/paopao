package com.wiscomwis.facetoface.data.model;

/**
 * Created by tianzhentao on 2018/5/14.
 */

public class FaceBookIntoModel extends BaseModel {
    private String abTest;//  1 AB测试   2 A状态   3 B状态

    public String getAbTest() {
        return abTest;
    }

    public void setAbTest(String abTest) {
        this.abTest = abTest;
    }
}
