package com.wiscomwis.facetoface.data.model;

/**
 * Created by tianzhentao on 2018/5/17.
 */

public class CheckTextModel extends BaseModel {
    private ResultMessage resultMessage;

    public ResultMessage getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(ResultMessage resultMessage) {
        this.resultMessage = resultMessage;
    }
}
