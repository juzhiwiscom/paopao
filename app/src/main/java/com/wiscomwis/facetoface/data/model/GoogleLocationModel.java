package com.wiscomwis.facetoface.data.model;

import java.util.Map;

/**
 * Created by tianzhentao on 2017/12/29.
 */

public class GoogleLocationModel {
    Map<String,String> results;

    public Map<String, String> getResults() {
        return results;
    }

    public void setResults(Map<String, String> results) {
        this.results = results;
    }
}
