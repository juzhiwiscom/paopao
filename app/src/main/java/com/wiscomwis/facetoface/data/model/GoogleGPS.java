package com.wiscomwis.facetoface.data.model;

import java.util.List;

/**
 * Created by tianzhentao on 2018/1/1.
 */

public class GoogleGPS {

    private List<GoogleGpsResult> results;

    private String status;

    public List<GoogleGpsResult> getResults() {
        return results;
    }

    public void setResults(List<GoogleGpsResult> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
