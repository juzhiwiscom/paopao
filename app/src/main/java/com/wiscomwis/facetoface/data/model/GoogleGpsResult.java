package com.wiscomwis.facetoface.data.model;

import java.util.List;

/**
 * Created by tianzhentao on 2018/1/1.
 */

public class GoogleGpsResult {

    private List<AddressCompents> address_components;
    private String formatted_address;
    private String place_id;
    private List<String> types;


    public List<AddressCompents> getAddress_components() {
        return address_components;
    }

    public void setAddress_components(List<AddressCompents> address_components) {
        this.address_components = address_components;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }
}
