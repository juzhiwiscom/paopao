package com.wiscomwis.facetoface.data.model;

/**
 * 视频心跳结果对象
 * Created by zhangdroid on 2017/6/23.
 */
public class VideoHeartBeat extends BaseModel {
    private String connectStatus;// 连接信息状态：-1 未建立 -2 已关闭 挂断声网接口 1连接正常
    private String beanStatus;// 余额状态 -1 余额不足 需挂断声网接口  1 通话状态正常
    private String totalMinute;// 视频聊天总时长 fen
    private String totalSeconds;// 视频聊天总时长 秒
    private long avalibleSeconds;// 视频聊天总时长 秒

    public String getConnectStatus() {
        return connectStatus;
    }

    public void setConnectStatus(String connectStatus) {
        this.connectStatus = connectStatus;
    }

    public String getBeanStatus() {
        return beanStatus;
    }

    public void setBeanStatus(String beanStatus) {
        this.beanStatus = beanStatus;
    }

    public String getTotalMinute() {
        return totalMinute;
    }

    public void setTotalMinute(String totalMinute) {
        this.totalMinute = totalMinute;
    }

    public long getAvalibleSeconds() {
        return avalibleSeconds;
    }

    public void setAvalibleSeconds(long avalibleSeconds) {
        this.avalibleSeconds = avalibleSeconds;
    }

    public String getTotalSeconds() {
        return totalSeconds;
    }

    public void setTotalSeconds(String totalSeconds) {
        this.totalSeconds = totalSeconds;
    }

    @Override
    public String toString() {
        return "VideoHeartBeat{" +
                "connectStatus='" + connectStatus + '\'' +
                ", beanStatus='" + beanStatus + '\'' +
                ", totalMinute='" + totalMinute + '\'' +
                ", totalSeconds='" + totalSeconds + '\'' +
                ", avalibleSeconds=" + avalibleSeconds +
                '}';
    }
}
