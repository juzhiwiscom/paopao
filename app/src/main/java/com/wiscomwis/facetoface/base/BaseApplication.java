package com.wiscomwis.facetoface.base;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.danikula.videocache.HttpProxyCacheServer;
import com.meizu.cloud.pushsdk.PushManager;
import com.meizu.cloud.pushsdk.util.MzSystemUtils;
import com.mob.MobApplication;
import com.umeng.commonsdk.UMConfigure;
import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.common.HyphenateHelper;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.hms.agent.HMSAgent;
import com.wiscomwis.facetoface.receiver.HomeWatcherReceiver;
import com.wiscomwis.facetoface.service.InitializeService;
import com.xiaomi.mipush.sdk.MiPushClient;

import java.util.List;

/**
 * Created by zhangdroid on 2017/5/11.
 */
public class BaseApplication extends MobApplication {
    public static final String TAG = "xiaomiPush";
    private static Context sApplicationContext;

    private HttpProxyCacheServer proxy;
    private int mFinalCount;

    public static HttpProxyCacheServer getProxy(Context context) {
        BaseApplication app = (BaseApplication) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer.Builder(this).maxCacheSize(1024 * 1024 * 1024)       // 1 Gb for cache
                .build();
    }

    public static Context getGlobalContext() {
        return sApplicationContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplicationContext = getApplicationContext();
        // 初始化环信
        HyphenateHelper.getInstance().init(this);
        InitializeService.start(this);
        DbModle.getInstance().init(this);
//        魅族推送只适用于Flyme系统,因此可以先行判断是否为魅族机型，再进行订阅，避免在其他机型上出现兼容性问题
        if(MzSystemUtils.isBrandMeizu(this)){
            PushManager.register(this, C.MEIZU_APPID, C.MEIZU_APPKEY);
            String pushId = PushManager.getPushId(this);
            Log.e("CCCCC",pushId);
        }

        String phonename = Build.MANUFACTURER;
        if (phonename != null && phonename != null && phonename != "") {
            if(phonename.equals("HUAWEI")){
                //        华为推送
                HMSAgent.init(this);
            }
            if(phonename.equals("Xiaomi")){
                //初始化小米push推送服务
                if(shouldInit()) {
                    MiPushClient.registerPush(this, C.XIAOMI_APPID, C.XIAOMI_APPKEY);
                    String regId = MiPushClient.getRegId(this);
                    Log.e("AAAAA","xiaomiAppPush="+regId);
                }
            }
        }
//        友盟
        UMConfigure.init(sApplicationContext,UMConfigure.DEVICE_TYPE_PHONE, null);

//        判断应用在前台还是后台的监听
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

            }

            @Override
            public void onActivityStarted(Activity activity) {
                mFinalCount++;
                //如果mFinalCount ==1，说明是从后台到前台
                Log.e("onActivityStarted", mFinalCount +"");
                if (mFinalCount == 1){
                    UserPreference.setReception(true);
                    //说明从后台回到了前台
                    Log.i("onActivityStopped","MiPushClient.resumePush");
//                    MiPushClient.resumePush(sApplicationContext,null);
                    MiPushClient.clearNotification(sApplicationContext);
                    unregisterHomeKeyReceiver(sApplicationContext);

                }
            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {
                mFinalCount--;
                //如果mFinalCount ==0，说明是前台到后台
                Log.i("onActivityStopped", mFinalCount +"");
                if (mFinalCount == 0){
                    //说明从前台回到了后台,暂停
                    UserPreference.setReception(false);
                    registerHomeKeyReceiver(sApplicationContext);
                    Log.i("onActivityStopped","MiPushClient.pausePush");
//                    MiPushClient.pausePush(sApplicationContext,null);
                }
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });

}

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }


    private boolean shouldInit() {
        ActivityManager am = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE));
        List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        String mainProcessName = getPackageName();
        int myPid = Process.myPid();
        for (ActivityManager.RunningAppProcessInfo info : processInfos) {
            if (info.pid == myPid && mainProcessName.equals(info.processName)) {
                return true;
            }
        }
        return false;
    }


    private static HomeWatcherReceiver mHomeKeyReceiver = null;

    private static void registerHomeKeyReceiver(Context context) {
//        Log.i(LOG_TAG, "registerHomeKeyReceiver");
        mHomeKeyReceiver = new HomeWatcherReceiver();
        final IntentFilter homeFilter = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);

        context.registerReceiver(mHomeKeyReceiver, homeFilter);
    }

    private static void unregisterHomeKeyReceiver(Context context) {
//        Log.i(LOG_TAG, "unregisterHomeKeyReceiver");
        if (null != mHomeKeyReceiver) {
            context.unregisterReceiver(mHomeKeyReceiver);
        }
    }



}
