package com.wiscomwis.facetoface.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.MiPushEvevnt;
import com.wiscomwis.facetoface.event.OpenVideoEvent;
import com.xiaomi.mipush.sdk.MiPushClient;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by tianzhentao on 2018/6/28.
 */

public class HomeWatcherReceiver extends BroadcastReceiver {
    private static final String LOG_TAG = "HomeReceiver";
    private static final String SYSTEM_DIALOG_REASON_KEY = "reason";
    private static final String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";
    private static final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";
    private static final String SYSTEM_DIALOG_REASON_LOCK = "lock";
    private static final String SYSTEM_DIALOG_REASON_ASSIST = "assist";

    @Override
    public void onReceive(final Context context, Intent intent) {
        String action = intent.getAction();

        Log.i(LOG_TAG, "onReceive: action: " + action);
        if (Intent.ACTION_SCREEN_ON.equals(action)) {
            // 开屏
            Log.i(LOG_TAG, "onReceive:开屏 ");

        } else if (Intent.ACTION_SCREEN_OFF.equals(action)) {
            // 锁屏
            Log.i(LOG_TAG, "onReceive:锁屏 ");
            UserPreference.setLockScreen(true);


        } else if (Intent.ACTION_USER_PRESENT.equals(action)) {
            // 解锁
            Log.i(LOG_TAG, "onReceive:解锁 ");
            UserPreference.setLockScreen(false);
            String phonename = Build.MANUFACTURER;
            if (Util.isOPPO()) {

            }else if(Util.isVivo()){
                EventBus.getDefault().post(new OpenVideoEvent());
            }
        }

        if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
            // android.intent.action.CLOSE_SYSTEM_DIALOGS
            String reason = intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY);
            Log.i(LOG_TAG, "reason: " + reason);

            if (SYSTEM_DIALOG_REASON_HOME_KEY.equals(reason)) {
                // 短按Home键
                Log.i(LOG_TAG, "homekey");
            }
            else if (SYSTEM_DIALOG_REASON_RECENT_APPS.equals(reason)) {
                // 长按Home键 或者 activity切换键
                Log.i(LOG_TAG, "long press home key or activity switch");
                MiPushClient.resumePush(context,null);
                Log.i("onActivityStopped","MiPushClient.resumePush");

                EventBus.getDefault().post(new MiPushEvevnt());
            }
            else if (SYSTEM_DIALOG_REASON_LOCK.equals(reason)) {
                // 锁屏
                Log.i(LOG_TAG, "lock");
            }
            else if (SYSTEM_DIALOG_REASON_ASSIST.equals(reason)) {
                // samsung 长按Home键
                Log.i(LOG_TAG, "assist");
            }

        }


    }

}
