package com.wiscomwis.facetoface.receiver;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.huawei.hms.support.api.push.PushReceiver;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.preference.UserPreference;

/**
 * Created by tianzhentao on 2018/8/7.
 */

public class HuaWeiPushReceive extends PushReceiver{
    @Override
    public void onToken(Context context, String token, Bundle extras) {
        super.onToken(context, token, extras);
//        接受token的方法
        Log.e("AAAAA", "收到华为token==" + token);
//        将token传给后台
        if (Util.isHuaWei()) {
            ApiManager.getMiPushToken(token,new IGetDataListener<BaseModel>() {
                        @Override
                        public void onResult(BaseModel baseModel, boolean isEmpty) {
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                        }
                    });
            UserPreference.setRegId(token);
        }

    }
    @Override
    public boolean onPushMsg(Context context, byte[] msg, Bundle bundle) {
        try {
            //CP可以自己解析消息内容，然后做相应的处理
            String content = new String(msg, "UTF-8");
            Log.e("AAAAA", "收到PUSH透传消息,消息内容为:" + content);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public void onEvent(Context context, Event event, Bundle extras) {

    }
    @Override
    public void onPushState(Context context, boolean pushState) {
    }

}
