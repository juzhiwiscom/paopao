package com.wiscomwis.facetoface.wxapi;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseTopBarActivity;
import com.wiscomwis.facetoface.common.CustomDialogAboutPay;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.WeChat;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.FinishWxActivity;
import com.wiscomwis.facetoface.event.PaySuccessEvent;
import com.wiscomwis.library.net.NetUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class WXPayEntryActivity extends BaseTopBarActivity implements IWXAPIEventHandler {
    // IWXAPI 是第三方app和微信通信的openapi接口
    private IWXAPI api;
    private String payInfo = "";
    private int type = 1;
    private String serviceName = "";
    private String price = "";
    private int paySource=6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_wxpay_entry;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return null;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        Intent intent = getIntent();
        if (intent != null) {
            payInfo = intent.getStringExtra("orderInf");
            serviceName = intent.getStringExtra("serviceName");
            price = intent.getStringExtra("price");
            type = intent.getIntExtra("type", 1);
            paySource = intent.getIntExtra("paySource",6);
        }

        if (!TextUtils.isEmpty(payInfo)) {
            WeChat weChat = new Gson().fromJson(payInfo, WeChat.class);
            if (weChat != null) {
                PayReq req = new PayReq();
                String appid = weChat.getAppid();
                Log.e("AAAAAA","微信APPID="+appid);
                // 通过WXAPIFactory工厂，获取IWXAPI的实例
//                api = WXAPIFactory.createWXAPI(this, "wx184b0ee493d39249", false);
                api = WXAPIFactory.createWXAPI(this, appid, false);
                // 将该app注册到微信
                api.registerApp(appid);
                api.handleIntent(getIntent(), this);
                req.appId = appid;
                req.partnerId = weChat.getPartnerid();
                req.prepayId = weChat.getPrepayid();
                req.nonceStr = weChat.getNoncestr();
                req.timeStamp = weChat.getTimestamp();
                req.packageValue = weChat.getPackageX();
                req.sign = weChat.getSign();
                req.extData = "app data"; // optional
                // 在支付之前，如果应用没有注册到微信，应该先调用IWXMsg.registerApp将应用注册到微信
                api.sendReq(req);
            }
        }
    }

    @Override
    protected void setListeners() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    // 微信发送请求到第三方应用时，会回调到该方法
    @Override
    public void onReq(BaseReq baseReq) {


    }


    // 第三方应用发送到微信的请求处理后的响应结果，会回调到该方法
    @Override
    public void onResp(BaseResp resp) {
        if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
            if (resp.errCode == 0) {
                setMsgFlag(String.valueOf(paySource));
                EventBus.getDefault().post(new PaySuccessEvent());
                CustomDialogAboutPay.paySucceedShow(WXPayEntryActivity.this, serviceName, price, type,paySource);
            } else {
//                if(paySource==11){
//                    CustomDialogAboutPay.payFaildShow(WXPayEntryActivity.this,2);
//                }else{
//                    CustomDialogAboutPay.payFaildShow(WXPayEntryActivity.this,1);
//                }
                CustomDialogAboutPay.payFaildShow(WXPayEntryActivity.this,1);
            }
        }
    }

    @Subscribe
    public void onEvent(FinishWxActivity finish) {
        finish();
    }
    //埋点
    private static void setMsgFlag(String extendTag){
        ApiManager.userActivityTag(UserPreference.getId(), "", "16", extendTag, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }
}