# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\Software\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

######################### sdk目录下的混淆配置（/Android/tools/proguard/proguard-android.txt） #########################

# 指定代码的压缩级别
-optimizationpasses 5
# 不使用大小写混合的类名
-dontusemixedcaseclassnames
# 不跳过library中非public的类
-dontskipnonpubliclibraryclasses
# 打印混淆的详细信息
-verbose
# 不进行优化（默认关闭） 建议使用此选项，因为根据proguard-android-optimize.txt中的描述，优化可能会造成一些潜在风险，不能保证在所有版本的Dalvik上都正常运行
-dontoptimize
# 不进行预校验  （这个预校验是作用在Java平台上的，Android平台上不需要这项功能，去掉之后还可以加快混淆速度）
-dontpreverify
# 保留注解中的参数
-keepattributes *Annotation*
# 不混淆下面两个类，接入Google Service使需要用到下面的类
-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService
# 不混淆任何包含native方法的类的类名和方法名
-keepclasseswithmembernames class * {
    native <methods>;
}
# 不混淆任何view中的setter和getter方法，因为属性动画中需要实现相应的setter和getter方法
-keepclassmembers public class * extends android.view.View {
   void set*(***);
   *** get*();
}
# 不混淆Activity中参数为View的方法，防止在XML中给View设置点击事件时无效（android:onClick=""）
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}
# 不混淆枚举中的values()和valueOf()方法
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
# 不混淆Paracelable和CREATOR字段，否则Paracelable机制将无法工作
-keepclassmembers class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator CREATOR;
}
# 不混淆Serializable
-keepnames class * implements java.io.Serializable{
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}
# 不混淆R文件的所有静态成员
-keepclassmembers class **.R$* {
    public static <fields>;
}
# 忽略support包下的警告，高版本代码兼容下的警告，可以直接忽略
-dontwarn android.support.**
# Understand the @Keep support annotation.（保持Keep注释）
-keep class android.support.annotation.Keep

-keep @android.support.annotation.Keep class * {*;}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <methods>;
}
-keep class com.example.android.InitUtils{ public *;}
-keepclasseswithmembers class * {
    @android.support.annotation.Keep <fields>;
}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <init>(...);
}
# 保护泛型
-keepattributes Signature, InnerClasses

######################### 通用的保护项目代码不被混淆的配置 #########################

# 保护指定的类不被混淆或移除（所有的Application,Activity,Fragment,Service,Receiver,provider）
-keep public class * extends android.app.Application
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Fragment
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Perference

# 保护support包下的所有类
-keep class android.support.** {*;}
# 保护所有继承support包下的类
-keep public class * extends android.support.v4.**
-keep public class * extends android.support.v7.**
-keep public class * extends android.support.annotation.**
-keep public class * extends android.support.design.**

# 保护自定义view不被混淆
-keepclasseswithmembers class * {
     public <init>(android.content.Context, android.util.AttributeSet);
}
-keepclasseswithmembers class * {
     public <init>(android.content.Context, android.util.AttributeSet, int);
}


######################### 指定保护项目中特定的代码不被混淆的配置 #########################


# 保护JSON解析需要用到的实体类
-keep class com.wiscomwis.facetoface.data.model**{
#     public void set*(***);
#     public *** get*();
 *;
}
# 保护一些sp数据保存的对象
-keep class com.wiscomwis.facetoface.data.preference**{*;
}
# 保护EventBus用到的实体类
-keep class com.wiscomwis.facetoface.event.**
-keep class myOkhttp.** { *; }
######################### 保护第三方库不被混淆的配置 #########################

# EventBus 3.x版本,保护onEvent()方法
-dontwarn org.greenrobot.eventbus.**
-keep class org.greenrobot.eventbus.** {*;}
-keepclassmembers class ** {
    public void onEvent(***);
}

# 环信
-dontwarn  com.hyphenate.**
-keep class com.hyphenate.** {*;}
-keep class com.superrtc.** {*;}

# okio
-dontwarn  org.codehaus.**
-keep public class org.codehaus.**{*;}
-dontwarn java.nio.**
-keep public class java.nio.**{*;}
#声网混淆
-keep class io.agora.**{*;}
#支付宝混淆
-keep class com.alipay.android.app.IAlixPay{*;}
-keep class com.alipay.android.app.IAlixPay$Stub{*;}
-keep class com.alipay.android.app.IRemoteServiceCallback{*;}
-keep class com.alipay.android.app.IRemoteServiceCallback$Stub{*;}
-keep class com.alipay.sdk.app.PayTask{ public *;}
-keep class com.alipay.sdk.app.AuthTask{ public *;}
-keep class com.alipay.sdk.app.H5PayCallback {
    <fields>;
    <methods>;
}
-keep class com.alipay.android.phone.mrpc.core.** { *; }
-keep class com.alipay.apmobilesecuritysdk.** { *; }
-keep class com.alipay.mobile.framework.service.annotation.** { *; }
-keep class com.alipay.mobilesecuritysdk.face.** { *; }
-keep class com.alipay.tscenter.biz.rpc.** { *; }
-keep class org.json.alipay.** { *; }
-keep class com.alipay.tscenter.** { *; }
-keep class com.ta.utdid2.** { *;}
-keep class com.ut.device.** { *;}
-dontwarn android.net.SSLCertificateSocketFactory
#微信支付
 -keep class com.tencent.mm.opensdk.** {*;}
 -keep class com.tencent.wxop.** {*;}
 -keep class com.tencent.mm.sdk.** {*;}



#GSYVideoPlayer混淆
-keep class com.popo.playerlibrary.gsyvideoplayer.video.** { *; }
-dontwarn com.popo.playerlibrary.gsyvideoplayer.video.**
-keep class com.popo.playerlibrary.gsyvideoplayer.video.base.** { *; }
-dontwarn com.popo.playerlibrary.gsyvideoplayer.video.base.**
-keep class com.popo.playerlibrary.gsyvideoplayer.utils.** { *; }
-dontwarn com.popo.playerlibrary.gsyvideoplayer.utils.**
-keep class tv.danmaku.ijk.** { *; }
-dontwarn tv.danmaku.ijk.**
#Glide混淆
-keep public class * implements  com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
    **[] $VALUES;
    public *;
}
-keep class com.bumptech.** {
    *;
}

-keepattributes EnclosingMethod

#sharesdk
-keep class cn.sharesdk.**{*;}
-keep class com.sina.**{*;}
-keep class **.R$* {*;}
-keep class **.R{*;}
-keep class com.mob.**{*;}
-keep class m.framework.**{*;}
-dontwarn cn.sharesdk.**
-dontwarn com.sina.**
-dontwarn com.mob.**
-dontwarn **.R$*
#友盟
-keep class com.umeng.** {*;}

-keepclassmembers class * {
   public <init> (org.json.JSONObject);
}
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
-keep public class com.wiscomwis.facetoface.R$*{
public static final int *;
}

#小米推送
 -keep class com.wiscomwis.facetoface.service.MiPushReceive {*;}
# 华为
-ignorewarning
-keepattributes *Annotation*
-keepattributes Exceptions
-keepattributes InnerClasses
-keepattributes Signature
-keepattributes SourceFile,LineNumberTable
-keep class com.hianalytics.android.**{*;}
-keep class com.huawei.updatesdk.**{*;}
-keep class com.huawei.hms.**{*;}
-keep class com.huawei.android.hms.agent.**{*;}
-keep class com.huawei.gamebox.plugin.gameservice.**{*;}